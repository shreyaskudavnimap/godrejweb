﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace GodrejCP
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface ICPService1
    {
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "GetContactUs/{projectid}")]
        ICPS_ContactUs[] GetContactUsjson(string projectid);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "GetContructionStatus/{projectid}/{year}/{month}")]
        ContructionStatus[] GetContructionStatusjson(string projectid, string year, string month);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "GetContructionYear")]
        ContructionStatus[] GetContructionYearjson();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "GetState")]
        StateInfo[] GetStatejson();

        [OperationContract]
        [WebInvoke(Method = "POST",
                    RequestFormat = WebMessageFormat.Json,
                    ResponseFormat = WebMessageFormat.Json,
                    UriTemplate = "InsertEmpl")]
        string InsertEmpl(Empanelment empl);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "ForgotPassword/{Email}")]
        Empanelment[] ForgotPassword(string Email);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "GetEmpl/{Email}")]
        Empanelment[] GetEmpl(string Email);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "GetCountry")]
        Country[] GetCountryjson();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "GetStateData/{Country}")]
        State[] GetStateDatajson(string Country);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "GetCity/{State}")]
        City[] GetCityjson(string State);

        //[OperationContract]
        //[WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
        //    UriTemplate = "GetCommunicationCountry")]
        //Country[] GetCommunicationCountryjson();

        //[OperationContract]
        //[WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
        //    UriTemplate = "GetRegisteredCountry")]
        //Country[] GetRegisteredCountryjson();

        //[OperationContract]
        //[WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
        //    UriTemplate = "GetCommunicationState/{Country}")]
        //State[] GetCommunicationStatejson(string Country);

        //[OperationContract]
        //[WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
        //    UriTemplate = "GetRegisteredState/{Country}")]
        //State[] GetRegisteredStatejson(string Country);

        //[OperationContract]
        //[WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
        //    UriTemplate = "GetCommunicationCity/{State}")]
        //City[] GetCommunicationCityjson(string State);

        //[OperationContract]
        //[WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
        //    UriTemplate = "GetRegisteredCity/{State}")]
        //City[] GetRegisteredCityjson(string State);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "GetPassword/{userid}")]
        Empanelment[] GetPassword(string userid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "ChangePassword/{userid}/{newpwd}")]
        Empanelment[] ChangePassword(string userid, string newpwd);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "DisplayAllCPKitProj/{cityid}")]
        Projects[] DisplayAllCPKitProj(string cityid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "DisplayAllKitCity")]
        City[] DisplayAllKitCity();        

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "GetCPKit/{projectid}")]
        ICPS_CPKit[] GetCPKit(string projectid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "DisplayTextDoc/{projectid}")]
        InboxDoc[] DisplayTextDoc(string projectid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "DisplayAllProjects/{cityid}")]
        Projects[] DisplayAllProjects(string cityid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "DisplayAllDocCity")]
        City[] DisplayAllDocCity();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "DisplayAllCampaign")]
        ICPS_Campaign[] DisplayAllCampaign();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "DisplayAllCampaignDocs/{campaignid}")]
        ICPS_Campaign[] DisplayAllCampaignDocs(string campaignid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "DisplayAllPolicy")]
        ICPS_GPLPolicy[] DisplayAllPolicy();
    }

    [DataContract]
    public class ICPS_ContactUs
    {
        [DataMember]
        public string projectid
        { get; set; }
        [DataMember]
        public string title
        { get; set; }
        [DataMember]
        public string description
        { get; set; }
    }


    [DataContract]
    public class ContructionStatus
    {
        [DataMember]
        public string projectid
        { get; set; }
        [DataMember]
        public string title
        { get; set; }
        [DataMember]
        public string description
        { get; set; }
        [DataMember]
        public string image
        { get; set; }
        [DataMember]

        public string month
        { get; set; }
        [DataMember]
        public string year
        { get; set; }
    }



    [DataContract]
    public class Empanelment
    {
        [DataMember]
        public string Password
        { get; set; }

        [DataMember]
        public string Company_Name
        { get; set; }

        [DataMember]
        public string entity_type
        { get; set; }

        [DataMember]
        public string Name
        { get; set; }

        [DataMember]
        public string Mobile
        { get; set; }

        [DataMember]
        public string Email
        { get; set; }

        [DataMember]
        public string communication_address
        { get; set; }

        [DataMember]
        public string registered_address
        { get; set; }

        [DataMember]
        public string Pan_No
        { get; set; }

        [DataMember]
        public string pancerti
        { get; set; }

        [DataMember]
        public string BrokerID
        { get; set; }

        [DataMember]
        public string BrokerContactID
        { get; set; }

        [DataMember]
        public List<ReraDocs> ReraDoc
        { get; set; }

        [DataMember]
        public List<GstDocs> GstDoc
        { get; set; }

        [DataMember]
        public string status
        { get; set; }

        [DataMember]
        public string Billing_City
        { get; set; }

        [DataMember]
        public string Billing_Zip
        { get; set; }

        [DataMember]
        public string Billing_Street
        { get; set; }

        [DataMember]
        public string Billing_State
        { get; set; }

        [DataMember]
        public string Billing_Country
        { get; set; }

        [DataMember]
        public string Registered_City
        { get; set; }

        [DataMember]
        public string Registered_Zip
        { get; set; }

        [DataMember]
        public string Registered_Street
        { get; set; }

        [DataMember]
        public string Registered_State
        { get; set; }

        [DataMember]
        public string Registered_Country
        { get; set; }

        [DataMember]
        public string Communication_City
        { get; set; }

        [DataMember]
        public string Communication_Zip
        { get; set; }

        [DataMember]
        public string Communication_Street
        { get; set; }

        [DataMember]
        public string Communication_State
        { get; set; }

        [DataMember]
        public string Communication_Country
        { get; set; }
    }



    [DataContract]
    public class ReraDocs
    {
        [DataMember]
        public string state
        { get; set; }

        [DataMember]
        public string rerano
        { get; set; }

        [DataMember]
        public string reracerti
        { get; set; }
    }


    [DataContract]
    public class GstDocs
    {
        [DataMember]
        public string state
        { get; set; }

        [DataMember]
        public string gstno
        { get; set; }

        [DataMember]
        public string gstcerti
        { get; set; }
    }

    [DataContract]
    public class StateInfo
    {
        [DataMember]
        public string StateId
        { get; set; }

        [DataMember]
        public string State
        { get; set; }

        [DataMember]
        public string StateCode
        { get; set; }

        [DataMember]
        public string Status
        { get; set; }
    }

    [DataContract]
    public class Country
    {
        [DataMember]
        public string CountryId
        { get; set; }

        [DataMember]
        public string CountryName
        { get; set; }

        [DataMember]
        public string Status
        { get; set; }
    }

    [DataContract]
    public class State
    {
        [DataMember]
        public string StateId
        { get; set; }

        [DataMember]
        public string StateName
        { get; set; }

        [DataMember]
        public string Status
        { get; set; }
    }

    [DataContract]
    public class City
    {
        [DataMember]
        public string CityId
        { get; set; }

        [DataMember]
        public string CityName
        { get; set; }

        [DataMember]
        public string Status
        { get; set; }
    }

    [DataContract]
    public class ICPS_RegionInfo
    {
        [DataMember]
        public string ID
        { get; set; }

        [DataMember]
        public string Region
        { get; set; }

        [DataMember]
        public string Status
        { get; set; }
    }

    [DataContract]
    public class ICPS_CPKit
    {
        [DataMember]
        public string ID
        { get; set; }

        [DataMember]
        public string RegionId
        { get; set; }

        [DataMember]
        public string Title
        { get; set; }

        [DataMember]
        public string Image
        { get; set; }

        [DataMember]
        public string Date
        { get; set; }

        [DataMember]
        public string Status
        { get; set; }
    }

    [DataContract]
    public class InboxDoc
    {
        [DataMember]
        public string id
        { get; set; }

        [DataMember]
        public string title
        { get; set; }

        [DataMember]
        public string image
        { get; set; }

        [DataMember]
        public string type
        { get; set; }

        [DataMember]
        public string date
        { get; set; }

        [DataMember]
        public string text
        { get; set; }

        [DataMember]
        public string status
        { get; set; }
    }


    [DataContract]
    public class Projects
    {
        [DataMember]
        public string projectid
        { get; set; }

        [DataMember]
        public string title
        { get; set; }        

        [DataMember]
        public string cityname
        { get; set; }        

        [DataMember]
        public string Status
        { get; set; }
    }


    [DataContract]
    public class ICPS_Campaign
    {
        [DataMember]
        public string CampaignName
        { get; set; }

        [DataMember]
        public string CampaignId
        { get; set; }

        [DataMember]
        public string Image
        { get; set; }

        [DataMember]
        public string Status
        { get; set; }

        [DataMember]
        public string title
        { get; set; }

        [DataMember]
        public string date
        { get; set; }
    }


    [DataContract]
    public class ICPS_GPLPolicy
    {

        [DataMember]
        public string title
        { get; set; }

        [DataMember]
        public string image
        { get; set; }

        [DataMember]
        public string Status
        { get; set; }
    }
}