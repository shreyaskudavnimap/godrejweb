﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Net.Mail;

namespace GodrejCP
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class CPService : ICPService1
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["godrejCPConnectionString"].ToString());
        string constring = ConfigurationManager.ConnectionStrings["godrejCPConnectionString"].ConnectionString;

        public ICPS_ContactUs[] GetContactUsjson(string projectid)
        {
            List<ICPS_ContactUs> ContactUsDetails = new List<ICPS_ContactUs>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetContactUs", conn);
                comm.Parameters.AddWithValue("@projectid", projectid);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ICPS_ContactUs b = new ICPS_ContactUs();


                        b.projectid = Convert.ToString(ds.Tables[0].Rows[i]["id"]);
                        b.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        b.description = Convert.ToString(ds.Tables[0].Rows[i]["description"]);

                        ContactUsDetails.Add(b);
                    }
                    return ContactUsDetails.ToArray();
                }
                else
                {
                    ICPS_ContactUs b = new ICPS_ContactUs();

                    b.projectid = "null";
                    b.title = "null";
                    b.description = "null";


                    ContactUsDetails.Add(b);
                    return ContactUsDetails.ToArray();
                }
            }
            catch (Exception ex)
            {
                ICPS_ContactUs b = new ICPS_ContactUs();
                b.projectid = "null";
                b.title = "null";
                b.description = "null";

                ContactUsDetails.Add(b);
                return ContactUsDetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }


        public ContructionStatus[] GetContructionStatusjson(string projectid, string year, string month)
        {
            List<ContructionStatus> ContructionStatusDetails = new List<ContructionStatus>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetConstructionStatus", conn);
                comm.Parameters.AddWithValue("@projectid", projectid);
                comm.Parameters.AddWithValue("@year", year);
                comm.Parameters.AddWithValue("@month", month);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ContructionStatus b = new ContructionStatus();


                        b.projectid = Convert.ToString(ds.Tables[0].Rows[i]["projectid"]);
                        b.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        b.description = Convert.ToString(ds.Tables[0].Rows[i]["description"]);
                        b.image = Convert.ToString(ds.Tables[0].Rows[i]["image"]);
                        b.month = Convert.ToString(ds.Tables[0].Rows[i]["month"]);
                        b.year = Convert.ToString(ds.Tables[0].Rows[i]["year"]);

                        ContructionStatusDetails.Add(b);
                    }
                    return ContructionStatusDetails.ToArray();
                }
                else
                {
                    ContructionStatus b = new ContructionStatus();

                    b.projectid = "null";
                    b.title = "null";
                    b.description = "null";
                    b.image = "null";
                    b.month = "null";
                    b.year = "null";


                    ContructionStatusDetails.Add(b);
                    return ContructionStatusDetails.ToArray();
                }
            }
            catch (Exception ex)
            {
                ContructionStatus b = new ContructionStatus();
                b.projectid = "null";
                b.title = "null";
                b.description = "null";
                b.image = "null";
                b.month = "null";
                b.year = "null";

                ContructionStatusDetails.Add(b);
                return ContructionStatusDetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }


        public ContructionStatus[] GetContructionYearjson()
        {
            List<ContructionStatus> ContructionStatusYearDetails = new List<ContructionStatus>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetConstructionYear", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ContructionStatus b = new ContructionStatus();

                        b.year = Convert.ToString(ds.Tables[0].Rows[i]["year"]);

                        ContructionStatusYearDetails.Add(b);
                    }
                    return ContructionStatusYearDetails.ToArray();
                }
                else
                {
                    ContructionStatus b = new ContructionStatus();
                    b.year = "null";


                    ContructionStatusYearDetails.Add(b);
                    return ContructionStatusYearDetails.ToArray();
                }
            }
            catch (Exception ex)
            {
                ContructionStatus b = new ContructionStatus();
                b.year = "null";

                ContructionStatusYearDetails.Add(b);
                return ContructionStatusYearDetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        public string InsertEmpl(Empanelment empl)
        {
            //Convert Datatable of rera
            DataTable dtrera = new DataTable();
            if (empl.ReraDoc.Count > 0)
                dtrera = ToDataTable(empl.ReraDoc);

            //Convert Datatable of gst
            DataTable dtgst = new DataTable();
            if (empl.GstDoc.Count > 0)
                dtgst = ToDataTable(empl.GstDoc);

            string status = "";
            string EmplID = "";
            SqlCommand comm;
            SqlDataAdapter da;
            //if (conn.State != ConnectionState.Open)
            //    conn.Open();

            SqlCommand cmd = new SqlCommand("SP_InsertEmplDetails", conn);

            cmd.Parameters.AddWithValue("@company", empl.Company_Name);
            cmd.Parameters.AddWithValue("@entitytype", empl.entity_type);
            cmd.Parameters.AddWithValue("@name", empl.Name);
            cmd.Parameters.AddWithValue("@mobile", empl.Mobile);
            cmd.Parameters.AddWithValue("@email", empl.Email);
            cmd.Parameters.AddWithValue("@commaddress", empl.communication_address);
            cmd.Parameters.AddWithValue("@regaddress", empl.registered_address);
            cmd.Parameters.AddWithValue("@panno", empl.Pan_No);
            cmd.Parameters.AddWithValue("@pancerti", empl.pancerti);
            cmd.Parameters.AddWithValue("@Billing_City", empl.Billing_City);
            cmd.Parameters.AddWithValue("@Billing_Zip", empl.Billing_Zip);
            cmd.Parameters.AddWithValue("@Billing_Street", empl.Billing_Street);
            cmd.Parameters.AddWithValue("@Billing_State", empl.Billing_State);
            cmd.Parameters.AddWithValue("@Billing_Country", empl.Billing_Country);
            cmd.Parameters.AddWithValue("@Registered_City", empl.Registered_City);
            cmd.Parameters.AddWithValue("@Registered_Zip", empl.Registered_Zip);
            cmd.Parameters.AddWithValue("@Registered_Street", empl.Registered_Street);
            cmd.Parameters.AddWithValue("@Registered_State", empl.Registered_State);
            cmd.Parameters.AddWithValue("@Registered_Country", empl.Registered_Country);
            cmd.Parameters.AddWithValue("@Communication_City", empl.Communication_City);
            cmd.Parameters.AddWithValue("@Communication_Zip", empl.Communication_Zip);
            cmd.Parameters.AddWithValue("@Communication_Street", empl.Communication_Street);
            cmd.Parameters.AddWithValue("@Communication_State", empl.Communication_State);
            cmd.Parameters.AddWithValue("@Communication_Country", empl.Communication_Country);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@ReraDocsList", dtrera);
            //cmd.Parameters.AddWithValue("@GstDocsList", dtgst);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    // message = Common_Function.ConvertResultToAlert(dr, queryString, model.User);
                    EmplID = dr["EmplID"].ToString();
                    dr.Close();
                    if (dtrera.Rows.Count > 0)
                    {
                        foreach (DataRow row in dtrera.Rows)
                        {
                            InsertRera(row, EmplID);
                        }
                    }


                    if (dtgst.Rows.Count > 0)
                    {
                        foreach (DataRow row in dtgst.Rows)
                        {
                            InsertGst(row, EmplID);
                        }
                    }

                    MailMessage mail1 = new MailMessage();
                    SmtpClient SmtpServer1 = new SmtpClient("smtp.gmail.com");

                    mail1.From = new MailAddress("sujatamirge1994@gmail.com");
                    mail1.To.Add(empl.Email);
                    mail1.Subject = "Godrej Partner Connect APP | Empanelment request";
                    mail1.IsBodyHtml = true;
                    string path1 = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                    StreamReader sr1 = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Mailer/empl_user.html"));
                    mail1.Body = sr1.ReadToEnd().Replace("user_name", empl.Name);
                    SmtpServer1.Port = Convert.ToInt32(587);
                    SmtpServer1.Credentials = new System.Net.NetworkCredential("sujatamirge1994@gmail.com", "change@123");
                    SmtpServer1.EnableSsl = true;
                    SmtpServer1.Send(mail1);
                    sr1.Close();

                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                    mail.From = new MailAddress("sujatamirge1994@gmail.com");
                    mail.To.Add("vikaspawar2112@igmail.com");

                    //mail.To.Add("sufyan@netbiz.in");
                    //mail.To.Add("pranav@netbiz.in");
                    //mail.To.Add("pranav.kans@gmail.com");
                    //mail.To.Add("netbiz.pranav@gmail.com");
                    mail.Subject = "CP Documents";
                    mail.IsBodyHtml = true;
                    string Uplodefile = ConfigurationManager.AppSettings["path"];
                    mail.Attachments.Add(new Attachment(Uplodefile + empl.pancerti));
                    string rera_state = "";
                    string rera_no = "";
                    string gst_state = "";
                    string gst_no = "";
                    if (dtrera.Rows.Count > 0)
                    {
                        foreach (DataRow row in dtrera.Rows)
                        {
                            rera_state += row["state"].ToString();
                            rera_no += row["rerano"].ToString();
                            mail.Attachments.Add(new Attachment(Uplodefile + row["reracerti"].ToString()));
                        }
                    }

                    if (dtgst.Rows.Count > 0)
                    {
                        foreach (DataRow row in dtgst.Rows)
                        {
                            gst_state += row["state"].ToString();
                            gst_no += row["gstno"].ToString();
                            mail.Attachments.Add(new Attachment(Uplodefile + row["gstcerti"].ToString()));
                        }
                    }
                    string commaddress = empl.Communication_Street + ' ' + empl.Communication_City + ' ' + empl.Communication_Zip + ' ' + empl.Communication_State + ' ' + empl.Communication_Country;
                    string regaddress = empl.Registered_Street + ' ' + empl.Registered_City + ' ' + empl.Registered_Zip + ' ' + empl.Registered_State + ' ' + empl.Registered_Country;

                    string path = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                    StreamReader sr = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Mailer/admin_mail.html"));
                    mail.Body = sr.ReadToEnd().Replace("user_name", empl.Name).Replace("user_mobile", empl.Mobile).Replace("user_email", empl.Email).Replace("user_company", empl.Company_Name).Replace("user_entitytype", empl.entity_type).Replace("user_commaddress", commaddress).Replace("user_regaddress", regaddress).Replace("user_pan", empl.Pan_No)
                    .Replace("user_rera_state", rera_state).Replace("user_rera_no", rera_no).Replace("user_gst_state", gst_state).Replace("user_gst_no", gst_no);
                    SmtpServer.Port = Convert.ToInt32(587);
                    // SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                    // SmtpServer.UseDefaultCredentials = false;
                    SmtpServer.Credentials = new System.Net.NetworkCredential("sujatamirge1994@gmail.com", "change@123");
                    SmtpServer.EnableSsl = true;
                    SmtpServer.Send(mail);
                    sr.Close();


                    return "success";
                }
            }

            //cmd.Parameters.AddWithValue("@brokerid", empl.brokerid);
            //cmd.Parameters.Add("@outID", SqlDbType.Char, 500);
            //cmd.Parameters["@outID"].Direction = ParameterDirection.Output;
            //cmd.Connection = conn;
            // cmd.CommandType = System.Data.CommandType.StoredProcedure;

            //int result = cmd.ExecuteNonQuery();

            //EmplID = cmd.Parameters["@outID"].Value.ToString().Trim();
            //if (result == 1)
            //{
            //    status = "success";
            //}
            //else
            //{
            //    status = "fail";
            //}

            conn.Close();
            //return status;
            return EmplID;
        }

        public bool InsertRera(DataRow row, string EmplID)
        {
            SqlCommand cmd = new SqlCommand("SP_InsertEmplReraDocs", conn);

            cmd.Parameters.AddWithValue("@EmplID", EmplID);
            cmd.Parameters.AddWithValue("@state", row["state"]);
            cmd.Parameters.AddWithValue("@rerano", row["rerano"]);
            cmd.Parameters.AddWithValue("@reracerti", row["reracerti"]);
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            int result = cmd.ExecuteNonQuery();

            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool InsertGst(DataRow row, string EmplID)
        {
            SqlCommand cmd = new SqlCommand("SP_InsertEmplGstDocs", conn);

            cmd.Parameters.AddWithValue("@EmplID", EmplID);
            cmd.Parameters.AddWithValue("@state", row["state"]);
            cmd.Parameters.AddWithValue("@gstno", row["gstno"]);
            cmd.Parameters.AddWithValue("@gstcerti", row["gstcerti"]);
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            int result = cmd.ExecuteNonQuery();

            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        //private DataTable objToDataTable(Docs obj)
        //{
        //    DataTable dt = new DataTable();
        //    Docs objmkt = new Docs();
        //    dt.Columns.Add("Column_Name");
        //    foreach (PropertyInfo info in typeof(Docs).GetProperties())
        //    {
        //        dt.Rows.Add(info.Name);
        //    }
        //    dt.AcceptChanges();
        //    return dt;
        //}



        //public string InsertReraDocs(string EmplID, string rera)
        //{
        //    string [] data;
        //    data = rera.Split('-');
        //    //string data[] = s.Split('-');

        //    List<ReraDocs> rdocs = new List<ReraDocs>();
        //    for (int i = 0; i < data.Length; i++)
        //    {
        //        string[] data1=data[i].Split(',');
        //        ReraDocs b = new ReraDocs();
        //        b.state = Convert.ToString(data1[0]);
        //        b.rerano = Convert.ToString(data1[1]);
        //        b.reracerti = Convert.ToString(data1[2]);

        //        rdocs.Add(b);
        //    }
        //    //SqlCommand comm;
        //    //DataTable dt = new DataTable();
        //    //dt = ToDataTable(s);
        //    //string status = "";
        //    //if (conn.State != ConnectionState.Open)
        //    //    conn.Open();

        //    //comm = new SqlCommand("SP_InsertEmplReraDocs", conn);
        //    //comm.Parameters.AddWithValue("@DocsList", dt);
        //    //comm.Connection = conn;
        //    //comm.CommandType = System.Data.CommandType.StoredProcedure;

        //    //int result = comm.ExecuteNonQuery();
        //    //if (result == 1)
        //    //{
        //    //    status = "success";
        //    //}
        //    //else
        //    //{
        //    //    status = "fail";
        //    //}

        //    //conn.Close();
        //    //return true;
        //    return "";
        //}

        //public string InsertGSTDocs(List<GstDocs> s)
        //{
        //    SqlCommand comm;
        //    DataTable dt = new DataTable();
        //    dt = ToDataTable(s);
        //    string status = "";
        //    if (conn.State != ConnectionState.Open)
        //        conn.Open();

        //    comm = new SqlCommand("SP_InsertEmplGstDocs", conn);
        //    comm.Parameters.AddWithValue("@DocsList", dt);
        //    comm.Connection = conn;
        //    comm.CommandType = System.Data.CommandType.StoredProcedure;

        //    int result = comm.ExecuteNonQuery();
        //    if (result == 1)
        //    {
        //        status = "success";
        //    }
        //    else
        //    {
        //        status = "fail";
        //    }

        //    conn.Close();
        //    return status;
        //}

        public StateInfo[] GetStatejson()
        {
            List<StateInfo> StateDetails = new List<StateInfo>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_StateList", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        StateInfo b = new StateInfo();
                        b.StateId = Convert.ToString(ds.Tables[0].Rows[i]["StateId"]);
                        b.State = Convert.ToString(ds.Tables[0].Rows[i]["Statename"]);
                        b.StateCode = Convert.ToString(ds.Tables[0].Rows[i]["StateCode"]);

                        StateDetails.Add(b);
                    }
                    return StateDetails.ToArray();
                }
                else
                {
                    StateInfo b = new StateInfo();
                    b.StateId = "null";
                    b.State = "null";

                    StateDetails.Add(b);
                    return StateDetails.ToArray();
                }
            }
            catch (Exception ex)
            {
                StateInfo b = new StateInfo();
                b.StateId = "null";
                b.State = "null";

                StateDetails.Add(b);
                return StateDetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        public Empanelment[] ForgotPassword(string Email)
        {
            List<Empanelment> EmpanelmentDetails = new List<Empanelment>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetEmplDetails", conn);
                comm.Parameters.AddWithValue("@Email", Email);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Empanelment b = new Empanelment();
                        b.Password = Convert.ToString(ds.Tables[0].Rows[i]["password"]);
                        b.Name = Convert.ToString(ds.Tables[0].Rows[i]["Name"]);
                        b.status = "success";
                        EmpanelmentDetails.Add(b);

                        SendMail(Convert.ToString(ds.Tables[0].Rows[i]["Name"]), Email, Convert.ToString(ds.Tables[0].Rows[i]["Password"]));
                    }
                    return EmpanelmentDetails.ToArray();
                }
                else
                {
                    Empanelment b = new Empanelment();
                    b.Password = "null";
                    b.status = "fail";

                    EmpanelmentDetails.Add(b);
                    return EmpanelmentDetails.ToArray();
                }
            }
            catch (Exception ex)
            {
                Empanelment b = new Empanelment();
                b.Password = ex.ToString();
                b.status = ex.ToString();

                EmpanelmentDetails.Add(b);
                return EmpanelmentDetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        private void SendMail(string name, string email, string pwd)
        {
            try
            {

                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.office365.com");
                mail.From = new MailAddress("partnerconnect@godrejproperties.com");
                mail.To.Add(email);
                mail.Subject = "Password Mailer";
                mail.IsBodyHtml = true;
                string path = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                StreamReader sr = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Mailer/forget_mail.html"));
                mail.Body = sr.ReadToEnd().Replace("user_name", name).Replace("user_pwd", pwd);
                SmtpServer.Port = Convert.ToInt32(25);
                // SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                // SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new System.Net.NetworkCredential("partnerconnect@godrejproperties.com", "Qwerty@321");
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
                sr.Close();

                //MailMessage mail = new MailMessage();
                //mail.From = "partnerconnect@godrejproperties.com";
                //mail.To = email;
                //mail.BodyFormat = MailFormat.Html;
                //mail.Subject = "Password Mailer";
                //StreamReader sr = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Mailer/forget_mail.html"));
                //mail.Body = sr.ReadToEnd().Replace("user_name", name).Replace("user_pwd", pwd);
                ////mail.Subject = "Password Request for Partner Connect App";
                ////string BodyMessage = "<html><body><table><tr><td>Dear "+ name +", </td></tr><tr><td></td></tr><tr><td></td></tr><tr><td></td></tr><tr><td>" +
                ////    "This is with regards to your request for Password</td></tr><tr><td></td></tr><tr><td>" +
                ////    "Plase find below details for Login" +
                ////    "<tr><td>Username : "+ email +"</td></tr>" +
                ////    "<tr><td>Password : " + pwd + "</td></tr></table></body></html><html><body></body></html>";
                ////mail.Body = BodyMessage;
                //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
                //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "info@netbizlabs.com"); //set your username here
                //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "w*F%t?JRuEO0");	//set your password here
                ////StreamReader sr = new StreamReader(Server.MapPath(""));
                ////mail.Body = sr.ReadToEnd().Replace("Email", email);
                //SmtpMail.SmtpServer = "mail.netbizlabs.com";
                //SmtpMail.Send(mail);
            }
            catch (Exception ex)
            {
                // Response.Write(ex.Message);
            }
        }

        public Empanelment[] GetEmpl(string Email)
        {
            List<Empanelment> EmpanelmentDetails = new List<Empanelment>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetEmplDetails", conn);
                comm.Parameters.AddWithValue("@Email", Email);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Empanelment b = new Empanelment();
                        b.Email = Convert.ToString(ds.Tables[0].Rows[i]["Email"]);
                        b.status = "Exist";

                        EmpanelmentDetails.Add(b);
                    }
                    return EmpanelmentDetails.ToArray();
                }
                else
                {
                    Empanelment b = new Empanelment();
                    b.Email = "";
                    b.status = "Fail";

                    EmpanelmentDetails.Add(b);
                    return EmpanelmentDetails.ToArray();
                }
            }
            catch (Exception ex)
            {
                Empanelment b = new Empanelment();
                b.Email = "";
                b.status = ex.ToString();

                EmpanelmentDetails.Add(b);
                return EmpanelmentDetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        public Country[] GetCountryjson()
        {
            List<Country> CountryDetails = 
                
                new List<Country>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_CountryList", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Country b = new Country();
                        b.CountryId = Convert.ToString(ds.Tables[0].Rows[i]["CountryId"]);
                        b.CountryName = Convert.ToString(ds.Tables[0].Rows[i]["Country"]);

                        CountryDetails.Add(b);
                    }
                    return CountryDetails.ToArray();
                }
                else
                {
                    Country b = new Country();
                    b.CountryId = "null";
                    b.CountryName = "null";

                    CountryDetails.Add(b);
                    return CountryDetails.ToArray();
                }
            }
            catch (Exception ex)
            {
                Country b = new Country();
                b.CountryId = "null";
                b.CountryName = "null";

                CountryDetails.Add(b);
                return CountryDetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        public State[] GetStateDatajson(string Country)
        {
            List<State> StateDetails = new List<State>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_StateInfoList", conn); ;
                comm.Parameters.AddWithValue("@Country", Country);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        State b = new State();
                        b.StateId = Convert.ToString(ds.Tables[0].Rows[i]["StateId"]);
                        b.StateName = Convert.ToString(ds.Tables[0].Rows[i]["State"]);

                        StateDetails.Add(b);
                    }
                    return StateDetails.ToArray();
                }
                else
                {
                    State b = new State();
                    b.StateId = "null";
                    b.StateName = "null";

                    StateDetails.Add(b);
                    return StateDetails.ToArray();
                }
            }
            catch (Exception ex)
            {
                State b = new State();
                b.StateId = "null";
                b.StateName = "null";

                StateDetails.Add(b);
                return StateDetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        public City[] GetCityjson(string State)
        {
            List<City> CityDetails = new List<City>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_CityList", conn);
                comm.Parameters.AddWithValue("@State", State);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        City b = new City();
                        b.CityId = Convert.ToString(ds.Tables[0].Rows[i]["CityId"]);
                        b.CityName = Convert.ToString(ds.Tables[0].Rows[i]["City"]);

                        CityDetails.Add(b);
                    }
                    return CityDetails.ToArray();
                }
                else
                {
                    City b = new City();
                    b.CityId = "null";
                    b.CityName = "null";

                    CityDetails.Add(b);
                    return CityDetails.ToArray();
                }
            }
            catch (Exception ex)
            {
                City b = new City();
                b.CityId = "null";
                b.CityName = "null";

                CityDetails.Add(b);
                return CityDetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        //public Country[] GetCommunicationCountryjson()
        //{
        //    List<Country> CountryDetails = new List<Country>();
        //    try
        //    {
        //        SqlCommand comm;
        //        SqlDataAdapter da;
        //        if (conn.State != ConnectionState.Open)
        //            conn.Open();
        //        comm = new SqlCommand("SP_CountryList", conn);
        //        comm.Connection = conn;
        //        comm.CommandType = System.Data.CommandType.StoredProcedure;

        //        DataSet ds = new DataSet();
        //        da = new SqlDataAdapter(comm);
        //        da.Fill(ds);
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //            {
        //                Country b = new Country();
        //                b.CountryId = Convert.ToString(ds.Tables[0].Rows[i]["CountryId"]);
        //                b.CountryName = Convert.ToString(ds.Tables[0].Rows[i]["Country"]);

        //                CountryDetails.Add(b);
        //            }
        //            return CountryDetails.ToArray();
        //        }
        //        else
        //        {
        //            Country b = new Country();
        //            b.CountryId = "null";
        //            b.CountryName = "null";

        //            CountryDetails.Add(b);
        //            return CountryDetails.ToArray();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Country b = new Country();
        //        b.CountryId = "null";
        //        b.CountryName = "null";

        //        CountryDetails.Add(b);
        //        return CountryDetails.ToArray();
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }
        //}

        //public Country[] GetRegisteredCountryjson()
        //{
        //    List<Country> CountryDetails = new List<Country>();
        //    try
        //    {
        //        SqlCommand comm;
        //        SqlDataAdapter da;
        //        if (conn.State != ConnectionState.Open)
        //            conn.Open();
        //        comm = new SqlCommand("SP_CountryList", conn);
        //        comm.Connection = conn;
        //        comm.CommandType = System.Data.CommandType.StoredProcedure;

        //        DataSet ds = new DataSet();
        //        da = new SqlDataAdapter(comm);
        //        da.Fill(ds);
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //            {
        //                Country b = new Country();
        //                b.CountryId = Convert.ToString(ds.Tables[0].Rows[i]["CountryId"]);
        //                b.CountryName = Convert.ToString(ds.Tables[0].Rows[i]["Country"]);

        //                CountryDetails.Add(b);
        //            }
        //            return CountryDetails.ToArray();
        //        }
        //        else
        //        {
        //            Country b = new Country();
        //            b.CountryId = "null";
        //            b.CountryName = "null";

        //            CountryDetails.Add(b);
        //            return CountryDetails.ToArray();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Country b = new Country();
        //        b.CountryId = "null";
        //        b.CountryName = "null";

        //        CountryDetails.Add(b);
        //        return CountryDetails.ToArray();
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }
        //}

        //public State[] GetCommunicationStatejson(string Country)
        //{
        //    List<State> StateDetails = new List<State>();
        //    try
        //    {
        //        SqlCommand comm;
        //        SqlDataAdapter da;
        //        if (conn.State != ConnectionState.Open)
        //            conn.Open();
        //        comm = new SqlCommand("SP_StateInfoList", conn);
        //        comm.Parameters.AddWithValue("@Country", Country);
        //        comm.Connection = conn;
        //        comm.CommandType = System.Data.CommandType.StoredProcedure;

        //        DataSet ds = new DataSet();
        //        da = new SqlDataAdapter(comm);
        //        da.Fill(ds);
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //            {
        //                State b = new State();
        //                b.StateId = Convert.ToString(ds.Tables[0].Rows[i]["StateId"]);
        //                b.StateName = Convert.ToString(ds.Tables[0].Rows[i]["State"]);

        //                StateDetails.Add(b);
        //            }
        //            return StateDetails.ToArray();
        //        }
        //        else
        //        {
        //            State b = new State();
        //            b.StateId = "null";
        //            b.StateName = "null";

        //            StateDetails.Add(b);
        //            return StateDetails.ToArray();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        State b = new State();
        //        b.StateId = "null";
        //        b.StateName = "null";

        //        StateDetails.Add(b);
        //        return StateDetails.ToArray();
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }
        //}

        //public State[] GetRegisteredStatejson(string Country)
        //{
        //    List<State> StateDetails = new List<State>();
        //    try
        //    {
        //        SqlCommand comm;
        //        SqlDataAdapter da;
        //        if (conn.State != ConnectionState.Open)
        //            conn.Open();
        //        comm = new SqlCommand("SP_StateInfoList", conn);
        //        comm.Parameters.AddWithValue("@Country", Country);
        //        comm.Connection = conn;
        //        comm.CommandType = System.Data.CommandType.StoredProcedure;

        //        DataSet ds = new DataSet();
        //        da = new SqlDataAdapter(comm);
        //        da.Fill(ds);
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //            {
        //                State b = new State();
        //                b.StateId = Convert.ToString(ds.Tables[0].Rows[i]["StateId"]);
        //                b.StateName = Convert.ToString(ds.Tables[0].Rows[i]["State"]);

        //                StateDetails.Add(b);
        //            }
        //            return StateDetails.ToArray();
        //        }
        //        else
        //        {
        //            State b = new State();
        //            b.StateId = "null";
        //            b.StateName = "null";

        //            StateDetails.Add(b);
        //            return StateDetails.ToArray();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        State b = new State();
        //        b.StateId = "null";
        //        b.StateName = "null";

        //        StateDetails.Add(b);
        //        return StateDetails.ToArray();
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }
        //}

        //public City[] GetCommunicationCityjson(string State)
        //{
        //    List<City> CityDetails = new List<City>();
        //    try
        //    {
        //        SqlCommand comm;
        //        SqlDataAdapter da;
        //        if (conn.State != ConnectionState.Open)
        //            conn.Open();
        //        comm = new SqlCommand("SP_CityList", conn);
        //        comm.Parameters.AddWithValue("@State", State);
        //        comm.Connection = conn;
        //        comm.CommandType = System.Data.CommandType.StoredProcedure;

        //        DataSet ds = new DataSet();
        //        da = new SqlDataAdapter(comm);
        //        da.Fill(ds);
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //            {
        //                City b = new City();
        //                b.CityId = Convert.ToString(ds.Tables[0].Rows[i]["CityId"]);
        //                b.CityName = Convert.ToString(ds.Tables[0].Rows[i]["City"]);

        //                CityDetails.Add(b);
        //            }
        //            return CityDetails.ToArray();
        //        }
        //        else
        //        {
        //            City b = new City();
        //            b.CityId = "null";
        //            b.CityName = "null";

        //            CityDetails.Add(b);
        //            return CityDetails.ToArray();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        City b = new City();
        //        b.CityId = "null";
        //        b.CityName = "null";

        //        CityDetails.Add(b);
        //        return CityDetails.ToArray();
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }
        //}

        //public City[] GetRegisteredCityjson(string State)
        //{
        //    List<City> CityDetails = new List<City>();
        //    try
        //    {
        //        SqlCommand comm;
        //        SqlDataAdapter da;
        //        if (conn.State != ConnectionState.Open)
        //            conn.Open();
        //        comm = new SqlCommand("SP_CityList", conn);
        //        comm.Parameters.AddWithValue("@State", State);
        //        comm.Connection = conn;
        //        comm.CommandType = System.Data.CommandType.StoredProcedure;

        //        DataSet ds = new DataSet();
        //        da = new SqlDataAdapter(comm);
        //        da.Fill(ds);
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //            {
        //                City b = new City();
        //                b.CityId = Convert.ToString(ds.Tables[0].Rows[i]["CityId"]);
        //                b.CityName = Convert.ToString(ds.Tables[0].Rows[i]["City"]);

        //                CityDetails.Add(b);
        //            }
        //            return CityDetails.ToArray();
        //        }
        //        else
        //        {
        //            City b = new City();
        //            b.CityId = "null";
        //            b.CityName = "null";

        //            CityDetails.Add(b);
        //            return CityDetails.ToArray();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        City b = new City();
        //        b.CityId = "null";
        //        b.CityName = "null";

        //        CityDetails.Add(b);
        //        return CityDetails.ToArray();
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }
        //}

        public Empanelment[] GetPassword(string userid)
        {
            List<Empanelment> EmpanelmentDetails = new List<Empanelment>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_getpassword", conn);
                comm.Parameters.AddWithValue("@userid", userid);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Empanelment b = new Empanelment();
                        b.Password = Convert.ToString(ds.Tables[0].Rows[i]["password"]);
                        b.status = "success";
                        EmpanelmentDetails.Add(b);
                    }
                    return EmpanelmentDetails.ToArray();
                }
                else
                {
                    Empanelment b = new Empanelment();
                    b.Password = "null";
                    b.status = "fail";

                    EmpanelmentDetails.Add(b);
                    return EmpanelmentDetails.ToArray();
                }
            }
            catch (Exception ex)
            {
                Empanelment b = new Empanelment();
                b.Password = ex.ToString();
                b.status = ex.ToString();

                EmpanelmentDetails.Add(b);
                return EmpanelmentDetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        public Empanelment[] ChangePassword(string userid, string newpwd)
        {
            List<Empanelment> EmpanelmentDetails = new List<Empanelment>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;

                SqlCommand cmd = new SqlCommand("SP_UpdatePassword", conn);

                cmd.Parameters.AddWithValue("@userid", userid);
                cmd.Parameters.AddWithValue("@password", newpwd);
                cmd.Parameters.Add("@outStatus", SqlDbType.VarChar, 30);
                cmd.Parameters["@outStatus"].Direction = ParameterDirection.Output;
                cmd.Connection = conn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                cmd.ExecuteNonQuery(); // MISSING
                string retunvalue = (string)cmd.Parameters["@outStatus"].Value;

                //if (conn.State == ConnectionState.Closed)
                //{
                //    conn.Open();
                //    SqlDataReader dr = cmd.ExecuteReader();
                //    while (dr.Read())
                //    {
                Empanelment b = new Empanelment();
                b.status = retunvalue;
                EmpanelmentDetails.Add(b);
                //    }
                //}

                return EmpanelmentDetails.ToArray();
            }
            catch (Exception ex)
            {
                Empanelment b = new Empanelment();
                b.Password = ex.ToString();
                b.status = ex.ToString();

                EmpanelmentDetails.Add(b);
                return EmpanelmentDetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        public Projects[] DisplayAllCPKitProj(string cityid)
        {
            List<Projects> RegionDetails = new List<Projects>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetKitProjectInfo", conn);
                comm.Parameters.AddWithValue("@cityid", cityid);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Projects b = new Projects();
                        b.projectid = Convert.ToString(ds.Tables[0].Rows[i]["projectid"]);
                        b.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        b.cityname = Convert.ToString(ds.Tables[0].Rows[i]["cityname"]);
                        b.Status = "Success";
                        RegionDetails.Add(b);
                    }
                    return RegionDetails.ToArray();
                }
                else
                {
                    Projects b = new Projects();
                    b.projectid = "null";
                    b.title = "null";
                    b.Status = "Fail";

                    RegionDetails.Add(b);
                    return RegionDetails.ToArray();
                }
            }
            catch (Exception ex)
            {
                Projects b = new Projects();
                b.projectid = "null";
                b.title = "null";
                b.Status = ex.ToString();

                RegionDetails.Add(b);
                return RegionDetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        public City[] DisplayAllKitCity()
        {
            List<City> RegionDetails = new List<City>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetKitCityInfo", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        City b = new City();
                        b.CityId = Convert.ToString(ds.Tables[0].Rows[i]["cityid"]);
                        b.CityName = Convert.ToString(ds.Tables[0].Rows[i]["cityname"]);
                        b.Status = "Success";
                        RegionDetails.Add(b);
                    }
                    return RegionDetails.ToArray();
                }
                else
                {
                    City b = new City();
                    b.CityId = "null";
                    b.CityName = "null";
                    b.Status = "Fail";

                    RegionDetails.Add(b);
                    return RegionDetails.ToArray();
                }
            }
            catch (Exception ex)
            {
                City b = new City();
                b.CityId = "null";
                b.CityName = "null";

                RegionDetails.Add(b);
                return RegionDetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        public ICPS_CPKit[] GetCPKit(string projectid)
        {
            List<ICPS_CPKit> CPKitDetails = new List<ICPS_CPKit>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_getCPKitInfo", conn);
                comm.Parameters.AddWithValue("@projectid", projectid);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ICPS_CPKit b = new ICPS_CPKit();
                        b.Title = Convert.ToString(ds.Tables[0].Rows[i]["Title"]);
                        b.Image = Convert.ToString(ds.Tables[0].Rows[i]["data"]);
                        b.Date = Convert.ToString(ds.Tables[0].Rows[i]["date"]);
                        b.Status = "success";
                        CPKitDetails.Add(b);
                    }
                    return CPKitDetails.ToArray();
                }
                else
                {
                    ICPS_CPKit b = new ICPS_CPKit();
                    b.Title = "null";
                    b.Image = "null";
                    b.Date = "null"; 
                    b.Status = "fail";

                    CPKitDetails.Add(b);
                    return CPKitDetails.ToArray();
                }
            }
            catch (Exception ex)
            {
                ICPS_CPKit b = new ICPS_CPKit();
                b.Title = ex.ToString();
                b.Image = ex.ToString();
                b.Date = ex.ToString(); 
                b.Status = ex.ToString();

                CPKitDetails.Add(b);
                return CPKitDetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        public InboxDoc[] DisplayTextDoc(string projectid)
        {
            List<InboxDoc> DocDetails = new List<InboxDoc>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_Get_textDoc", conn);
                comm.Parameters.AddWithValue("@projectid", projectid);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        InboxDoc b = new InboxDoc();
                        b.id = Convert.ToString(ds.Tables[0].Rows[i]["id"]);
                        b.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        b.type = Convert.ToString(ds.Tables[0].Rows[i]["type"]);
                        b.image = Convert.ToString(ds.Tables[0].Rows[i]["data"]);
                        b.date = Convert.ToString(ds.Tables[0].Rows[i]["date"]);
                        b.text = Convert.ToString(ds.Tables[0].Rows[i]["text"]);
                        b.status = "success";
                        DocDetails.Add(b);
                    }
                    return DocDetails.ToArray();
                }
                else
                {
                    InboxDoc b = new InboxDoc();
                    b.title = "null";
                    b.image = "null";
                    b.date = "null";
                    b.status = "fail";

                    DocDetails.Add(b);
                    return DocDetails.ToArray();
                }
            }
            catch (Exception ex)
            {
                InboxDoc b = new InboxDoc();
                b.title = ex.ToString();
                b.image = ex.ToString();
                b.date = ex.ToString();
                b.status = ex.ToString();

                DocDetails.Add(b);
                return DocDetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        public Projects[] DisplayAllProjects(string cityid)
        {
            List<Projects> RegionDetails = new List<Projects>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetProjectInfo", conn);
                comm.Parameters.AddWithValue("@cityid", cityid);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Projects b = new Projects();
                        b.projectid = Convert.ToString(ds.Tables[0].Rows[i]["projectid"]);
                        b.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        b.cityname = Convert.ToString(ds.Tables[0].Rows[i]["cityname"]);
                        b.Status = "Success";
                        RegionDetails.Add(b);
                    }
                    return RegionDetails.ToArray();
                }
                else
                {
                    Projects b = new Projects();
                    b.projectid = "null";
                    b.title = "null";
                    b.Status = "Fail";

                    RegionDetails.Add(b);
                    return RegionDetails.ToArray();
                }
            }
            catch (Exception ex)
            {
                Projects b = new Projects();
                b.projectid = "null";
                b.title = "null";

                RegionDetails.Add(b);
                return RegionDetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        public City[] DisplayAllDocCity()
        {
            List<City> RegionDetails = new List<City>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetDocCityInfo", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        City b = new City();
                        b.CityId = Convert.ToString(ds.Tables[0].Rows[i]["cityid"]);
                        b.CityName = Convert.ToString(ds.Tables[0].Rows[i]["cityname"]);
                        b.Status = "Success";
                        RegionDetails.Add(b);
                    }
                    return RegionDetails.ToArray();
                }
                else
                {
                    City b = new City();
                    b.CityId = "null";
                    b.CityName = "null";
                    b.Status = "Fail";

                    RegionDetails.Add(b);
                    return RegionDetails.ToArray();
                }
            }
            catch (Exception ex)
            {
                City b = new City();
                b.CityId = "null";
                b.CityName = "null";

                RegionDetails.Add(b);
                return RegionDetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        public ICPS_Campaign[] DisplayAllCampaign()
        {
            List<ICPS_Campaign> RegionDetails = new List<ICPS_Campaign>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetCampaignInfo", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ICPS_Campaign b = new ICPS_Campaign();
                        b.CampaignId = Convert.ToString(ds.Tables[0].Rows[i]["campaignid"]);
                        b.CampaignName = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        b.Status = "Success";
                        RegionDetails.Add(b);
                    }
                    return RegionDetails.ToArray();
                }
                else
                {
                    ICPS_Campaign b = new ICPS_Campaign();
                    b.CampaignId = "null";
                    b.CampaignName = "null";
                    b.Status = "Fail";

                    RegionDetails.Add(b);
                    return RegionDetails.ToArray();
                }
            }
            catch (Exception ex)
            {
                ICPS_Campaign b = new ICPS_Campaign();
                b.CampaignId = "null";
                b.CampaignName = "null";

                RegionDetails.Add(b);
                return RegionDetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        public ICPS_Campaign[] DisplayAllCampaignDocs(string campaignid)
        {
            List<ICPS_Campaign> RegionDetails = new List<ICPS_Campaign>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetCampaignDocInfo", conn);
                comm.Parameters.AddWithValue("@campaignid", campaignid);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ICPS_Campaign b = new ICPS_Campaign();
                        b.Image = Convert.ToString(ds.Tables[0].Rows[i]["Image"]);
                        b.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        b.date = Convert.ToString(ds.Tables[0].Rows[i]["date"]);
                        b.Status = "Success";
                        RegionDetails.Add(b);
                    }
                    return RegionDetails.ToArray();
                }
                else
                {
                    ICPS_Campaign b = new ICPS_Campaign();
                    b.Image = "null";
                    b.title = "null";
                    b.Status = "Fail";

                    RegionDetails.Add(b);
                    return RegionDetails.ToArray();
                }
            }
            catch (Exception ex)
            {
                ICPS_Campaign b = new ICPS_Campaign();
                b.Image = "null";
                b.title = "null";

                RegionDetails.Add(b);
                return RegionDetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }


        public ICPS_GPLPolicy[] DisplayAllPolicy()
        {
            List<ICPS_GPLPolicy> RegionDetails = new List<ICPS_GPLPolicy>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetGPLPolicy", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ICPS_GPLPolicy b = new ICPS_GPLPolicy();
                        b.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        b.image = Convert.ToString(ds.Tables[0].Rows[i]["image"]);
                        b.Status = "Success";
                        RegionDetails.Add(b);
                    }
                    return RegionDetails.ToArray();
                }
                else
                {
                    ICPS_GPLPolicy b = new ICPS_GPLPolicy();
                    b.title = "null";
                    b.image = "null";
                    b.Status = "Fail";

                    RegionDetails.Add(b);
                    return RegionDetails.ToArray();
                }
            }
            catch (Exception ex)
            {
                ICPS_GPLPolicy b = new ICPS_GPLPolicy();
                b.title = "null";
                b.image = "null";
                b.Status = ex.InnerException.ToString();

                RegionDetails.Add(b);
                return RegionDetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }
    }
}

