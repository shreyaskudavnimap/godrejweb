<?php
	//error_reporting(1);
	/*header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: PUT, GET, POST");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");*/

	//ini_set("soap.wsdl_cache_enabled", "0");
	//$pan_id = "AAZFS8386E";
	require_once ($_SERVER['DOCUMENT_ROOT'].'/GodrejPartnerConnect/org_config.php'); //configuration file containg login credentials
	require_once ($_SERVER['DOCUMENT_ROOT'].'/GodrejPartnerConnect/soapclient/SforcePartnerClient.php');
	$mySforceConnection = new SforcePartnerClient();
	$mySforceConnection->createConnection($_SERVER['DOCUMENT_ROOT']."/GodrejPartnerConnect/soapclient/Partnerwsdl_Production-14-3-17.jsp.xml");
	$mySforceConnection->login(USERNAME, PASSWORD.SECURITY_TOKEN);	

	$title = '';
	$firstName = 'umesh';
	$lastname = 'testlead';
	$city = 'Mumbai';
	$state = 'Maharashtra';
	$Country = 'India';
	$phoneNo = '7845784757';
	$email = 'umeshtest@lead.com';
	$prjcode = 'a2X6F000000JEuIUAW';
	$comment = 'test lead';
	//$visitDate=date('Y-m-d H:i:s');
	//$visitDate=str_replace(' ','T',$visitDate);
	$visitDate='2015-07-07T11:11:11';
	$websiteName = '';
	$addCode = '48103';
	$type = '2 BHK';
	$Src = 'Broker';
	$CountryCode = '';
	$rating = 'Hot';
	$trnsId = '';
	$trnsStatus = '';
	$BrokerAccID = '0016F00001zYm4x';
	//$BrokerConID = 'a066F00000wSUGWQA4';
	$BrokerConID = '0036F000026tTOs';
	
	/*$result = file_get_contents("http://gplpartnerconnect.com/GodrejPartnerConnect/RestServiceImpl.svc/GetleadDetails/$Projectid");
	$result = json_decode($result);
	foreach($result->GetleadCodesResult as $data) {
		if($data->status == 'success'){
			$prjcode = $data->Projectcode;
			$addCode = $data->addcode;
		}
	}*/
	$responseLeadData = array(
		"title" => $title,
		"firstName" => $firstName,
		"lastname" => $lastname,
		"city" => $city,
		"state" => $state,
		"Country" => $Country,
		"phoneNo" => $phoneNo,
		"email" => $email,
		"prjcode" => $prjcode,
		"comment" => $comment,
		"visitDate" => $visitDate,
		"websiteName" => $websiteName,
		"addCode" => $addCode,
		"type" => $type,
		"Src" => $Src,
		"CountryCode" => $CountryCode,
		"rating" => $rating,
		"trnsId" => $trnsId,
		"trnsStatus" => $trnsStatus,
		"BrokerAccID" => $BrokerAccID,
		"BrokerConID" => $BrokerConID		
	);
	echo '<pre>';
	print_r($responseLeadData);
	//echo "</br>".'--';
	try {
		$parsedURL = parse_url($mySforceConnection->getLocation());
		define ("_SFDC_SERVER_", substr($parsedURL['host'],0,strpos($parsedURL['host'], '.')));
		define ("_WS_NAME_", 'WebToBrokerEnquiryCreation');
		define ("_WS_WSDL_", $_SERVER['DOCUMENT_ROOT'].'/GodrejPartnerConnect/soapclient/WebToBrokerEnquiryCreation.xml');
		define ("_WS_ENDPOINT_", 'https://' . _SFDC_SERVER_ . '.salesforce.com/services/wsdl/class/' . _WS_NAME_);
		define ("_WS_NAMESPACE_", 'http://soap.sforce.com/schemas/class/' . _WS_NAME_);
		$client = new SoapClient(_WS_WSDL_);
		$sforce_header = new SoapHeader(_WS_NAMESPACE_, "SessionHeader", array("sessionId" => $mySforceConnection->getSessionId()));
		$client->__setSoapHeaders(array($sforce_header));

		$upsertResponse = $client->EnquiryContact($responseLeadData);
		echo 'response-->';	
		print_r($upsertResponse);
		//exit;
		$response = array();
		
		foreach ($upsertResponse as $created){
			//echo $created->id;

			if($created->Id != ''){
				$response['status'] = 'success';
			}else{
				$response['status'] = 'error';
			}
		}

		echo json_encode($response);
	} catch (Exception $e) {
		global $errors;
		$errors = $e->faultstring;
		echo "Ooop! Error: <b>" . $errors . "</b>";
		die;
	}
?>