﻿
// To Get Cities

function GetCity() {

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: 'RestServiceImpl.svc/GetCities',
        processData: false,
        success: function (response) {

            var optionsValues = '<select>';
            $.each(response.FetchCitiesResult, function (index, value) {
                optionsValues += '<option value="' + value.cityid + '">' + value.cityname + '</option>';
            });
            optionsValues += '</select>';
            var options = $('#Select1');
            options.replaceWith(optionsValues);

        },
        failure: function (response) {
            alert(response.d);
        }
    });

};


// To add User information
function Adduser() {
    var username = document.getElementById('txtUsername').value;
    var useremail = document.getElementById('txtUseremail').value;
    var usermobile = document.getElementById('txtUsermobile').value;
    var usercity = document.getElementById('txtUsercity').value;
    var loginname = document.getElementById('txtLoginname').value;
    var password = document.getElementById('txtPassword').value;
    var name = document.getElementById('txtName').value;
    if (name == null || name == "") {
        alert("Name must be filled out");
        return false;
    }

    if (username == null || username == "") {
        alert("username must be filled out");
        return false;
    }

    if (useremail == null || useremail == "") {
        alert("useremail must be filled out");
        return false;
    }

    if (usermobile == null || usermobile == "") {
        alert("usermobile Number");
        return false;
    }

    if (usercity == null || usercity == "") {
        alert("usercity must be filled out");
        return false;
    }

    if (loginname == null || loginname == "") {
        alert("loginname must be filled out");
        return false;
    }
    if (password == null || password == "") {
        alert("password must be filled out");
        return false;
    }

    var email = useremail;
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(useremail)) {
        alert('Please provide a valid email address');
        return false;
    }

    var numPattern = /\d+(,\d{1,3})?/;

    if (!numPattern.test(usermobile)) {
        alert('Please Enter Numeric Value');
        return false;
    }
    var addstring = username + "/" + useremail + "/" + usermobile + "/" + usercity + "/" + loginname + "/" + password + "/" + name;


    $.ajax({

        ServiceCallID: 1,
        type: "POST",
        url: "RestServiceImpl.svc/AddUser/" + addstring,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            $.each(response.InsertUserInfoResult, function (index, value) {
                alert(value.status);
            });


        },

        failure: function (response) {
            alert('ffff');
        }
    });
};


// To Get User Information
function GetUserInfo() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: 'RestServiceImpl.svc/FetchUsers',
        processData: false,
        success: function (response) {
            debugger;
            var table = $("#table tbody");
            $.each(response.GetUserInfoResult, function (index, value) {

                table.append("<tr><td>" + value.username + "</td><td>" + value.useremail + "</td>   <td>" + value.usermobile + "</td></tr>" + "</td>   <td>" + value.createdon + "</td></tr>" + "</td>   <td>" + value.userpic + "</td></tr>");
            });

        },
        failure: function (response) {
            alert('Fail');
        }
    });
};


// To Get ProjectsInnerDetails accoding to menuid and projectid
function GetInnerDetail(txtmenuid, txtprojectid) {
    //var txtmenuid = document.getElementById('txtmenuid').value;
    //var txtprojectid = document.getElementById('txtprojectid').value;

    //if (txtmenuid == null || txtmenuid == "") {
    //    alert("txtmenuid must be filled out");
    //    return false;
    //}

    //if (txtprojectid == null || txtprojectid == "") {
    //    alert("txtprojectid must be filled out");
    //    return false;
    //}

    //var numPattern = /\d+(,\d{1,3})?/;

    //if (!numPattern.test(txtmenuid)) {
    //    alert('Please Enter Numeric Value');
    //    return false;
    //}
    //if (!numPattern.test(txtprojectid)) {
    //    alert('Please Enter Numeric Value');
    //    return false;
    //}
    var addstring = txtmenuid + "/" + txtprojectid;


    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "RestServiceImpl.svc/FetchInnerDetails/" + addstring,
        processData: false,
        success: function (response) {
            debugger;
            $.each(response.GetProjectsInnerDetailsResult, function (index, value) {
                alert(value.menutitle);
            });

        },
        failure: function (response) {
            alert('Fail');
        }
    });
};

// To Get property details accoding to Title 
function GetPropertyTitle() {
    if (txtprojectid == null || txtprojectid == "") {
        alert("Project title must be filled out");
        return false;
    }

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "RestServiceImpl.svc/FetchSearchPropertytitle/" + txtprojectid,
        processData: false,
        success: function (response) {
            debugger;
            $.each(response.GetSearchProperty_titleResult, function (index, value) {
                alert(value.title);
            });

        },
        failure: function (response) {
            alert('Fail');
        }
    });
};

// To Get property FOR City  
function GetPropertyCity() {
    var txtprojectid = document.getElementById('txtprojectid').value;
    alert(txtprojectid);
    if (txtprojectid == null || txtprojectid == "") {
        alert("Project title must be filled out");
        return false;
    }

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "RestServiceImpl.svc/FetchSearchPropertyCity/" + txtprojectid,
        processData: false,
        success: function (response) {

            $.each(response.GetSearchProperty_CityResult, function (index, value) {
                alert(value.title);
            });

        },
        failure: function (response) {
            alert('Fail');
        }
    });
};



// To Get property FOR DisplayGetProjects 
function DisplayGetProjects(string, string) {
    var txtprojectid = document.getElementById('txtprojectid').value;
    var txtmenuid = document.getElementById('txtmenuid').value;
    alert(txtprojectid);
    if (txtprojectid == null || txtprojectid == "") {
        alert("Project title must be filled out");
        return false;
    }
    var addstring = txtprojectid + "/" + txtmenuid;
    $.ajax({

        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "RestServiceImpl.svc/DisplayGetProjects/" + addstring,
        processData: false,
        success: function (response) {

            $.each(response.GetProjectsResult, function (index, value) {
                alert(value.title);
            });

        },
        failure: function (response) {
            alert('Fail');
        }
    });
};

// To Get Response Message 
function Message() {
    var userid = document.getElementById('txtuserid').value;
    var projectid = document.getElementById('txtprojectid').value;
    var msgtype = document.getElementById('txtmsgtype').value;
    var message = document.getElementById('txtmessage').value;

    var addstring = userid + "/" + projectid + msgtype + "/" + message;
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "RestServiceImpl.svc/AddMessages/" + addstring,
        processData: false,
        success: function (response) {

            $.each(response.InsertMessagesResult, function (index, value) {
                alert(value.status);
            });

        },
        failure: function (response) {
            alert('Fail');
        }
    });
};


// To  get All Rooms information
function GetAllRoomsInfo() {

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "RestServiceImpl.svc/DisplayGetRooms",
        processData: false,
        success: function (response) {

            $.each(response.GetRoomsResult, function (index, value) {
                alert(value.title);
            });

        },
        failure: function (response) {
            alert('Fail');
        }
    });
};



// To  get All ProjectVSRoom Information
function GetAllProjectVSRoomInfo() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "RestServiceImpl.svc/DisplayGetProjectVSRoom",
        processData: false,
        success: function (response) {

            $.each(response.GetProjectVSRoomResult, function (index, value) {
                alert(value.title);
            });

        },
        failure: function (response) {
            alert('Fail');
        }
    });
};

function Mobilelenght(obj, length) {
    var numPattern = /\d+(,\d{1,3})?/;
    if (obj.value != null || obj.value != "") {
        if (!numPattern.test(obj.value)) {
            alert('Please Enter Numeric Value');
            return false;
        }
    }
    var maxlength = length
    if (obj.value.length > maxlength)
        obj.value = obj.value.substring(0, maxlength)


}


function Emaillenght(obj, length) {

    var maxlength = length
    if (obj.value.length > maxlength)
        obj.value = obj.value.substring(0, maxlength)
}

function Email(obj) {

    var email = obj.value;
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(email)) {
        document.getElementById('txtemail').focus();
        alert('Please provide a valid email address');
        document.getElementById('txtemail').focus();
        return false;
    }


    function limitlength(obj, length) {

        var maxlength = length
        if (obj.value.length > maxlength)
            obj.value = obj.value.substring(0, maxlength)
    }


    // To add User Enquiry
    function AddEnquiry() {

        var salutation = document.getElementById('txtsalutation').value;
        var firstname = document.getElementById('txtfirstname').value;
        var lastname = document.getElementById('txtlastname').value;
        var email = document.getElementById('txtemail').value;
        var country = document.getElementById('txtcountry').value;
        var mobile = document.getElementById('txtmobile').value;
        var projectid = document.getElementById('txtprojectid').value;
        var description = document.getElementById('txtdescription').value;
        //var enquirydate = document.getElementById('txtenquirydate').value;
        var bookvisit = document.getElementById('txtbookvisit').value;
        var dateOfvisit = document.getElementById('txtdateOfvisit').value;


        if (salutation == null || salutation == "" || salutation.length > 6) {
            alert("username must be filled out.");
            return false;
        }

        if (firstname == null || firstname == "" || firstname.lenght > 80) {
            alert("useremail must be filled out.");
            return false;
        }

        if (lastname == null || lastname == "" || lastname.lenght > 80) {
            alert("usermobile must be filled out.");
            return false;
        }

        if (email == null || email == "" || email.lenght > 40) {
            alert("usercity must be filled out.");
            return false;
        }

        if (country == null || country == "" || country.lenght > 40) {
            alert("loginname must be filled out.");
            return false;
        }
        if (projectid == null || projectid == "" || projectid.lenght > 12) {
            alert("password must be filled out.");
            return false;
        }

        if (description == null || description == "" || description.lenght > 110) {
            alert("username must be filled out.");
            return false;
        }

        //if (enquirydate == null || enquirydate == "") {
        //    alert("useremail must be filled out.");
        //    return false;
        //}

        if (bookvisit == null || bookvisit == "" || bookvisit.lenght > 40) {
            alert("Bookvisit must be filled out.");
            return false;
        }

        if (dateOfvisit == null || dateOfvisit == "") {
            alert("dateOfvisit must be filled out");
            return false;
        }

        var numPattern = /\d+(,\d{1,3})?/;

        if (!numPattern.test(mobile)) {
            alert('Please Enter Numeric Value');
            return false;
        }
        if (mobile.lenght != 10) {
            alert('Please Enter Proper Mobile Number');
            return false;
        }

        var email = email;
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (!filter.test(email)) {
            alert('Please provide a valid email address');
            return false;
        }

        var addstring = salutation + "/" + firstname + "/" + lastname + "/" + email + "/" + country + "/" + mobile + "/";
        addstring += projectid + "/" + description + "/" + bookvisit + "/" + dateOfvisit.replaceWith("/", "-");
              
        $.ajax({
            ServiceCallID: 1,
            type: "POST",
            url: "RestServiceImpl.svc/AddEnquiry/" + addstring,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                $.each(response.InsertEnquiryResult, function (index, value) {
                    alert(value.status);
                });
            },

            failure: function (response) {
                alert('Fail');
            }
        });
    };
}
 
function UserCredentials()
{
    var Username = document.getElementById('txtUsername').value;
    var Password = document.getElementById('txtPassword').value;

    if (Username == null || Username == "") {
        alert("Username must be filled out.");
        return false;
    }

    if (Password == null || Password == "") {
        alert("Password must be filled out");
        return false;
    }

    var addstring = Username + "/" + Password;

    $.ajax({
        ServiceCallID: 1,
        type: "POST",
        url: "RestServiceImpl.svc/DisplayGetCredentials/" + addstring,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            $.each(response.GetCredentialsResult, function (index, value) {
                alert(value.name);
            });
        },

        failure: function (response) {
            alert('Fail');
        }
    });
}