﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net.Mail;

public partial class Register : System.Web.UI.Page
{
    private Random random = new Random();
    GodejCPDataContext dataContext = new GodejCPDataContext();
    string Nik = "";
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        #region add new record
        #region to check if same entry exists

        var objChk = (from obj in dataContext.tbldrawRegistrations
                      where obj.emailid == txtEmail.Text
                      select obj).FirstOrDefault();

        #endregion
        if (objChk != null)
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>exist();</script>");
            return;
        }
        else
        {
            tbldrawRegistration objsubadmin = new tbldrawRegistration();

            objsubadmin.name = txtName.Text;
            objsubadmin.mobile = txtmobile.Text;
            objsubadmin.emailid = txtEmail.Text;
            objsubadmin.companyname = txtcompanyname.Text;
            objsubadmin.createdate = DateTime.Now;
            dataContext.tbldrawRegistrations.InsertOnSubmit(objsubadmin);
            dataContext.SubmitChanges();

            //div_thank.Style.Add("display", "block");
            //dvwrapper.Style.Add("display", "block");

            //sendEmail(txtEmail.Text);

            txtName.Text = "";
            txtmobile.Text = "";
            txtEmail.Text = "";
            txtcompanyname.Text = "";
        }

        #endregion
    }

    public void sendEmail(string id)
    {
        try
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("mail.netbizlabs.com");
            mail.From = new MailAddress("info@netbizlabs.com");
            mail.To.Add(txtEmail.Text);
            mail.Subject = "Acknowledgement Email";
            mail.Body = "Thank You";
            SmtpServer.Port = 25;

            SmtpServer.Credentials = new System.Net.NetworkCredential("info@netbizlabs.com", "info321");
            SmtpServer.EnableSsl = true;
            SmtpServer.Send(mail);

        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
    }
}