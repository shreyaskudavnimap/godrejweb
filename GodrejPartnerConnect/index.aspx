﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8">
    <title>Channel Partner</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
    <script src="js/jquery.1.11.2.min.js"></script>
    <link rel="stylesheet" href="css/jquery.bxslider.css">
    <!-- Owl Carousel Assets -->
    <link href="owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="owl-carousel/owl.theme.css" rel="stylesheet">
    <link type="text/css" href="css/reset.css" rel="stylesheet" />
    <link type="text/css" href="css/style.css" rel="stylesheet" />
    <link type="text/css" href="css/responsive.css" rel="stylesheet" />
    <link type="text/css" href="css/fonts.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {

            //if (/android/i.test(navigator.userAgent.toLowerCase()) == true) {
            //    $("#a_ios").hide();
            //}
            //if (/ipad|iphone|ipod/i.test(navigator.userAgent.toLowerCase()) == true) {
            //    $("#a_android").hide();
            //}

            $.ajax({
                type: "GET",
                url: "http://gplpartnerconnect.com/GodrejPartnerConnect/RestServiceImpl.svc/GetPerformer",
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    var ul = $("#owl-demo1");
                    $.each(response.GetPerformerDetailsResult, function (index, value) {
                        ul.append('<div class="item"><img src="images/dummy_img_top.jpg" alt="image"><p>' + value.name + '</p><span>' + value.org_Name + '</span></div>');
                        //<li><img src="images/dummy_img_top.jpg" alt="image"><p>' + value.name + '</p><span>' + value.org_Name + '</span><div class="clear"></div></li>');


                    });

                    $("#owl-demo1").owlCarousel({
                        items: 3,
                        lazyLoad: true,
                        navigation: true,
                        auto: true
                    });

                },
                error: function (xhr) {
                    alert(xhr.responseText);
                }
            });

        });

        function checkdetails() {
            var panno = $('#panno').val();

            if (panno == null || panno == "" || panno.lenght > 50) {
                alert('Please enter PAN No.');
                return false;
            }
            if (!panno.match(/[A-Z]{5}[0-9]{4}[A-Z]{1}/)) {
                alert('Please enter valid PAN No.');
                return false;
            }
            $('#preloader').show();
            $.ajax({
                ServiceCallID: 1,
                type: "GET",
                url: 'http://gplpartnerconnect.com/GodrejPartnerConnect/RestServiceImpl.svc/GetEmplDetails/' + panno,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $.each(response.GetEmplInfoResult, function (index, value) {
                        //alert(value.status);
                        if (value.status == "success") {

                            if (value.brokerid == "" || value.brokerid == null || value.brokerid == "null") {
                                alert('BrokerId not created.Please try after some time!!');
                                return false;
                            }
                            else if (value.brokerid != "") {
                                alert('Broker already exist.Please Login!!');
                                return false;
                            }

                            $('#preloader').hide();
                        }
                        else {
                            $.ajax({
                                ServiceCallID: 1,
                                type: "POST",
                                url: 'http://gplpartnerconnect.com/GodrejPartnerConnect/get_broker.php',
                                data: 'pan_no=' + panno,
                                dataType: "json",
                                success: function (response) {
                                    if (response == "") {
                                        $('#pan').val(panno);
                                    }
                                    else {
                                        $('#fname').val(response.First_name);
                                        $('#lname').val(response.Last_Name);
                                        $('#mobile').val(response.Mobile_Number);
                                        $('#email').val(response.email_id);
                                        $('#pan').val(response.Pan_Number);
                                        $('#comp').val(response.service_tax_number);
                                        $('#hdnbroker').val(response.id);
                                    }
                                    $('#preloader').hide();
                                    $('#empl_pan').css('display', 'none');
                                    $("#emplform").removeAttr("style");
                                    setTimeout(function () {
                                    }, 500);
                                }
                            });
                        }
                    });
                },

                failure: function (response) {
                    alert('Fail');
                }
            });
        }

        function InsertEmpl() {

            var fname = $('#fname').val();
            var lname = $('#lname').val();
            var mobile = $('#mobile').val();
            var email = $('#email').val();
            var panno = $('#pan').val();
            var company = $('#comp').val();
            var region = $('#ddlRegion :selected').text();
            var brokerid = hdnbroker.value;
            var taxno = "";
            var tinno = "";
            var enterpriseid = "";
            //alert("broker"+brokerid);

            if (fname == null || fname == "" || fname.lenght > 100 || fname.lenght < 1) {
                alert('Please enter first name.');
                return false;
            }
            var chkname = alphanumeric_checkname(fname);

            if (chkname == 'NO') {
                alert('Please enter valid first name.');
                return false;
            }
            if (lname == null || lname == "" || lname.lenght > 100 || lname.lenght < 3) {
                alert('Please enter last name.');
                return false;
            }
            var chklname = alphanumeric_checkname(lname);

            if (chklname == 'NO') {
                alert('Please enter valid last name.');
                return false;
            }
            if (mobile == null || mobile == "") {
                alert('Please enter mobile number.');
                return false;
            }

            if (mobile.length < 10 || mobile.length > 10) {
                alert('Mobile number Should be 10 digits.');
                return false;
            }

            var numPattern = /\d+(,\d{1,3})?/;

            if (!numPattern.test(mobile)) {
                alert('Please enter numeric value.');
                return false;
            }

            if (email == null || email == "" || email.lenght > 100) {
                alert('Please enter email ID.');
                return false;
            }
            var filter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

            if (!filter.test(email)) {
                alert('Please enter valid email id.');
                return false;
            }

            if (panno == null || panno == "" || panno.lenght > 50) {
                alert('Please enter PAN No.');
                return false;
            }
            if (!panno.match(/[A-Z]{5}[0-9]{4}[A-Z]{1}/)) {
                alert('Please enter valid PAN No.');
                return false;
            }

            if (company == null || company == "") {
                alert('Please enter company name.');
                return false;
            }
            if (company.length > 100) {
                alert('Company name should be maximum 100 alphabets.');
                return false;
            }

            if (region == 'REGION INTERESTED') {
                alert('Please select region');
                return false;
            }

            if ($("#chkAgree").prop("checked") == false) {
                alert('Please accept declaration');
                return false;
            }

            if (company == "") {
                company = "null";
            }
            if (brokerid == "") {
                brokerid = "null";
            }
            if (taxno == "") {
                taxno = "null";
            }
            if (tinno == "") {
                tinno = "null";
            }
            if (enterpriseid == "") {
                enterpriseid = "null";
            }
            var fullname = fname + ' ' + lname;
            var addstring = fullname + "/" + mobile + "/" + email + "/" + panno + "/" + taxno + "/" + company + "/" + region + "/" + brokerid;
            var uname = fname + brokerid;
            var pwd = fname.replace(/\s/g, '') + randString();
            
            if (brokerid == "" || brokerid == null || brokerid == "null") {

                $('#preloader').show();
                $.ajax({
                    ServiceCallID: 1,
                    type: "POST",
                    url: baseURL + 'check_emailid.php',
                    data: 'email_id=' + $('#emailid').val() + '&mobile=' + mobile,
                    dataType: "json",
                    //async:false,
                    success: function (response) {
                        $('#preloader').show();
                        //alert(response.status);
                        if (response.status == "success") {
                            //$('.ui-loader').hide();
                            alert('The email id or mobile no you have entered already exists');
                            //console.log("check_emailid");
                            //$('#error_content').empty().html('The email id or mobile no you have entered already exists.');
                            //setTimeout(function () {
                            //    $('#enquiry_inline').simpledialog2({
                            //        callbackClose: function () {
                            //            //var me = this;
                                       $('#preloader').hide();
                            //            //setTimeout(function () { me.close(); }, 2000);
                            //        }
                            //    });
                            //    $('.ui-simpledialog-screen').css('height', $(document).height());
                            //}, 500);
                            return false;
                        }
                        else {
                            $('#preloader').show();
                            $.ajax({
                                ServiceCallID: 1,
                                type: "GET",
                                url: baseURL + 'RestServiceImpl.svc/InsertEmpanelment/' + addstring,
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (response) {
                                    $.each(response.InsertEmplResult, function (index, value) {
                                        if (value.status == "success") {
                                            $('#preloader').show();
                                            console.log("InsertEmpanelment");
                                            var emplid = value.userid;
                                            //api to insert broker details in SFDC
                                            $.ajax({
                                                ServiceCallID: 1,
                                                type: "POST",
                                                url: baseURL + 'insert_broker.php',
                                                data: 'company_name=' + company + '&pan_number=' + panno + '&first_name=' + fname + '&last_name=' + lname + '&mobile_number=' + mobile + '&service_tax_no=' + taxno + '&enterprise_membership_id=' + enterpriseid + '&email=' + email + '&region=' + region + '&empanelment_id=' + value.userid + '&tin_number=' + tinno,
                                                dataType: "json",
                                                success: function (response) {
                                                    var broker = response.broker_id;
                                                    var string = panno + "/" + broker;
                                                    var username = email;

                                                    //api to update brokerid in empanelment table 
                                                    $.ajax({
                                                        ServiceCallID: 1,
                                                        type: "GET",
                                                        contentType: "application/json; charset=utf-8",
                                                        url: baseURL + "RestServiceImpl.svc" + "/UpdateEmplDetails/" + string,
                                                        success: function (response) {
                                                            $.each(response.UpdateEmplInfoResult, function (index, value) {
                                                                if (value.status == "success") {
                                                                    $('#preloader').show();
                                                                    console.log("UpdateEmplDetails");
                                                                    //api to update empanelment id in SFDC table
                                                                    $.ajax({
                                                                        type: "POST",
                                                                        url: baseURL + 'update_broker.php',
                                                                        data: 'empanelment_id=' + emplid + '&broker_id=' + broker,
                                                                        success: function (response) {
                                                                            //$('.ui-loader').hide();

                                                                            console.log("update_broker");
                                                                        }
                                                                    });

                                                                    //api to create login details
                                                                    var addstring = emplid + "/" + pwd + "/" + email + "/" + fullname;
                                                                    $.ajax({
                                                                        ServiceCallID: 1,
                                                                        type: "GET",
                                                                        contentType: "application/json; charset=utf-8",
                                                                        url: baseURL + "RestServiceImpl.svc" + '/CreateLogin/' + addstring,
                                                                        success: function (response) {
                                                                            $.each(response.InsertLoginResult, function (index, value) {
                                                                                if (value.status == "success") {
                                                                                    $('#preloader').show();
                                                                                    //console.log("CreateLogin");
                                                                                    //$('.ui-loader').hide();
                                                                                    alert('You have registered successfully. You will receive your user id and password shortly.');
                                                                                    //$('#error_content').empty().html('You have registered successfully. You will receive your user id and password shortly.');
                                                                                    //setTimeout(function () {
                                                                                    //    $('#enquiry_inline').simpledialog2({
                                                                                    //        callbackClose: function () {
                                                                                    //            //$("#a_submit").removeAttr("onclick");
                                                                                    //            // $('#a_submit').attr("href", "login.html");
                                                                                    //            //$('#a_submit').trigger('click');
                                                                                    //            $.mobile.pageContainer.pagecontainer("change", "login.html", { transition: "none" });
                                                                                                $('#preloader').hide();
                                                                                    //        }
                                                                                    //    });
                                                                                    //    $('.ui-simpledialog-screen').css('height', $(document).height());
                                                                                    //}, 500);
                                                                                }
                                                                            });
                                                                        },
                                                                        failure: function (response) {
                                                                            alert('Fail');
                                                                        }
                                                                    });
                                                                }
                                                            });

                                                        },
                                                        failure: function (response) {
                                                            alert('Fail');
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                },
                                failure: function (response) {
                                    alert('Fail');
                                }
                            });
                        }
                    }
                });

                //console.log('done');
                //$('.ui-loader').hide();

            }
            else {
                $('.ui-loader').show();
                $.ajax({
                    ServiceCallID: 1,
                    type: "GET",
                    url: baseURL + 'RestServiceImpl.svc/InsertEmpanelment/' + addstring,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        $.each(response.InsertEmplResult, function (index, value) {
                            if (value.status == "success") {
                                $('#preloader').show();
                                var emplid = value.userid;
                                $.ajax({
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    url: baseURL + 'update_broker.php',
                                    data: 'empanelment_id=' + emplid + '&broker_id=' + brokerid,
                                    success: function (response) {
                                        console.log("InsertEmpanelment");
                                        //$('.ui-loader').hide();
                                    }
                                });

                                var addstring = emplid + "/" + pwd + "/" + email + "/" + fullname;
                                $.ajax({
                                    ServiceCallID: 1,
                                    type: "GET",
                                    contentType: "application/json; charset=utf-8",
                                    url: baseURL + "RestServiceImpl.svc" + '/CreateLogin/' + addstring,
                                    success: function (response) {
                                        $.each(response.InsertLoginResult, function (index, value) {
                                            if (value.status == "success") {
                                                console.log("CreateLogin");
                                                //$('.ui-loader').hide();
                                                //$('#error_content').empty().html('You have registered successfully. You will receive your user id and password shortly.');
                                                //setTimeout(function () {
                                                //    $('#enquiry_inline').simpledialog2({
                                                //        callbackClose: function () {
                                                //            //$("#a_submit").removeAttr("onclick");
                                                //            //$('#a_submit').attr("href", "login.html");
                                                //            //$('#a_submit').trigger('click');
                                                //            $.mobile.pageContainer.pagecontainer("change", "login.html", { transition: "none" });
                                                            $('#preloader').hide();
                                                //        }
                                                //    });
                                                //    $('.ui-simpledialog-screen').css('height', $(document).height());
                                                //}, 500);
                                            }
                                        });
                                    },
                                    failure: function (response) {
                                        alert('Fail');
                                    }
                                });
                            }
                        });
                    }
                });
            }

        }

        //function InsertEmpl() {
        //    var fname = $('#fname').val();
        //    var lname = $('#lname').val();
        //    var mobile = $('#mobile').val();
        //    var email = $('#email').val();
        //    var panno = $('#pan').val();
        //    var company = $('#comp').val();
        //    var region = $('#ddlRegion :selected').text();
        //    var brokerid = hdnbroker.value;
        //    var taxno = "";
        //    var tinno = "";
        //    var enterpriseid = "";

        //    if (fname == null || fname == "" || fname.lenght > 100 || fname.lenght < 1) {
        //        alert('Please enter first name.');
        //        return false;
        //    }
        //    var chkname = alphanumeric_checkname(fname);

        //    if (chkname == 'NO') {
        //        alert('Please enter valid first name.');
        //        return false;
        //    }
        //    if (lname == null || lname == "" || lname.lenght > 100 || lname.lenght < 3) {
        //        alert('Please enter last name.');
        //        return false;
        //    }
        //    var chklname = alphanumeric_checkname(lname);

        //    if (chklname == 'NO') {
        //        alert('Please enter valid last name.');
        //        return false;
        //    }
        //    if (mobile == null || mobile == "") {
        //        alert('Please enter mobile number.');
        //        return false;
        //    }

        //    if (mobile.length < 10 || mobile.length > 10) {
        //        alert('Mobile number Should be 10 digits.');
        //        return false;
        //    }

        //    var numPattern = /\d+(,\d{1,3})?/;

        //    if (!numPattern.test(mobile)) {
        //        alert('Please enter numeric value.');
        //        return false;
        //    }

        //    if (email == null || email == "" || email.lenght > 100) {
        //        alert('Please enter email ID.');
        //        return false;
        //    }
        //    var filter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        //    if (!filter.test(email)) {
        //        alert('Please enter valid email id.');
        //        return false;
        //    }

        //    if (panno == null || panno == "" || panno.lenght > 50) {
        //        alert('Please enter PAN No.');
        //        return false;
        //    }
        //    if (!panno.match(/[A-Z]{5}[0-9]{4}[A-Z]{1}/)) {
        //        alert('Please enter valid PAN No.');
        //        return false;
        //    }

        //    if (company == null || company == "") {
        //        alert('Please enter company name.');
        //        return false;
        //    }
        //    if (company.length > 100) {
        //        alert('Company name should be maximum 100 alphabets.');
        //        return false;
        //    }

        //    if (region == 'REGION INTERESTED') {
        //        alert('Please select region');
        //        return false;
        //    }

        //    if ($("#chkAgree").prop("checked") == false) {
        //        alert('Please accept declaration');
        //        return false;
        //    }

        //    if (company == "") {
        //        company = "null";
        //    }
        //    if (brokerid == "") {
        //        brokerid = "null";
        //    }
        //    if (taxno == "") {
        //        taxno = "null";
        //    }
        //    if (tinno == "") {
        //        tinno = "null";
        //    }
        //    if (enterpriseid == "") {
        //        enterpriseid = "null";
        //    }
        //    var fullname = fname + ' ' + lname;
        //    var addstring = fullname + "/" + mobile + "/" + email + "/" + panno + "/" + taxno + "/" + company + "/" + region + "/" + brokerid;
        //    var uname = fname + brokerid;
        //    var pwd = fname + randString();
        //    // alert(addstring);
        //    $('#preloader').show();
        //    //api to insert empanelment details
        //    $.ajax({
        //        ServiceCallID: 1,
        //        type: "GET",
        //        url: 'http://gplpartnerconnect.com/GodrejPartnerConnect/RestServiceImpl.svc/InsertEmpanelment/' + addstring,
        //        contentType: "application/json; charset=utf-8",
        //        dataType: "json",
        //        success: function (response) {
        //            $.each(response.InsertEmplResult, function (index, value) {
        //                if (value.status == "success") {
        //                    var emplid = value.userid;
        //                    if (brokerid == "" || brokerid == null || brokerid == "null") {
        //                        //api to insert broker details in SFDC
        //                        $.ajax({
        //                            ServiceCallID: 1,
        //                            type: "POST",
        //                            url: 'http://gplpartnerconnect.com/GodrejPartnerConnect/insert_broker.php',
        //                            data: 'company_name=' + company + '&pan_number=' + panno + '&first_name=' + fname + '&last_name=' + lname + '&mobile_number=' + mobile + '&service_tax_no=' + taxno + '&enterprise_membership_id=' + enterpriseid + '&email=' + email + '&region=' + region + '&empanelment_id=' + value.userid + '&tin_number=' + tinno,
        //                            dataType: "json",
        //                            success: function (response) {
        //                                var broker = response.broker_id;
        //                                var string = panno + "/" + broker;
        //                                var username = email;

        //                                //api to update brokerid in empanelment table 
        //                                $.ajax({
        //                                    type: "GET",
        //                                    contentType: "application/json; charset=utf-8",
        //                                    url: "http://gplpartnerconnect.com/GodrejPartnerConnect/RestServiceImpl.svc" + "/UpdateEmplDetails/" + string,
        //                                    async: false,
        //                                    success: function (response) {
        //                                        $.each(response.UpdateEmplInfoResult, function (index, value) {
        //                                            if (value.status == "success") {
        //                                                //api to update empanelment id in SFDC table
        //                                                $.ajax({
        //                                                    type: "POST",
        //                                                    contentType: "application/json; charset=utf-8",
        //                                                    url: 'http://gplpartnerconnect.com/GodrejPartnerConnect/update_broker.php',
        //                                                    data: 'empanelment_id=' + emplid + '&broker_id=' + broker,
        //                                                    success: function (response) {
        //                                                    }
        //                                                });

        //                                                //api to create login details
        //                                                var addstring = emplid + "/" + pwd + "/" + email + "/" + fullname;
        //                                                $.ajax({
        //                                                    type: "GET",
        //                                                    contentType: "application/json; charset=utf-8",
        //                                                    url: "http://gplpartnerconnect.com/GodrejPartnerConnect/RestServiceImpl.svc" + '/CreateLogin/' + addstring,
        //                                                    async: false,
        //                                                    success: function (response) {
        //                                                        $.each(response.InsertLoginResult, function (index, value) {
        //                                                        });
        //                                                    },
        //                                                    failure: function (response) {
        //                                                        alert('Fail');
        //                                                    }
        //                                                });
        //                                            }
        //                                        });

        //                                    },
        //                                    failure: function (response) {
        //                                        alert('Fail');
        //                                    }
        //                                });
        //                            }
        //                        });
        //                    }
        //                    else {
        //                        var addstring = emplid + "/" + pwd + "/" + email + "/" + fullname;
        //                        $.ajax({
        //                            type: "GET",
        //                            contentType: "application/json; charset=utf-8",
        //                            url: "http://gplpartnerconnect.com/GodrejPartnerConnect/RestServiceImpl.svc" + '/CreateLogin/' + addstring,
        //                            async: false,
        //                            success: function (response) {
        //                                $.each(response.InsertLoginResult, function (index, value) {
        //                                });
        //                            },
        //                            failure: function (response) {
        //                                alert('Fail');
        //                            }
        //                        });
        //                    }
        //                    $('#panno').val('');
        //                    $('#fname').val('');
        //                    $('#lname').val('');
        //                    $('#mobile').val('');
        //                    $('#email').val('');
        //                    $('#pan').val('');
        //                    $('#comp').val('');
        //                    $('#ddlRegion :selected').text('REGION INTERESTED');
        //                    $('#chkAgree').attr('checked', false);
        //                    $('#empl_pan').removeAttr("style");
        //                    $("#emplform").css('display', 'none');
        //                    //$("#btnSubmit").removeAttr("onclick");
        //                    //$('#btnSubmit').attr("href", "index.aspx");
        //                    //$('#btnSubmit').trigger('click');
        //                    $('#preloader').hide();
        //                    alert('Please check your email for login details!!');
        //                    return false;
        //                }
        //            });
        //        },

        //        failure: function (response) {
        //            alert('Fail');
        //        }
        //    });
        //}

        function alphanumeric_check(e) {

            var charpos = e.search("[^A-Za-z\. ]");
            if (charpos >= 0 || e.charAt(0) == ' ' || e.length < 2) {
                return 'NO';
            }
            else
                return 'YES';
        }

        function alphanumeric_checkname(e) {

            var charpos = e.search("[^A-Za-z\. ]");
            if (charpos >= 0 || e.charAt(0) == ' ' || e.length < 1) {
                return 'NO';
            }
            else
                return 'YES';
        }

        function randString() {

            var text = '';
            var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

            for (var i = 0; i < 5; i++) {
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            }

            return text;
        }

    </script>

    <script>
        jQuery(document).ready(function ($) {

            // site preloader -- also uncomment the div in the header and the css style for #preloader
            //$('#btnnext').click(function(){
            //$('#preloader').fadeOut('slow',function(){$(this).remove();});
            //});

            $(document).on('click', '.btnnext', function () {
                // $('#preloader').show();

            });

        });
    </script>
</head>

<body>
    <form id="form1" runat="server">
        <input id="hdnbroker" type="hidden" />
        <header>
            <section class="main_container">
                <div class="logo_connect">
                    <a href="#">
                        <img src="images/logo_connect.png" alt="image"></a>
                </div>
                <div class="right_header">
                    <ul>
                        <li class="tele_number">
                            <img src="images/tele_icon.png" alt="icon">1800 258 2588</li>
                        <li class="li_divider">&nbsp;</li>
                        <li class="gp_logo"><a href="#">
                            <img src="images/gp_logo.png" alt="logo"></a></li>
                    </ul>
                </div>
            </section>
        </header>
        <div class="clear"></div>
        <article class="banner_container">
            <ul class="bxslider">
                <li>
                    <img src="images/banner01.jpg" alt="image"><!--<div class="banner_text">Godrej Properties</div>--></li>
                <li>
                    <img src="images/banner02.jpg" alt="image" /></li>
                <li>
                    <img src="images/banner03.jpg" alt="image" /></li>
            </ul>
        </article>
        <div class="clear"></div>
        <article class="main_container main_body_container">
            <h1>Make work easy with Godrej Partner Connect</h1>
            <h2>Get instant access to all your details - Godrej properties channel partners</h2>
            <div class="clear"></div>
            <section class="left_img">
                <img src="images/mobile_img.png" alt="image">
            </section>
            <section class="home_main_content">
                <p>Around the country, our Channel Partners make a difference in the process of creating awareness about our new properties and selling them. We have collaborated with the industry’s largest professional network and are working towards creating brand new opportunities. Here we present our app, designed exclusively for our Channel Partners to make it easier for them access, manage and share information.</p>
                <div class="clear"></div>
                <div class="divider"></div>
                <span>Download the app now</span>
                <div class="clear"></div>
                <a id="a_ios" href="#">
                    <img src="images/app_store_img.png" alt="image"></a> <a id="a_android" target="_blank" href="https://play.google.com/store/apps/details?id=org.godrej.channelpartner" class="m-l-10">
                        <img src="images/googe_play_img.png" alt="image"></a>
            </section>
        </article>
        <div class="clear"></div>
        <article class="advantage_container">
            <h1>The Godrej Channel Partner Advantage</h1>
            <section class="main_container">
                <div id="owl-demo" class="owl-carousel">
                    <div class="item">
                        <img src="images/partner_icon01.png" alt="image">
                        <h2>Licensed Channel Partner</h2>
                        <p>Gain better collaboration for open communication, sharing best practice ideas and problem solving.</p>
                    </div>
                    <div class="item">
                        <img src="images/partner_icon02.png" alt="image">
                        <h2>Set Your Own Hours</h2>
                        <p>
                            Reap the benefits of great profitability, enhanced productivity and customers previously beyond your reach, all at your
          set timelines.
                        </p>
                    </div>
                    <div class="item">
                        <img src="images/partner_icon03.png" alt="image">
                        <h2>Reap Bounty of Benefits</h2>
                        <p>With becoming the Godrej Channel Partner, comes a great product portfolio along with excellent training and support from us.</p>
                    </div>


                </div>
            </section>
        </article>
        <div class="clear"></div>
        <article class="main_container month_partner_container">
            <h1>Channel Partner of the month</h1>
            <!--<ul id="" class="month_list">  ul_performer
                <li>
                    <img src="images/dummy_img_top.jpg" alt="image">
                    <p>Tata Capital Financial Services</p>
                    <span>Godrej Garden City</span>
                    <div class="clear"></div>
                   
                </li>
                <li>
                    <img src="images/dummy_img_top.jpg" alt="image">
                    <p>IIFL Realty Limited</p>
                    <span>Godrej Garden City</span>
                    <div class="clear"></div>
                    
                </li>
                <li>
                    <img src="images/dummy_img_top.jpg" alt="image">
                    <p>Centrum Wealth Management</p>
                    <span>Godrej Garden City</span>
                    <div class="clear"></div>
                   
                </li>
                <li>
                    <img src="images/dummy_img_top.jpg" alt="image">
                    <p>Invest In Reality LLP</p>
                    <span>Godrej Central</span>
                    <div class="clear"></div>
                   
                </li>
                <li class="last">
                    <img src="images/dummy_img_top.jpg" alt="image">
                    <p>Ravi Raaj Realtors</p>
                    <span>Godrej Panvel Tnship</span>
                    <div class="clear"></div>
                   
                </li>
                
                <div class="clear"></div>
                <a href="#" class="blue_btn view_all">View All</a>
            </ul> -->
            <div id="owl-demo1" class="owl-carousel month_list">
                <%--<div class="item">
                    <img src="images/dummy_img_top.jpg" alt="image">
                    <p>Tata Capital Financial Services</p>
                    <span>Godrej Garden City</span>
                </div>

                <div class="item">
                    <img src="images/dummy_img_top.jpg" alt="image">
                    <p>IIFL Realty Limited</p>
                    <span>Godrej Garden City</span>
                </div>

                <div class="item">
                    <img src="images/dummy_img_top.jpg" alt="image">
                    <p>Centrum Wealth Management</p>
                    <span>Godrej Garden City</span>
                </div>

                <div class="item">
                    <img src="images/dummy_img_top.jpg" alt="image">
                    <p>Invest In Reality LLP</p>
                    <span>Godrej Central</span>
                </div>

                <div class="item">
                    <img src="images/dummy_img_top.jpg" alt="image">
                    <p>Ravi Raaj Realtors</p>
                    <span>Godrej Panvel Tnship</span>
                </div>--%>
            </div>

        </article>
        <div class="clear"></div>
        <article class="empanelment_form_container empanel_bg">
            <div id="preloader"></div>
            <div class="emempanelment_image">
                <img src="images/empanel_bg.png" />
            </div>

            <section id="empl" class="main_container">
                <h1>Empanelment Form</h1>
                <h2>Become our channel partners and reap the bounty of benefits that come<br>
                    from our vast product portfolio and greater customer reach.</h2>
                <div id="empl_pan" class="empanel_form">
                    <input id="panno" type="text" class="input_text load_input" placeholder="PAN No">
                    <div class="clear"></div>
                    <a class="blue_btn submit_btn" id="btnnext" onclick="return checkdetails();">NEXT</a>
                    <%--<input type="submit" class="blue_btn submit_btn" value="Submit" onclick="javascript:checkdetails();">--%>
                </div>
                <div id="emplform" class="empanel_form" style="display: none;">
                    <input id="fname" type="text" class="input_text" placeholder="First Name">
                    <input id="lname" type="text" class="input_text m-l-15" placeholder="Last Name">
                    <input id="mobile" type="text" class="input_text" placeholder="Mobile No">
                    <input id="email" type="text" class="input_text m-l-15" placeholder="Email ID">
                    <input id="pan" type="text" class="input_text" placeholder="PAN No" readonly>
                    <input id="comp" type="text" class="input_text m-l-15" placeholder="COMPANY/FIRM NAME">
                    <select data-role="none" id="ddlRegion" placeholder="REGION INTERESTED" class="input_text select_field" onfocus="this.placeholder = ''" onblur="this.placeholder = 'REGION INTERESTED'">
                        <option selected>REGION INTERESTED</option>
                        <option>ALL</option>
                        <option>NORTH</option>
                        <option>SOUTH</option>
                        <option>EAST</option>
                        <option>WEST</option>
                    </select>
                    <%-- <input type="text" class="input_text" placeholder="Communication Address">
      <input type="text" class="input_text" placeholder="Location">
      <input type="text" class="input_text m-l-15" placeholder="City">
      <input type="text" class="input_text" placeholder="Country">--%>
                    <div class="chk_container m-l-15">
                        <input id="chkAgree" type="checkbox" class="chk_input">
                        <span>Declaration
                        <br>
                            I, hereby, declare that all the above mentioned information provided by me is true to the best of my knowledge and belief.</span>
                    </div>
                    <div class="clear"></div>
                    <a class="blue_btn submit_btn" id="btnSubmit" onclick="return InsertEmpl();">NEXT</a>
                    <%--<input type="submit" class="blue_btn submit_btn" value="Submit">--%>
                    <div class="clear"></div>
                    <em>Appointing of channel partners is at sole discretion of Godrej properties Limited. Applications may be rejected without providing any reason.</em>
                </div>
            </section>


        </article>
        <div class="clear"></div>
        <article class="main_container latest_project_container">
            <h1>Latest Projects</h1>
            <ul class="latest_proj_list">
                <li><a href="https://www.godrejproperties.com">
                    <div class="proj_img">
                        <img src="images/proj01.jpg" alt="image">
                        <div class="proj_name">Residential</div>
                    </div>
                </a></li>
                <li><a href="https://www.godrejproperties.com">
                    <div class="proj_img">
                        <img src="images/proj02.jpg" alt="image">
                        <div class="proj_name">Commercial</div>
                    </div>
                </a></li>
            </ul>
            <h2>Help your customers find the perfect residential destination for an iconic lifestyle and unmatched indulgence in the celebration of life. Find them homes that offer the grandeur of life with open arms.</h2>
        </article>
        <div class="clear"></div>
        <footer>
            <div class="main_container">
                <p class="copy">Copyright 2015 Godrej Properties Channel Partner Programme. | Powered by NetBiz.</p>
                <ul class="social_media">
                    <li><a href="https://www.facebook.com/godrejproperties" target="_blank">
                        <img src="images/fb_icon.png" alt="fb_icon"></a></li>
                    <li><a href="https://www.linkedin.com/company/godrejproperties?trk=tyah" target="_blank">
                        <img src="images/in_icon.png" alt="in_icon"></a></li>
                    <li><a href="https://www.youtube.com/user/godrejpropertiesIN" target="_blank">
                        <img src="images/youtube_icon.png" alt="youtube_icon"></a></li>
                    <li><a href="https://plus.google.com/+godrejproperties" target="_blank">
                        <img src="images/gplus_icon.png" alt="gplus_icon"></a></li>

                </ul>
            </div>
        </footer>
        <script src="js/jquery.1.11.2.min.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function (e) {
                $('.bxslider').bxSlider({
                    mode: 'fade',
                    auto: true
                });

            });
        </script>

        <!-- Owl Carousel Assets -->
        <script src="owl-carousel/owl.carousel.min.js"></script>
        <script>
            $(document).ready(function () {

                $("#owl-demo").owlCarousel({
                    //items : 3,
                    lazyLoad: true,
                    navigation: true,
                    auto: true
                });

            });
        </script>


        <script>
            $(document).ready(function () {

                // $("#owl-demo1").owlCarousel({
                //                    items: 3,
                //                    lazyLoad: true,
                //                    navigation: true,
                //                    auto: true
                //                });

            });
        </script>

    </form>
</body>
</html>
