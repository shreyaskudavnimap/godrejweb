﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Timers;

namespace RestService
{
    public class Global : System.Web.HttpApplication
    {
        Schedular sc = new Schedular();
        protected void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            #region timer for download sales SAP details

            System.Timers.Timer myTimerSAP = new System.Timers.Timer();
            myTimerSAP.Interval = 300000; // 21600000for every 15 minutes


            myTimerSAP.AutoReset = true;
            myTimerSAP.Elapsed += new ElapsedEventHandler(myTimerSAP_Elapsed);
            myTimerSAP.Enabled = true;
            #endregion

            // Code that runs on application startup
            #region timer for download financial SAP details

            System.Timers.Timer myTimerFinance = new System.Timers.Timer();
            myTimerFinance.Interval = 300000; // for every 15 minutes


            myTimerFinance.AutoReset = true;
            myTimerFinance.Elapsed += new ElapsedEventHandler(myTimerSAPFinance_Elapsed);
            myTimerFinance.Enabled = true;
            #endregion

            // Code that runs on application startup
            #region timer for download service SAP details

            System.Timers.Timer myTimerService = new System.Timers.Timer();
            myTimerService.Interval = 300000; // for every 15 minutes


            myTimerService.AutoReset = true;
            myTimerService.Elapsed += new ElapsedEventHandler(myTimerSAPService_Elapsed);
            myTimerService.Enabled = true;
            #endregion

            // Code that runs on application startup
            #region timer for download Performer SAP details

            System.Timers.Timer myTimerPerformer = new System.Timers.Timer();
            myTimerPerformer.Interval = 300000; // for every 15 minutes


            myTimerPerformer.AutoReset = true;
            myTimerPerformer.Elapsed += new ElapsedEventHandler(myTimerSAPPerformer_Elapsed);
            myTimerPerformer.Enabled = true;
            #endregion

            // Code that runs on application startup
            #region timer for update main dump

            System.Timers.Timer myTimerMainDump = new System.Timers.Timer();
            myTimerMainDump.Interval = 21600000;// for 15 minutes
            // 3600000; // for every 1 hour

            myTimerMainDump.AutoReset = true;
            myTimerMainDump.Elapsed += new ElapsedEventHandler(myTimerMainDump_Elapsed);
            myTimerMainDump.Enabled = true;
            #endregion

            // Code that runs on application startup
            #region timer for sending login details through email

            System.Timers.Timer myTimerLoginEmail = new System.Timers.Timer();
            myTimerLoginEmail.Interval = 300000; // for every 10 minutes

            myTimerLoginEmail.AutoReset = true;
            myTimerLoginEmail.Elapsed += new ElapsedEventHandler(myTimerLoginEmail_Elapsed);
            myTimerLoginEmail.Enabled = true;
            #endregion

            // Code that runs on application startup
            #region timer for sending appointment details through email

            System.Timers.Timer myTimerAppt = new System.Timers.Timer();
            myTimerAppt.Interval = 300000; // for every 10 minutes


            myTimerAppt.AutoReset = true;
            myTimerAppt.Elapsed += new ElapsedEventHandler(myTimerAppt_Elapsed);
            myTimerAppt.Enabled = true;
            #endregion

            // Code that runs on application startup
            #region timer for sending password details through email

            System.Timers.Timer myTimerPwd = new System.Timers.Timer();
            myTimerPwd.Interval = 120000; // for every 2 minutes


            myTimerPwd.AutoReset = true;
            myTimerPwd.Elapsed += new ElapsedEventHandler(myTimerPwd_Elapsed);
            myTimerPwd.Enabled = true;
            #endregion

            // Code that runs on application startup
            #region timer for sending faq mailers

            System.Timers.Timer myTimerFaq = new System.Timers.Timer();
            myTimerFaq.Interval = 600000; // for every 10 minutes


            myTimerFaq.AutoReset = true;
            myTimerFaq.Elapsed += new ElapsedEventHandler(myTimerFaq_Elapsed);
            myTimerFaq.Enabled = true;
            #endregion

            // Code that runs on application startup
            #region timer for sending ask expert mailers

            System.Timers.Timer myTimerExpert = new System.Timers.Timer();
            myTimerExpert.Interval = 600000; // for every 10 minutes


            myTimerExpert.AutoReset = true;
            myTimerExpert.Elapsed += new ElapsedEventHandler(myTimerExpert_Elapsed);
            myTimerExpert.Enabled = true;
            #endregion

            // Code that runs on application startup
            #region timer for sending ask lead details to channel partner through email

            System.Timers.Timer myTimerLead = new System.Timers.Timer();
            myTimerLead.Interval = 600000; // for every 10 minutes


            myTimerLead.AutoReset = true;
            myTimerLead.Elapsed += new ElapsedEventHandler(myTimerLead_Elapsed);
            myTimerLead.Enabled = true;
            #endregion

            

        }

        // for every 10 minutes code for Login Mailers
        public void myTimerLoginEmail_Elapsed(object source, System.Timers.ElapsedEventArgs e)
        {
            sc.LoginMail();
        }

        // for every 10 minutes code for appointment mailers
        public void myTimerAppt_Elapsed(object source, System.Timers.ElapsedEventArgs e)
        {
            sc.AppointmentMail();
        }

        // for every 2 minutes code for forgot password
        public void myTimerPwd_Elapsed(object source, System.Timers.ElapsedEventArgs e)
        {
            sc.ForgotPwdMail();
        }

        // for every 10 minutes code for faq mailer
        public void myTimerFaq_Elapsed(object source, System.Timers.ElapsedEventArgs e)
        {
            sc.FaqMail();
        }

        // for every 10 minutes code for Expert mailer
        public void myTimerExpert_Elapsed(object source, System.Timers.ElapsedEventArgs e)
        {
            sc.ExpertMail();
        }

        // for every 10 minutes code for lead details to channel partner
        public void myTimerLead_Elapsed(object source, System.Timers.ElapsedEventArgs e)
        {
            sc.LeadMail();
        }

        // for every 6 hour code for SAP sales table update
        public void myTimerSAP_Elapsed(object source, System.Timers.ElapsedEventArgs e)
        {
            sc.GetSAPDetails();
        }

        // for every 6 hour code for SAP sales table update
        public void myTimerSAPFinance_Elapsed(object source, System.Timers.ElapsedEventArgs e)
        {
            sc.GetSAPFinancialDetails();
        }

        // for every 6 hour code for SAP service table update
        public void myTimerSAPService_Elapsed(object source, System.Timers.ElapsedEventArgs e)
        {
            sc.GetSAPServiceDetails();
        }

        // for every 6 hour code for SAP Performer table update
        public void myTimerSAPPerformer_Elapsed(object source, System.Timers.ElapsedEventArgs e)
        {
            sc.GetPerformers();
        }

        // for every 6 hour code for main dump
        public void myTimerMainDump_Elapsed(object source, System.Timers.ElapsedEventArgs e)
        {
            sc.CreateMainDumpCsvFiles();
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
            {
                HttpContext.Current.Response.AddHeader("Cache-Control", "no-cache");
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "GET, POST");
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept");
                HttpContext.Current.Response.AddHeader("Access-Control-Max-Age", "1728000");
                HttpContext.Current.Response.End();
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}