﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="privacy-policy.aspx.cs" Inherits="privacy_policy" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <title>Channel Partner</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
    <script src="js/jquery.1.11.2.min.js"></script>
    <link rel="stylesheet" href="css/jquery.bxslider.css">
    <!-- Owl Carousel Assets -->
    <link href="owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="owl-carousel/owl.theme.css" rel="stylesheet">
    <link type="text/css" href="css/reset.css" rel="stylesheet" />
    <link type="text/css" href="css/style.css" rel="stylesheet" />
    <link type="text/css" href="css/responsive.css" rel="stylesheet" />
    <link type="text/css" href="css/fonts.css" rel="stylesheet" />
     <link type="text/css" href="css/Policy.css" rel="stylesheet" />
</head><body>
    <form id="form1" runat="server">
        <input id="hdnbroker" type="hidden" />
        <header>
            <section class="main_container">
                <div class="logo_connect">
                    <a href="#">
                        <img src="images/logo_connect.png" alt="image"></a>
                </div>
                <div class="right_header">
                    <ul>
                        <li class="tele_number">
                            <img src="images/tele_icon.png" alt="icon">1800 258 2588</li>
                        <li class="li_divider">&nbsp;</li>
                        <li class="gp_logo"><a href="#">
                            <img src="images/gp_logo.png" alt="logo"></a></li>
                    </ul>
                </div>
            </section>
        </header>
        <div class="clear"></div>
        <div class="how_list">
                <!-- <h1>Terms of use/Disclaimer</h1> -->
                This application is to help our Channel Partners to procure bookings of premises developed/promoted by GPL (“Agreement”), and GPL’s Code of Conduct details the terms (“Terms”) for use of the application for mobile devices (“App”) owned and operated by Godrej Properties Limited (“GPL").<br><br>

The App is designed to provide easy access to information including GPL real estate products/offerings, your leads, conversions, payment history/schedules through your individual account ("Account"). <br><br>

To use the App, you will require a mobile device, internet connectivity and appropriate telecommunication links and you will be responsible for all costs you incur for the same. This App is available to you on Play Store (Android phones) & App Store (Apple phones) . The download/installation, authentication of your ID and password, filling up the Empanelment forms and use of the App constitutes your acceptance of the Terms. You agree to provide accurate and complete information when you register with GPL, and to update the same from time to time. You agree not to violate/otherwise tamper with any element of the App or permit another person to do so. You shall not submit to GPL or to the App, anything which is inaccurate, out-of-date or in breach of any law in any respect.<br><br>

GPL shall, in no event, be liable/ responsible for any direct, indirect, punitive, incidental, special, consequential damages or any damages whatsoever including, without limitation, damages for loss of data or profits, arising out of or in any way connected with the use of the App.<br><br>

You represent and warrant that your use of the App will be strictly in accordance with the Terms and all applicable laws and regulations. The App, including but not limited to, listings of GPL real estate products/offerings ("Products"), graphics, audio/video clips and software used to implement the App, contains proprietary information of GPL, protected by applicable intellectual property and other laws. You agree to perform advertising and publicity activities in respect of the Products as per GPL instructions and not to copy any part of the App, save as permitted by GPL.<br><br>

You are solely responsible for maintaining the confidentiality and security of your Account, user identification code, password, related information and for all activities that occur on/through your Account. You shall not assign, or transfer the rights, benefits or interests herein or directly/indirectly disclose to any person any data, technical/financial details, information or designs relating to GPL without written permissions from GPL. You shall immediately notify GPL of any security breach of your Account. GPL shall not be responsible if any information is altered/removed/obscured after downloading complete/partial text/information contained in the App or for any loss arising out of the unauthorised use of your Account.<br><br>

<strong>No Warranty</strong><br>

It is imperative to note that GPL has taken required efforts to ensure that the information /statement provided on the App is reasonably accurate. However, GPL does not guarantee its accuracy, correctness, completeness or suitability whatsoever for any purpose. As such database provided is without any warranty, express or implied, as to their legal effect.<br><br>

Certain services, such as statement of accounts depend on continuous connection to the GPL’s database. GPL makes no representations about the suitability or reliability of the services contained on the App for any purpose. GPL makes no warranty that your access to the App will be uninterrupted, timely or error-free or compatible with the hardware/software you use. The App may be unavailable for reasons of force majeure including but not limited to fire, flood, earthquakes, storms, explosions, governmental restrictions, accidents, riots or other civil commotion, enemy action or other causes beyond GPL control. GPL shall not be liable for damage to, or viruses or other code that may affect, any equipment (including but not limited to your mobile device), software, data or other property as a result of your download, installation, access to or use of the App. <br><br>

  <strong>No Liability to GPL</strong><br>
Though, GPL has taken proper care/precautions to make the database reliable, GPL will not be held responsible for any liability that may arise out of any error in the data base. Use of any information on GPL’s App shall be at your own risk. All information should be used in accordance with applicable laws. GPL does not undertake any kind of liability whatsoever for the same. In case of any information which is not in agreement with your record, you are requested to write to GPL. You are also free to mail your queries to customercare@godrejproperties.com. GPL has provided information/data on the App on an "as is where is" basis. GPL expressly disclaims to the maximum limit permissible by law, all warranties, express or implied, including, but not limiting to implied warranties of merchantability, fitness for a particular purpose and non-infringement and all responsibility for any loss, injury, liability or damage of any kind resulting from and arising out of your use of the App. <br><br>

These Terms do not create any employer, or employee relationship or any partnership, joint venture between GPL and you. You shall not act or hold out as a GPL agent or accept any money/offer any discounts but shall only solicit business in GPL name, subject to written approval/ratification by GPL. Payments shall only be received in name/s specified by GPL through at par cheques/Demand Draft/Pay Order/NEFT/RTGS/ wire transfer/Telegraphic Transfer/Inward Remittances from abroad.<br><br> 

It shall be mandatory for you to understand the written/printed terms of the GPL Application Form completely, explain the same to the prospective purchaser/s clearly and to follow GPL processes, including collecting the required documentation, before accepting bookings. You shall be entitled to commissions upon your raising Bills/Invoices as detailed in the Agreement, only for GPL Application Forms that bear your rubber stamp and signature as approved by GPL. Bills/Invoices should include the service tax amount, Service Tax Registration Number, PAN and the classification of the category clearly. You shall not be entitled to any share in the transfer/nomination charges paid by the purchaser/s for transfer/nomination of his/her premises.<br><br>

You agree to indemnify and hold GPL and each of GPL affiliates, successors and assigns, and their respective officers, directors, employees, agents, representatives, harmless from and against any and all losses, expenses, damages, costs and expenses (including attorneys’ fees), resulting from your unauthorized use of the App or violation of the Terms, for your not taking requisite permissions/approvals or your noncompliance of statutory obligations, including Income Tax laws, FEMA 1999, labour laws or any act/omission whatsoever. <br><br>

GPL reserves the right to suspend these services if in GPL’s opinion, security of the App or of the data could be compromised. GPL may also suspend services on the App for any customer at its sole discretion without assigning any reason whatsoever. In such event, you shall contact GPL offices for any clarification. Even after such termination, you shall be liable for all bookings done through it before the aforesaid termination. <br><br>

Disputes, if any in relation to the Terms, shall be settled through arbitration as per the Arbitration and Conciliation Act, 1996, or any statutory amendments/modifications thereof for the time being in force, by a sole arbitrator selected from the names of two arbitrators proposed by the GPL. In case you delay/neglect /refuse to select one of the suggested names within 15 days of intimation, the first named arbitrator shall be deemed acceptable as the sole arbitrator whose award shall be final and binding. The proceedings shall be held in Mumbai and costs shall be shared equally. <br><br>

The parties shall be governed by the laws of India and courts at Mumbai alone shall have jurisdiction in respect of matters arising out of the Terms.<br><br>

  <strong>Other Terms and Conditions</strong><br>

GPL shall, in no event, be liable/ responsible for any direct, indirect, punitive, incidental, special, consequential damages or any damages for the delay or inability to use the App, or failure to provide services, or for any information, data and any other services obtained through the App, or otherwise arising out of the use of App. <br><br>

The App is offered as privileged service to you (without any charge). However, GPL reserves the right to levy any service charges as applicable from time to time in consideration for the services provided herein. You may opt out of these services, if you do not consent to such service charges. <br><br>

                                   
            </div>
        </form>
</body>
</html>
