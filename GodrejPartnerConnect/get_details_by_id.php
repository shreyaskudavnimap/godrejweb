<?php
	error_reporting(1);
	/*header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: PUT, GET, POST");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");*/

	//ini_set("soap.wsdl_cache_enabled", "0");
	//$pan_id = "AAZFS8386E";
	require_once ($_SERVER['DOCUMENT_ROOT'].'/GodrejPartnerConnect/org_config.php'); //configuration file containg login credentials
	require_once ($_SERVER['DOCUMENT_ROOT'].'/GodrejPartnerConnect/soapclient/SforcePartnerClient.php');
	$mySforceConnection = new SforcePartnerClient();
	$mySforceConnection->createConnection($_SERVER['DOCUMENT_ROOT']."/GodrejPartnerConnect/soapclient/Partnerwsdl_Production-14-3-17.jsp.xml");
	$mySforceConnection->login(USERNAME, PASSWORD.SECURITY_TOKEN);	

	$broker_id = $_POST['broker_id'];
	$broker_account_id = $_POST['broker_account_id'];
	
	$responseLeadData = array(
		"BrokerAccID" => $broker_account_id,
		"BrokerConID" => $broker_id		
	);
	//print_r($leadData);
	
	$parsedURL = parse_url($mySforceConnection->getLocation());
	define ("_SFDC_SERVER_", substr($parsedURL['host'],0,strpos($parsedURL['host'], '.')));
	define ("_WS_NAME_", 'WebToBrokerEnquiryCreation');
	define ("_WS_WSDL_", $_SERVER['DOCUMENT_ROOT'].'/GodrejPartnerConnect/soapclient/WebToBrokerEnquiryCreation.xml');
	define ("_WS_ENDPOINT_", 'https://' . _SFDC_SERVER_ . '.salesforce.com/services/wsdl/class/' . _WS_NAME_);
	define ("_WS_NAMESPACE_", 'http://soap.sforce.com/schemas/class/' . _WS_NAME_);
	$client = new SoapClient(_WS_WSDL_);
	$sforce_header = new SoapHeader(_WS_NAMESPACE_, "SessionHeader", array("sessionId" => $mySforceConnection->getSessionId()));
	$client->__setSoapHeaders(array($sforce_header));

	$upsertResponse = $client->EnquiryContact($responseLeadData);
	print_r($upsertResponse);
	exit;
	/*$response = array();
	
	foreach ($upsertResponse as $created){
		//echo $created->id;

		if($created->Id != ''){
			$response['status'] = 'success';
			$response['broker_id'] = $created->Id;
			$response['broker_account_id'] = $created->AccountId;
		}else{
			$response['status'] = 'error';
			$response['broker_id'] = '';
		}
	}

	echo json_encode($response);*/
	
?>