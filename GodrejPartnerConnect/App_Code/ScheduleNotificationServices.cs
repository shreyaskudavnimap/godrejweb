﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.DAL;
using GPLPartnerConnect.Model.Empanel;
using Hangfire;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestService
{
    public class ScheduleNotificationServices
    {
        DbCommand _sqlcommand = null;
        DataAccess dataAccess    = new DataAccess();

        public void Start_Scheduler()
        {
            RecurringJob.AddOrUpdate(() => Initiate(), Cron.Minutely);
        }

        private void Initiate()
        {
            
        }

        public void SendNotification(DataTable dt, string userid)
        {
            int cnt = 0, totaldevicecnt = 0;
            mNotificationLog obj = new mNotificationLog();
            foreach (DataRow row in dt.Rows)
            {
                obj.NotificationID = row["NotificationID"].ToString();
                obj.NotificationTitle = row["NotificationTitle"].ToString();
                obj.NotificationBody = row["NotificationBody"].ToString();
                obj.NotificationSourceID = row["NotificationSourceID"].ToString();
                obj.NotificationType = row["NotificationType"].ToString();
                obj.ChannelType = row["ChannelType"].ToString();
                obj.MobileUrl = row["MobileUrl"].ToString();
                obj.ChannelID = row["ChannelID"].ToString();
                obj.IsRedirect = row["IsRedirect"].ToString();
                obj.InsertedBy = userid;
                obj.Active = "Y";
                obj.SubTitle = row["SubTitle"].ToString();
                obj.NotificationImage = row["NotificationImage"].ToString();

                DataSet dsEmp = GetActiveDevice();
                totaldevicecnt += dsEmp.Tables[0].Rows.Count;
                foreach (DataRow rowEmp in dsEmp.Tables[0].Rows)
                {
                    obj.SendStatus = "";
                    obj.ErrorMsg = "";
                    obj.EmplContDeviceLogID = rowEmp["EmplContDeviceLogID"].ToString();
                    obj.EmplContID = rowEmp["EmplContID"].ToString();
                    obj.DeviceToken = rowEmp["DeviceToken"].ToString();
                    obj.MobilePlatform = rowEmp["MobilePlatform"].ToString();

                    //obj.NotificationLogID = InsertNotificationLog(obj).ToString();

                    GPLPartnerConnect.Model.FCM.FcmMessage fcmmsg = new GPLPartnerConnect.Model.FCM.FcmMessage();
                    fcmmsg.to = obj.DeviceToken;

                    fcmmsg.data.title = obj.NotificationTitle;
                    fcmmsg.data.body = obj.SubTitle;
                    fcmmsg.data.notificationLogID = obj.NotificationID;

                    fcmmsg.notification.title = obj.NotificationTitle;
                    fcmmsg.notification.body = obj.SubTitle;
                    fcmmsg.notification.notificationLogID = obj.NotificationID;

                    GPLPartnerConnect.Model.FCM.mRespon fcmResp = GPLNotification.Instance.SendNotification(fcmmsg);

                    cnt += Convert.ToInt16(fcmResp.success);
                    if (fcmResp.success == "0")
                    {
                        obj.SendStatus = "N";
                        obj.ErrorMsg = fcmResp.results[0].error;
                    }
                    else
                    {
                        obj.SendStatus = "Y";
                    }

                    //UpdateNotificationLog(obj);
                }
                //UpdateNotificationSendStatus(obj.NotificationID);
            }
            //return cnt + " of " + totaldevicecnt;
        }

        public DataSet GetActiveDevice()
        {
            mEmplContDeviceLog mobj = new mEmplContDeviceLog();
            return EmplContDeviceLogData(mobj, "D");
        }

        public DataSet EmplContDeviceLogData(mEmplContDeviceLog empDev, String Flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_EmplContDeviceLog", ref _sqlcommand,
               "@EmplContDeviceLogID", SqlDbType.VarChar, empDev.EmplContDeviceLogID, ParameterDirection.Input,
               "@EmplContID", SqlDbType.VarChar, empDev.EmplContID, ParameterDirection.Input,
               "@DeviceToken", SqlDbType.VarChar, empDev.DeviceToken, ParameterDirection.Input,
               "@MobilePlatform", SqlDbType.VarChar, empDev.MobilePlatform, ParameterDirection.Input,
               "@Active", SqlDbType.VarChar, empDev.Active, ParameterDirection.Input,
               "@Flag", SqlDbType.VarChar, Flag, ParameterDirection.Input);
            return ds;

        }
    }
}
