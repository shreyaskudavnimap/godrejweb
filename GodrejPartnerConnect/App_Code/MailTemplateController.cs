﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MailTemplateController
/// </summary>
public class MailTemplateController
{

    public mMailTemplate GetMailTempleteByCode(string Code)
    {
        string constring = ConfigurationManager.ConnectionStrings["godrejCPConnectionString"].ConnectionString;

        SqlConnection conn = new SqlConnection(constring);
        SqlCommand comm;
        SqlDataAdapter da;
        mMailTemplate mobj = null;
        try
        {
            
            
            if (conn.State != ConnectionState.Open)
                conn.Open();
            comm = new SqlCommand("usp_MailTemplate", conn);
            comm.Connection = conn;
            comm.CommandType = System.Data.CommandType.StoredProcedure;
            comm.Parameters.AddWithValue("@MailTemplateID", "");
            comm.Parameters.AddWithValue("@MailTempleteCode", Code);
            comm.Parameters.AddWithValue("@Subject", "");
            comm.Parameters.AddWithValue("@Body", "");
            comm.Parameters.AddWithValue("@Active", "");
            comm.Parameters.AddWithValue("@InsertedBy", "");
            comm.Parameters.AddWithValue("@flag", "E");
            DataSet ds = new DataSet();
            da = new SqlDataAdapter(comm);
            da.Fill(ds);
            //DataSet ds = objDAL.Call_SP_MailTemplate(mobj, "E");
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow row = ds.Tables[0].Rows[0];
                mobj = new mMailTemplate();
                mobj.MailTemplateID = row["MailTemplateID"].ToString();
                mobj.MailTempleteCode = row["MailTempleteCode"].ToString();
                mobj.Subject = row["Subject"].ToString();
                mobj.Body = row["Body"].ToString();
                mobj.Active = row["Active"].ToString();
                
            }

        }
        catch
        {

        }
        finally
        {
            if(conn.State == ConnectionState.Closed)
                    conn.Close();

        }
        
        return mobj;
    }
}
public class mMailTemplate
{
    public string MailTemplateID { get; set; }
    public string MailTempleteCode { get; set; }
    public string Subject { get; set; }
    public string Body { get; set; }
    public string Active { get; set; }
    public string InsertedBy { get; set; }
}