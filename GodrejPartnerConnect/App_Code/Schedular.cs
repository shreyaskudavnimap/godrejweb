﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;

namespace RestService
{
    public class Schedular
    {
        string constring = ConfigurationManager.ConnectionStrings["godrejCPConnectionString"].ConnectionString;

        /// <summary>
        /// Get SAP details from server
        /// </summary>
        public void GetSAPDetails()
        {
            SqlConnection conn;
            SqlCommand comm;
            SqlDataAdapter da;
            conn = new SqlConnection(constring);
            try
            {
                List<string> splitted = new List<string>();
                string fileList = getCSV("http://gplpartnerconnect.com/GodrejPartnerConnect/sap/sales_details/sales_detl.txt");
                string[] tempStr;
                string singleStr;
                string[] finalStr;
                tempStr = fileList.Split('\r');
                //System.Web.HttpContext.Current.Response.Write(tempStr.Length);
                //finalStr = tempStr.('\t');

                for (int j = 0; j < tempStr.Length - 1; j++)
                {
                    singleStr = tempStr[j];
                    finalStr = singleStr.Split('\t');
                    if (j > 0)
                    {
                        //for (int k = 0; k < finalStr.Length; k++)
                        //{
                        if (conn.State != ConnectionState.Open)
                            conn.Open();

                        comm = new SqlCommand("SP_insert_Sales_Test", conn);
                        comm.Connection = conn;
                        comm.CommandType = System.Data.CommandType.StoredProcedure;
                        comm.Parameters.AddWithValue("@SalesDocument", finalStr[2]);
                        comm.Parameters.AddWithValue("@Date", finalStr[3]);
                        comm.Parameters.AddWithValue("@EntryTime", finalStr[4]);
                        comm.Parameters.AddWithValue("@DocumentDate", finalStr[5]);
                        comm.Parameters.AddWithValue("@Project", finalStr[6]);
                        comm.Parameters.AddWithValue("@OrgName", finalStr[7]);
                        comm.Parameters.AddWithValue("@Customer", finalStr[8]);
                        comm.Parameters.AddWithValue("@CustomerName", finalStr[9]);
                        comm.Parameters.AddWithValue("@Flatcode", finalStr[11]);
                        comm.Parameters.AddWithValue("@Tower", finalStr[12]);
                        comm.Parameters.AddWithValue("@PlantName", finalStr[13]);
                        comm.Parameters.AddWithValue("@Vendor_Acct_No", finalStr[0]);
                        comm.Parameters.AddWithValue("@Per_Acct_No", finalStr[1]);
                        comm.Parameters.AddWithValue("@Brokerage", finalStr[10]);
                        comm.Parameters.AddWithValue("@REJ_IN", finalStr[14]);
                        comm.Parameters.AddWithValue("@REJ_DATE", finalStr[15]);
                        int l = comm.ExecuteNonQuery();
                        //}
                    }

                }
                Createerrorlog("sales", "success", Convert.ToString(tempStr.Length - 1));
            }
            catch (Exception ex)
            {
                Createerrorlog("sales", ex.Message.ToString(), "0");
            }
            finally
            {
                conn.Close();
            }
        }

        public void GetSAPFinancialDetails()
        {
            SqlConnection conn;
            SqlCommand comm;
            SqlDataAdapter da;
            conn = new SqlConnection(constring);
            try
            {
                List<string> splitted = new List<string>();
                string fileList = getCSV("http://gplpartnerconnect.com/GodrejPartnerConnect/sap/financial_details/financial_detl.txt");
                string[] tempStr;
                string singleStr;
                string[] finalStr;
                tempStr = fileList.Split('\r');

                //finalStr = tempStr.('\t');

                for (int j = 0; j < tempStr.Length - 1; j++)
                {
                    singleStr = tempStr[j];
                    finalStr = singleStr.Split('\t');
                    if (j > 0)
                    {
                        //for (int k = 0; k < finalStr.Length; k++)
                        //{;
                        if (conn.State != ConnectionState.Open)
                            conn.Open();

                        comm = new SqlCommand("SP_insert_Payment_Test", conn);
                        comm.Connection = conn;
                        comm.CommandType = System.Data.CommandType.StoredProcedure;
                        comm.Parameters.AddWithValue("@Comp_Code", finalStr[2]);
                        comm.Parameters.AddWithValue("@Fiscal_Year", finalStr[3]);
                        comm.Parameters.AddWithValue("@Accounting_Document_No", finalStr[4]);
                        comm.Parameters.AddWithValue("@Posting_Date", finalStr[6]);
                        comm.Parameters.AddWithValue("@Document_Date", finalStr[7]);
                        comm.Parameters.AddWithValue("@Document_Entered_Date", finalStr[8]);
                        comm.Parameters.AddWithValue("@Account_No", finalStr[0]);
                        comm.Parameters.AddWithValue("@Per_Acct_No", finalStr[1]);
                        comm.Parameters.AddWithValue("@BLAR", finalStr[9]);
                        comm.Parameters.AddWithValue("@STBLG", finalStr[10]);
                        comm.Parameters.AddWithValue("@Amount", finalStr[12]);
                        comm.Parameters.AddWithValue("@STJAH", finalStr[11]);
                        comm.Parameters.AddWithValue("@ITEM", finalStr[5]);
                        int l = comm.ExecuteNonQuery();
                        //}
                    }

                }
                Createerrorlog("Finacial", "success", Convert.ToString(tempStr.Length - 1));
            }
            catch (Exception ex)
            {
                Createerrorlog("Finacial", ex.Message.ToString(), "0");
            }
            finally
            {
                conn.Close();
            }
        }

        public void GetSAPServiceDetails()
        {
            SqlConnection conn;
            SqlCommand comm;
            SqlDataAdapter da;
            conn = new SqlConnection(constring);
            try
            {
                List<string> splitted = new List<string>();
                string fileList = getCSV("http://gplpartnerconnect.com/GodrejPartnerConnect/sap/service_details/service_detl.txt");
                string[] tempStr;
                string singleStr;
                string[] finalStr;
                tempStr = fileList.Split('\r');

                //finalStr = tempStr.('\t');

                for (int j = 0; j < tempStr.Length - 1; j++)
                {
                    singleStr = tempStr[j];
                    finalStr = singleStr.Split('\t');
                    if (j > 0)
                    {
                        //for (int k = 0; k < finalStr.Length; k++)
                        //{
                        if (conn.State != ConnectionState.Open)
                            conn.Open();

                        comm = new SqlCommand("SP_insert_Purchase_Test", conn);
                        comm.Connection = conn;
                        comm.CommandType = System.Data.CommandType.StoredProcedure;
                        comm.Parameters.AddWithValue("@Purchasing_Document_No", finalStr[2]);
                        comm.Parameters.AddWithValue("@Date", finalStr[3]);
                        comm.Parameters.AddWithValue("@Purchasing_Doc_Date", finalStr[4]);
                        comm.Parameters.AddWithValue("@Vendor_Acct_No", finalStr[0]);
                        comm.Parameters.AddWithValue("@Per_Acct_No", finalStr[1]);
                        comm.Parameters.AddWithValue("@StartValidityPeriod", finalStr[5]);
                        comm.Parameters.AddWithValue("@EndValidityPeriod", finalStr[6]);
                        comm.Parameters.AddWithValue("@ReleaseValue", finalStr[7]);
                        comm.Parameters.AddWithValue("@EKORG", finalStr[8]);
                        int l = comm.ExecuteNonQuery();
                        //}
                    }

                }
                Createerrorlog("service", "success", Convert.ToString(tempStr.Length - 1));
            }
            catch (Exception ex)
            {
                Createerrorlog("service", ex.Message.ToString(), "0");
            }
            finally
            {
                conn.Close();
            }
        }

        public void GetPerformers()
        {
            SqlConnection conn;
            SqlCommand comm;
            SqlDataAdapter da;
            conn = new SqlConnection(constring);
            try
            {
                List<string> splitted = new List<string>();
                string fileList = getCSV("http://gplpartnerconnect.com/GodrejPartnerConnect/sap/top_performer/top_performer.txt");
                string[] tempStr;
                string singleStr;
                string[] finalStr;
                tempStr = fileList.Split('\r');

                //finalStr = tempStr.('\t');

                for (int j = 0; j < tempStr.Length - 1; j++)
                {
                    singleStr = tempStr[j];
                    finalStr = singleStr.Split('\t');
                    if (j > 0)
                    {
                        //for (int k = 0; k < finalStr.Length; k++)
                        //{
                        if (conn.State != ConnectionState.Open)
                            conn.Open();

                        comm = new SqlCommand("SP_insert_Performer", conn);
                        comm.Connection = conn;
                        comm.CommandType = System.Data.CommandType.StoredProcedure;
                        comm.Parameters.AddWithValue("@Vendor_Acct_No", finalStr[0]);
                        comm.Parameters.AddWithValue("@Per_Acct_No", finalStr[1]);
                        comm.Parameters.AddWithValue("@Name", finalStr[2]);
                        comm.Parameters.AddWithValue("@Project", finalStr[3]);
                        comm.Parameters.AddWithValue("@OrgName", finalStr[4]);
                        comm.Parameters.AddWithValue("@Sales_No", finalStr[5]);
                        int l = comm.ExecuteNonQuery();
                        //}
                    }

                }
                Createerrorlog("Performer", "success", Convert.ToString(tempStr.Length - 1));
            }
            catch (Exception ex)
            {
                Createerrorlog("Performer", ex.Message.ToString(), "0");
            }
            finally
            {
                conn.Close();
            }
        }

        public string getCSV(string url)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

            StreamReader sr = new StreamReader(resp.GetResponseStream());
            string results = sr.ReadToEnd();
            sr.Close();

            return results;
        }

        /// <summary>
        /// Function to update the CSV file on the server from database
        /// </summary>
        public void CreateMainDumpCsvFiles()
        {
            SqlConnection conn;
            SqlCommand comm;
            SqlDataAdapter da;
            conn = new SqlConnection(constring);
            try
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_CreateDump", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                //string filePath = HttpContext.Current.Server.MapPath("sap/main_dump/main_dump.csv");
                string filePath = System.Web.Hosting.HostingEnvironment.MapPath("~/sap/main_dump/main_dump.csv");
                if (File.Exists(filePath))
                    File.Delete(filePath);

                if (!File.Exists(filePath))
                {
                    File.Create(filePath).Close();
                }

                //Createerrorlog("dump", "file", "0");
                List<string> strDetailIDList = new List<string>();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    strDetailIDList.Add("Empanelment_Id," + "Broker_id," + "J_1IPANNO," + "LIFNR");
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        strDetailIDList.Add(item["EmplID"].ToString() + "," + item["BrokerID"].ToString() + "," + item["Pan_No"].ToString() + "," + item["SAP_Vendor_Code"].ToString());

                    }
                }
                string[] strDetailID = strDetailIDList.ToArray();

                StringBuilder sb = new StringBuilder();
                for (int index = 0; index < strDetailID.Length; index++)
                {
                    sb.AppendLine(strDetailID[index]);

                }
                //Createerrorlog("dump", Convert.ToString(strDetailID.Length), "0");
                //System.Web.HttpContext.Current.Response.Write(strDetailID.Length);

                File.AppendAllText(filePath, sb.ToString());
                Createerrorlog("dump", "success", Convert.ToString(ds.Tables[0].Rows.Count));
            }
            catch (Exception ex)
            {
                Createerrorlog("dump", ex.Message.ToString(), "0");
            }
            finally
            {
                conn.Close();
            }
        }

        //public void CreateMainDumpCsvFiles()
        //{
        //    try
        //    {
        //        SqlConnection conn;
        //        SqlCommand comm;
        //        SqlDataAdapter da;
        //        conn = new SqlConnection(constring);
        //        if (conn.State != ConnectionState.Open)
        //            conn.Open();
        //        comm = new SqlCommand("SP_CreateDump", conn);
        //        comm.Connection = conn;
        //        comm.CommandType = System.Data.CommandType.StoredProcedure;
        //        DataSet ds = new DataSet();
        //        da = new SqlDataAdapter(comm);
        //        da.Fill(ds);
        //        string filePath = HttpContext.Current.Server.MapPath("sap/main_dump/maindump.csv");
        //        if (!File.Exists(filePath))
        //        {
        //            File.Create(filePath).Close();
        //        }
        //        string delimiter = ",";
        //        string[][] output = new string[][]{
        //        new string[]{"empanelment_id","broker_id","J_1IPANNO","LIFNR"} /*add the values that you want inside a csv file. Mostly this function can be used in a foreach loop.*/
        //    }; ;
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            foreach (DataRow item in ds.Tables[0].Rows)
        //            {
        //                output = new string[][]{
        //        new string[]{Convert.ToString(item["EmplID"]),Convert.ToString(item["BrokerID"]),Convert.ToString(item["Pan_No"]),Convert.ToString(item["SAP_Vendor_Code"])} /*add the values that you want inside a csv file. Mostly this function can be used in a foreach loop.*/
        //    }; ;
        //            }
        //        }

        //        int length = output.GetLength(0);
        //        StringBuilder sb = new StringBuilder();
        //        for (int index = 0; index < length; index++)
        //            sb.AppendLine(string.Join(delimiter, output[index]));
        //        File.AppendAllText(filePath, sb.ToString());
        //        Createerrorlog("dump", "success", Convert.ToString(ds.Tables[0].Rows.Count));
        //    }
        //    catch (Exception ex)
        //    {
        //        Createerrorlog("dump", ex.Message.ToString(), "0");
        //    }
        //}

        /// <summary>
        /// Function to capture the schedular log for both
        /// </summary>
        /// <param name="type"></param>
        /// <param name="msg"></param>
        /// <param name="rows"></param>
        public void Createerrorlog(string type, string msg, string rows)
        {
            #region code to update schedular log

            SqlConnection conn;
            SqlCommand comm;
            conn = new SqlConnection(constring);
            if (conn.State != ConnectionState.Open)
                conn.Open();
            comm = new SqlCommand("SP_SchedularLog", conn);
            comm.Connection = conn;
            comm.CommandType = System.Data.CommandType.StoredProcedure;
            comm.Parameters.AddWithValue("@type", type);
            comm.Parameters.AddWithValue("@errormsg", msg);
            comm.Parameters.AddWithValue("@rows", rows);
            int i = comm.ExecuteNonQuery();
            conn.Close();
            #endregion
        }

        public void LoginMail()
        {
            #region code to send Login Details
            int j = 0;
            SqlConnection conn;
            SqlCommand cmd = new SqlCommand();
            SqlCommand cmdd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            conn = new SqlConnection(constring);
            try
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                cmd = new SqlCommand("SP_GetDetails", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                da.SelectCommand = cmd;
                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        MailMessage mail = new MailMessage();
                        SmtpClient SmtpServer = new SmtpClient("smtp.office365.com");
                        mail.From = new MailAddress("marketing@godrejproperties.com");
                        mail.To.Add(dt.Rows[i]["Email"].ToString());
                        mail.Bcc.Add("srikkanth.thevar@godrejproperties.com");
                        mail.Subject = "Godrej Partner Connect - Welcome details";
                        mail.BodyEncoding = Encoding.UTF8;
                        mail.IsBodyHtml = true;
                        string path = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                        StreamReader sr = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Mailer/empanelment_form.html"));
                        mail.Body = sr.ReadToEnd().Replace("user_name", dt.Rows[i]["Name"].ToString()).Replace("user_email", dt.Rows[i]["Email"].ToString()).Replace("user_pwd", dt.Rows[i]["password"].ToString());
                        SmtpServer.Port = Convert.ToInt32(25);
                        SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                        SmtpServer.UseDefaultCredentials = false;
                        SmtpServer.Credentials = new System.Net.NetworkCredential("support_it@godrejproperties.com", "Welcome@2016");
                        SmtpServer.EnableSsl = true;
                        SmtpServer.Send(mail);
                        sr.Close();

                        cmdd = new SqlCommand("SP_UpdateLogin", conn);
                        cmdd.Connection = conn;
                        cmdd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmdd.Parameters.AddWithValue("@id", dt.Rows[i]["EmplID"].ToString());
                        j = cmdd.ExecuteNonQuery();
                        if (j > 1)
                        {
                            j += 1;
                        }
                    }
                    Createmaillog("Login", "success", Convert.ToString(j));
                }
            }
            catch (Exception ex)
            {
                Createmaillog("Login", "fail", Convert.ToString(ex));
            }
            finally
            {
                conn.Close();
            }
            #endregion
        }

        public void AppointmentMail()
        {
            #region code to send Login Details
            int j = 0;
            SqlConnection conn;
            SqlCommand cmd = new SqlCommand();
            SqlCommand cmdd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            conn = new SqlConnection(constring);
            try
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                cmd = new SqlCommand("SP_ApptDetails", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                da.SelectCommand = cmd;
                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        MailMessage mail = new MailMessage();
                        SmtpClient SmtpServer = new SmtpClient("smtp.office365.com");
                        mail.From = new MailAddress("marketing@godrejproperties.com");
                        mail.To.Add(dt.Rows[i]["emailid"].ToString());
                        mail.Subject = "Godrej Partner Connect - Book A Site Visit";
                        mail.IsBodyHtml = true;
                        string path = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                        StreamReader sr = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Mailer/book_a_site_visit.html"));
                        mail.Body = sr.ReadToEnd().Replace("cust_name", dt.Rows[i]["Name"].ToString()).Replace("user_code", dt.Rows[i]["SAP_vendor_Code"].ToString()).Replace("user_date", dt.Rows[i]["date"].ToString());
                        SmtpServer.Port = Convert.ToInt32(25);
                        SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                        SmtpServer.UseDefaultCredentials = false;
                        SmtpServer.Credentials = new System.Net.NetworkCredential("support_it@godrejproperties.com", "Welcome@2016");
                        SmtpServer.EnableSsl = true;
                        SmtpServer.Send(mail);
                        sr.Close();

                        cmdd = new SqlCommand("SP_UpdateAppt", conn);
                        cmdd.Connection = conn;
                        cmdd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmdd.Parameters.AddWithValue("@id", dt.Rows[i]["appointmentID"].ToString());
                        j = cmdd.ExecuteNonQuery();
                        if (j > 1)
                        {
                            j += 1;
                        }
                    }
                    Createmaillog("Appointment", "success", Convert.ToString(j));
                }
            }
            catch (Exception ex)
            {
                Createmaillog("Appointment", "fail", Convert.ToString(ex));
            }
            finally
            {
                conn.Close();
            }
            #endregion
        }

        public void ForgotPwdMail()
        {
            #region code to send forgot Emails
            int j = 0;
            SqlConnection conn;
            SqlCommand cmd = new SqlCommand();
            SqlCommand cmdd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            conn = new SqlConnection(constring);
            try
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                cmd = new SqlCommand("SP_ForgotPwdMail", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                da.SelectCommand = cmd;
                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        MailMessage mail = new MailMessage();
                        SmtpClient SmtpServer = new SmtpClient("smtp.office365.com");
                        mail.From = new MailAddress("marketing@godrejproperties.com");
                        mail.To.Add(dt.Rows[i]["Email"].ToString());
                        mail.Subject = "Godrej Partner Connect - Forgot Password";
                        mail.IsBodyHtml = true;
                        string path = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                        StreamReader sr = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Mailer/forget_mail.html"));
                        mail.Body = sr.ReadToEnd().Replace("user_name", dt.Rows[i]["username"].ToString()).Replace("user_pwd", dt.Rows[i]["password"].ToString());
                        SmtpServer.Port = Convert.ToInt32(25);
                        SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                        SmtpServer.UseDefaultCredentials = false;
                        SmtpServer.Credentials = new System.Net.NetworkCredential("support_it@godrejproperties.com", "Welcome@2016");
                        SmtpServer.EnableSsl = true;
                        SmtpServer.Send(mail);
                        sr.Close();

                        cmdd = new SqlCommand("SP_UpdatePwd", conn);
                        cmdd.Connection = conn;
                        cmdd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmdd.Parameters.AddWithValue("@id", dt.Rows[i]["EmplID"].ToString());
                        j = cmdd.ExecuteNonQuery();
                        if (j > 1)
                        {
                            j += 1;
                        }
                    }
                    Createmaillog("Password", "success", Convert.ToString(j));
                }
            }
            catch (Exception ex)
            {
                Createmaillog("Password", "fail", Convert.ToString(ex));
            }
            finally
            {
                conn.Close();
            }
            #endregion
        }

        public void FaqMail()
        {
            #region code to send faq Emails
            int j = 0;
            SqlConnection conn;
            SqlCommand cmd = new SqlCommand();
            SqlCommand cmdd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            conn = new SqlConnection(constring);
            try
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                cmd = new SqlCommand("SP_FaqMail", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                da.SelectCommand = cmd;
                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        MailMessage mail = new MailMessage();
                        SmtpClient SmtpServer = new SmtpClient("smtp.office365.com");
                        mail.From = new MailAddress("marketing@godrejproperties.com");
                        mail.To.Add(dt.Rows[i]["emailid"].ToString());
                        mail.Subject = "Godrej Partner Connect - FAQ";
                        mail.IsBodyHtml = true;
                        string path = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                        StreamReader sr = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Mailer/faq.html"));
                        mail.Body = sr.ReadToEnd().Replace("user_name", dt.Rows[i]["Name"].ToString()).Replace("user_code", dt.Rows[i]["sap_vendor_code"].ToString());
                        SmtpServer.Port = Convert.ToInt32(25);
                        SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                        SmtpServer.UseDefaultCredentials = false;
                        SmtpServer.Credentials = new System.Net.NetworkCredential("support_it@godrejproperties.com", "Welcome@2016");
                        SmtpServer.EnableSsl = true;
                        SmtpServer.Send(mail);
                        sr.Close();


                        cmdd = new SqlCommand("SP_UpdateFaq", conn);
                        cmdd.Connection = conn;
                        cmdd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmdd.Parameters.AddWithValue("@id", dt.Rows[i]["id"].ToString());
                        j = cmdd.ExecuteNonQuery();
                        if (j > 1)
                        {
                            j += 1;
                        }

                    }
                    Createmaillog("Faq", "success", Convert.ToString(j));
                }
            }
            catch (Exception ex)
            {
                Createmaillog("Faq", "fail", Convert.ToString(ex));
            }
            finally
            {
                conn.Close();
            }
            #endregion
        }

        public void ExpertMail()
        {
            #region code to send Expert Emails
            int j = 0;
            SqlConnection conn;
            SqlCommand cmd = new SqlCommand();
            SqlCommand cmdd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            conn = new SqlConnection(constring);
            try
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                cmd = new SqlCommand("SP_ExpertMail", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                da.SelectCommand = cmd;
                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        MailMessage mail = new MailMessage();
                        SmtpClient SmtpServer = new SmtpClient("smtp.office365.com");
                        mail.From = new MailAddress("marketing@godrejproperties.com");
                        mail.To.Add(dt.Rows[i]["emailid"].ToString());
                        mail.Subject = "Godrej Partner Connect - GPL Expert";
                        mail.IsBodyHtml = true;
                        string path = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                        StreamReader sr = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Mailer/expert.html"));
                        mail.Body = sr.ReadToEnd().Replace("user_name", dt.Rows[i]["Name"].ToString()).Replace("user_code", dt.Rows[i]["sap_vendor_code"].ToString());
                        SmtpServer.Port = Convert.ToInt32(25);
                        SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                        SmtpServer.UseDefaultCredentials = false;
                        SmtpServer.Credentials = new System.Net.NetworkCredential("support_it@godrejproperties.com", "Welcome@2016");
                        SmtpServer.EnableSsl = true;
                        SmtpServer.Send(mail);
                        sr.Close();

                        cmdd = new SqlCommand("SP_UpdateExpert", conn);
                        cmdd.Connection = conn;
                        cmdd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmdd.Parameters.AddWithValue("@id", dt.Rows[i]["id"].ToString());
                        j = cmdd.ExecuteNonQuery();
                        if (j > 1)
                        {
                            j += 1;
                        }
                    }
                    Createmaillog("Expert", "success", Convert.ToString(j));
                }
            }
            catch (Exception ex)
            {
                Createmaillog("Expert", "fail", Convert.ToString(ex));
            }
            finally
            {
                conn.Close();
            }
            #endregion
        }

        public void LeadMail()
        {
            #region code to send lead Emails
            int j = 0;
            SqlConnection conn;
            SqlCommand cmd = new SqlCommand();
            SqlCommand cmdd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            conn = new SqlConnection(constring);
            try
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                cmd = new SqlCommand("SP_LeadMail", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                da.SelectCommand = cmd;
                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        MailMessage mail = new MailMessage();
                        SmtpClient SmtpServer = new SmtpClient("smtp.office365.com");
                        mail.From = new MailAddress("marketing@godrejproperties.com");
                        mail.To.Add(dt.Rows[i]["email"].ToString());
                        mail.Subject = "Godrej Partner Connect - Lead Details";
                        mail.IsBodyHtml = true;
                        string path = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                        StreamReader sr = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Mailer/lead.html"));
                        mail.Body = sr.ReadToEnd().Replace("partner_name", dt.Rows[i]["Name"].ToString()).Replace("user_name", dt.Rows[i]["en_name"].ToString()).Replace("user_email", dt.Rows[i]["en_email"].ToString()).Replace("user_mob", dt.Rows[i]["mobile"].ToString()).Replace("user_city", dt.Rows[i]["city"].ToString()).Replace("user_state", dt.Rows[i]["state"].ToString()).Replace("user_country", dt.Rows[i]["country"].ToString()).Replace("user_project", dt.Rows[i]["title"].ToString()).Replace("user_apartment", dt.Rows[i]["choiceOfResidence"].ToString());
                        SmtpServer.Port = Convert.ToInt32(25);
                        SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                        SmtpServer.UseDefaultCredentials = false;
                        SmtpServer.Credentials = new System.Net.NetworkCredential("support_it@godrejproperties.com", "Welcome@2016");
                        SmtpServer.EnableSsl = true;
                        SmtpServer.Send(mail);
                        sr.Close();

                        cmdd = new SqlCommand("SP_UpdateLead", conn);
                        cmdd.Connection = conn;
                        cmdd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmdd.Parameters.AddWithValue("@id", dt.Rows[i]["enquiryid"].ToString());
                        j = cmdd.ExecuteNonQuery();
                        if (j > 1)
                        {
                            j += 1;
                        }

                    }
                    Createmaillog("Lead", "success", Convert.ToString(j));
                }
            }
            catch (Exception ex)
            {
                Createmaillog("Lead", "fail", Convert.ToString(ex));
            }
            finally
            {
                conn.Close();
            }
            #endregion
        }

        public void Createmaillog(string type, string msg, string rows)
        {
            #region code to update mail log

            SqlConnection conn;
            SqlCommand comm;
            conn = new SqlConnection(constring);
            if (conn.State != ConnectionState.Open)
                conn.Open();
            comm = new SqlCommand("SP_MailLog", conn);
            comm.Connection = conn;
            comm.CommandType = System.Data.CommandType.StoredProcedure;
            comm.Parameters.AddWithValue("@type", type);
            comm.Parameters.AddWithValue("@errormsg", msg);
            comm.Parameters.AddWithValue("@rows", rows);
            int i = comm.ExecuteNonQuery();
            conn.Close();
            #endregion
        }
    }
}