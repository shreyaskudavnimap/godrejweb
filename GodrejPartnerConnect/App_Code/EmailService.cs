﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Web;

/// <summary>
/// Summary description for EmailService
/// </summary>
public class EmailService
{
    public class ServiceProperties
    {
        List<EmailProperties> ToEmail { get; set; }
        List<EmailProperties> CcEmail { get; set; }
        List<EmailProperties> BccEmail { get; set; }
        public string MailSubject { get; set; }
        public string MailBody { get; set; }
        public List<string> MailAttachPath { get; set; }
        public int MailLogID { get; set; }
        string SMTP_CLIENT { get; set; }
        public void AddToEmail(string address, string name)
        {
            ToEmail.Add(new EmailProperties(address, name));
        }
        public void AddToEmail(string address)
        {
            ToEmail.Add(new EmailProperties(address));
        }
        public List<EmailProperties> GetToEmail()
        {
            return ToEmail;
        }
        public void AddCcEmail(string address, string name)
        {
            CcEmail.Add(new EmailProperties(address, name));
        }
        public void AddCcEmail(string address)
        {
            CcEmail.Add(new EmailProperties(address));
        }
        public List<EmailProperties> GetCcEmail()
        {
            return CcEmail;
        }
        public void AddBccEmail(string address, string name)
        {
            CcEmail.Add(new EmailProperties(address, name));
        }
        public void AddBccEmail(string address)
        {
            CcEmail.Add(new EmailProperties(address));
        }
        public List<EmailProperties> GetBccEmail()
        {
            return BccEmail;
        }

        public ServiceProperties()
        {
            ToEmail = new List<EmailProperties>();
            CcEmail = new List<EmailProperties>();
            BccEmail = new List<EmailProperties>();
        }
    }
    public class EmailProperties
    {
        public string name { get; set; }
        public string address { get; set; }

        public EmailProperties(string address, string name)
        {
            this.address = address;
            this.name = name;
        }
        public EmailProperties(string address)
        {
            this.address = address;
        }
    }
    public ServiceProperties properties { get; set; }

    public EmailService()
    {
        properties = new ServiceProperties();
        properties.MailAttachPath = new List<string>();
    }
    public void SendMail()
    {
        if (properties.GetToEmail().Count < 1)
            return;
        try
        {
            MailMessage Message = new MailMessage();
            //Message.Bcc.Add(new MailAddress(Common.strBCC));
            string strFromEmail = ConfigurationManager.AppSettings["FromEmail"], strFromName = ConfigurationManager.AppSettings["FromName"];
            Message.From = (new MailAddress(strFromEmail, strFromName));
            Message.Subject = properties.MailSubject;
            Message.Body = properties.MailBody;
            Message.IsBodyHtml = true;


            foreach (string str in properties.MailAttachPath)
            {
                if (str != "")
                {
                    Attachment att = new Attachment(str);
                    Message.Attachments.Add(att);
                }
            }


            List<EmailProperties> ToEmail = properties.GetToEmail();
            foreach (EmailProperties em in ToEmail)
            {
                if (em.address != null && em.address != "")
                {
                    if (em.name != null && em.name != "")
                        Message.To.Add(new MailAddress(em.address, em.name));
                    else
                        Message.To.Add(new MailAddress(em.address));
                }
            }

            List<EmailProperties> CcEmail = properties.GetCcEmail();
            foreach (EmailProperties em in CcEmail)
            {
                if (em.address != null && em.address != "")
                {
                    if (em.name != null && em.name != "")
                        Message.CC.Add(new MailAddress(em.address, em.name));
                    else
                        Message.CC.Add(new MailAddress(em.address));
                }
            }

            List<EmailProperties> BccEmail = properties.GetBccEmail();
            foreach (EmailProperties em in BccEmail)
            {
                if (em.address != null && em.address != "")
                {
                    if (em.name != null && em.name != "")
                        Message.Bcc.Add(new MailAddress(em.address, em.name));
                    else
                        Message.Bcc.Add(new MailAddress(em.address));
                }
            }



            System.Net.Mail.SmtpClient sc = new SmtpClient(ConfigurationManager.AppSettings["SMTP_CLIENT"]);
            if (ConfigurationManager.AppSettings["SMTP_ISCredentials"] == "Y")
            {
                sc.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTP_CLIENTID"], ConfigurationManager.AppSettings["SMTP_CLIENTPASSWORD"]);
                sc.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTP_CLIENTPORT"]);
                sc.EnableSsl = true;
            }

            WLog.AdminWriteLog("From: " + Message.From + " Message.Subject: " + Message.Subject + " strtoEmail: " + properties.GetToEmail().ToArray()[0].address + " Message.Body: " + Message.Body, "EmailService");
            HttpContext ctx = HttpContext.Current;
            Thread t1 = new Thread(delegate ()
            {
                try
                {
                    HttpContext.Current = ctx;
                    sc.Send(Message);
                }
                catch (Exception e)
                {
                    WLog.AdminWriteLog("From: " + Message.From + " Message.Subject: " + Message.Subject + " strtoEmail: " + properties.GetToEmail().ToArray()[0].address + "\n" + e.ToString(), "EmailServicesThreadError");

                }
            });
            t1.Start();
            //sc.Send(Message);
            //wire up the event for when the Async send is completed
            //sc.SendCompleted += new SendCompletedEventHandler(smtpClient_SendCompleted);            
            //sc.SendAsync(Message, sc);
        }
        catch (Exception ex)
        {
            WLog.AdminWriteLog(ex.ToString(), "EmailServiceError");
            return;
        }
        return;
    }

}
