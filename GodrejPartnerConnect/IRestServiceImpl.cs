﻿using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;


namespace RestService
{
    [ServiceContract]
    public interface IRestServiceImpl
    {
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetUser_Android")]
        UserInfo[] FetchAndroidUser();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetUser_Ios")]
        UserInfo[] FetchIosUser();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "UpdateDevice/{userid}/{deviceid}/{platform}")]
        Empanelment[] GetDevice(string userid, string deviceid, string platform);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "ChangePic/{brokerid}")]
        Empanelment[] RemovePic(string brokerid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetEmail/{email}")]
        Empanelment[] FetchEmail(string email);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "ForgotPassword/{email}")]
        Empanelment[] Password(string email);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetPassword/{brokerid}")]
        Empanelment[] FetchPassword(string brokerid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "ChangePassword/{brokerid}/{password}")]
        Empanelment[] UpdatePassword(string brokerid, string password);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetLead/{brokerid}")]
        Lead[] GetLeadDetails(string brokerid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetPurchase/{brokerid}")]
        Purchase[] GetPurchaseDetails(string brokerid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetPurchaseSum/{brokerid}")]
        Purchase[] GetPurchaseTotal(string brokerid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetCommision/{brokerid}")]
        Sales[] GetCommisionDetails(string brokerid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetTotalCommision/{brokerid}")]
        Sales[] GetTotalCommisionDetails(string brokerid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetPerformer")]
        Performer[] GetPerformerDetails();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetDetails/{panno}")]
        Broker[] GetBrokerDetails(string panno);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetEmplDetails/{panno}")]
        Empanelment[] GetEmplInfo(string panno);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "UpdateEmplDetails/{panno}/{brokerid}")]
        Empanelment[] UpdateEmplInfo(string panno,string brokerid);        

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "InsertEmpanelment/{name}/{mobile}/{email}/{panno}/{taxno}/{tinno}/{company}/{region}/{enterpriseid}/{brokerid}")]
        Empanelment[] InsertEmpl(string name, string mobile, string email, string panno, string taxno, string tinno, string company, string region, string enterpriseid, string brokerid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "InsertAppointment/{name}/{mobile}/{email}/{company}/{city}/{project}/{comments}/{brokerid}/{date}")]
        Appointment[] InsertAppt(string name, string mobile, string email, string company, string city, string project, string comments, string brokerid,string date);
        
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "InsertFaq/{question}/{brokerid}/{type}")]
        Faq[] EnterFaq(string question,string brokerid,string type);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetFaq/{type}")]
        Faq[] FetchFaq(string type);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetNews")]
        News[] FetchNews();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetVideoByNews/{id}")]
        News[] FetchVideoByNews(string id);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetNewsDetails/{id}")]
        News[] FetchNewsDetails(string id);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "UpdateReminder/{brokerid}/{eventid}")]
        Event[] InsertReminder(string brokerid,string eventid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetEventsReminder/{brokerid}")]
        Event[] FetchEventsReminder(string brokerid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetEventsReminderDtae/{brokerid}/{date}")]
        Event[] FetchEventsReminderDate(string brokerid, string date);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetEvents")]
        Event[] FetchEvents();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetAttend/{broker}/{id}")]
        Event[] Attend(string broker, string id);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetEventsDetails/{id}")]
        Event[] FetchEventsDetails(string id);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "UpdateEvents/{id}/{attend}")]
        Event[] UpdateEventsDetails(string id,string attend);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "DisplayOffers")]
        Offer[] GetOffers();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "DisplayOfferdetails/{offerid}")]
        Offer[] GetOffersdetails(string offerid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "InsertBroker/{emplid}/{name}/{mobile}/{email}/{panno}/{taxno}/{tinno}/{company}/{enterpriseid}")]
        Broker[] InsertBrokerInfo(string emplid, string name, string mobile, string email, string panno, string taxno, string tinno, string company, string enterpriseid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "FetchUserDetails/{userid}")]
        Empanelment[] GetUserDetails(string userid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "CreateLogin/{emplid}/{pwd}/{email}/{name}")]
        Empanelment[] InsertLogin(string emplid, string pwd, string email,string name);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "AddLead/{firstname}/{lastname}/{email}/{country}/{mobile}/{projectid}/{project}/{description}/{state}/{city}/{apartment}/{userid}/{brokerid}")]
        Enquiry[] InsertLead(string firstname, string lastname, string email, string country, string mobile, string projectid, string project, string description, string state, string city, string apartment, string userid, string brokerid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "DisplayGetProjects/{projecttype}/{projectstatus}")]
        Projects[] GetProjects(string projecttype, string projectstatus);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "DisplayProperty")]
        Property[] GetProperty();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "DisplayPropertyDetails/{propertyid}")]
        Property[] GetPropertyDetails(string propertyid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "DisplayPropertyByCity/{cityid}")]
        Property[] GetPropertyByCity(string cityid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetCities")]
        City[] FetchCities();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "CheckLogin/{username}/{password}")]
        Empanelment[] GetLoginDetails(string username, string password);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "getbannerjson")]
        Banners[] gethomeBanners();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "FetchProjectCount/{projecttype}")]
        ProjectsCount[] GetProjectCount(string projecttype);

        [OperationContract]
        [WebInvoke(Method = "GET",
          ResponseFormat = WebMessageFormat.Json,
          BodyStyle = WebMessageBodyStyle.Wrapped,
          UriTemplate = "AddUser/{username}/{useremail}/{usermobile}/{google_fbid}/{logintype}")]
        UserInfo[] InsertUserInfo(string username, string useremail, string usermobile, string google_fbid, string logintype);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "FetchUsers/{brokerid}")]
        Empanelment[] GetUserInfo(string brokerid);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "FetchInnerDetails/{menuid}/{projectid}")]
        ProjectsInnerDetails[] GetProjectsInnerDetails(string menuid, string projectid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "FetchSearchPropertytitle/{projecttitle}")]
        Projects[] GetSearchProperty_title(string projecttitle);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "FetchSearchPropertyCity/{projectCity}")]
        Projects[] GetSearchProperty_City(string projectCity);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "AddMessages/{brokerid}/{msgtype}/{message}/{country}")]
        Messages[] InsertMessages(string brokerid, string msgtype, string message, string country);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "DisplayGetRooms")]
        Rooms[] GetRooms();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "DisplayGetProjectVSRoom")]
        ProjectVSRoom[] GetProjectVSRoom();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "AddEnquiry/{firstname}/{lastname}/{email}/{country}/{mobile}/{projectid}/{description}/{state}/{city}/{userid}/{apartment}")]
        Enquiry[] InsertEnquiry(string firstname, string lastname, string email, string country, string mobile, string projectid, string description, string state, string city, string userid, string apartment);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "DisplayGetCredentials/{Username}/{Password}")]

        UserInfo[] GetCredentials(string Username, string Password);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "DisplayGetProjectsIds/{ids}")]
        Projects[] GetProjectsIds(string ids);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetGallery/{projectid}")]
        Gallery[] FetchGallery(string projectid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "DisplayApptDetails/{userid}/{month}")]
        Appointment[] GetApptDetails(string userid, string month);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetVideos/{projectid}/{userid}")]
        Videos[] FetchVideos(string projectid, string userid);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "Display_AddvanceSearch/{projectid}/{projectRoom}")]
        Projects[] GetProperty_AddvanceSearch(string projectid, string projectRoom);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "DisplayGetAutocomplete/{projecttitle}")]
        Projects[] GetAutocomplete(string projecttitle);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "Fetchlatlongitude/{projectId}")]
        Project_lati_longi[] Getlatlongitude(string projectId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "SubmitUserFavourite/{projectid}/{userid}/{flag}")]
        UserFavourites[] InsertUserFavourite(string projectid, string userid, string flag);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetUserFavourite/{projectid}/{userid}")]
        UserFavourites[] FetchUserFavourite(string projectid, string userid);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "SubmitRecentSearchinfo/{projectid}/{userid}")]
        RecentSearch[] InsertRecentSearchinfo(string projectid, string userid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetRecentSearchinfo/{userid}")]
        RecentSearch[] FetchRecentSearchinfo(string userid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "SubmitEmiInfo/{userid}/{projectid}/{EmiAmount}/{EmiIntrest}/{NoofYears}/{EmiPM}")]
        EmiEnquire[] InsertEmiInfo(string userid, string projectid, string EmiAmount, string EmiIntrest, string NoofYears, string EmiPM);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetProjectFetures/{projectid}")]
        ProjectHighlights[] FetchProjectFetures(string projectid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetProjectFloorPlans/{projectid}")]
        FloorPlans[] FetchProjectFloorPlans(string projectid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "Submitsubscription/{Subscription}/{userid}")]
        getEmailSubscription[] Insertsubscription(string Subscription, string userid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "Getpushnotifications/{userid}")]
        Getpushnotifications[] Fetchpushnotifications(string userid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "Submitpushnotifications/{userid}/{status}/{message}")]
        Getpushnotifications[] Insertpushnotifications(string userid, string status, string message);

         [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "DisplayProjectMenu/{projectid}")]
        ProjectsMenu[] GetProjectMenu(string projectid);

         [OperationContract]
         [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
          UriTemplate = "GetProjectName/{projectid}")]
         ProjectsMenu[] FetchProjectName(string projectid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetProjImage/{projectid}")]
         ProjectsMenu[] FetchProjImage(string projectid);

        [OperationContract]
         [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "DisplayProjectinnerFloorPlans/{projectid}/{planid}")]
         FloorPlans[] FetchProjectinnerFloorPlans(string projectid, string planid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "DisplayProjectApartment/{projectid}")]
        Projects[] GetProjectApartment(string projectid);
                
         [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "DisplayAllProjects")]
        Projects[] GetAllProjects();

         [OperationContract]
         [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "ChangeUserInfo/{userid}/{usermobile}/{name}")]
         UserInfo[] updatetUserInfo(string userid, string usermobile, string name);

         [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "FetchProperty_ByCityId/{cityid}/{projecttype}/{projectstatus}")]
         Projects[] GetProperty_ByCityId(string cityid,string projecttype,string projectstatus);


         [OperationContract]
         [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
          UriTemplate = "DisplayProperty_ByRoomID/{roomid}/{projecttype}/{projectstatus}/{userid}")]
         Projects[] GetProperty_ByRoomID(string roomid,string projecttype,string projectstatus,string userid);
       
          [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "GetUserFavourite_userid/{userid}")]
         UserFavourites[] FetchUserFavourite_userid(string userid);

          [OperationContract]
          [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
           UriTemplate = "changeUserInfo_myaccount/{userid}/{type}/{typevalue}")]
          UserInfo[] updatetUserInfo_myaccount(string userid, string type, string typevalue);


          [OperationContract]
          [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
           UriTemplate = "GetEmailSubcription/{userid}")]
          getEmailSubscription[] FetchEmailSubcription(string userid);

         [OperationContract]
          [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
           UriTemplate = "DisplayAllProject_lati_longi")]
          Project_lati_longi[] GetAllProject_lati_longi();
      

          [OperationContract]
         [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
          UriTemplate = "DisplayApartment/{cityid}")]
         Projects[] Get_apartment(string cityid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "test")]
        void Redirect();

        


        [OperationContract]
         [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
          UriTemplate = "DisplayProject_apartment/{roomid}/{cityid}")]
         Projects[] Get_Project_apartment(string roomid,string cityid);



           
         
    }

    #region To add DataContact.


     [DataContract]
    public class Purchase
    {
        [DataMember]
        public string po_no { get; set; }
        [DataMember]
        public string date { get; set; }
        [DataMember]
        public string amount { get; set; }
        [DataMember]
        public string status { get; set; }
    }

    [DataContract]
    public class Lead
    {
        [DataMember]
        public string po_no { get; set; }
        [DataMember]
        public string project { get; set; }
        [DataMember]
        public string leadstatus { get; set; }
        [DataMember]
        public string status { get; set; }
    }

    [DataContract]
    public class Sales
    {
        [DataMember]
        public string Vendor_code { get; set; }
        [DataMember]
        public string brokerage { get; set; }
        [DataMember]
        public string project { get; set; }
        [DataMember]
        public string date { get; set; }
        [DataMember]
        public string sum { get; set; }
        [DataMember]
        public string status { get; set; }
    }

    [DataContract]
    public class Performer
    {
        [DataMember]
        public string Vendor_code { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string project { get; set; }
        [DataMember]
        public string org_Name { get; set; }
        [DataMember]
        public string sales_no { get; set; }
        [DataMember]
        public string status { get; set; }
    }

    [DataContract]
    public class Broker
    {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public string mobile { get; set; }
        [DataMember]
        public string panno { get; set; }
        [DataMember]
        public string service_tax_no { get; set; }
        [DataMember]
        public string tinno { get; set; }
        [DataMember]
        public string company { get; set; }
        [DataMember]
        public string enterprise_membership_id { get; set; }
        [DataMember]
        public string empanelmentid { get; set; }
        [DataMember]
        public string brokerid { get; set; }
        [DataMember]
        public string status { get; set; }
    }

    [DataContract]
    public class Faq
    {
        [DataMember]
        public string question { get; set; }
        [DataMember]
        public string brokerid { get; set; }
        [DataMember]
        public string answer { get; set; }
        [DataMember]
        public string status { get; set; }
    }

    [DataContract]
    public class Project_lati_longi
    {
        [DataMember]
        public string longitude { get; set; }
        [DataMember]
        public string latitude { get; set; }
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public string projectid { get; set; }
        [DataMember]
        public string title { get; set; }
    }


    [DataContract]
    public class ProjectsCount
    {
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public string projectstatus { get; set; }
        [DataMember]
        public string current_count { get; set; }
         [DataMember]
        public string complete_count { get; set; }
         [DataMember]
        public string upcoming_count { get; set; }
    }

    [DataContract]
    public class Property
    {
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public string propertyid { get; set; }
        [DataMember]
        public string title { get; set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public string image { get; set; }
    }

    [DataContract]
    public class Offer
    {
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public string image { get; set; }
        [DataMember]
        public string title { get; set; }
        [DataMember]
        public string offerid { get; set; }
    }

    [DataContract]
    public class Getpushnotifications
    {
        [DataMember]
        public string userid { get; set; }
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public string createddate { get; set; }
        [DataMember]
        public string status { get; set; }
    }

    [DataContract]
    public class getEmailSubscription
    {
        [DataMember]
        public string userid { get; set; }
        [DataMember]
        public string Subscription { get; set; }
        [DataMember]
        public string createddate { get; set; }
        [DataMember]
        public string status { get; set; }
    }

    [DataContract]
    public class Appointment
    {
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string mobileno { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public string company { get; set; }
        [DataMember]
        public string city { get; set; }
        [DataMember]
        public string project { get; set; }
        [DataMember]
        public string appointmentDate { get; set; }
        [DataMember]
        public string comments { get; set; }
        [DataMember]
        public string approve { get; set; }
        [DataMember]
        public string brokerid { get; set; }
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public string length { get; set; }

    }

    [DataContract]
    public class Empanelment
    {
        [DataMember]
        public string userid { get; set; }
        [DataMember]
        public string password { get; set; }
        [DataMember]
        public string brokerid { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string mobileno { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public string panno { get; set; }
        [DataMember]
        public string company { get; set; }
        [DataMember]
        public string region { get; set; }
        [DataMember]
        public string image { get; set; }
        [DataMember]
        public string imagename { get; set; }
        [DataMember]
        public string status { get; set; }

    }

    [DataContract]
    public class FloorPlans
    {
        [DataMember]
        public string cityname { get; set; }
        [DataMember]
        public string projecttitle { get; set; }
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string projectid { get; set; }
        [DataMember]
        public string menuid { get; set; }
        [DataMember]
        public string title { get; set; }
        [DataMember]
        public string image { get; set; }
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public string createddate { get; set; }

    }

    [DataContract]
    public class ProjectHighlights
    {
        [DataMember]
        public string projectid { get; set; }
        [DataMember]
        public string menuid { get; set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public string icon { get; set; }
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public string createddate { get; set; }

    }

    [DataContract]
    public class EmiEnquire
    {
        [DataMember]
        public string userid { get; set; }
        [DataMember]
        public string projectid { get; set; }
        [DataMember]
        public string EmiAmount { get; set; }
        [DataMember]
        public string EmiIntrest { get; set; }
        [DataMember]
        public string NoofYears { get; set; }
        [DataMember]
        public string EmiPM { get; set; }
        [DataMember]
        public string status { get; set; }

        [DataMember]
        public string username { get; set; }
        [DataMember]
        public string email { get; set; }
    }

    [DataContract]
    public class ProjectsMenu
    {
        [DataMember]
        public string projecttitle { get; set; } 
        [DataMember]
        public string cityname { get; set; }
        [DataMember]
        public string projectid { get; set; }
        [DataMember]
        public string menuid { get; set; }
        [DataMember]
        public string menutitle { get; set; }
        [DataMember]
        public string icon { get; set; }
        [DataMember]
        public string image { get; set; }
        [DataMember]
        public string status { get; set; }
    }
    public class RecentSearch
    {
        [DataMember]
        public string title { get; set; }
        [DataMember]
        public string image { get; set; }
        [DataMember]
        public string cityname { get; set; }


        [DataMember]
        public string projectid { get; set; }
        [DataMember]
        public string createddate { get; set; }
        [DataMember]
        public string userid { get; set; }

        [DataMember]
        public string status { get; set; }

    }

    [DataContract]
    public class UserFavourites
    {
        [DataMember]
        public string title { get; set; }
        [DataMember]
        public string cityname { get; set; }
        [DataMember]
        public string favorite { get; set; }

        [DataMember]
        public string projectid { get; set; }
        [DataMember]
        public string createddate { get; set; }
        [DataMember]
        public string userid { get; set; }

        [DataMember]
        public string status { get; set; }

    }

    [DataContract]
    public class Banners
    {
        [DataMember]
        public string cityname { get; set; }

        [DataMember]
        public string status { get; set; }
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string imagename { get; set; }
        [DataMember]
        public string createddate { get; set; }
        [DataMember]
        public string relatedLink { get; set; }
        [DataMember]
        public string title { get; set; }
        [DataMember]
        public string caption { get; set; }
    }

    [DataContract]
    public class City
    {
        [DataMember]
        public string cityid { get; set; }
        [DataMember]
        public string cityname { get; set; }
        [DataMember]
        public string status { get; set; }
    }

    [DataContract]
    public class UserInfo
    {
        [DataMember]
        public string name { get; set; }

        [DataMember]
        public string userid { get; set; }

        [DataMember]
        public string address { get; set; }

        [DataMember]
        public string username { get; set; }
        [DataMember]
        public string useremail { get; set; }
        [DataMember]
        public string usermobile { get; set; }
        [DataMember]
        public string usercity { get; set; }
        [DataMember]
        public string createdon { get; set; }
        [DataMember]
        public string google_fbid { get; set; }
        [DataMember]
        public string userpic { get; set; }
        [DataMember]
        public string profiledesc { get; set; }
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public string usertype { get; set; }
        [DataMember]
        public string qbid { get; set; }
        [DataMember]
        public string loginname { get; set; }
        [DataMember]
        public string logintype { get; set; }
        [DataMember]
        public string deviceid { get; set; }
        [DataMember]
        public string password { get; set; }


    }

    [DataContract]
    public class ProjectsInnerDetails
    {
        [DataMember]
        public string cityname { get; set; }
        [DataMember]
        public string favourite { get; set; }
        [DataMember]
        public string projectid { get; set; }

        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string menutitle { get; set; }


        [DataMember]
        public string innermenuid { get; set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public string image { get; set; }
        [DataMember]
        public string projecttitle { get; set; }
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public string createdby { get; set; }
        public string createddate { get; set; }
        [DataMember]
        public string innermenupriority { get; set; }

    }

    [DataContract]
    public class Event
    {
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string title { get; set; }
        [DataMember]
        public string date { get; set; }
        [DataMember]
        public string place { get; set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public string attend { get; set; }
        [DataMember]
        public string image { get; set; }
        [DataMember]
        public string type { get; set; }
        [DataMember]
        public string brokerid { get; set; }
        [DataMember]
        public string status { get; set; }
    }

    [DataContract]
    public class Projects
    {
        [DataMember]
        public string projectid { get; set; }
        [DataMember]
        public string title { get; set; }
        [DataMember]
        public string cityname { get; set; }
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public string apartment { get; set; }
    }

    [DataContract]
    public class Messages
    {
        [DataMember]
        public string userid { get; set; }
        [DataMember]
        public string projectid { get; set; }
        [DataMember]
        public string msgtype { get; set; }
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public string mobile { get; set; }
        [DataMember]
        public string audiofilename { get; set; }
        [DataMember]
        public string createdon { get; set; }
        [DataMember]
        public string status { get; set; }


    }

    [DataContract]
    public class Rooms
    {
        [DataMember]
        public string Roomid { get; set; }
        [DataMember]
        public string Roomtype { get; set; }
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public string Createddate { get; set; }

    }

    [DataContract]
    public class ProjectVSRoom
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Roomtype { get; set; }
        [DataMember]
        public string Createddate { get; set; }
        [DataMember]
        public string Status { get; set; }


    }

    [DataContract]
    public class Enquiry
    {
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public string url { get; set; }
        [DataMember]
        public string salutation { get; set; }
        [DataMember]
        public string firstname { get; set; }
        [DataMember]
        public string lastname { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public string country { get; set; }
        [DataMember]
        public string mobile { get; set; }
        [DataMember]
        public string projectid { get; set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public string enquirydate { get; set; }
        [DataMember]
        public string bookvisit { get; set; }
        [DataMember]
        public string dateOfvisit { get; set; }
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public string apartment { get; set; }
    }

    [DataContract]
    public class Gallery
    {
        [DataMember]
        public string GalleryID { get; set; }
        [DataMember]
        public string Image { get; set; }
        [DataMember]
        public string projectid { get; set; }
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public string createdby { get; set; }
        [DataMember]
        public string createdate { get; set; }
    }


    [DataContract]
    public class Videos
    {
        [DataMember]
        public string favourite { get; set; }

        [DataMember]
        public string VideoId { get; set; }
        [DataMember]
        public string videoUrl { get; set; }
        [DataMember]
        public string projectId { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string Createddate { get; set; }
        [DataMember]
        public string VideoName { get; set; }
    }

    [DataContract]
    public class News
    {
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string title { get; set; }
        [DataMember]
        public string video { get; set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public string image { get; set; }
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public string date { get; set; }
    }
}
    #endregion