﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Diagnostics;
using System.Media;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Text.RegularExpressions;
using System.Data;
using System.Net;
public partial class uploadimage : System.Web.UI.Page
{
    //pixelDataContext dataContext = new pixelDataContext();
    SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["godrejCPConnectionString"].ToString());
    Guid random = Guid.NewGuid();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            string filename = "";
            string finalfile = "";
            string Filepath = "";
            string status = "";
            #region read posted parameters
            //if (!string.IsNullOrEmpty(Request.Form["filename"]) && !string.IsNullOrEmpty(Request.Form["type"]))
            //{
            if (Request.Form["type"] == "front")
            {
                finalfile = Convert.ToString(Request.Form["filename"]);
                //Filepath = Server.MapPath("images/ProfilePic/") + finalfile;
                Filepath = System.Web.Hosting.HostingEnvironment.MapPath("~/images/ProfilePic/") + finalfile;
                HttpFileCollection MyFileCollection = Request.Files;
                //Response.Write("type came: " + Request.Form["type"]);
                //Response.Write(MyFileCollection.Count);
                //Response.End();
                if (MyFileCollection.Count > 0)
                {
                    // Save the File
                    MyFileCollection[0].SaveAs(Filepath);
                    //Response.End();

                    if (conn.State != ConnectionState.Open)
                        conn.Open();

                    SqlCommand comm = new SqlCommand("SP_UpdateEmpl", conn);
                    comm.Connection = conn;
                    comm.CommandType = System.Data.CommandType.StoredProcedure;
                    comm.Parameters.AddWithValue("@brokerid", Request.Form["brokerid"]);
                    comm.Parameters.AddWithValue("@profilepic", finalfile);
                    SqlParameter pmout = new SqlParameter();
                    pmout.ParameterName = "@outStatus";
                    pmout.DbType = DbType.String;
                    pmout.Size = 1024;
                    pmout.Direction = ParameterDirection.Output;
                    comm.Parameters.Add(pmout);
                    Int32 sqlRows = comm.ExecuteNonQuery();
                    if (sqlRows != 0)
                    {
                        status = comm.Parameters["@outStatus"].Value.ToString();
                    }
                    else
                    {
                        status = "fail";
                    }

                    Response.Write("http://gplpartnerconnect.com/GodrejPartnerConnect/images/ProfilePic/" + finalfile);
                    Response.End();
                }
            }
        //}
           #endregion
        }
        catch (Exception ex)
        {
            //Response.Write("fail," + ex.Message.ToString());
            Response.End();
        }
    }
}
