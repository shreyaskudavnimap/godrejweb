﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;

public partial class upload_Docs : System.Web.UI.Page
{
    //pixelDataContext dataContext = new pixelDataContext();
    SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["godrejCPConnectionString"].ToString());
    Guid random = Guid.NewGuid();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string finalfile = "";
            string Filepath = "";
            #region read posted parameters
            //if (!string.IsNullOrEmpty(Request.Form["filename"]) && !string.IsNullOrEmpty(Request.Form["type"]))
            //{
            if (Request.Form["type"] != "front")
            {
                finalfile = Convert.ToString(Request.Form["filename"]);
                WLog.AdminWriteLog("filename=" + finalfile, "upload_Docs");
                //Filepath = Server.MapPath("images/ProfilePic/") + finalfile;
                Filepath = ConfigurationManager.AppSettings["BasePath"] + finalfile;//System.Web.Hosting.HostingEnvironment.MapPath("~/images/EmplDocs/") + finalfile;
                HttpFileCollection MyFileCollection = Request.Files;
                //Response.Write("type came: " + Request.Form["type"]);
                //Response.Write(MyFileCollection.Count);
                //Response.End();
                if (MyFileCollection.Count > 0)
                {
                    // Save the File
                    MyFileCollection[0].SaveAs(Filepath);

                    Response.Write("http://gplpartnerconnect.com/GodrejPartnerConnect/images/EmplDocs/" + finalfile);
                    Response.End();
                }
            }
            else
            {
                Response.Write("BasePath" + Filepath);
                Response.Write("Path " + System.Web.Hosting.HostingEnvironment.MapPath("~/images/EmplDocs/"));
            }
            //}
            #endregion
        }
        catch (Exception)
        {
            //Response.Write("fail," + ex.Message.ToString());
            //Response.End();
        }
    }
}
