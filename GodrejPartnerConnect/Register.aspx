﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Register" %>


<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Admin/MenuControl.ascx" TagName="Menu" TagPrefix="UC1" %>
<!DOCTYPE HTML>
<html class="no-js">
<head>
    <!-- Basic Page Needs
  ================================================== -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Godrej GPL</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.js"></script>
    <style type="text/css">
        .wrapper {
            width: 600px;
            margin: 0 auto;
            padding: 10px;
            border: 1px solid #000;
            text-align: center;
            position: relative;
        }

        .head {
            padding: 2% 3% 1%;
            display: inline-block;
            width: 94%;
        }

        a.left_logo {
            float: left;
            width: 29%;
            padding-top: 1%;
        }

        a.right_logo {
            float: right;
        }

        .form {
            padding: 3% 0 2% 0;
        }

            .form input[type="text"] {
                border: 1px solid #ccc;
                padding: 5px 9px;
                margin-bottom: 3%;
                font-size: 16px;
                width: 92%;
                font-family: "Helvetica Neue","Arial",sans-serif;
                font-weight: normal;
                line-height: 21px;
            }

        .overlay_wrapper {
            position: fixed;
            height: 100%;
            width: 100%;
            background: rgba(255,255,255,0.8);
            top: 0;
            left: 0;
        }

        .overlay_wrap {
            position: absolute;
            top: 36%;
            width: 60%;
            left: 22%;
            background: #fff;
            border: 1px solid #3295d8;
        }

        a.close_btn {
            position: absolute;
            right: -10px;
            top: -12px;
            background: url(images/close.png) no-repeat;
            width: 28px;
            height: 30px;
            background-size: cover;
        }

        h1 {
            font-family: "Helvetica","Arial",sans-serif;
            font-size: 20px;
            padding: 3% 7%;
            color: #be306b;
            font-weight: normal;
            line-height: 29px;
        }

        input#btnSubmit {
            background: url(images/submit_btn.jpg) no-repeat;
            border: none;
            width: 134px;
            height: 33px;
            outline: none;
        }

        .overlay_wrap img {
            width: 100%;
        }

        .overlay_wrap a img {
            width: 76px;
        }

        @media (max-width: 767px) {
            .wrapper {
                width: 96%;
                padding: 2%;
            }

            .body_content img {
                max-width: 100%;
            }

            h1 {
                font-size: 17px;
                padding: 3% 3%;
                line-height: normal;
            }

            .overlay_wrap {
                width: 75%;
                left: 13%;
            }

            a.right_logo img {
                width: 100%;
            }

            a.right_logo {
                width: 48%;
            }
        }

        @media (min-width: 768px) {
            .wrapper {
                margin-top: 10%;
            }
        }

        @media (min-width: 800px) {
            .wrapper {
                margin-top: 22%;
            }
        }
    </style>
    <!-- Mobile Specific Metas
  ================================================== -->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">

    <!-- Load Webfont loader -->

    <script type="text/javascript">
        $(document).on('click', '.overlay_wrap a', function () {
            $('.overlay_wrapper').hide();
            $('.overlay_wrap').hide();
        });
        function IsValidEmail(stremail, empty) {
            if (stremail == '' && empty)
                return true;
            var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            return filter.test(stremail);
        }

        function CheckValid(chars, validchars) {
            var blnresult = true;
            for (var i = 0; i < chars.length && blnresult == true; i++) {
                var cchar = chars.charAt(i);
                if (validchars.indexOf(cchar) == -1) {
                    blnresult = false;
                }
            }
            return blnresult;
        }
        function exist() {
            $('#error_msg').html('');
            $('#error_msg').append('Your entry already exists.');
            setTimeout(function () {
                $('#dvwrapper').show();
                // document.getElementById('error_msg').text("Please enter name");
                document.getElementById('div_error').style.display = "block";
                return false;
            }, 500);

        }
        function validate() {
            if (document.getElementById('txtName').value == "" || document.getElementById('txtName').value == "Name") {
                //alert("Please enter name");
                $('#error_msg').html('');
                $('#error_msg').append('Please enter name');
                document.getElementById('txtName').focus();
                setTimeout(function () {
                    $('#dvwrapper').show();
                    // document.getElementById('error_msg').text("Please enter name");
                    document.getElementById('div_error').style.display = "block";
                    return false;
                }, 500);
            }
            if (document.getElementById('txtName').value.charAt(0) == " ") {
                $('#error_msg').html('');
                $('#error_msg').append('Please enter valid name');
                $('#dvwrapper').show();
                //alert("Please enter valid name");
                document.getElementById('div_error').style.display = "block";
                document.getElementById('txtName').focus();
                return false;
            }
            if (document.getElementById('txtName').value.length < 3 || document.getElementById('txtName').value.length > 100) {
                $('#error_msg').html('');
                $('#error_msg').append('Name should be minimum 3 and maximum 100 characters');

                document.getElementById('txtName').focus();
                setTimeout(function () {
                    document.getElementById('div_error').style.display = "block";
                    $('#dvwrapper').show();
                    // alert("Name should be minimum 3 and maximum 100 characters");
                }, 1000);
                return false;
            }
            if (!CheckValid(document.getElementById('txtName').value, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.' ")) {
                $('#error_msg').html('');
                $('#error_msg').append('Please enter valid name');
                document.getElementById('div_error').style.display = "block";
                $('#dvwrapper').show();
                // alert('Please enter valid name');
                document.getElementById('txtName').focus();
                return false;
            }
            if (document.getElementById('txtmobile').value == "" || document.getElementById('txtmobile').value == 'MobileNo') {
                $('#error_msg').html('');
                $('#error_msg').append('Please enter Mobile No');
                document.getElementById('div_error').style.display = "block";
                $('#dvwrapper').show();
                // alert("Please enter Mobile No");
                document.getElementById('txtmobile').focus();
                return false;
            }
            if (document.getElementById('txtmobile').value.charAt(0) == " " || document.getElementById('txtmobile').value.charAt(0) == "0") {
                $('#error_msg').html('');
                $('#error_msg').append('Please enter valid Mobile No');
                document.getElementById('div_error').style.display = "block";
                $('#dvwrapper').show();
                //alert("Please enter valid Mobile No");
                document.getElementById('txtmobile').focus();
                return false;
            }
            if (document.getElementById('txtmobile').value.length < 10) {
                $('#error_msg').html('');
                $('#error_msg').append('Mobile Number Should be Minimum 10 Digits');

                document.getElementById('div_error').style.display = "block";
                $('#dvwrapper').show();
                // alert("Mobile Number Should be Minimum 10 Digits");
                document.getElementById('txtmobile').focus();
                return false;
            }
            if (document.getElementById('txtEmail').value == "" || document.getElementById('txtEmail').value == "Email") {
                $('#error_msg').html('');
                $('#error_msg').append('Please enter Email ID');
                document.getElementById('div_error').style.display = "block";
                // alert("Please enter Email ID");
                document.getElementById('txtEmail').focus();
                return false;
            }
            if (document.getElementById('txtEmail').value.length < 3 || document.getElementById('txtEmail').value.length > 50) {
                $('#error_msg').html('');
                $('#error_msg').append('Email ID should be minimum 3 and maximum 50 characters');
                document.getElementById('div_error').style.display = "block";
                $('#dvwrapper').show();
                // alert("Email ID should be minimum 3 and maximum 50 characters");
                document.getElementById('txtEmail').focus();
                return false;
            }
            if (!IsValidEmail(document.getElementById('txtEmail').value)) {
                $('#error_msg').html('');
                $('#error_msg').append('Enter Valid Email ID and Try again');
                document.getElementById('div_error').style.display = "block";
                $('#dvwrapper').show();
                // alert('Enter Valid Email ID and Try again');
                document.getElementById('txtEmail').focus();
                return false;
            }
            if (document.getElementById('txtcompanyname').value == "" || document.getElementById('txtcompanyname').value == "Company") {
                $('#error_msg').html('');
                $('#error_msg').append('Please enter company name');
                document.getElementById('div_error').style.display = "block";
                $('#dvwrapper').show();
                //alert("Please enter company name");
                document.getElementById('txtcompanyname').focus();
                return false;
            }
            if (document.getElementById('txtcompanyname').value.charAt(0) == " ") {
                $('#error_msg').html('');
                $('#error_msg').append('Please enter valid company name');
                document.getElementById('div_error').style.display = "block";
                $('#dvwrapper').show();
                // alert("Please enter valid company name");
                document.getElementById('txtcompanyname').focus();
                return false;
            }
            if (document.getElementById('txtcompanyname').value.length < 3 || document.getElementById('txtcompanyname').value.length > 100) {
                $('#error_msg').html('');
                $('#error_msg').append('Company name should be minimum 3 and maximum 100 characters');
                document.getElementById('div_error').style.display = "block";
                $('#dvwrapper').show();
                // alert("Company name should be minimum 3 and maximum 100 characters");
                document.getElementById('txtcompanyname').focus();
                return false;
            }
        }

        function AvoidSpace() {
            if (event.keyCode == 32) {
                event.returnValue = false;
                return false;
            }
        }
        $(window).load(function () {
            var _originalSize = $(window).width() + $(window).height();
            var input_first_height = $('.form .form_row:first-child input').offset().top;
            $(document).on('click', '.form .form_row:first-child input', function () {
                setTimeout(function () {
                    $('body,html').animate({
                        scrollTop: input_first_height
                    },
                  {
                      duration: 100,
                      easing: "linear",
                      complete: function () {

                      },

                  });
                }, 1000)

            });

        });
        /*$(window).resize(function () {
            if ($(window).width() + $(window).height() != _originalSize) {
                console.log("keyboard show up");
                //$('div[data-role="footer"]').hide();
                $('body,html').animate({
                    scrollTop: input_first_height
                },
                   {
                       duration: 100,
                       easing: "linear",
                       complete: function () {

                       },

                   });
            } else {
                console.log("keyboard closed");
                //$('div[data-role="footer"]').show();
            }
        });*/
    </script>

</head>
<body>

    <body>
        <div class="wrapper">

            <div class="overlay_wrapper" style="display: none;" id="dvwrapper" runat="server"></div>
            <div class="overlay_wrap" style="display: none;" id="div_thank" runat="server">
                <!-- <a href="#" class="close_btn"></a> -->
                <img src="images/Thank-you-pop-up.jpg">
                <a href="#">
                    <img src="images/btn_ok.jpg"></a><br>
                <br>
            </div>

            <div class="overlay_wrap" style="display: none;" id="div_error" runat="server">
                <a href="#" class="close_btn"></a>
                <h1 id="error_msg"></h1>
            </div>


            <div class="head">
                <a href="#" class="left_logo">
                    <img src="images/logo_prelaunch.jpg" style="width: 100%;"></a>
                <a href="#" class="right_logo">
                    <img src="images/logo2.jpg"></a>
            </div>
            <div class="body_content">
                <form id="form1" runat="server">
                    <!-- The loading box -->
                    <input type="hidden" runat="server" id="hidSummary" />
                    <img src="images/banner.jpg" style="">
                    <img src="images/banner_text.jpg" style="">

                    <div class="form">
                        <div class="form_row">
                            <asp:TextBox ID="txtName" runat="server" CssClass="inp-form" placeholder="NAME"></asp:TextBox>
                        </div>
                        <div class="form_row">
                            <asp:TextBox ID="txtmobile" runat="server" CssClass="inp-form" MaxLength="10" onkeypress="AvoidSpace();" placeholder="MOBILE NO."> </asp:TextBox>
                        </div>
                        <div class="form_row">
                            <asp:TextBox ID="txtEmail" runat="server" CssClass="inp-form" placeholder="EMAIL ID"></asp:TextBox>
                        </div>
                        <div class="form_row">

                            <asp:TextBox ID="txtcompanyname" runat="server" CssClass="inp-form" placeholder="COMPANY NAME"></asp:TextBox>
                        </div>
                        <div class="form_row">
                            <asp:Button ID="btnSubmit" runat="server" OnClientClick="return validate();" CssClass="submit" Text="" OnClick="btnSubmit_Click" />
                            <!--<a href="#"><img src="images/submit_btn.jpg"></a>-->
                        </div>
                    </div>

                </form>

            </div>
        </div>

    </body>
</html>
