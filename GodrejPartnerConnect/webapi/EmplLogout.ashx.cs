﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for EmplLogout
    /// </summary>
    public class EmplLogout : IHttpHandler
    {

        EmpanelmentController objcon = new EmpanelmentController();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string res = context.Request.Form["Logout"];
            WLog.WriteLog("logout = " + res, "EmplLogout");
            mEmplLogout logout = Common.JSONDeSerialize<mEmplLogout>(res);
            objcon.EmplLogout(logout);
            context.Response.Write("Hello World");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}