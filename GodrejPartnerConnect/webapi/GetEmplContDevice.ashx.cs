﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for GetEmplContDevice
    /// </summary>
    public class GetEmplContDevice : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string deviceToken = context.Request["DeviceToken"];

            EmplContDeviceController obj = new EmplContDeviceController();
            DataSet ds = obj.GetEmplContDeviceId(deviceToken);
            context.Response.Write(ds.Tables[0].Rows[0][0].ToString());
        }
        
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}