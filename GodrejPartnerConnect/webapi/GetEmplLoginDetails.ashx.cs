﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model;
using GPLPartnerConnect.Model.Empanel;
using GPLPartnerConnect.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for GetEmplLoginDetails
    /// </summary>
    public class GetEmplLoginDetails : IHttpHandler
    {
        EmpanelmentController objcon = new EmpanelmentController();

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string res = context.Request.Form["Login"];
            WLog.WriteLog("login = " + res, "GetEmplLoginDetails");
            mEmplLogin login = Common.JSONDeSerialize<mEmplLogin>(res);
            mCommonOutput o = new mCommonOutput();

            mEmplLoginDetails mobj = objcon.GetEmplLoginDetails(login);
            if (mobj != null)
            {
                o.status = Status.Success.ToString();
                o.statusCode = Convert.ToInt32(Status.Success);
                o.data = mobj;
            }
            else
            {
                o.status = Status.Fail.ToString();
                o.statusCode = Convert.ToInt32(Status.Fail);
            }
            context.Response.Write(Common.JSONSerialize(o));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }


}