﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Controller.Master;
using GPLPartnerConnect.Model;
using GPLPartnerConnect.Model.Enums;
using GPLPartnerConnect.Model.Master;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for ForgotPassword
    /// </summary>
    public class ForgotPassword : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            EmpanelmentController EmpCon = new EmpanelmentController();
            string Email = context.Request["Email"];
            mCommonOutput o = new mCommonOutput();
            DataSet ds = EmpCon.GetEmpanelmentContactDetailsByEmail(Email);
            if(ds.Tables[0].Rows.Count > 0)
            {

                string EmplContID = ds.Tables[0].Rows[0]["EmplContID"].ToString();
                string NewPassword = EmpCon.GenerateRandomString(6);
                EmpCon.UpdatePassword(EmplContID, NewPassword);

                o.status = Status.Success.ToString();
                o.statusCode = Convert.ToInt32(Status.Success);
                o.msg = "Your request has been accepted. You will receive a new password shortly.";


                EmailService es = new EmailService();
                MailTemplateController mailCont = new MailTemplateController();
                mMailTemplate mail = mailCont.GetMailTempleteByCode("FORPASS");
                string email = Email;
                string name = ds.Tables[0].Rows[0]["Name"].ToString();
                es.properties.AddToEmail(email, name);

                es.properties.MailSubject = mail.Subject;
                string Body = mail.Body;
                Body = Body.Replace("#EmpanelmentName#", name);
                Body = Body.Replace("#Password#", NewPassword);
                es.properties.MailBody = Body;
                es.SendMail();
            }
            else
            {
                o.status = Status.Fail.ToString();
                o.statusCode = Convert.ToInt32(Status.Fail);
                o.msg = "Email ID Not found";
            }
            context.Response.Write(Common.JSONSerialize(o));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}