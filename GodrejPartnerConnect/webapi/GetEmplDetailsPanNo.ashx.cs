﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for GetEmplDetailsPanNo
    /// </summary>
    public class GetEmplDetailsPanNo : IHttpHandler
    {
        EmpanelmentController objcon = new EmpanelmentController();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string panno = context.Request["panno"];
            WLog.WriteLog("panno = " + panno, "GetEmplDetailsPanNo");
            string output = Common.JSONSerialize(GetEmplInfo(panno));
            context.Response.Write(output);
        }

        public mEmpanelment GetEmplInfo(string panno)
        {
            mEmpanelment obj = new mEmpanelment();
            DataSet ds = objcon.GetEmpanelmentByPanNo(panno);
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow row = ds.Tables[0].Rows[0];
                obj.EmplID = row["EmplID"].ToString();
                obj.Company_Name = row["Company_Name"].ToString();
                obj.entity_type = row["EntityType"].ToString();
                obj.FirstName = row["FirstName"].ToString();
                obj.LastName = row["LastName"].ToString();
                obj.Mobile = row["Mobile"].ToString();
                obj.Email = row["Email"].ToString();
                obj.Pan_No = row["Pan_No"].ToString();
                obj.pancerti = row["Pan_Certi"].ToString();
                obj.status = row["Status"].ToString();
                obj.Billing_City = row["Billing_City"].ToString();
                obj.Billing_Zip = row["Billing_Zip"].ToString();
                obj.Billing_Street = row["Billing_Street"].ToString();
                obj.Billing_State = row["Billing_State"].ToString();
                obj.Billing_Country = row["Billing_Country"].ToString();
                obj.Registered_City = row["Registered_City"].ToString();
                obj.Registered_Zip = row["Registered_Zip"].ToString();
                obj.Registered_Street = row["Registered_Street"].ToString();
                obj.Registered_State = row["Registered_State"].ToString();
                obj.Registered_Country = row["Registered_Country"].ToString();
                obj.Communication_City = row["Communication_City"].ToString();
                obj.Communication_Zip = row["Communication_Zip"].ToString();
                obj.Communication_Street = row["Communication_Street"].ToString();
                obj.Communication_State = row["Communication_State"].ToString();
                obj.Communication_Country = row["Communication_Country"].ToString();

                foreach (DataRow rowdoc in ds.Tables[1].Rows)
                {
                    mGstDocs doc = new mGstDocs();
                    doc.id = rowdoc["ID"].ToString();
                    doc.emplid = rowdoc["EmplID"].ToString();
                    doc.state = rowdoc["State"].ToString();
                    doc.gstno = rowdoc["Gst_no"].ToString();
                    doc.gstcerti = rowdoc["Gst_Certi"].ToString();
                    doc.status = rowdoc["Status"].ToString();
                    doc.comment = rowdoc["Comment"].ToString();
                    doc.sfdcregisted = rowdoc["SFDCRegisted"].ToString();
                    obj.GstDoc.Add(doc);

                }
                foreach (DataRow rowdoc in ds.Tables[2].Rows)
                {
                    mReraDocs doc = new mReraDocs();
                    doc.id = rowdoc["ID"].ToString();
                    doc.emplid = rowdoc["EmplID"].ToString();
                    doc.state = rowdoc["State"].ToString();
                    doc.rerano = rowdoc["Rera_no"].ToString();
                    doc.reracerti = rowdoc["Rera_Certi"].ToString();
                    doc.status = rowdoc["Status"].ToString();
                    doc.comment = rowdoc["Comment"].ToString();
                    doc.sfdcregisted = rowdoc["SFDCRegisted"].ToString();
                    obj.ReraDoc.Add(doc);
                }

            }
            else
            {
                obj.EmplID = "0";
            }
            return obj;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}