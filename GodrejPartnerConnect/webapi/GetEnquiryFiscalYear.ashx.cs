﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model;
using GPLPartnerConnect.Model.Empanel;
using GPLPartnerConnect.Model.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for GetEnquiryFiscalYear
    /// </summary>
    public class GetEnquiryFiscalYear : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            mCommonOutput o = new mCommonOutput();
            BrokerSFDCDataController objCon = new BrokerSFDCDataController();
            string EmplContID = context.Request["userid"];
            DataSet ds = objCon.GetBrokerEnquiryFiscalYear(EmplContID);
            List<mFiscalYear> list = Common.ToObject<mFiscalYear>(ds.Tables[0]);
            foreach(mFiscalYear fy in list)
            {
                DataSet dsq = objCon.GetBrokerEnquiryQuarter(EmplContID, fy.FiscalYearID);
                fy.Quarters = Common.ToObject<mQuarter>(dsq.Tables[0]);
            }
            o.data = list;
            o.status = Status.Success.ToString();
            o.statusCode = Convert.ToInt32(Status.Success);
            context.Response.Write(Common.JSONSerialize(o));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}