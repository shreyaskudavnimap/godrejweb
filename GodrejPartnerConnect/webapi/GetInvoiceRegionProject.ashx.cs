﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model;
using GPLPartnerConnect.Model.Empanel;
using GPLPartnerConnect.Model.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for GetBookingData
    /// </summary>
    public class GetInvoiceRegionProject : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            mCommonOutput o = new mCommonOutput();
            InvoiceController objCon = new InvoiceController();
            mInvoices obj = new mInvoices();
            string flag = context.Request["flag"];
            obj.EmplId = context.Request["userid"];
            obj.Quarters = context.Request["quarter"];

            if (flag == "region")
            {
                DataSet ds = objCon.GetRegionList(obj);
                List<mRegions> rlist = Common.ToObject<mRegions>(ds.Tables[0]);
                o.data = rlist;
            }

            else if(flag == "project")
            {
                obj.Region = context.Request["region"];
                DataSet ds = objCon.GetProjectList(obj);
                List<mProjects> plist = Common.ToObject<mProjects>(ds.Tables[0]);
                o.data = plist;
            }

            o.status = Status.Success.ToString();
            o.statusCode = Convert.ToInt32(Status.Success);
            context.Response.Write(Common.JSONSerialize(o));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class mRegions
        {
            public string SFDC_Region { get; set; }
        }

        public class mProjects
        {
            public string Project { get; set; }

            public string Location { get; set; }

            public string EmplID { get; set; }

            public string EmplContID { get; set; }

            public string File { get; set; }
        }

        public class mProjectDetails
        {
            public string File { get; set; }
        }
    }
}