﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model;
using GPLPartnerConnect.Model.Empanel;
using GPLPartnerConnect.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for DeactivateDevice
    /// </summary>
    public class DeactivateDevice : IHttpHandler
    {

        EmpanelmentController objcon = new EmpanelmentController();

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string jsonempl = context.Request["Device"];
            WLog.WriteLog("jsonempl=" + jsonempl, "Device Status");
            mEmplContDeviceLog empl = Common.JSONDeSerialize<mEmplContDeviceLog>(jsonempl);
            context.Response.Write(Common.JSONSerialize(SetActiveNo(empl)));
        }

        mCommonOutput SetActiveNo(mEmplContDeviceLog empl)
        {
            mCommonOutput o = new mCommonOutput();
            o.status = Status.Success.ToString();
            o.statusCode = Convert.ToInt32(Status.Success);
            objcon.SetActiveNo(empl);

            return o;

        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}