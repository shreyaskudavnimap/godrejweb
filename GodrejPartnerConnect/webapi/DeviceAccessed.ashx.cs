﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model;
using GPLPartnerConnect.Model.Empanel;
using GPLPartnerConnect.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for LastAccessed
    /// </summary>
    public class DeviceAccessed : IHttpHandler
    {
        EmpanelmentController objcon = new EmpanelmentController();

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string jsonempl = context.Request["Device"];
            //WLog.WriteLog("jsonempl=" + jsonempl, "Last Accessed");
            mEmplContDeviceLog empl = Common.JSONDeSerialize<mEmplContDeviceLog>(jsonempl);
            context.Response.Write(Common.JSONSerialize(UpdateLastAccessed(empl)));
        }

        mCommonOutput UpdateLastAccessed(mEmplContDeviceLog empl)
        {
            mCommonOutput o = new mCommonOutput();
            o.status = Status.Success.ToString();
            o.statusCode = Convert.ToInt32(Status.Success);
            objcon.UpdateLastAccessed(empl);

            return o;

        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}