﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for GetNotificationLog
    /// </summary>
    public class GetNotificationLog : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string EmplContDeviceLogID = context.Request["devicelogid"];
            List<mNotificationLog> list = new List<mNotificationLog>();
            NotificationController objCon = new NotificationController();
            DataSet ds = objCon.GetNotificationLog(EmplContDeviceLogID);
            list = Common.ToObject<mNotificationLog>(ds.Tables[0]);

            context.Response.Write(Common.JSONSerialize(list));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}