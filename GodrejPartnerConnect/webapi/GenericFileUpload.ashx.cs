﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Controller.Master;
using GPLPartnerConnect.Model;
using GPLPartnerConnect.Model.Empanel;
using GPLPartnerConnect.Model.Enums;
using GPLPartnerConnect.Model.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for ChangePassword
    /// </summary>
    public class GenericFileUpload : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            EmpanelmentController objcon = new EmpanelmentController();
            context.Response.ContentType = "text/plain";
            string res = context.Request.Form["Login"];
            res = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(res));
            WLog.WriteLog("login = " + res, "GetEmplLoginDetails");
            mEmplLogin login = Common.JSONDeSerialize<mEmplLogin>(res);
            mCommonOutput o = new mCommonOutput();

            mEmplLoginDetails mobj = objcon.GetEmplLoginDetails(login);
            if (mobj != null)
            {
                o.status = Status.Success.ToString();
                o.statusCode = Convert.ToInt32(Status.Success);
                o.data = mobj;
            }
            else
            {
                o.status = Status.Fail.ToString();
                o.statusCode = Convert.ToInt32(Status.Fail);
            }
            context.Response.Write(Common.JSONSerialize(o));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}