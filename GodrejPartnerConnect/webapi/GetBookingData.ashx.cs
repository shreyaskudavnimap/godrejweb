﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model;
using GPLPartnerConnect.Model.Empanel;
using GPLPartnerConnect.Model.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for GetBookingData
    /// </summary>
    public class GetBookingData : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            mCommonOutput o = new mCommonOutput();
            BrokerSFDCDataController objCon = new BrokerSFDCDataController();
            string EmplContID = context.Request["userid"];
            string quarter = context.Request["quarter"];
            string SFDCProjectID = context.Request["sfdcprojectid"];
            
            WLog.WriteLog("quarter = '" + quarter + "', ProjectID = '"+ SFDCProjectID + "'", "GetBookingData");
            DataSet ds = objCon.GetBrokerBookingData(EmplContID, quarter, SFDCProjectID);
            List<mBrokerBookingMoblie> list = Common.ToObject<mBrokerBookingMoblie>(ds.Tables[0]);
            o.data = list;
            o.status = Status.Success.ToString();
            o.statusCode = Convert.ToInt32(Status.Success);
            context.Response.Write(Common.JSONSerialize(o));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}