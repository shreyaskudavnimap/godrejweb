﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model;
using GPLPartnerConnect.Model.Empanel;
using GPLPartnerConnect.Model.Enums;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for GetEnquiryData
    /// </summary>
    public class GetProfileData : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string module = context.Request["module"];
            switch (module)
            {
                case "base":
                    GetProfileDetails(context);
                    break;

                case "get_all":
                    GetUserProfileDetails(context);
                    break;

                case "send_otp":
                    SendOTP(context);
                    break;

                case "submit_otp":
                    UpdateSubmitContact(context);
                    break;

                case "email":
                    UpdateEmail(context);
                    break;

                case "comm_addr":
                    UpdateCommAddress(context);
                    break;

                case "reg_addr":
                    UpdateRegAddress(context);
                    break;

                case "reragst_login":
                    ValidateLogin(context);
                    break;

                case "rera_submit":
                    UpdateRera(context);
                    break;

                case "gst_submit":
                    UpdateGst(context);
                    break;

                case "get_reragst":
                    GetEmplReraGst(context);
                    break;

                case "getToken":
                    getToken(context);
                    break;
            }

        }

        private void GetEmplReraGst(HttpContext context)
        {
            mCommonOutput o = new mCommonOutput();
            ProfileDataController objCon = new ProfileDataController();
            string EmplContID = context.Request["userid"];
            string doctype = context.Request["doctype"];

            var dataDt = objCon.GetEmplReraGst(EmplContID, doctype);

            if (doctype == "GET_RERA")
            {
                IList<mReraDocs> data = dataDt.Tables[0].AsEnumerable().Select(row => new mReraDocs
                {
                    emplid = row.Field<int>("EmplID").ToString(),
                    state = row.Field<string>("State"),
                    rerano = row.Field<string>("Rera_no"),
                    reracerti = row.Field<string>("Rera_Certi")
                }).ToList();

                o.data = data;
                o.status = Status.Success.ToString();
                o.statusCode = Convert.ToInt32(Status.Success);
                context.Response.Write(Common.JSONSerialize(o));
            }
            if (doctype == "GET_GST")
            {
                IList<mGstDocs> data = dataDt.Tables[0].AsEnumerable().Select(row => new mGstDocs
                {
                    emplid = row.Field<int>("EmplID").ToString(),
                    state = row.Field<string>("State"),
                    gstno = row.Field<string>("Gst_no"),
                    gstcerti = row.Field<string>("Gst_Certi")
                }).ToList();

                o.data = data;
                o.status = Status.Success.ToString();
                o.statusCode = Convert.ToInt32(Status.Success);
                context.Response.Write(Common.JSONSerialize(o));
            }

        }

        private void UpdateRera(HttpContext context)
        {
            mCommonOutput o = new mCommonOutput();
            string jsonData = context.Request["data"];
            mReraUpdate reraData = Common.JSONDeSerialize<mReraUpdate>(jsonData);
            ProfileDataController objCon = new ProfileDataController();
            int count = reraData.list.Where(c => c.insertflag == true).Count();
            string msg;
            if (count != 0)
            {
                objCon.AddRera(reraData);
                msg = "Added";
            }
            else
            {
                msg = "no_insertflag";
            }
            o.data = msg;
            o.status = Status.Success.ToString();
            o.statusCode = Convert.ToInt32(Status.Success);
            context.Response.Write(Common.JSONSerialize(o));
        }

        private void UpdateGst(HttpContext context)
        {
            mCommonOutput o = new mCommonOutput();
            string jsonData = context.Request["data"];
            mGstUpdate gstData = Common.JSONDeSerialize<mGstUpdate>(jsonData);
            ProfileDataController objCon = new ProfileDataController();
            int count = gstData.list.Where(c => c.insertflag == true).Count();
            string msg;
            if (count != 0)
            {
                objCon.AddGst(gstData);
                msg = "Added";
            }
            else
            {
                msg = "no_insertflag";
            }
            o.data = msg;
            o.status = Status.Success.ToString();
            o.statusCode = Convert.ToInt32(Status.Success);
            context.Response.Write(Common.JSONSerialize(o));
        }

        private void ValidateLogin(HttpContext context)
        {
            mCommonOutput o = new mCommonOutput();
            ProfileDataController objCon = new ProfileDataController();
            string EmplContID = context.Request["userid"];
            string input_user = context.Request["input_user"];
            string input_pass = context.Request["input_pass"];
            string msg = objCon.ValidateLogin(EmplContID, input_user, input_pass);
            o.data = msg;
            o.status = Status.Success.ToString();
            o.statusCode = Convert.ToInt32(Status.Success);
            context.Response.Write(Common.JSONSerialize(o));
        }

        private void UpdateRegAddress(HttpContext context)
        {
            mCommonOutput o = new mCommonOutput();
            ProfileDataController objCon = new ProfileDataController();
            string EmplContID = context.Request["userid"];
            string oldRegAddress = context.Request["oldRegAddress"];
            string newRegAddress = context.Request["newRegAddress"];
            string addrProof = context.Request["addrProof"];

            string msg = objCon.UpdateRegAddress(EmplContID, oldRegAddress, newRegAddress, addrProof);
            o.data = msg;

            o.status = Status.Success.ToString();
            o.statusCode = Convert.ToInt32(Status.Success);
            context.Response.Write(Common.JSONSerialize(o));
        }

        private void UpdateCommAddress(HttpContext context)
        {
            mCommonOutput o = new mCommonOutput();
            ProfileDataController objCon = new ProfileDataController();
            string EmplContID = context.Request["userid"];
            string oldCommAddress = context.Request["oldCommAddress"];
            string newCommAddress = context.Request["newCommAddress"];
            string oldCommCity = context.Request["oldCommCity"];
            string newCommCity = context.Request["newCommCity"];
            string oldCommState = context.Request["oldCommState"];
            string newCommState = context.Request["newCommState"];
            string oldCommZipPostal = context.Request["oldCommZipPostal"];
            string newCommZipPostal = context.Request["newCommZipPostal"];
            string input_user = context.Request["input_user"];
            string input_pass = context.Request["input_pass"];

            string msg = objCon.UpdateCommAddress(EmplContID, newCommAddress, newCommCity, newCommState, newCommZipPostal, input_user, input_pass);
            WLog.WriteLog("UpdateCommAddress  : " + EmplContID + "-" + msg, "DELETE_LOG");

            if (msg == "Updated")
            {
                //DataSet ds = objCon.GetProfileData(EmplContID);
                //mProfileData user = Common.ToObject<mProfileData>(ds.Tables[0])[0];

                var data = objCon.GetUserDetailsForAlert(EmplContID);
                mEmpanelmentContact empl = data.Tables[0].AsEnumerable()
                                                    .Select(x => new mEmpanelmentContact()
                                                    {
                                                        Email = x.Field<string>("Email"),
                                                        Name = x.Field<string>("Name")
                                                    }).ToList()[0];
                string comm_template = "<div>Dear " + empl.Name + ",</div> <div>&nbsp;</div> <div>As per your request, your Communication address associated with your account has been changed from " + oldCommAddress + " to " + newCommAddress + " and we will be using " + newCommAddress + " for further communication.</div> <div>&nbsp;</div> <div>This is an auto-generated mail, please do not reply to this message.</div> <div><br />Thanks &amp; Regards<br />Godrej Properties Limited</div>";
                SendMail(empl.Email, comm_template);
            }

            o.data = msg;
            o.status = Status.Success.ToString();
            o.statusCode = Convert.ToInt32(Status.Success);
            context.Response.Write(Common.JSONSerialize(o));
        }

        private void UpdateEmail(HttpContext context)
        {
            mCommonOutput o = new mCommonOutput();
            ProfileDataController objCon = new ProfileDataController();
            string EmplContID = context.Request["userid"];
            string oldEmail = context.Request["oldEmail"];
            string newEmail = context.Request["newEmail"];
            string input_user = context.Request["input_user"];
            string input_pass = context.Request["input_pass"];

            string msg = objCon.UpdateEmail(EmplContID, newEmail, input_user, input_pass);
            if (msg == "Updated")
            {
                var data = objCon.GetUserDetailsForAlert(EmplContID);
                mEmpanelmentContact empl = data.Tables[0].AsEnumerable()
                                                    .Select(x => new mEmpanelmentContact()
                                                    {
                                                        Email = x.Field<string>("Email"),
                                                        Name = x.Field<string>("Name")
                                                    }).ToList()[0];
                string email_template = "<div>Dear " + empl.Name + ",</div> <div>&nbsp;</div> <div>As per your request your email ID associated with your account has been changed from " + oldEmail + " to " + newEmail + ", and all further communicaion will be sent on " + newEmail + "</div> <div>&nbsp;</div> <div>&nbsp;</div> <div>This is an auto-generated mail, please do not reply to this message.</div> <div><br />Thanks &amp; Regards<br />Godrej Properties Limited</div>";
                SendMail(oldEmail, email_template);
            }
            o.data = msg;
            o.status = Status.Success.ToString();
            o.statusCode = Convert.ToInt32(Status.Success);
            context.Response.Write(Common.JSONSerialize(o));
        }

        private void UpdateSubmitContact(HttpContext context)
        {
            mCommonOutput o = new mCommonOutput();
            ProfileDataController objCon = new ProfileDataController();
            string EmplContID = context.Request["userid"];
            string newContact = context.Request["new_contact"];
            string oldContact = context.Request["old_contact"];
            string vertifyOtp = context.Request["txtVerifyOtp"];
            string input_user = context.Request["input_user"];
            string input_pass = context.Request["input_pass"];


            string msg = objCon.UpdateContact(EmplContID, vertifyOtp, newContact, oldContact, input_user, input_pass);
            if (msg == "Updated")
            {
                var data = objCon.GetUserDetailsForAlert(EmplContID);
                mEmpanelmentContact empl = data.Tables[0].AsEnumerable()
                                                    .Select(x => new mEmpanelmentContact()
                                                    {
                                                        Email = x.Field<string>("Email"),
                                                    }).ToList()[0];
                string smsMsg = "As per your request your contact no associated with your account has been changed from " + oldContact + " to " + newContact;
                objCon.SendSms(newContact, smsMsg);
                //objCon.SendMail("", emplObj[0].Email, "", "", "", smsMsg);
                string contactTemplate = "<div>Dear " + empl.Name + ",</div> <div>&nbsp;</div> <div>As per your request your contact no associated with your account has been changed from " + oldContact + " to " + newContact + "</div> <div>&nbsp;</div> <div>This is an auto-generated mail, please do not reply to this message.</div> <div><br />Thanks &amp; Regards<br />Godrej Properties Limited</div>";
                SendMail(empl.Email, smsMsg);
            }

            o.data = msg;
            o.status = Status.Success.ToString();
            o.statusCode = Convert.ToInt32(Status.Success);
            context.Response.Write(Common.JSONSerialize(o));
        }

        private void SendMail(string toEmail, string body)
        {
            EmailService es = new EmailService();
            es.properties.AddToEmail(toEmail, "Godrej Connect App");
            es.properties.MailSubject = "Godrej Partner Connect APP";
            es.properties.MailBody = body;
            es.SendMail();
        }

        private void SendOTP(HttpContext context)
        {
            mCommonOutput o = new mCommonOutput();
            ProfileDataController objCon = new ProfileDataController();
            string EmplContID = context.Request["userid"];
            string newContact = context.Request["new_contact"];
            objCon.SendOTP(EmplContID, newContact);
            o.data = EmplContID;
            o.status = Status.Success.ToString();
            o.statusCode = Convert.ToInt32(Status.Success);
            context.Response.Write(Common.JSONSerialize(o));
        }

        public void GetProfileDetails(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            mCommonOutput o = new mCommonOutput();
            ProfileDataController objCon = new ProfileDataController();
            string EmplContID = context.Request["userid"];
            DataSet ds = objCon.GetProfileData(EmplContID);
            List<mProfileData> list = Common.ToObject<mProfileData>(ds.Tables[0]);
            o.data = list;
            o.status = Status.Success.ToString();
            o.statusCode = Convert.ToInt32(Status.Success);
            context.Response.Write(Common.JSONSerialize(o));
        }

        public void GetUserProfileDetails(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            mCommonOutput o = new mCommonOutput();
            ProfileDataController objCon = new ProfileDataController();
            string EmplContID = context.Request["userid"];
            DataSet ds = objCon.GetUserProfileDetails(EmplContID);
            List<mEmpanelment> list = Common.ToObject<mEmpanelment>(ds.Tables[0]);
            o.data = list;
            o.status = Status.Success.ToString();
            o.statusCode = Convert.ToInt32(Status.Success);
            context.Response.Write(Common.JSONSerialize(o));
        }

        public void getToken(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            mCommonOutput o = new mCommonOutput();
            ProfileDataController objCon = new ProfileDataController();
            using (WebClient client = new WebClient())
            {
                var reqparm = new System.Collections.Specialized.NameValueCollection();
                reqparm.Add("param1", "");
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                Uri myUri = new Uri(ConfigurationManager.AppSettings["SFDCURL"] + ConfigurationManager.AppSettings["GRANTSERVICE"] + "&client_id="
                    + ConfigurationManager.AppSettings["CLIENTID"] + "&client_secret=" + ConfigurationManager.AppSettings["CLIENTSECRET"] + "&username="
                    + ConfigurationManager.AppSettings["USERNAME"] + "&password=" + ConfigurationManager.AppSettings["Password"], UriKind.Absolute);

                //Uri myUri = new Uri("https://godrej.my.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9Y6d_Btp4xp7lt5FL02Cc.bHBCI_vpcrJYvpeBev1Ob5nXpobkxkmhygUekoOvbQMscya0i3r7EacuDWz&client_secret=332639414523432900&username=external.app@godrejproperties.com&password=Nov@12345Svgw3cZob0A7HWlJs8I07Jcdb", UriKind.Absolute);

                byte[] responsebytes = client.UploadValues(myUri, reqparm);

                string responseBody = System.Text.Encoding.UTF8.GetString(responsebytes);
                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                var result = jsSerializer.DeserializeObject(responseBody);
                Dictionary<string, dynamic> obj2 = new Dictionary<string, dynamic>();
                obj2 = (Dictionary<string, dynamic>)(result);
                o.data = obj2["access_token"].ToString(); ;
                o.status = Status.Success.ToString();
                o.statusCode = Convert.ToInt32(Status.Success);
                context.Response.Write(Common.JSONSerialize(o));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}