﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model.Empanel;
using GPLPartnerConnect.Model.FCM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for SendNotification
    /// </summary>
    public class SendNotification : IHttpHandler
    {
        NotificationController objCon = new NotificationController();

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            DataSet ds = objCon.GetNotificationReady();
            string str = objCon.SendNotification(ds.Tables[0], "0");
            context.Response.Write(str + " Notification Send Successfully");
        }
        

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}