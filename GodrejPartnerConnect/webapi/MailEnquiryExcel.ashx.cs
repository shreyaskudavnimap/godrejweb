﻿using ClosedXML.Excel;
using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Controller.Master;
using GPLPartnerConnect.Model;
using GPLPartnerConnect.Model.Enums;
using GPLPartnerConnect.Model.Master;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for MailWalkInExcel
    /// </summary>
    public class MailEnquiryExcel : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            MailTemplateController objMail = new MailTemplateController();
            mCommonOutput o = new mCommonOutput();
            BrokerSFDCDataController objCon = new BrokerSFDCDataController();
            string EmplContID = context.Request["userid"];
            string FiscalYearID = context.Request["fid"];
            DataSet ds = objCon.GetBrokerEnquiryExcel(EmplContID, FiscalYearID);
            string path = context.Server.MapPath("~/data/Excel/");
            if (ds.Tables[0].Rows.Count > 0)
            {
                mMailTemplate mobj = objMail.GetMailTempleteByCode("EXCELWALKIN"); 
                if(mobj != null)
                {
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(ds.Tables[0], "Walkin");
                        path += "Walkin_" + EmplContID + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmssffff") + ".xlsx";
                        wb.SaveAs(path);
                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            DataRow row = ds.Tables[1].Rows[0];
                            string Emailid = row["Email"].ToString();
                            EmailService es = new EmailService();
                            string mailSub = mobj.Subject, mailBody = mobj.Body;
                            mailSub = mailSub.Replace("##FiscalYear##", "FY" + row["FiscalYearName"].ToString());
                            mailBody = mailBody.Replace("##FiscalYear##", "FY" + row["FiscalYearName"].ToString());
                            mailBody = mailBody.Replace("##ToName##", row["Name"].ToString());
                            es.properties.MailSubject = mailSub;
                            es.properties.MailBody = mailBody;
                            es.properties.AddToEmail(Emailid);
                            //es.properties.AddToEmail("jipson.k@pixieitsolutions.com");
                            //es.properties.AddToEmail("partnerconnect@godrejproperties.com");
                            //es.properties.AddToEmail("gshetty@godrejproperties.com");
                            es.properties.AddAttachment(path);
                            es.SendMail();
                        }

                    }
                }

            }
            o.status = Status.Success.ToString();
            o.statusCode = Convert.ToInt32(Status.Success);
            context.Response.Write(Common.JSONSerialize(o));

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}