﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for SaveEmplContDeviceLog
    /// </summary>
    public class SaveEmplContDeviceLog : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            EmpanelmentController cont = new EmpanelmentController();

            try
            {
                string res = context.Request.Form["Device"];
                WLog.WriteLog("Device = " + res, "SaveEmplContDeviceLog");
                mEmplContDeviceLog mobj = Common.JSONDeSerialize<mEmplContDeviceLog>(res);

                WLog.WriteLog("mobj.DeviceToken = " + mobj.DeviceToken, "SaveEmplContDeviceLog");
                WLog.WriteLog("mobj.EmplContDeviceLogID = " + mobj.EmplContDeviceLogID, "SaveEmplContDeviceLog");
                WLog.WriteLog("mobj.EmplContID = " + mobj.EmplContID, "SaveEmplContDeviceLog");
                WLog.WriteLog("mobj.MobilePlatform = " + mobj.MobilePlatform, "SaveEmplContDeviceLog");
                DataSet ds = cont.InsertUpdateEmplContDeviceLog(mobj);
                WLog.WriteLog("ds.Tables[0].Rows.Count " + ds.Tables[0].Rows.Count, "SaveEmplContDeviceLog");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    mobj.EmplContDeviceLogID = ds.Tables[0].Rows[0]["EmplContDeviceLogID"].ToString();
                }
                context.Response.Write(Common.JSONSerialize(mobj));

            }
            catch (Exception ex)
            {
                WLog.WriteLog("error:" + ex.ToString(), "SaveEmplContDeviceLog");
                context.Response.Write(null);

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}