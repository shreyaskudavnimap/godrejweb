﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model;
using GPLPartnerConnect.Model.Empanel;
using GPLPartnerConnect.Model.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for GetEnquiryData
    /// </summary>
    public class GetEnquirySummary : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            BrokerSFDCDataController objCon = new BrokerSFDCDataController();
            string EmplContID = context.Request["userid"];
            string quarter = context.Request["quarter"];
            WLog.WriteLog("quarter = '" + quarter + "'", "GetEnquiryData");
            DataSet ds = objCon.GetBrokerEnquirySummary(EmplContID, quarter);
            List<mBrokerEnquirySummary> o = new List<mBrokerEnquirySummary>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                mBrokerEnquirySummary bs = new mBrokerEnquirySummary();
                bs.EmplID = row["EmplID"].ToString();
                bs.EmplContID = row["EmplContID"].ToString();
                bs.Location = row["Location"].ToString();
                bs.EnquiryCount = row["EnquiryCount"].ToString();
                DataSet ds2 = objCon.GetBrokerEnquirySummaryDetail(EmplContID, quarter, bs.Location);
                foreach (DataRow row2 in ds2.Tables[0].Rows)
                {
                    mBrokerEnquirySummaryDetail bsd = new mBrokerEnquirySummaryDetail();
                    bsd.EmplID = row2["EmplID"].ToString();
                    bsd.SFDCProjectID = row2["SFDCProjectID"].ToString();
                    bsd.EmplContID = row2["EmplContID"].ToString();
                    bsd.ProjectName = row2["ProjectName"].ToString();
                    bsd.Location = row2["Location"].ToString();
                    bsd.EnquiryCount = row2["EnquiryCount"].ToString();
                    bs.detail.Add(bsd);
                }
                if (bs.Location == "")
                    bs.Location = "NULL";
                o.Add(bs);
            }
            context.Response.Write(Common.JSONSerialize(o));
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}