﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model;
using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for BrokerDataSync
    /// </summary>
    public class BrokerDataSync : IHttpHandler
    {

        EmpanelmentController objEmpCon = new EmpanelmentController();
        BrokerSFDCDataController objCon = new BrokerSFDCDataController();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            mCommonOutput o = new mCommonOutput();
            //string EmplContID = context.Request["userid"];

            try
            {

                if (context.Request["userid"] != null)
                {
                    //WLog.WriteLog("context.Request[userid] = " + context.Request["userid"], "BrokerDataSync");
                    UpdateData(context.Request["userid"]);
                }
                else
                {
                    DataSet ds = objEmpCon.GetAllApproved();
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        //WLog.WriteLog("row[EmplContID] = " + row["EmplContID"].ToString(), "BrokerDataSync");
                        UpdateData(row["EmplContID"].ToString());
                    }
                }
            }
            catch(Exception ex)
            {

            }

            context.Response.Write(Common.JSONSerialize(o));
        }

        public void UpdateData(string EmplContID)
        {
            mEmpanelmentContact EmplCont = objEmpCon.GetEmpanelmentContactDetails(EmplContID);
            string url = "https://d4u1.gplapps.com:8085/HerokuCommonAPI/getViewBookings?pancrd=" + EmplCont.Pan_No;
            //string url = "https://d4u.gplapps.com:8085/HerokuCommonAPI/getViewBookings?pancrd=" + EmplCont.Pan_No;
            WebRequest request = WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string responseValue;
            //read the response
            StreamReader responseReader = new StreamReader(response.GetResponseStream());
            responseValue = responseReader.ReadToEnd();
            List<mBrokerBookingSFDC> list = Common.JSONDeSerialize<List<mBrokerBookingSFDC>>(responseValue);
            //WLog.WriteLog("list.Count = " + list.Count, "BrokerDataSync");
            objCon.DeleteBrokerBooking(EmplCont.EmpanelmentID, EmplContID);
            foreach (mBrokerBookingSFDC objSFDC in list)
            {
                mBrokerBooking obj = objSFDC.ToBrokerBooking();
                obj.EmplContID = EmplContID;
                obj.EmplID = EmplCont.EmpanelmentID;
                //WLog.WriteLog("obj = " + Common.JSONSerialize(obj), "BrokerDataSync");
                objCon.AddBrokerBooking(obj);
            }
            url = "https://d4u1.gplapps.com:8085/HerokuCommonAPI/getViewEnquirys?pancrd=" + EmplCont.Pan_No;
            request = WebRequest.Create(url);
            response = (HttpWebResponse)request.GetResponse();
            //read the response
            responseReader = new StreamReader(response.GetResponseStream());
            responseValue = responseReader.ReadToEnd();
            List<mBrokerEnquirySFDC> list2 = Common.JSONDeSerialize<List<mBrokerEnquirySFDC>>(responseValue);
            //WLog.WriteLog("list2.Count = " + list2.Count, "BrokerDataSync");
            objCon.DeleteBrokerEnquiry(EmplCont.EmpanelmentID, EmplContID);
            foreach (mBrokerEnquirySFDC objSFDC in list2)
            {
                mBrokerEnquiry obj = objSFDC.ToBrokerEnquiry();
                obj.EmplContID = EmplContID;
                obj.EmplID = EmplCont.EmpanelmentID;
                //WLog.WriteLog("obj = " + Common.JSONSerialize(obj), "BrokerDataSync");
                objCon.AddBrokerEnquiry(obj);
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}