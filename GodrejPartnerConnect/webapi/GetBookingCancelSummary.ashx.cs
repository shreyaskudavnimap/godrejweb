﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for GetBookingSummary
    /// </summary>
    public class GetBookingCancelSummary : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string userid = context.Request["userid"];
            string quarter = context.Request["quarter"];
            BrokerSFDCDataController obj = new BrokerSFDCDataController();

            DataSet ds = obj.GetBrokerBookingCancelSummary(userid, quarter);
            List<mBookingSummary> o = new List<mBookingSummary>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                mBookingSummary bs = new mBookingSummary();
                bs.EmplID = row["EmplID"].ToString();
                bs.EmplContID = row["EmplContID"].ToString();
                bs.Location = row["Location"].ToString();
                bs.BookingCount = row["BookingCount"].ToString();
                bs.NetBV = row["NetBV"].ToString();
                DataSet ds2 = obj.GetBrokerBookingCancelSummaryDetail(userid, quarter, bs.Location);
                foreach (DataRow row2 in ds2.Tables[0].Rows)
                {
                    mBookingSummaryDetail bsd = new mBookingSummaryDetail();
                    bsd.EmplID = row2["EmplID"].ToString();
                    bsd.SFDCProjectID = row2["SFDCProjectID"].ToString();
                    bsd.EmplContID = row2["EmplContID"].ToString();
                    bsd.ProjectName = row2["ProjectName"].ToString();
                    bsd.Location = row2["Location"].ToString();
                    bsd.BookingCount = row2["BookingCount"].ToString();
                    bsd.NetBV = row2["NetBV"].ToString();
                    bs.detail.Add(bsd);
                }
                if (bs.Location == "")
                    bs.Location = "NULL";
                o.Add(bs);
            }

            context.Response.Write(Common.JSONSerialize(o));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}