﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for GetEmplDetailsEmail
    /// </summary>
    public class GetEmplDetailsEmail : IHttpHandler
    {
        EmpanelmentController objcon = new EmpanelmentController();
        public void ProcessRequest(HttpContext context)
        {

            context.Response.ContentType = "text/plain";
            string email = context.Request["email"];
            string EmplID = context.Request["EmplID"];
            WLog.WriteLog("email = " + email, "GetEmplDetailsEmail");
            string output = Common.JSONSerialize(GetEmplInfo(email, EmplID));
            context.Response.Write(output);
        }
        public mEmpanelment GetEmplInfo(string email, string EmplID)
        {
            mEmpanelment obj = new mEmpanelment();
            DataSet ds = objcon.GetEmpanelmentByEmail(email, EmplID);
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow row = ds.Tables[0].Rows[0];
                obj.status = row["status"].ToString();
                obj.EmplID = row["EmplID"].ToString();
            }
            else
            {
                obj.EmplID = "0";
            }

            return obj;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}