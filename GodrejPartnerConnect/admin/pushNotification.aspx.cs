﻿using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_pushNotification : System.Web.UI.Page
{
    NotificationController objCon = new NotificationController();
    string userid = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");

        userid = Convert.ToString(Session["User"]);
        if (!IsPostBack)
        {
            BindGrid();
            BindTemplateDropDown();
        }

    }

    private void BindTemplateDropDown()
    {
        stateDropDOwn.Items.Add(new ListItem("All States", "All"));
        cityDropDown.Items.Add(new ListItem("All Cities", "All"));
        DataSet ds = objCon.GetRegionsPushNotifications("GET_STATE", null);
        foreach (DataRow row in ds.Tables[0].Rows)
        {
            stateDropDOwn.Items.Add(new ListItem(row["Registered_State"].ToString(), row["Registered_State"].ToString()));
        }
    }

    protected void getStateDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
        cityDropDown.Items.Clear();
        string selected_State = stateDropDOwn.SelectedValue.ToString().Replace(@"\r\n", "");
        if (selected_State != "All")
        {
            DataSet ds = objCon.GetRegionsPushNotifications("GET_CITY", selected_State);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                cityDropDown.Items.Add(new ListItem(row["Registered_City"].ToString(), row["Registered_City"].ToString()));
            }
        }
    }
    private void BindGrid()
    {
        divgrid.Visible = true;
        divDetail.Visible = false;
        DataSet ds = objCon.GetNotificationAll();
        grView.DataSource = ds.Tables[0];
        grView.DataBind();

    }

    protected void grView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grView.PageIndex = e.NewPageIndex;
        BindGrid();
    }



    protected void grView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {
            lblID.Text = e.CommandArgument.ToString();
            Showfrom(lblID.Text);
        }
        else if (e.CommandName == "Send")
        {
            SendNotification(e.CommandArgument.ToString());
        }
    }

    void SendNotification(string id)
    {
        DataSet ds = objCon.GetNotificationByID(id);
        string str = objCon.SendNotification(ds.Tables[0], userid);
        ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('" + str + " Notification Send Successfully'); window.location='PushNotification.aspx';</script>");
    }

    void Showfrom(string id)
    {
        DataSet ds = objCon.GetNotificationByID(id);
        if (ds.Tables[0].Rows.Count > 0)
        {
            DataRow row = ds.Tables[0].Rows[0];
            divDetail.Visible = true;
            divgrid.Visible = false;
            hideFileUpload.Visible = false;
            if (row["NotificationImage"].ToString() == "" || row["NotificationImage"].ToString() == null)
                Image1.ImageUrl = "~/admin/images/noimage.png";
            else
                Image1.ImageUrl = "~/" + row["NotificationImage"].ToString();
            spTitle.InnerText = "View";
            if (row["HasSend"].ToString() == "Y")
            {
                txtNotificationBody.Enabled = false;
                txtNotificationTitle.Enabled = false;
                txtSubTitle.Enabled = false;
                btnSubmit.Visible = false;
            }
            else
            {
                txtNotificationBody.Enabled = true;
                txtNotificationTitle.Enabled = true;
                txtSubTitle.Enabled = true;
            }
            txtNotificationBody.Text = row["NotificationBody"].ToString();
            txtNotificationTitle.Text = row["NotificationTitle"].ToString();
            txtSubTitle.Text = row["SubTitle"].ToString();
            link.Text = row["Link"].ToString();
            txtLink.Text = row["LinkText"].ToString();

        }
        else
        {
            Response.Redirect("PushNotification.aspx");
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        divDetail.Visible = true;
        divgrid.Visible = false;
        lblID.Text = "0";
        spTitle.InnerText = "Add";
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        mNotification obj = new mNotification();
        obj.NotificationID = lblID.Text;
        obj.NotificationTitle = txtNotificationTitle.Text;
        obj.NotificationBody = txtNotificationBody.Text;
        obj.SubTitle = txtSubTitle.Text;
        obj.IsRedirect = "N";
        obj.HasSend = "N";
        obj.ChannelType = "notification";

        obj.NotificationType = "M";
        obj.MobileUrl = "";
        obj.ChannelID = "0";
        obj.Active = "Y";
        obj.InsertedBy = userid;

        if((link.Text != null && link.Text.Length != 0) && (!link.Text.Contains("http") || !link.Text.Contains("https")))
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please add http or https.'); window.location='PushNotification.aspx';</script>");
            return;
        }

        obj.Link = link.Text;
        obj.LinkText = txtLink.Text;


        obj.State = stateDropDOwn.SelectedValue.ToString();
        if(cityDropDown.SelectedValue.ToString() == "")
        {
            obj.City = "All";
        }
        else
        {
            obj.City = cityDropDown.SelectedValue.ToString();
        }

        if (fileUpload.PostedFile != null && fileUpload.PostedFile.ContentLength != 0)
        {
            string[] supportFormts = { ".png", ".jpg", ".jpeg" };
            if (supportFormts.Contains(System.IO.Path.GetExtension(fileUpload.FileName).ToLower()))
            {
                string subfolder = "Admin/Uploads/NotificationImages/";
                string extn = System.IO.Path.GetExtension(fileUpload.FileName);
                string path = System.AppDomain.CurrentDomain.BaseDirectory;
                string newFileName = "IMG" + DateTime.Now.ToString("yyyyMMdd_HHmmsstt");

                string fullPath = path + subfolder + newFileName + extn;
                fileUpload.SaveAs(fullPath);
                obj.NotificationImage = subfolder + newFileName + extn;
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Invalid File Format. (Supported: .png, .jpg, .jpeg)'); window.location='PushNotification.aspx';</script>");
                return;
            }
        }

        objCon.InsertUpdateNotification(obj);
        ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record Saved Successfully'); window.location='PushNotification.aspx';</script>");
    }
}