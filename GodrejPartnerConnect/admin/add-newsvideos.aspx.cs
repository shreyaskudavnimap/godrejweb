﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;
//using System.Transactions;

public partial class Admin_add_newsvideos : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    Guid random = Guid.NewGuid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");

        //if (!IsPostBack)
        //{
            //if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["newsId"])))
            //{
                FillData(Convert.ToString(Request.QueryString["newsId"]));
            //}
        //}

        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("NewsAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("NewsLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

    }

    private void FillData(string id)
    {
        var objNews = (from obj in dataContext.NewsVideos
                       where obj.newsid == Convert.ToInt32(Session["newsId"])
                       select obj).ToList();
        string video = "";
        if (objNews.Count > 0)
        {
            foreach (var s in objNews)
            {
                if (video == "")
                {
                    video = s.videourl;
                }
                else
                {
                    video += ","+s.videourl;
                }
            }
        }
        hdnVideo.Value = video;
    }

    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewnewsfeed.aspx");
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        int newsid = Convert.ToInt32(Session["newsId"]);
        //string video = Session["Video"].ToString();
            //Convert.ToString(Session["Video"]);
        string video = Request.Form["hdnVideo"];
        //if (string.IsNullOrEmpty(Convert.ToString(Session["newsId"])))
        //{
        #region to check if same offer exists

        var objChk = (from obj in dataContext.NewsVideos
                      where obj.newsid == Convert.ToInt32(Session["newsId"])
                      select obj).FirstOrDefault();

        #endregion
        if (objChk != null)
        {
            var objDelVideo = from d in dataContext.NewsVideos where (d.newsid == Convert.ToInt32(Session["newsId"])) select d;
            foreach (NewsVideo item in objDelVideo)
            {
                dataContext.NewsVideos.DeleteOnSubmit(item);
                dataContext.SubmitChanges();
            }
        }
        string[] videolist = video.Split(new string[] { "," }, StringSplitOptions.None);
        foreach (string s in videolist)
        {
            if (s != "")
            {
                NewsVideo objNews = new NewsVideo();
                objNews.newsid = newsid;
                objNews.videourl = s;
                objNews.status = "A";
                objNews.createdon = DateTime.Now;
                dataContext.NewsVideos.InsertOnSubmit(objNews);
                dataContext.SubmitChanges();
            }
        }
        Response.Redirect("viewnewsfeed.aspx?save=1");
        //        #region add new record
        //        NewsFeed objNews = new NewsFeed();
        //        #region Save Image
        //        if (FileUpload1.HasFile)
        //        {
        //            if (FileUpload1.PostedFile.ContentLength > 1048576)
        //            {
        //                //ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual Image size should not be more than 1MB');</script>");
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Actual Image size should not be more than 1MB.');</script>", false);
        //                return;
        //            }
        //            string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
        //            if (imgext.ToLower() == ".jpg" || imgext.ToLower() == ".gif" || imgext.ToLower() == ".png")
        //            {
        //                string fileName = Path.GetFileName(FileUpload1.FileName);
        //                string fileNameEdited = random.ToString() + "_" + fileName;
        //                float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
        //                float height = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
        //                if (width == 300 && height == 300)
        //                {
        //                    FileUpload1.SaveAs(Server.MapPath("~/images/News/") + fileNameEdited);
        //                    objNews.image = fileNameEdited;
        //                }
        //                else
        //                {
        //                    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please upload image with minimum dimensions 300 x 300px');</script>");
        //                    return;
        //                }
        //            }
        //            else
        //            {
        //                // ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif or .jpg or .png file');</script>");
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload only .jpg,.gif,.png file.');</script>", false);
        //                return;
        //            }
        //        }
        //        else
        //        {
        //            // ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload Image');</script>");
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload Image');</script>", false);
        //            return;
        //        }
        //        #endregion

        //        objNews.title = txtTitle.Text;
        //        objNews.description = txtDescription.Text;
        //        objNews.status = "A";
        //        objNews.createdon = DateTime.Now;
        //        dataContext.NewsFeeds.InsertOnSubmit(objNews);
        //        dataContext.SubmitChanges();
        //        Response.Redirect("viewnewsfeed.aspx?save=1");
        //        #endregion

        //    }
        //}
        //else
        //{
        //    Response.Redirect("viewnewsfeed.aspx");
        //}
        //Div1.Style.Add("display", "block");
    }
}
