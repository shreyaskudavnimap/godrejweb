﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;

public partial class Admin_add_constructionstatus : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    Guid random = Guid.NewGuid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        if (!IsPostBack)
        {
            BindProject();
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["statusId"])))
            {
                FillData(Convert.ToString(Request.QueryString["statusId"]));
            }
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("ProjectAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("StatusLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

    }

    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewconstructionstatus.aspx");
    }

    private void BindProject()
    {
        #region Code for Project
        var objProject = (from obj in dataContext.Projects
                          join objDesgn in dataContext.CityMasters
                          on obj.cityid equals objDesgn.cityid
                          join objPlace in dataContext.PlaceMasters
                          on obj.placeid equals objPlace.placeid into defaultPlace
                          from c in defaultPlace.DefaultIfEmpty()
                          where obj.status.Trim() == "A"
                          orderby obj.projectid descending
                          select new
                          {
                              obj.projectid,
                              projectname = (c == null ? obj.title + "," + objDesgn.cityname : obj.title + "," + objDesgn.cityname + " " + c.placename)

                          }).Distinct().ToList();
        ddlProject.DataTextField = "projectname";
        ddlProject.DataValueField = "projectid";
        ddlProject.DataSource = objProject;
        ddlProject.DataBind();
        ddlProject.Items.Insert(0, new ListItem("--Select--", "0"));
        #endregion
    }

    private void FillData(string statusId)
    {
        var objstatus = dataContext.ConstructionStatus.Single(doc => doc.StatusID == Convert.ToInt32(statusId));
        ddlProject.SelectedValue = Convert.ToString(objstatus.projectid);
        FileUpload1.Attributes.Add("maxlength", "1");
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["statusId"])))
            {
                #region To add record

                #region save images
                HttpFileCollection upload = Request.Files;
                HttpPostedFile File1;
                if (upload.Count == 0)
                {
                }
                if (upload.Count <= 5)
                {
                    int k = 0;
                    int j = 0;
                    string notUplodedfileName = string.Empty;
                    for (int i = 0; i < upload.Count; i++)
                    {

                        ConstructionStatus obj = new ConstructionStatus();
                        k = 0;
                        string fileNameEdited = "";
                        string fileName = "";

                        HttpPostedFile file = upload[i];
                        fileName = Path.GetFileName(file.FileName);
                        if (file.ContentLength > 1048576)
                        {
                            if (j != 0)
                                notUplodedfileName = notUplodedfileName + ", " + fileName;
                            if (j == 0)
                                notUplodedfileName = fileName;
                            j++;
                            k++;
                        }
                        else
                        {
                            string imgext = System.IO.Path.GetExtension(file.FileName.ToLower());
                            if (imgext.ToLower() == ".gif" || imgext.ToLower() == ".jpg" || imgext.ToLower() == ".jpeg")
                            {
                                float width = System.Drawing.Image.FromStream(file.InputStream).PhysicalDimension.Width;
                                float height = System.Drawing.Image.FromStream(file.InputStream).PhysicalDimension.Height;
                                if (width >= 217 && height >= 147)
                                {
                                    fileNameEdited = random.ToString() + "_" + fileName;
                                    file.SaveAs(Server.MapPath("~/images/ConstructionStatus/") + fileNameEdited);
                                    obj.Image = fileNameEdited;
                                }
                                else
                                {
                                    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please upload image with  dimensions 217 x 147px');</script>");
                                    return;
                                }
                            }
                            else
                            {
                                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif, .jpg or .jpeg files');</script>");
                                return;
                            }
                        }

                        obj.projectid = Convert.ToInt32(ddlProject.SelectedValue);
                        obj.title = Convert.ToString(txtTitle.Text);
                        obj.month = Convert.ToString(ddlMonth.SelectedValue);
                        obj.year = Convert.ToString(ddlYear.SelectedValue);
                        obj.createdate = System.DateTime.Now;
                        obj.status = "A";
                        dataContext.ConstructionStatus.InsertOnSubmit(obj);
                        dataContext.SubmitChanges();

                    }
                    if (j != 0)
                    {
                        ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('file size is more than 1 MB.');</script>");
                        return;
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('You can upload 5 images at a time.');</script>");
                    return;

                }
                #endregion

                Response.Redirect("viewconstructionstatus.aspx?save=1");
                Div1.Style.Add("display", "block");
                #endregion
            }
            else
            {
                int id = Convert.ToInt32(Session["statusId"]);
                var obj = dataContext.ConstructionStatus.Single(doc => doc.StatusID == id);
                #region update image
                if (FileUpload1.HasFile)
                {
                    if (FileUpload1.PostedFile.ContentLength > 1048576)
                    {
                        ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual Image size should not be more than 1MB.');</script>");

                        return;
                    }
                    string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                    if (imgext.ToLower() == ".jpg" || imgext.ToLower() == ".jpeg")
                    {
                        string fileName = Path.GetFileName(FileUpload1.FileName);
                        string fileNameEdited = random.ToString() + "-" + fileName;
                        FileUpload1.SaveAs(Server.MapPath("~/images/ConstructionStatus/") + fileNameEdited);
                        obj.Image = fileNameEdited;
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please upload only .jpg or .jpeg');</script>");
                        return;
                    }
                }

                #endregion

                obj.projectid = Convert.ToInt32(ddlProject.SelectedValue);
                obj.title = Convert.ToString(txtTitle.Text);
                obj.month = Convert.ToString(ddlMonth.SelectedValue);
                obj.year = Convert.ToString(ddlYear.SelectedValue);
                dataContext.SubmitChanges();
                Response.Redirect("viewconstructionstatus.aspx?save=1");
                Session["statusId"] = null;
                Div1.Style.Add("display", "block");
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }

    }
}
