﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;

public partial class admin_Default : System.Web.UI.Page
{
    string constring = ConfigurationManager.ConnectionStrings["godrejCPConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        GetSAPDetails();
    }
    public void GetSAPDetails()
    {
        try
        {
            List<string> splitted = new List<string>();
            string fileList = getCSV("http://godrejchanp.netbizlabs.com/sales_detl.txt");
            string[] tempStr;
            string singleStr;
            string[] finalStr;
            tempStr = fileList.Split('\r');

            //finalStr = tempStr.('\t');

            for (int j = 0; j < tempStr.Length; j++)
            {
                singleStr = tempStr[j];
                finalStr = singleStr.Split('\t');
                if (j > 0)
                {
                    for (int k = 0; k < finalStr.Length; k++)
                    {
                        SqlConnection conn;
                        SqlCommand comm;
                        SqlDataAdapter da;
                        conn = new SqlConnection(constring);
                        if (conn.State != ConnectionState.Open)
                            conn.Open();

                        comm = new SqlCommand("SP_insert_Sales_Test", conn);
                        comm.Connection = conn;
                        comm.CommandType = System.Data.CommandType.StoredProcedure;
                        comm.Parameters.AddWithValue("@SalesDocument", finalStr[2]);
                        comm.Parameters.AddWithValue("@Date", finalStr[3]);
                        comm.Parameters.AddWithValue("@EntryTime", finalStr[4]);
                        comm.Parameters.AddWithValue("@DocumentDate", finalStr[5]);
                        comm.Parameters.AddWithValue("@Project", finalStr[6]);
                        comm.Parameters.AddWithValue("@OrgName", finalStr[7]);
                        comm.Parameters.AddWithValue("@Customer", finalStr[8]);
                        comm.Parameters.AddWithValue("@CustomerName", finalStr[9]);
                        comm.Parameters.AddWithValue("@Flatcode", finalStr[11]);
                        comm.Parameters.AddWithValue("@Tower", finalStr[12]);
                        comm.Parameters.AddWithValue("@PlantName", finalStr[13]);
                        comm.Parameters.AddWithValue("@Vendor_Acct_No", finalStr[0]);
                        comm.Parameters.AddWithValue("@Per_Acct_No", finalStr[1]);
                        comm.Parameters.AddWithValue("@Brokerage", finalStr[10]);
                        comm.Parameters.AddWithValue("@REJ_IN", finalStr[14]);
                        comm.Parameters.AddWithValue("@REJ_DATE", finalStr[15]);
                        int l = comm.ExecuteNonQuery();
                    }
                }

            }
            Createerrorlog("sap", "success", Convert.ToString(tempStr.Length - 1));
        }
        catch (Exception ex)
        {
            Createerrorlog("sap", ex.Message.ToString(), "0");
        }
    }
    public string getCSV(string url)
    {
        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
        HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

        StreamReader sr = new StreamReader(resp.GetResponseStream());
        string results = sr.ReadToEnd();
        sr.Close();

        return results;
    }
    public void Createerrorlog(string type, string msg, string rows)
    {
        #region code to update schedular log

        SqlConnection conn;
        SqlCommand comm;
        conn = new SqlConnection(constring);
        if (conn.State != ConnectionState.Open)
            conn.Open();
        comm = new SqlCommand("SP_SchedularLog", conn);
        comm.Connection = conn;
        comm.CommandType = System.Data.CommandType.StoredProcedure;
        comm.Parameters.AddWithValue("@type", type);
        comm.Parameters.AddWithValue("@errormsg", msg);
        comm.Parameters.AddWithValue("@rows", rows);
        int i = comm.ExecuteNonQuery();
        #endregion
    }
}