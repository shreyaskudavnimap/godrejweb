﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProfileChangeRequest.aspx.cs" Inherits="ProfileChangeRequest" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Admin/MenuControl.ascx" TagName="Menu" TagPrefix="UC1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
    <!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
    <!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
    <!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
    <!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
    <!--[if gt IE 8]><!-->
    <!--<![endif]-->
    <meta charset="utf-8">
    <link rel="dns-prefetch" href="http://fonts.googleapis.com" />
    <link rel="dns-prefetch" href="http://themes.googleusercontent.com" />
    <link rel="dns-prefetch" href="http://ajax.googleapis.com" />
    <link rel="dns-prefetch" href="http://cdnjs.cloudflare.com" />
    <link rel="dns-prefetch" href="http://agorbatchev.typepad.com" />
    <!-- Use the .htaccess and remove these lines to avoid edge case issues.
More info: h5bp.com/b/378 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Godrej Channel Partner :: Admin</title>
    <meta name="description" content="Godrej Channel Partner App">
    <meta name="author" content="Godrej Channel Partner App">
    <!-- Mobile viewport optimized: h5bp.com/viewport -->
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
    <!-- iPhone: Don't render numbers as call links -->
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" href="favicon.ico" />
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory: mathiasbynens.be/notes/touch-icons -->
    <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->
    <!-- The Styles -->
    <!-- ---------- -->
    <!-- Layout Styles -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/layout.css">
    <!-- Icon Styles -->
    <link rel="stylesheet" href="css/icons.css">
    <link rel="stylesheet" href="css/fonts/font-awesome.css">
    <!--[if IE 8]><link rel="stylesheet" href="css/fonts/font-awesome-ie7.css"><![endif]-->
    <!-- External Styles -->
    <link rel="stylesheet" href="css/external/jquery-ui-1.9.1.custom.css">
    <link rel="stylesheet" href="css/external/jquery.chosen.css">
    <link rel="stylesheet" href="css/external/jquery.cleditor.css">
    <link rel="stylesheet" href="css/external/jquery.colorpicker.css">
    <link rel="stylesheet" href="css/external/jquery.elfinder.css">
    <link rel="stylesheet" href="css/external/jquery.fancybox.css">
    <link rel="stylesheet" href="css/external/jquery.jgrowl.css">
    <link rel="stylesheet" href="css/external/jquery.plupload.queue.css">
    <link rel="stylesheet" href="css/external/syntaxhighlighter/shCore.css" />
    <link rel="stylesheet" href="css/external/syntaxhighlighter/shThemeDefault.css" />
    <!-- Elements -->
    <link rel="stylesheet" href="css/elements.css">
    <link rel="stylesheet" href="css/forms.css">
    <!-- OPTIONAL: Print Stylesheet for Invoice -->
    <link rel="stylesheet" href="css/print-invoice.css">
    <!-- Typographics -->
    <link rel="stylesheet" href="css/typographics.css">
    <!-- Responsive Design -->
    <link rel="stylesheet" href="css/media-queries.css">
    <!-- Bad IE Styles -->
    <link rel="stylesheet" href="css/ie-fixes.css">
    <!-- The Scripts -->
    <!-- ----------- -->
    <!-- JavaScript at the top (will be cached by browser) -->
    <!-- Grab frameworks from CDNs -->
    <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.js"></script>

    <script>        window.jQuery || document.write('<script src="js/libs/jquery-1.8.2.js"><\/script>')</script>

    <!-- Do the same with jQuery UI -->

    <script src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>

    <script>        window.jQuery.ui || document.write('<script src="js/libs/jquery-ui-1.9.1.js"><\/script>')</script>

    <!-- Do the same with Lo-Dash.js -->
    <!--[if gt IE 8]><!-->

    <script src="http://cdnjs.cloudflare.com/ajax/libs/lodash.js/0.8.2/lodash.js"></script>

    <script>        window._ || document.write('<script src="js/libs/lo-dash.js"><\/script>')</script>

    <!--<![endif]-->
    <!-- IE8 doesn't like lodash -->
    <!--[if lt IE 9]><script src="http://documentcloud.github.com/underscore/underscore.js"></script><![endif]-->
    <!-- Do the same with require.js -->

    <script src="http://cdnjs.cloudflare.com/ajax/libs/require.js/2.0.6/require.js"></script>

    <script>        window.require || document.write('<script src="js/libs/require-2.0.6.min.js"><\/script>')</script>

    <!-- Load Webfont loader -->

    <script type="text/javascript">
        window.WebFontConfig = {
            google: { families: ['PT Sans:400,700'] },
            active: function () { $(window).trigger('fontsloaded') }
        };

        function doClick(buttonName, e) {
            //the purpose of this function is to allow the enter key to 
            //point to the correct button to click.
            var key;

            if (window.event)
                key = window.event.keyCode;     //IE
            else
                key = e.which;     //firefox

            if (key == 13) {
                //Get the button the user wants to have clicked
                var btn = document.getElementById(buttonName);
                if (btn != null) { //If we find the button click it
                    btn.click();
                    event.keyCode = 0
                }
            }
        }
        // To clear default value 
        function clearDefault(ele) {
            if (ele.defaultValue == ele.value) {
                ele.value = ''
            }
        }

        // To restore default value 
        function restore(ele) {
            if (ele.value == '') {
                ele.value = ele.defaultValue;
            }
        }
        //$(".close_div_Gst").click(function () {
        function closeGst() {
            $('#div_Gst').css('display', 'none');
            $('#div_Rera').css('display', 'none');

        }
        //$(".close_div_Rera").click(function () {
        function closeRera() {
            $('#div_Gst').css('display', 'none');
            $('#div_Rera').css('display', 'none');

        }
    </script>

    <script defer async src="https://ajax.googleapis.com/ajax/libs/webfont/1.0.28/webfont.js"></script>

    <!-- Essential polyfills -->

    <script src="js/mylibs/polyfills/modernizr-2.6.1.min.js"></script>

    <script src="js/mylibs/polyfills/respond.js"></script>

    <script src="js/mylibs/polyfills/matchmedia.js"></script>

    <!--[if lt IE 9]><script src="js/mylibs/polyfills/selectivizr.js"></script><![endif]-->
    <!--[if lt IE 10]><script src="js/mylibs/polyfills/excanvas.js"></script><![endif]-->
    <!--[if lt IE 10]><script src="js/mylibs/polyfills/classlist.js"></script><![endif]-->
    <!-- scripts concatenated and minified via build script -->
    <!-- Scripts required everywhere -->

    <script src="js/mylibs/jquery.hashchange.js"></script>

    <script src="js/mylibs/jquery.idle-timer.js"></script>

    <script src="js/mylibs/jquery.plusplus.js"></script>

    <script src="js/mylibs/jquery.scrollTo.js"></script>

    <script src="js/mylibs/jquery.ui.touch-punch.js"></script>

    <script src="js/mylibs/jquery.ui.multiaccordion.js"></script>

    <script src="js/mylibs/number-functions.js"></script>

    <script src="js/mylibs/fullstats/jquery.css-transform.js"></script>

    <script src="js/mylibs/fullstats/jquery.animate-css-rotate-scale.js"></script>

    <script src="js/mylibs/forms/jquery.validate.js"></script>

    <!-- Do not touch! -->

    <script src="js/mango.js"></script>

    <script src="js/plugins.js"></script>

    <script src="js/script.js"></script>

    <!-- Your custom JS goes here -->

    <script src="js/app.js"></script>

    <!-- end scripts -->
</head>
<body>
    <form id="form1" runat="server">
        <!-- The loading box -->
        <div id="loading-overlay">
        </div>
        <div id="loading">
            <span>Loading...</span>
        </div>
        <!-- End of loading box -->
        <!--------------------------------->
        <!-- Now, the page itself begins -->
        <!--------------------------------->
        <!-- The toolbar at the top -->
        <section id="toolbar">
<div class="container_12">
<!-- Right side -->
<div class="right">
<ul>
    <li class="red"><a href="AdminLogin.aspx">Logout</a></li>
</ul>
</div>
<!-- End of .right -->
<!-- Phone only items -->
<div class="phone">
<!-- Navigation -->
<li><a class="navigation" href="#"><span class="icon icon-list"></span></a></li>
</div>
<!-- End of phone items -->
</div>
<!-- End of .container_12 -->
</section>
        <!-- End of #toolbar -->
        <!-- The header containing the logo -->
        <header class="container_12">	
<div class="container">
<!-- Your logos -->
<a href="viewcity.aspx"><img src="img/logo.png" alt="Godrej Channel Partner App"></a>
<a class="phone-title" href="viewbanner.aspx"><img src="img/logo-mobile.png" alt="Godrej Channel Partner App" height="30" /></a>
</div><!-- End of .container -->
</header>
        <!-- End of header -->
        <!-- The container of the sidebar and content box -->
        <div role="main" id="main" class="container_12 clearfix">
            <!-- The blue toolbar stripe -->
            <section class="toolbar">
<asp:TextBox ID="txtSearch" onfocus="clearDefault(this)" Text="search by name" onblur="restore(this)" runat="server" CssClass="tooltip"></asp:TextBox>
<asp:Button ID="btnSubmit" runat="server" CssClass="submit go" Text="Go"
onclick="btnSubmit_Click" />
</section>
            <!-- End of .toolbar-->
            <!-- The sidebar -->
            <UC1:Menu ID="Menu" runat="server" />
            <!-- End of sidebar -->
            <!-- Here goes the content. -->
            <section id="content" class="container_12 clearfix" data-sort="true">
<h1 class="grid_12">
Update Profile</h1>
<div class="grid_12">
<div class="box">
                    
<div id="divGrid" runat="server">
        <div class="content">
            <div class="tabletools">
                <div class="left">
                <%--<asp:LinkButton ID="lnkBtnAddContent" runat="server" ToolTip="Add Project"
                CssClass="open-add-client-dialog" OnClick="lnkBtnAddContent_Click"><i class="icon-plus"></i>Update Code</asp:LinkButton>--%>
                    <%--<a class="open-add-client-dialog" href="add-projectdetails.aspx"><i class="icon-plus">
                    </i>Add Project</a>--%>
                </div>
                <div class="right">
                </div>
            </div>
            <asp:GridView ID="reqDetails" runat="server" AutoGenerateColumns="false" DataKeyNames="EmplID"
                AllowPaging="true" PageSize="10" OnPageIndexChanging="reqDetails_PageIndexChanging"
                OnRowCommand="reqDetails_RowCommand" OnRowDataBound="reqDetails_RowDataBound"
                CssClass="dynamic styled with-prev-next">
                <Columns>
                        <asp:BoundField HeaderText="Email" DataField="Email">
                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                        <ItemStyle CssClass="icon-1 info-tooltip" />
                    </asp:BoundField>
                        <asp:BoundField HeaderText="Pan No" DataField="Pan_No">
                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                        <ItemStyle CssClass="icon-1 info-tooltip" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="RequestType" DataField="RequestType">
                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                        <ItemStyle CssClass="icon-1 info-tooltip" />
                    </asp:BoundField>
                    <%--<asp:BoundField HeaderText="RequestTypeId" DataField="RequestTypeId">
                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                        <ItemStyle CssClass="icon-1 info-tooltip" />
                    </asp:BoundField>--%>
                    <asp:TemplateField HeaderText="Status">
                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                        <ItemStyle CssClass="icon-1 info-tooltip" />
                        <ItemTemplate>
                            <asp:Label ID="lblStatusText" runat="server" />
                            <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>' Visible="false" />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Action">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkBtnView" Text="View" runat="server" CommandName="View"
                                CommandArgument='<%# Eval("ID") + "," + Eval("RequestTypeId") + "," + Eval("EmplID") %>'></asp:LinkButton>
                        </ItemTemplate>
                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                        <ItemStyle CssClass="icon-1 info-tooltip" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:Label ID="lblMsg" runat="server" Text="" Visible="false"></asp:Label>
                            
                        
        </div>
        <!-- End of .content -->
    </div>
    <asp:HiddenField ID="txtEmplId" runat="server" />
<div id="divRegAddr" runat="server">
    <div class="header">
        <h2>Registered Address</h2>
        <asp:Label ID="Label1" runat="server"  Visible="false"></asp:Label>
        <asp:Label ID="Label2" runat="server"  Visible="false"></asp:Label>
    </div>
    <div class="content">
    <div class="full validate">
        <div class="row">  
            <asp:HiddenField ID="txtRegAddrRowId" runat="server" />
            <label for="txtNew_RegisteredAddress">
                <strong style="width: 121px;">Registered Address</strong>
            </label>
            <div class="Label">
                <asp:Label ID="txtNew_RegisteredAddress" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="full validate">
        <div class="row">  
            <asp:HiddenField ID="HiddenField1" runat="server" />
            <label for="txtNew_RegisteredAddress">
                <strong style="width: 121px;">Registered Country</strong>
            </label>
            <div class="Label">
                <asp:Label ID="txtNew_RegisteredCountry" runat="server"></asp:Label>
            </div>
        </div>
    </div>
        <div class="full validate">
        <div class="row">  
            <asp:HiddenField ID="HiddenField2" runat="server" />
            <label for="txtNew_RegisteredAddress">
                <strong style="width: 121px;">Registered State</strong>
            </label>
            <div class="Label">
                <asp:Label ID="txtNew_RegisteredState" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="full validate">
        <div class="row">  
            <asp:HiddenField ID="HiddenField3" runat="server" />
            <label for="txtNew_RegisteredAddress">
                <strong style="width: 121px;">Registered City</strong>
            </label>
            <div class="Label">
                <asp:Label ID="txtNew_RegisteredCity" runat="server"></asp:Label>
            </div>
        </div>
    </div>
        </div>
        <div class="full validate">
        <div class="row">  
            <asp:HiddenField ID="HiddenField4" runat="server" />
            <label for="txtNew_RegisteredAddress">
                <strong style="width: 121px;">Registered Zipcode</strong>
            </label>
            <div class="Label">
                <asp:Label ID="txtNew_RegisteredZipCode" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="full validate">
        <div class="row">
            <label for="lblAddressProof">
                <strong style="width: 121px;">Address Proof</strong>
            </label>
            <div class="Label">
                <a id="txtAddressProof" runat="server" target="_blank" >View Doc</a>
            </div>
        </div>
    </div>
    <div class="full validate">
        <div class="row">  
            <label for="txtNew_RegisteredAddress">
                <strong style="width: 121px;">Rejection Comment</strong>
            </label>
            <div class="TextBox">
                <asp:TextBox ID="txtRegAddressComment" runat="server"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="full validate">
        <div class="row">
            <label for="">
                <strong style="width: 121px;">Status</strong>
            </label>
            <div class="Label" style="margin-top:-20px;"">
                    <asp:DropDownList ID="statusRegAddrDropDown" runat="server"></asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="right">
            <asp:Button ID="btnRegister" runat="server" CssClass="submit" Text="Submit" OnClick="submitRegAddr_Click"  />
        </div>
    </div>

<%--<div id="divGst" runat="server">
    <div class="header">
        <h2>GST</h2>
        <asp:Label ID="Label3" runat="server"  Visible="false"></asp:Label>
        <asp:Label ID="Label4" runat="server"  Visible="false"></asp:Label>
    </div>
    <div class="content">
        <div class="full validate">
            <div class="row">--%>
                <%--<asp:HiddenField ID="txtGstRowId" runat="server" />--%>
                <%--<label for="txtGstNo">
                    <strong style="width: 121px;">GST Number</strong>
                </label>
                <div class="Label">
                    <asp:Label ID="txtGstNo" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="full validate">
            <div class="row">
                <label for="txtState">
                    <strong style="width: 121px;">State</strong>
                </label>
                <div class="Label">
                    <asp:Label ID="txtGstState" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="full validate">
            <div class="row">
                <label for="txtGstCerti">
                    <strong style="width: 121px;">GST Certificate</strong>
                </label>
                <div class="Label">
                    <a id="txtGstCerti" runat="server" target="_blank" >View Doc</a>
                </div>
            </div>
        </div>
        <div class="full validate">
            <div class="row">
                <label for="txtReraComment">
                    <strong style="width: 121px;">Rejection Comment</strong>
                </label>
                <div class="TextBox">
                    <asp:TextBox ID="txtGstComment" runat="server"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="full validate">
            <div class="row">
                <label for="">
                    <strong style="width: 121px;">Status</strong>
                </label>
                <div class="Label" style="margin-top:-20px;"">
                    <asp:DropDownList ID="statusGstDropDown" runat="server"></asp:DropDownList>
                </div>
            </div>
        </div>
    <div class="actions">
        <div class="right">
            <asp:Button ID="Button1" runat="server" CssClass="submit" Text="Submit" OnClick="submitGst_Click"  />
        </div>
    </div>
    </div>--%>

<div id="divRera" runat="server">
    <div class="header">
        <h2>RERA</h2>
        <asp:Label ID="Label5" runat="server"  Visible="false"></asp:Label>
        <asp:Label ID="Label6" runat="server"  Visible="false"></asp:Label>
    </div>
    <div class="content">
        <div class="full validate">
            <div class="row">
                <asp:HiddenField ID="txtReraRowId" runat="server" />
                <asp:HiddenField ID="txtGstRowId" runat="server" />

                <label for="txtReraNo">
                    <strong style="width: 121px;">RERA Number</strong>
                </label>
                <div class="Label">
                    <asp:Label ID="txtReraNo" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="full validate">
            <div class="row">
                <label for="txtReraState">
                    <strong style="width: 121px;">RERA State</strong>
                </label>
                <div class="Label">
                    <asp:Label ID="txtReraState" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="full validate">
            <div class="row">
                <label for="txtReraCerti">
                    <strong style="width: 121px;">RERA Certificate</strong>
                </label>
                <div class="Label">
                    <a id="txtReraCerti" runat="server" target="_blank" >View Doc</a>
                </div>
            </div>
        </div>
        <div class="full validate">
            <div class="row">
                <label for="">
                    <strong style="width: 121px;">GST Number</strong>
                </label>
                <div class="Label">
                    <asp:Label ID="txtGstNumberNew" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="full validate">
            <div class="row">
                <label for="">
                    <strong style="width: 121px;">GST State</strong>
                </label>
                <div class="Label">
                    <asp:Label ID="txtGstStateNew" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="full validate">
            <div class="row">
                <label for="txtReraCerti">
                    <strong style="width: 121px;">GST Certificate</strong>
                </label>
                <div class="Label">
                    <a id="txtGstCertiNew" runat="server" target="_blank" >View Doc</a>
                </div>
            </div>
        </div>
        <div class="full validate">
            <div class="row">
                <label for="">
                    <strong style="width: 121px;">Rejection Comment</strong>
                </label>
                <div class="TextBox">
                    <asp:TextBox ID="txtReraComment" runat="server"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="full validate">
            <div class="row">
                <label for="">
                    <strong style="width: 121px;">Status</strong>
                </label>
                <div class="Label" style="margin-top:-20px;"">
                        <asp:DropDownList ID="statusReraDropDown" runat="server"></asp:DropDownList>
                </div>
            </div>
        </div>
    <div class="actions">
        <div class="right">
            <asp:Button ID="Button2" runat="server" CssClass="submit" Text="Submit" OnClick="submitRera_Click"  />
        </div>
    </div>
    </div>


</section>
            <!-- End of #content -->
        </div>
        <!-- End of #main -->
        <!-- The footer -->
        <footer class="container_12">
<span class="grid_12">Copyright &copy; 2013 Netbiz Systems </span>
</footer>
        <!-- End of footer -->
        <!-- Spawn $$.loaded -->

        <script>
            $$.loaded();
        </script>

        <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
chromium.org/developers/how-tos/chrome-frame-getting-started -->
        <!--[if lt IE 7 ]>
<script defer src="http://ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
<script defer>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
<![endif]-->
    </form>
</body>
</html>
