﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Data.SqlClient;
using System.Data;

public partial class Admin_add_menu : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["godrejCPConnectionString"].ToString());
    GodejCPDataContext dataContext = new GodejCPDataContext();
    Guid random = Guid.NewGuid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["menuId"])))
            {
                FillData(Convert.ToString(Request.QueryString["menuId"]));
            }
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("MasterAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("MenuLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

    }

    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewmenu.aspx");
    }

    private void FillData(string menuid)
    {
        var objMenu = dataContext.Menus.Single(doc => doc.menuid == Convert.ToInt32(menuid));
        txtTitle.Text = objMenu.menutitle;
        txtPriority.Text = Convert.ToString(objMenu.Priority);
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["menuId"])))
        {
            var priority = (from obj in dataContext.Menus
                            select obj).Max(p => p.Priority);

            #region to check if same menu exists

            var objChk = (from obj in dataContext.Menus
                          where obj.menutitle == txtTitle.Text
                          select obj).FirstOrDefault();

            #endregion
            if (objChk != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Menu already exists.');</script>");
                return;
            }
            else
            {
                #region For Adding the Record
                Menu objMenu = new Menu();
                #region Save Image
                if (FileUpload1.HasFile)
                {
                    if (FileUpload1.PostedFile.ContentLength > 1048576)
                    {
                        //ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual Image size should not be more than 1MB');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Actual Image size should not be more than 1MB.');</script>", false);
                        return;
                    }
                    string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                    if (imgext.ToLower() == ".jpg" || imgext.ToLower() == ".jpeg" || imgext.ToLower() == ".png")
                    {
                        string fileName = Path.GetFileName(FileUpload1.FileName);
                        string fileNameEdited = random.ToString() + "_" + fileName;
                        float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
                        float height = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
                        if (width >= 50 && height >= 50)
                        {
                            FileUpload1.SaveAs(Server.MapPath("~/images/Menu/") + fileNameEdited);
                            objMenu.icon = fileNameEdited;
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please upload icon with minimum dimensions 50 x 50px');</script>");
                            return;
                        }
                    }
                    else
                    {
                        // ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif or .jpg or .png file');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload only .jpg,.jpeg,.png file.');</script>", false);
                        return;
                    }
                }
                else
                {
                    // ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload Image');</script>");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload Image');</script>", false);
                    return;
                }
                #endregion
                objMenu.menutitle = txtTitle.Text.Trim();
                objMenu.Priority = Convert.ToInt32(priority.Value + 1);
                objMenu.createddate = DateTime.Now;
                dataContext.Menus.InsertOnSubmit(objMenu);
                dataContext.SubmitChanges();
                Response.Redirect("viewmenu.aspx?save=1");
                #endregion
            }
        }
        else
        {
            #region Code For Updation
            int id = Convert.ToInt32(Session["menuId"]);
            var objMenu = dataContext.Menus.Single(doc => doc.menuid == id);
            #region Update Image
            if (FileUpload1.HasFile)
            {
                if (FileUpload1.PostedFile.ContentLength > 1048576)
                {
                    //ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual Image size should not be more than 1MB');</script>");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Actual Image size should not be more than 1MB.');</script>", false);

                    return;
                }
                string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                if (imgext.ToLower() == ".jpg" || imgext.ToLower() == ".jpeg" || imgext.ToLower() == ".png")
                {
                    string fileName = Path.GetFileName(FileUpload1.FileName);
                    string fileNameEdited = random.ToString() + "_" + fileName;

                    float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
                    float height = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
                    if (width >= 50 && height >= 50)
                    {
                        FileUpload1.SaveAs(Server.MapPath("~/images/Menu/") + fileNameEdited);
                        objMenu.icon = fileNameEdited;
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please upload icon with minimum dimensions 50 x 50px');</script>");
                        return;
                    }
                    try
                    {
                        if (ViewState["Image1"] != null && ViewState["Image1"].ToString() != "")
                            File.Delete(Server.MapPath(@"img/" + ViewState["Image1"].ToString()));
                    }
                    catch (Exception ex)
                    {
                    }
                }
                else
                {
                    //  ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif or .jpg or .png file');</script>");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload only .png file.');</script>", false);

                    return;
                }
            }
            #endregion
            objMenu.menutitle = txtTitle.Text.Trim();
            //objMenu.Priority = Convert.ToInt32(txtPriority.Text);
            dataContext.SubmitChanges();

            var query = (from obj in dataContext.Menus
                         where obj.menuid == id
                         select new
                         {
                             obj.Priority
                         }).SingleOrDefault();

            if (query != null)
            {
                int priority = Convert.ToInt32(query.Priority);
            }
            SqlCommand cmd = new SqlCommand("sp_menu_priority_update", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@currentsrno", query.Priority);
            cmd.Parameters.AddWithValue("@newsrno", Convert.ToInt32(txtPriority.Text));
            cmd.Parameters.AddWithValue("@menuid", Convert.ToInt32(id));
            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();
                int k = cmd.ExecuteNonQuery();
                if (con.State != ConnectionState.Closed)
                    con.Close();
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message.ToString());
            }

            Session["menuId"] = null;
            Response.Redirect("viewmenu.aspx?saved=1");
            #endregion
        }
        Div1.Style.Add("display", "block");
    }
}
