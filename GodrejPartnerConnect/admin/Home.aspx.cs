﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Net;
using System.Web.Script.Serialization;

public partial class Admin_Home : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["godrejcpmsConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        getToken();
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("BannerAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("BannerLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

        if (Convert.ToString(Request.QueryString["saved"]) == "1")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record updated successfully.');</script>");
        }
        if (Convert.ToString(Request.QueryString["save"]) == "1")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record added successfully.');</script>");
        }
    }

    protected void getToken() {
      using(WebClient client = new WebClient())
      {
        var reqparm = new System.Collections.Specialized.NameValueCollection();
        string PARM= "par1=value1&par=value2";
        ServicePointManager.Expect100Continue = true;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
        // string responsebytes = client.UploadString("https://test.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9Nvmjd9lcjRnoT4GG3E8o7ZbQcp3HqKaX6KsWkBg77OzU6SN.6oqr00W1pLR_P50oeF8xzGIk7RWT9TTA&client_secret=8491910721028248323&username=sachin_more@magicsoftware.com&password=Godrej@2018N96qqGVtAstcSut9a0dhqF2lo", PARM);
        // string responsebody = Encoding.UTF8.GetString(responsebytes);
        // var serializer = new JavaScriptSerializer();
        // var serializedResult = serializer.Serialize(responsebytes);
        // JObject json = JObject.Parse(str);
        // ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert(" + json + ");</script>");
      }
    }


    public int GetPriority(int productid)
    {
        int priority = 0;

        var obj = from u in dataContext.HomeBanners where (u.id == productid) select u;

        foreach (var item in obj)
        {
            priority = Convert.ToInt32(item.Priority);
        }

        return priority;
    }
}
