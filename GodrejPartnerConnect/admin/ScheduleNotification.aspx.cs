﻿using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ScheduleNotification : System.Web.UI.Page
{
    ScheduleNotificationController objCon = new ScheduleNotificationController();

    string userid = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");

        userid = Convert.ToString(Session["User"]);
        if (!IsPostBack)
        {
            BindGrid();
            BindTemplateDropDown();
        }

    }

    private void BindTemplateDropDown()
    {
        templateDropDown.Items.Add(new ListItem("", "0"));
        templateDropDown.Items.Add(new ListItem("CP-Meet", "1"));
        templateDropDown.Items.Add(new ListItem("New Launched Project Update", "2"));
        templateDropDown.Items.Add(new ListItem("Update about new feature", "3"));
        templateDropDown.Items.Add(new ListItem("Update in CP's personal information", "4"));
        templateDropDown.Items.Add(new ListItem("For new offers or Schemes for Channel Partner", "5"));
        templateDropDown.Items.Add(new ListItem("Festive Offers", "6"));
        templateDropDown.Items.Add(new ListItem("About Project", "7"));

        stateDropDOwn.Items.Add(new ListItem("All States", "All"));
        cityDropDown.Items.Add(new ListItem("All Cities", "All"));
        DataSet ds = objCon.GetRegionsPushNotifications("GET_STATE", null);
        foreach (DataRow row in ds.Tables[0].Rows)
        {
            stateDropDOwn.Items.Add(new ListItem(row["Registered_State"].ToString(), row["Registered_State"].ToString()));
        }
    }

    protected void getStateDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
        cityDropDown.Items.Clear();
        string selected_State = stateDropDOwn.SelectedValue.ToString().Replace(@"\r\n", "");
        if (selected_State != "All")
        {
            DataSet ds = objCon.GetRegionsPushNotifications("GET_CITY", selected_State);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                cityDropDown.Items.Add(new ListItem(row["Registered_City"].ToString(), row["Registered_City"].ToString()));
            }
        }
    }

    private void BindGrid()
    {
        divgrid.Visible = true;
        divDetail.Visible = false;
        DataSet ds = objCon.GetScheduleNotificationAll();
        grView.DataSource = ds.Tables[0];
        grView.DataBind();

    }

    protected void grView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grView.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void grView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        DataSet ds = objCon.GetScheduleNotificationAll();
        grView.DataSource = ds.Tables[0];
        grView.DataBind();
    }

    protected void grView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {
            lblID.Text = e.CommandArgument.ToString();
            Showfrom(lblID.Text);
        }
        else if (e.CommandName == "Delete")
        {
            DeleteById(e.CommandArgument.ToString());
        }
    }

    private void DeleteById(string id)
    {
        objCon.DeleteNotificationByID(id);
    }

    //void SendNotification(string id)
    //{
    //    DataSet ds = objCon.GetNotificationByID(id);
    //    string str = objCon.SendNotification(ds.Tables[0], userid);
    //    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('" + str + " Notification Send Successfully'); window.location='PushNotification.aspx';</script>");
    //}

    void Showfrom(string id)
    {
        DataSet ds = objCon.GetScheduleNotificationByID(id);
        if (ds.Tables[0].Rows.Count > 0)
        {
            DataRow row = ds.Tables[0].Rows[0];
            divDetail.Visible = true;
            divgrid.Visible = false;
            spTitle.InnerText = "View";
            txtNotificationBody.Enabled = false;
            txtNotificationTitle.Enabled = false;
            txtSubTitle.Enabled = false;
            btnSubmit.Visible = false;
            txtNotificationDate.Enabled = false;
            txtNotificationBody.Text = row["NotificationBody"].ToString();
            txtNotificationDate.Text = row["NotificationDate"].ToString();
            if(row["NotificationImage"].ToString() == "" || row["NotificationImage"].ToString() == null)
                Image1.ImageUrl = "~/admin/images/noimage.png";
            else
                Image1.ImageUrl = "~/" + row["NotificationImage"].ToString();
            txtNotificationTitle.Text = row["NotificationTitle"].ToString();
            txtSubTitle.Text = row["SubTitle"].ToString();
            txtScheduleDate.Text = row["ScheduleDate"].ToString();
            txtScheduleDate.Enabled = false;
            hideTemplate.Visible = false;
            hideCheckbox.Visible = false;
            hideFileUpload.Visible = false;
            hideScheduleDate.Visible = true;

        }
        else
        {
            Response.Redirect("ScheduleNotification.aspx");
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        divDetail.Visible = true;
        divgrid.Visible = false;
        lblID.Text = "0";
        spTitle.InnerText = "Add";
        hideScheduleDate.Visible = false;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        mScheduleNotification obj = new mScheduleNotification();
        obj.NotificationDate = txtNotificationDate.Text;       
        obj.NotificationID = lblID.Text;
        obj.NotificationTitle = txtNotificationTitle.Text;
        obj.NotificationBody = txtNotificationBody.Text;
        obj.SubTitle = txtSubTitle.Text;
        obj.IsRedirect = "N";
        obj.HasSend = "N";
        obj.ChannelType = "notification";
        obj.NotificationType = "M";
        obj.MobileUrl = "";
        obj.ChannelID = "0";
        obj.Active = "Y";
        obj.InsertedBy = userid;

        if (fileUpload.PostedFile.ContentLength != 0)
        {
            string[] supportFormts = { ".png", ".jpg", ".jpeg" };
            if (supportFormts.Contains(System.IO.Path.GetExtension(fileUpload.FileName).ToLower()))
            {
                string subfolder = "Admin/Uploads/NotificationImages/";
                string extn = System.IO.Path.GetExtension(fileUpload.FileName);
                string path = System.AppDomain.CurrentDomain.BaseDirectory;
                string newFileName = "IMG" + DateTime.Now.ToString("yyyyMMdd_HHmmsstt");

                string fullPath = path + subfolder + newFileName + extn;
                fileUpload.SaveAs(fullPath);
                obj.NotificationImage = subfolder + newFileName + extn;
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Invalid File Format. (Supported: .png, .jpg, .jpeg)'); window.location='ScheduleNotification.aspx';</script>");
                return;
            }
        }

        if (obj.NotificationDate == "")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Error while submitting. Please enter notification date.'); window.location='ScheduleNotification.aspx';</script>");
            return;
        }

        if (schedule7Days.Checked)
        {
            obj.ScheduleDate = Convert.ToDateTime(obj.NotificationDate).AddDays(-7).ToString();
            objCon.InsertUpdateScheduleNotification(obj);
        }
        if (schedule4Days.Checked)
        {
            obj.ScheduleDate = Convert.ToDateTime(obj.NotificationDate).AddDays(-4).ToString();
            objCon.InsertUpdateScheduleNotification(obj);
        }
        if (schedule1Day.Checked)
        {
            obj.ScheduleDate = Convert.ToDateTime(obj.NotificationDate).AddDays(-1).ToString();
            objCon.InsertUpdateScheduleNotification(obj);
        }
        if (schedule2Hrs.Checked)
        {
            obj.ScheduleDate = Convert.ToDateTime(obj.NotificationDate).AddHours(-2).ToString();
            objCon.InsertUpdateScheduleNotification(obj);
        }
        if (scheduleLast.Checked)
        {
            obj.ScheduleDate = Convert.ToDateTime(obj.NotificationDate).ToString();
            objCon.InsertUpdateScheduleNotification(obj);
        }

        if(!schedule7Days.Checked && !schedule4Days.Checked &&!schedule2Hrs.Checked && !schedule1Day.Checked && !scheduleLast.Checked)
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Error while submitting. Please select scheduling time.'); window.location='ScheduleNotification.aspx';</script>");
            return;
        }
        
        ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record Saved Successfully'); window.location='ScheduleNotification.aspx';</script>");
    }

    protected void templateDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (templateDropDown.SelectedValue.ToString() == "0")
        {
            txtNotificationTitle.Text = string.Empty;
            txtNotificationBody.Text = string.Empty;
        }
        else
        {
            DataSet ds = objCon.GetTemplateById(Convert.ToInt32(templateDropDown.SelectedValue));
            txtNotificationTitle.Text = ds.Tables[0].Rows[0]["TemplateTitle"].ToString();
            txtNotificationBody.Text = ds.Tables[0].Rows[0]["TemplateBody"].ToString();
        }
    }
}