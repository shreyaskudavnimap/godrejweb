﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuControl.ascx.cs" Inherits="Admin_MenuControl" %>
<link rel="shortcut icon" type="image/ico" href="images/favicon.ico" />
<link rel="shortcut icon" type="image/gif" href="images/favicon.gif" />
<aside>
    <div class="top">
        <nav>
            <% if (Convert.ToString(Session["type"]).Equals("SA") || Convert.ToString(Session["type"]).Equals("admin") || Convert.ToString(Session["type"]).Equals("subadmin"))
                { // Role_dev %>

            <ul class="collapsible accordion">
                <li id="UserLi" runat="server"><a id="UserAnchor" runat="server" href="viewuser.aspx">User Info</a> </li>
                <%-- <li id="PwdLi" runat="server"><a id="PwdAnchor" runat="server" href="SendPwd.aspx">
                    Send Password</a> </li>--%>
                <li id="BannerLi" runat="server"><a id="BannerAnchor" runat="server" href="viewbanner.aspx">New Launch</a> </li>
                <li id="MasterLi" runat="server"><a href="javascript:void(0);" id="MasterAnchor" runat="server">Master</a>
                    <ul>
                        <li id="CityLi" runat="server"><a href="viewcity.aspx"><span class="icon icon-list-alt"></span>City Master</a></li>
                        <li id="PlaceLi" runat="server"><a href="viewplace.aspx"><span class="icon icon-list-alt"></span>Place Master</a> </li>
                        <li id="MenuLi" runat="server"><a href="viewmenu.aspx"><span class="icon icon-list-alt"></span>Project Menu</a> </li>
                        <li id="RoomTypeLi1" runat="server"><a href="ViewRooms.aspx"><span class="icon icon-list-alt"></span>Room Master</a> </li>
                        <li id="RegionLi" runat="server"><a href="viewregion.aspx"><span class="icon icon-list-alt"></span>Region Master</a></li>
                        <li id="CampaignLi" runat="server"><a href="viewcampaign.aspx"><span class="icon icon-list-alt"></span>Campaign Master</a></li>
                    </ul>
                </li>
                <li id="ProjectsLi" runat="server"><a href="javascript:void(0);" id="ProjectAnchor" runat="server">Projects</a>
                    <ul>
                        <li id="ProjectDetailsLi" runat="server"><a href="viewprojectdetails.aspx"><span class="icon icon-list-alt"></span>
                            Project Details</a></li>
                        <li id="ProjectInnerDetailsLi" runat="server"><a href="viewprojectinnerdetails.aspx"><span class="icon icon-list-alt"></span>Project Inner Details</a> </li>
                        <li id="ProjectHighlights" runat="server"><a href="viewprojecthighlights.aspx"><span class="icon icon-list-alt"></span>Project Highlights</a> </li>
                        <li id="FloorPlansLi" runat="server"><a href="viewfloorplans.aspx"><span class="icon icon-list-alt"></span>Floor Plans</a> </li>
                        <li id="InnerFloorPlansLi" runat="server"><a href="viewinnerfloorplans.aspx"><span class="icon icon-list-alt"></span>Inner Floor Plans</a> </li>
                        <li id="GalleryLi" runat="server"><a href="viewgallery.aspx"><span class="icon icon-list-alt"></span>Gallery</a> </li>
                        <li id="ProjectVSRoomsLi" runat="server"><a href="viewProjectVSRooms.aspx"><span class="icon icon-list-alt"></span>Project VS Rooms</a></li>
                        <li id="StatusLi" runat="server"><a href="viewconstructionstatus.aspx"><span class="icon icon-list-alt"></span>Construction Status</a></li>
                        <li id="ContactsLi" runat="server"><a href="viewcontactus.aspx"><span class="icon icon-list-alt"></span>Contact Us</a></li>
                    </ul>
                </li>
                <li id="EnquiryLi" runat="server"><a href="javascript:void(0);" id="EnquiryAnchor" runat="server">Enquiry</a>
                    <ul>
                        <li id="MassageLi" runat="server"><a id="MessageAnchor" runat="server" href="ViewMessage.aspx"><span class="icon icon-list-alt"></span>
                            Messages</a> </li>
                        <li id="FaqLi" runat="server"><a id="FaqAnchor" runat="server" href="ViewFaq.aspx"><span class="icon icon-list-alt"></span>
                            FAQ</a> </li>
                        <li id="ExpertLi" runat="server"><a id="ExpertAnchor" runat="server" href="ViewAskExpert.aspx"><span class="icon icon-list-alt"></span>
                            Ask GPL Expert</a> </li>
                    </ul>
                </li>
                <li id="EventsLi" runat="server"><a id="EventsAnchor" runat="server" href="viewevents.aspx">Events</a> </li>
                <li id="NewsLi" runat="server"><a id="NewsAnchor" runat="server" href="viewnewsfeed.aspx">News Feed</a> </li>
                <li id="OffersLi" runat="server"><a id="OffersAnchor" runat="server" href="viewoffers.aspx">Offers and Schemes </a></li>
                <li id="PropertyLi" runat="server"><a id="PropertyAnchor" runat="server" href="viewproperty.aspx">Property Updates </a>
                </li>
                <li id="ApptLi" runat="server"><a id="ApptAnchor" runat="server" href="viewrequest.aspx">Appointment Request</a> </li>
                <li id="VendorLi" runat="server"><a id="Vendoranchor" runat="server" href="ViewVendorCode.aspx">Update Vendor Code</a> </li>
                <li id="SubAdminLi" runat="server"><a id="SubAdmin" runat="server" href="ViewSubAdmin.aspx">Sub Admin Creation </a></li>
                <li id="DataDetailsLi" runat="server"><a id="DataAnchor" runat="server" href="datadetails.aspx">Add Document </a></li>
                <li id="RegisterLi" runat="server" visible="false"><a id="RegisterAnchor" runat="server" href="View-Registration.aspx">Registration</a> </li>
                <li id="EmplLi" runat="server"><a id="EmplAnchor" runat="server" href="ViewEmpanelment.aspx">View Empanelment</a> </li>
                <li id="Li2" runat="server"><a id="A1" runat="server" href="ProfileChangeRequest.aspx">View Profile Change Request</a> </li>
                <li id="CPKitLi" runat="server"><a id="CPKitAnchor" runat="server" href="ViewCPKit.aspx">CP Kit</a> </li>
                <li id="CampaignDocLi" runat="server"><a id="CampaignDocAnchor" runat="server" href="viewcampaigndocs.aspx">Campaign Details</a> </li>
                <li id="GPLPolicycLi" runat="server"><a id="GPLPolicyAnchor" runat="server" href="viewgplpolicy.aspx">GPL Policy</a> </li>
                <li id="pushNotificationLi" runat="server"><a id="PushNotification" runat="server" href="pushNotification.aspx">Push Notification</a></li>
                <li id="ScheduleNotificationLi" runat="server"><a id="ScheduleNotification" runat="server" href="ScheduleNotification.aspx">Schedule Notification</a></li>
            </ul>

            <% } %>

            <% if (Convert.ToString(Session["type"]).Equals("callcentre"))
                { // Role_dev %>
            <ul class="accordion">
            <li id="Li10" runat="server"><a href="javascript:void(0);" id="A4" runat="server">Menu</a>
                <ul>
                    <li id="Li11" runat="server"><a href="empanelmentform.aspx"><span class="icon icon-list-alt"></span>
                        Empanelment Form</a></li>
                    <li id="Li1" runat="server"><a href="viewempanelmenthistory.aspx"><span class="icon icon-list-alt"></span>
                        View History</a></li>
                  </ul>
            </li>
            

            </ul>
            <% } %>
        </nav>
    </div>
</aside>
