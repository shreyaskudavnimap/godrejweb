﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;

public partial class View_Registration : System.Web.UI.Page
{
    GridView dgExcel = new GridView();
    GodejCPDataContext dataContext = new GodejCPDataContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        BindGrid("");
        if (Convert.ToString(Session["type"]) == "subadmin")
        {
            HtmlControl User = (HtmlControl)Menu.FindControl("UserLi");
            User.Visible = false;
            HtmlControl Launch = (HtmlControl)Menu.FindControl("BannerLi");
            Launch.Visible = false;
            HtmlControl Master = (HtmlControl)Menu.FindControl("MasterLi");
            Master.Visible = false;
            HtmlControl Projects = (HtmlControl)Menu.FindControl("ProjectsLi");
            Projects.Visible = false;
            HtmlControl Enquiry = (HtmlControl)Menu.FindControl("EnquiryLi");
            Enquiry.Visible = false;
            HtmlControl Events = (HtmlControl)Menu.FindControl("EventsLi");
            Events.Visible = false;
            HtmlControl News = (HtmlControl)Menu.FindControl("NewsLi");
            News.Visible = false;
            HtmlControl Offers = (HtmlControl)Menu.FindControl("OffersLi");
            Offers.Visible = false;
            HtmlControl Property = (HtmlControl)Menu.FindControl("PropertyLi");
            Property.Visible = false;
            HtmlControl SubAdmin = (HtmlControl)Menu.FindControl("SubAdminLi");
            SubAdmin.Visible = false;
            //HtmlControl PwdAnchor = (HtmlControl)Menu.FindControl("PwdLi");
            //PwdAnchor.Visible = false;
            HtmlControl DataAnchor = (HtmlControl)Menu.FindControl("DataDetailsLi");
            DataAnchor.Visible = false;
            HtmlControl ApptAnchor = (HtmlControl)Menu.FindControl("ApptLi");
            ApptAnchor.Visible = false;
            HtmlControl VendorAnchor = (HtmlControl)Menu.FindControl("VendorLi");
            VendorAnchor.Visible = false;
            HtmlControl RegisterAnchor = (HtmlControl)Menu.FindControl("RegisterLi");
            RegisterAnchor.Visible = true;
        }

        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("RegisterAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("RegisterLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

        if (Convert.ToString(Request.QueryString["saved"]) == "1")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record updated successfully.');</script>");
        }
        if (Convert.ToString(Request.QueryString["save"]) == "1")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record added successfully.');</script>");
        }
        txtSearch.Attributes.Add("onKeyPress", "doClick('" + btnSubmit.ClientID + "',event)");
    }


    protected void btnExport_Click(object sender, EventArgs e)
    {
        string style = @"<style> .text { mso-number-format:\@; } </script> ";
        Response.ClearContent();
        Response.AddHeader("content-disposition", "attachment;filename=User_Details.xls");
        Response.ContentType = "application/excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        LoadExcel(txtSearch.Text);

        dgExcel.RenderControl(htw);
        // Style is added dynamically
        Response.Write(style);
        Response.Write(sw.ToString());
        Response.End();
    }

    public void LoadExcel(string searchtext)
    {
        string search = "";
        if (searchtext != "" && searchtext != "search by Name")
            search = searchtext;


        var objUser = from obj in dataContext.tbldrawRegistrations
                      orderby obj.id descending
                      select new
                      {
                          Name = obj.name,
                          Email = obj.emailid,
                          Mobile = obj.mobile,
                          Companyname=obj.companyname,

                      };

        if (searchtext != "" && searchtext != "search by user")
            objUser = objUser.Where(d => d.Name.Contains(searchtext));

        if (objUser.Count() > 0)
        {
            dgExcel.DataSource = objUser;
            dgExcel.DataBind();
        }
        else
        {
            dgExcel.DataSource = objUser;
            dgExcel.DataBind();
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (txtSearch.Text != "" && txtSearch.Text != "search by user")
        {
            BindGrid(txtSearch.Text);
        }
    }

    private void BindGrid(string searchtext)
    {
        var objUser = from obj in dataContext.tbldrawRegistrations
                      orderby obj.createdate descending
                      select new
                      {
                          obj.id,
                          obj.name,
                          obj.emailid,
                          obj.mobile,
                          obj.companyname,
                          obj.createdate
                      };

        if (searchtext != "" && searchtext != "search by user")
            objUser = objUser.Where(d => d.name.Contains(searchtext));

        if (objUser.Count() > 0)
        {
            lblMsg.Visible = false;
            lblMsg.Text = "";
            grdDetails.DataSource = objUser;
            grdDetails.DataBind();
        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "<br/>No records found.";
            grdDetails.DataSource = objUser;
            grdDetails.DataBind();
        }
    }

    protected void grdDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdDetails.PageIndex = e.NewPageIndex;
        BindGrid(txtSearch.Text);
    }
}