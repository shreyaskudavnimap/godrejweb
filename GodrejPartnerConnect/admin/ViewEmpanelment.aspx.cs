﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using System.Net.Mail;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Collections.Generic;

public partial class Admin_ViewEmpanelment : System.Web.UI.Page
{
    string brokerId;
    JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
    GodejCPDataContext dataContext = new GodejCPDataContext();
    string ConStr = ConfigurationManager.ConnectionStrings["godrejCPConnectionString"].ConnectionString;


    public class Empanelment
    {
        public string Password
        { get; set; }

        public string Company_Name
        { get; set; }

        public string entity_type
        { get; set; }

        public string Name
        { get; set; }

        public string Mobile
        { get; set; }

        public string Email
        { get; set; }

        public string communication_address
        { get; set; }

        public string registered_address
        { get; set; }

        public string Pan_No
        { get; set; }

        public string pancerti
        { get; set; }


        public string BrokerID
        { get; set; }


        public string BrokerContactID
        { get; set; }

        public string status
        { get; set; }


        public string Billing_City
        { get; set; }


        public string Billing_Zip
        { get; set; }


        public string Billing_Street
        { get; set; }


        public string Billing_State
        { get; set; }


        public string Billing_Country
        { get; set; }


        public string Registered_City
        { get; set; }


        public string Registered_Zip
        { get; set; }


        public string Registered_Street
        { get; set; }

        public string Registered_State
        { get; set; }


        public string Registered_Country
        { get; set; }


        public string Communication_City
        { get; set; }


        public string Communication_Zip
        { get; set; }


        public string Communication_Street
        { get; set; }


        public string Communication_State
        { get; set; }


        public string Communication_Country
        { get; set; }
        public string BrokerAccountID
        { get; set; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        if (Convert.ToString(Session["type"]) == "admin")
        {
            HtmlControl User = (HtmlControl)Menu.FindControl("UserLi");
            User.Visible = false;
            HtmlControl Launch = (HtmlControl)Menu.FindControl("BannerLi");
            Launch.Visible = false;
            HtmlControl Master = (HtmlControl)Menu.FindControl("MasterLi");
            Master.Visible = false;
            HtmlControl Projects = (HtmlControl)Menu.FindControl("ProjectsLi");
            Projects.Visible = false;
            HtmlControl Enquiry = (HtmlControl)Menu.FindControl("EnquiryLi");
            Enquiry.Visible = false;
            HtmlControl Events = (HtmlControl)Menu.FindControl("EventsLi");
            Events.Visible = false;
            HtmlControl News = (HtmlControl)Menu.FindControl("NewsLi");
            News.Visible = false;
            HtmlControl Offers = (HtmlControl)Menu.FindControl("OffersLi");
            Offers.Visible = false;
            HtmlControl Property = (HtmlControl)Menu.FindControl("PropertyLi");
            Property.Visible = false;
            HtmlControl SubAdmin = (HtmlControl)Menu.FindControl("SubAdminLi");
            SubAdmin.Visible = false;
            //HtmlControl PwdAnchor = (HtmlControl)Menu.FindControl("PwdLi");
            //PwdAnchor.Visible = false;
            HtmlControl DataAnchor = (HtmlControl)Menu.FindControl("DataDetailsLi");
            DataAnchor.Visible = false;
        }

        if (!IsPostBack)
        {
            divValidate.Visible = false;
            BindGrid("");
            if (Convert.ToString(Request.QueryString["saved"]) == "1")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record updated successfully.');</script>");
            }
            if (Convert.ToString(Request.QueryString["save"]) == "1")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record added successfully.');</script>");
            }
        }

        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("EmplAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("EmplLi");
        //openAnchor.Attributes.Add("class", "open");
        //selectMenu.Attributes.Add("class", "current");


        txtSearch.Attributes.Add("onKeyPress", "doClick('" + btnSubmit.ClientID + "',event)");

        //SendMail("test", "test", "anaghasht@gmail.com", "test123");
    }



    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (txtSearch.Text != "" && txtSearch.Text != "search by name")
        {
            BindGrid(txtSearch.Text);
        }
    }

    protected void editSubmit_Click(object sender, EventArgs e)
    {
        var empId = txtEmpId.Value;
        var newEmail = txtEmail.Text;
        if (newEmail != "" && newEmail != null)
        {
            int ResponseCode = 0;
            SqlConnection con = new SqlConnection(ConStr);
            try
            {
                SqlCommand cmd = new SqlCommand("usp_EditViewPanelment", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmpId", empId);
                cmd.Parameters.AddWithValue("@Email", newEmail);
                con.Open();

                cmd.ExecuteScalar();
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record updated successfully.');</script>");
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Error while submitting changes.');</script>");
            }
        }
        else
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter correct email.');</script>");
        }
    }


    private void BindGrid(string searchtext)
    {
        divForm.Visible = false;
        divGrid.Visible = true;
        divEdit.Visible = false;
        var objEmpl = from obj in dataContext.tbl_Empanelments
                      orderby obj.EmplID descending
                      //where obj.Status == "null"
                      select new
                      {
                          obj.FirstName,
                          obj.LastName,
                          obj.Email,
                          obj.Mobile,
                          obj.EmplID,
                          obj.Pan_No,
                          obj.EntityType,
                          obj.Status,
                          obj.RequestedOn,
                          obj.RequestUpdatedOn,
                          obj.RegisteredOn
                      };

        if (searchtext != "" && searchtext != "search by name")
        {
            objEmpl = objEmpl.Where(d => d.FirstName.Contains(searchtext) || d.LastName.Contains(searchtext)                        // Shreyas 30.07.2020 Search_All
                                    || d.Email.Contains(searchtext) || d.Mobile.Contains(searchtext)
                                    || d.Pan_No.Contains(searchtext) || d.Status.Contains(convertStatus(searchtext))
                                    || d.RegisteredOn.ToString().Contains(searchtext) || d.RequestedOn.ToString().Contains(searchtext)
                                    );
        }

        if (objEmpl.Count() > 0)
        {
            lblMsg.Visible = false;
            lblMsg.Text = "";
            grdDetails.DataSource = objEmpl;
            grdDetails.DataBind();
        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "<br/>No records found.";
            grdDetails.DataSource = objEmpl;
            grdDetails.DataBind();
        }
    }

    private string convertStatus(string searchtext)
    {
        if (searchtext == "Approved")
        {
            return "A";
        }
        else if (searchtext == "Rejected")
        {
            return "R";
        }
        else if (searchtext == "Pending")
        {
            return "P";
        }
        else
        {
            return searchtext;
        }
    }

    protected void grdDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdDetails.PageIndex = e.NewPageIndex;
        BindGrid(txtSearch.Text);
    }
    private void BindGstGrid(int EmplID, int pageIndex)
    {
        var objGstEmpl = from obj in dataContext.Empl_GstDocs
                         where obj.EmplID == Convert.ToInt32(EmplID) && obj.Status != "Deleted"
                         orderby obj.State
                         select new
                         {
                             obj.State,
                             obj.Gst_no,
                             obj.Gst_Certi,
                             obj.EmplID,
                             obj.ID,
                             obj.Status,
                             obj.Comment
                         };

        if (objGstEmpl.Count() > 0)
        {
            lblMsgGst.Visible = false;
            lblMsgGst.Text = "";
            gdvGst.DataSource = objGstEmpl;
            gdvGst.PageIndex = pageIndex;
            gdvGst.DataBind();
        }
        else
        {
            lblMsgGst.Visible = true;
            lblMsgGst.Text = "<br/>No records found.";
            gdvGst.DataSource = objGstEmpl;
            gdvGst.DataBind();
        }
    }
    private void BindReraGrid(int EmplID, int pageIndex)
    {
        var objReraEmpl = from obj in dataContext.Empl_ReraDocs
                          where obj.EmplID == Convert.ToInt32(EmplID) && obj.Status != "Deleted"
                          orderby obj.State
                          select new
                          {
                              obj.State,
                              obj.Rera_no,
                              obj.Rera_Certi,
                              obj.EmplID,
                              obj.ID,
                              obj.Status,
                              obj.Comment
                          };

        if (objReraEmpl.Count() > 0)
        {
            lblMsgRera.Visible = false;
            lblMsgRera.Text = "";
            gdvRera.DataSource = objReraEmpl;
            gdvRera.PageIndex = pageIndex;
            gdvRera.DataBind();
        }
        else
        {
            lblMsgRera.Visible = true;
            lblMsgRera.Text = "<br/>No records found.";
            gdvRera.DataSource = objReraEmpl;
            gdvRera.DataBind();
        }
    }

    protected void grdDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {
            int EmplID = Convert.ToInt32(e.CommandArgument);
            Showfrom(EmplID);
            divValidate.Visible = true;
        }
        if (e.CommandName == "EditRow")
        {
            var data = e.CommandArgument.ToString().Split(',');
            string empId = data[0];
            string email = data[1];
            txtEmpId.Value = empId;
            txtEmail.Text = email;
            divEdit.Visible = true;
            divGrid.Visible = false;
            divForm.Visible = false;
        }

        if (e.CommandName == "ViewPan")
        {
            #region Code For Updation

            int id = Convert.ToInt32(e.CommandArgument);
            var objEmpa = dataContext.tbl_Empanelments.Single(doc => doc.EmplID == id);
            var url = ConfigurationManager.AppSettings["BaseUrl"] + ConfigurationManager.AppSettings["AttachmentPhysicalPath"] + objEmpa.Pan_Certi;
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('" + url + "');", true);
            // Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('" + url + "','_newtab');", true);
            // myframe.Attributes["src"] = url;

            // divDoc.Style.Remove("display");
            #endregion
        }
        //if (e.CommandName == "PushSales")
        //{
        //}
        if (e.CommandName == "ViewGst")
        {
            #region Code For Updation

            int id = Convert.ToInt32(e.CommandArgument);
            BindGstGrid(id, 0);
            //var objcontact = dataContext.tbl_EmpanelmentContacts.Single(doc => doc.EmplID == id);

            //objcontact.Status = "Reject";
            //dataContext.SubmitChanges();

            ////send data to sfdc
            ////"IncomeTaxregistrationnumber=" + "NA" + "&LicenseNumber=" + "NA" + "&Description=" + "NA" + "&Phone=" + "NA" + "&Pannumber=" + objcontact.Pan_No + "&Name=" + objcontact.Name + "&BrokerType=NA&BrokerCode=" + objcontact.EmplID + "&ServiceTax=" + objcontact.Service_Tax_No + "&active=NA&Fristname=" + objcontact.Name + "&lastname=&Mobile=" + objcontact.Mobile + "&Email=" + objcontact.Email;
            //string urlnew = "http://gplpartnerconnect.com/GodrejPartnerConnect/lead_submit_website_new.php?" + "IncomeTaxregistrationnumber=" + "NA" + "&LicenseNumber=" + "NA" + "&Description=" + "NA" + "&Phone=" + "NA" + "&Pannumber=" + objcontact.Pan_No + "&Name=" + objcontact.Name + "&BrokerType=NA&BrokerCode=" + objcontact.EmplID + "&ServiceTax=" + objcontact.Service_Tax_No + "&active=NA&Fristname=" + objcontact.Name + "&lastname=&Mobile=" + objcontact.Mobile + "&Email=" + objcontact.Email;
            ////" + "title=" + "NA" + "&firstName=" + txtFirstName.Text + "&lastname=" + txtLastName.Text + "&city=" + "NA" + "&state=" + "NA" + "&Country=" + ddlCountry.SelectedItem.Text + "&phoneNo=" + mb + "&email=" + txtEmailID.Text + "&prjcode=" + Projectcode + "&comment=" + "NA" + "&visitDate=" + "'2015-07-07T11:11:11" + "&websiteName=" + "m.godrejproperties.com" + "&addCode=" + addcode + "&type=" + "WAP" + "&Src=" + "WAP" + "&CountryCode=" + "NA" + "&rating=" + "Hot" + "&trnsId=" + "NA" + "&trnsStatus=" + "NA";
            //HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(urlnew);
            //request1.KeepAlive = false;
            //request1.ProtocolVersion = HttpVersion.Version10;
            //request1.Method = "GET";
            //request1.Timeout = 30000;
            //HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
            //var brokerid = response1.Headers["brokerid"];


            //objcontact.Status = "Approve";
            //objcontact.BrokerID = brokerid;
            //dataContext.SubmitChanges();

            //ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data deleted successfully');</script>");
            //BindGrid("");

            #endregion
        }
        if (e.CommandName == "ViewRera")
        {
            int id = Convert.ToInt32(e.CommandArgument);

            BindReraGrid(id, 0);
            //var objcontact = dataContext.tbl_EmpanelmentContacts.Single(doc => doc.EmplID == id);

            //objcontact.Status = "Reject";
            //dataContext.SubmitChanges();
            //ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data deleted successfully');</script>");
            //BindGrid("");
        }
    }

    private void GenerateJsonFile(int EmplID)
    {
        var Empldetails = dataContext.tbl_Empanelments.Single(doc => doc.EmplID == EmplID);
        var body = new
        {
            ei = new
            {
                Name = Empldetails.Company_Name,
                Pannumber = Empldetails.Pan_No,
                GSTIN = "",// objGST.Gst_no,
                GSTINState = "",//objGST.State,
                BrokerType = "",
                BrokerCategory = "RCP",
                CompanyCategory = "",
                MobileNo = Empldetails.Mobile,
                Phone = Empldetails.Mobile,
                BrokerEntityType = Empldetails.EntityType,
                RegionName = "",
                EmailId = Empldetails.Email,
                Empanelment = "EMPL" + Empldetails.EmplID.ToString(),
                BillingCity = Empldetails.Billing_City,
                BillingZipPostal = Empldetails.Billing_Zip,
                BillingStreet = Empldetails.Billing_Street,
                BillingStateProvince = Empldetails.Billing_State,
                BillingCountry = Empldetails.Billing_Country,
                RegisteredCity = Empldetails.Registered_City,
                RegisteredZipPostal = Empldetails.Registered_Zip,
                RegisteredStreet = Empldetails.Registered_Street,
                RegisteredStateProvince = Empldetails.Registered_State,
                RegisteredCountry = Empldetails.Registered_Country,
                CommunicationCity = Empldetails.Communication_City,
                CommunicationZipPostal = Empldetails.Communication_Zip,
                CommunicationStreet = Empldetails.Communication_Street,
                CommunicationStateProvince = Empldetails.Communication_State,
                CommunicationCountry = Empldetails.Communication_Country,
                sourcedBy = "CC - SEC"

            }
        };

        var jsonText = JsonConvert.SerializeObject(body);
        //jsonText = Encoding.ASCII.GetString(
        //        Encoding.Convert(
        //            Encoding.UTF8,
        //            Encoding.GetEncoding(
        //                Encoding.ASCII.EncodingName,
        //                new EncoderReplacementFallback(" "),
        //                new DecoderExceptionFallback()
        //                ),
        //            Encoding.UTF8.GetBytes(jsonText)
        //        )
        //    );

        string rootPhysicalPath = "";
        if (System.Web.HttpRuntime.AppDomainAppId != null)
        {
            rootPhysicalPath = System.Web.HttpContext.Current.Server.MapPath("~");
        }
        else
        {
            rootPhysicalPath = "";
        }
        rootPhysicalPath = rootPhysicalPath + @"admin\Log\JsonValidate\";
        if (!Directory.Exists(rootPhysicalPath))
        {
            Directory.CreateDirectory(rootPhysicalPath);
        }
        jsonText = "\n" + jsonText;

        string filename = "JSONDATA" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString()
             + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + ".docx";
        string filePath = rootPhysicalPath + filename;
        using (StreamWriter writer = new StreamWriter(filePath, true))
        {
            writer.Write(jsonText);
        }

        string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/');

        Response.ContentType = "application/octet-stream";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
        Response.TransmitFile(Server.MapPath("~/admin/Log/JsonValidate/" + filename));
        Response.End();
    }

    protected void ExportJson(object sender, CommandEventArgs e)
    {
        int emplid = Convert.ToInt32(lblEmpanelmentID.Text.Replace("EMP", ""));
        GenerateJsonFile(emplid);
    }

    protected void gdvGst_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ViewGst")
        {
            #region Code For Updation

            int id = Convert.ToInt32(e.CommandArgument);
            var objGST = dataContext.Empl_GstDocs.Single(doc => doc.ID == id);
            var url = ConfigurationManager.AppSettings["BaseUrl"] + ConfigurationManager.AppSettings["AttachmentPhysicalPath"] + objGST.Gst_Certi;
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('" + url + "');", true);
            // Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('" + url + "','_newtab');", true);
            // myframe.Attributes["src"] = url;

            // divDoc.Style.Remove("display");
            #endregion
        }
        if (e.CommandName == "Approve")
        {
            #region Code For Updation

            int id = Convert.ToInt32(e.CommandArgument);
            var objGST = dataContext.Empl_GstDocs.Single(doc => doc.ID == id);
            var Empldetails = dataContext.tbl_Empanelments.Single(doc => doc.EmplID == objGST.EmplID);
            GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            TextBox txtComment = (TextBox)gvr.FindControl("txtComment");
            objGST.Comment = txtComment.Text;

            objGST.Status = "Approve";
            //send data to sfdc
            dataContext.SubmitChanges();
            //Empanelment emp = GetCPProperties(Empldetails.EmplID);
            //List<string> val;
            //string sendURL = ConfigurationManager.AppSettings["createAccount"]; //"https://godrej--test.cs5.my.salesforce.com/services/apexrest/RestWebToBrokerCreation";
            //string updateURL = ConfigurationManager.AppSettings["updateAccount"];//"https://godrej--test.cs5.my.salesforce.com/services/apexrest/RestBrokerUpdate";
            //var body = new
            //{
            //    ei = new
            //    {
            //        Name = Empldetails.Company_Name,
            //        Pannumber = Empldetails.Pan_No,
            //        GSTIN = objGST.Gst_no,
            //        GSTINState = objGST.State,
            //        BrokerType = "",
            //        BrokerCategory = "",
            //        CompanyCategory = "",
            //        MobileNo = Empldetails.Mobile,
            //        Phone = Empldetails.Mobile,
            //        BrokerEntityType = Empldetails.EntityType,
            //        RegionName = "",
            //        EmailId = Empldetails.Email,
            //        Empanelment = Empldetails.EmplID,
            //        BillingCity = emp.Billing_City,
            //        BillingZipPostal = emp.Billing_Zip,
            //        BillingStreet = emp.Billing_Street,
            //        BillingStateProvince = emp.Billing_State,
            //        BillingCountry = emp.Billing_Country,
            //        RegisteredCity = emp.Registered_City,
            //        RegisteredZipPostal = emp.Registered_Zip,
            //        RegisteredStreet = emp.Registered_Street,
            //        RegisteredStateProvince = emp.Registered_State,
            //        RegisteredCountry = emp.Registered_Country,
            //        CommunicationCity = emp.Communication_City,
            //        CommunicationZipPostal = emp.Communication_Zip,
            //        CommunicationStreet = emp.Communication_Street,
            //        CommunicationStateProvince = emp.Communication_State,
            //        CommunicationCountry = emp.Communication_Country,

            //    }
            //};
            //var dataString = JsonConvert.SerializeObject(body);
            //var response = sendRequest(dataString, sendURL);
            //val = jsSerializer.Deserialize<List<string>>(response);
            //brokerId = val[1];
            //string FirstName = Empldetails.Name.ToString().Split(' ')[0];
            //string LastName = Empldetails.Name.ToString().Split(' ')[1];
            //List<string> contactVal;
            //var contactResponse = createOrUpdateContact(FirstName, LastName, brokerId, Empldetails.Email, Empldetails.Mobile, emp.Registered_Country,
            //    emp.Communication_City, emp.Communication_Zip, emp.Communication_Street, emp.Communication_State, emp.Communication_Country);
            //contactVal = jsSerializer.Deserialize<List<string>>(contactResponse);
            //string brokerContactId = contactVal[1];

            //UpdateAccountContactId(Empldetails.EmplID,brokerId, brokerContactId);
            //if ( val[0].Contains("already"))
            //{
            //    var newBody = new
            //    {
            //        ei = new
            //        {
            //            brokerid = brokerId,
            //            Name = Empldetails.Name,
            //        }
            //    };
            //    var newDataString = JsonConvert.SerializeObject(newBody);
            //    var newResponse = sendRequest(newDataString, updateURL);
            //}
            ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Record Approved successfully');</script>");
            BindGstGrid(objGST.EmplID, 0);

            #endregion
        }
        if (e.CommandName == "Reject")
        {
            int id = Convert.ToInt32(e.CommandArgument);
            var objGST = dataContext.Empl_GstDocs.Single(doc => doc.ID == id);


            GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            TextBox txtComment = (TextBox)gvr.FindControl("txtComment");
            if (txtComment.Text == "")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Comment is required for rejection');</script>");
                return;
            }
            objGST.Comment = txtComment.Text;
            objGST.Status = "Reject";
            //objGST.BrokerID = brokerid;
            //var objcontact = dataContext.tbl_EmpanelmentContacts.Single(doc => doc.EmplID == id);

            var Empldetail = dataContext.tbl_Empanelments.Single(doc => doc.EmplID == objGST.EmplID);
            Empldetail.Status = "R";
            dataContext.SubmitChanges();

            //SendRejectMail(Empldetail.FirstName + ' ' + Empldetail.LastName, Empldetail.Email);
            //objcontact.Status = "Reject";
            //dataContext.SubmitChanges();
            ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Record rejected successfully');</script>");
            BindGstGrid(objGST.EmplID, 0);
            lblStatus.Text = "R";
            lblDisplayStatus.Text = "Rejected";
        }
    }
    protected void gdvRera_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ViewDoc")
        {
            #region Code For Updation

            int id = Convert.ToInt32(e.CommandArgument);
            var objGST = dataContext.Empl_ReraDocs.Single(doc => doc.ID == id);
            var url = ConfigurationManager.AppSettings["BaseUrl"] + ConfigurationManager.AppSettings["AttachmentPhysicalPath"] + objGST.Rera_Certi;
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('" + url + "');", true);
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open(" + url + ",'_newtab');", true);
            //myframe.Attributes["src"] = url ;

            //divDoc.Style.Remove("display");
            #endregion
        }
        if (e.CommandName == "Approve")
        {
            #region Code For Updation

            int id = Convert.ToInt32(e.CommandArgument);

            var objRera = dataContext.Empl_ReraDocs.Single(doc => doc.ID == id);

            var result = System.Linq.Enumerable.ToList((from N in dataContext.Empl_ReraDocs
                                                        where N.EmplID == objRera.EmplID && N.Status == "Approve"
                                                        select new { N.EmplID }));
            var Empldetails = dataContext.tbl_Empanelments.Single(doc => doc.EmplID == objRera.EmplID);
            GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            TextBox txtComment = (TextBox)gvr.FindControl("txtComment");
            objRera.Comment = txtComment.Text;
            objRera.Status = "Approve";
            //objGST.BrokerID = brokerid;
            dataContext.SubmitChanges();


            //JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            //string sendURL = ConfigurationManager.AppSettings["ReraApprove"];//"https://godrej--test.cs5.my.salesforce.com/services/apexrest/api/BrokerReradetail";
            //var body = new
            //{
            //    ei = new
            //    {
            //        pannumber = Empldetails.Pan_No,
            //        reranumber = objRera.Rera_no,
            //        statecode = objRera.State
            //    }
            //};
            //var dataString = JsonConvert.SerializeObject(body);
            //var response = sendRequest(dataString, sendURL);
            //// <<<<<<<<<<<<   ======================== comment this code by sujata =======================================>>>>>>
            //if (result.Count > 0)
            //{
            //}
            //else
            //{
            //    var Empldetail = dataContext.tblEmpanelmentDetails.Single(doc => doc.EmplID == objRera.EmplID);
            //    string[] name = Empldetail.Name.ToString().Split(' ');
            //    string password = name[0] + GenerateRandomString(5);
            //    Console.Write(Empldetail);
            //    ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>console.log(" + Empldetail + ");</script>");

            //    //if (Empldetail.id != "")
            //    //{
            //    tblEmpanelment objEmpl = new tblEmpanelment();

            //    objEmpl.Name = Empldetail.Name;
            //    objEmpl.Mobile = Empldetail.Mobile;
            //    objEmpl.Email = Empldetail.Email;

            //    objEmpl.Pan_No = Empldetail.Pan_No;
            //    objEmpl.Company_Name = Empldetail.Company_Name;
            //    objEmpl.username = Empldetail.Email;
            //    objEmpl.password = password;
            //    objEmpl.createddate = DateTime.Now;
            //    objEmpl.Status = "A";
            //    objEmpl.RefEmplID = objRera.EmplID;
            //    //dataContext.tblEmpanelments.InsertOnSubmit(objEmpl);
            //    //dataContext.SubmitChanges();
            //    //}
            //    UpdateInsertTblEmpanelments(objEmpl);
            //    SendMail(Empldetail.Name, Empldetail.Email, password);
            //}
            ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Record Approved successfully');</script>");
            BindReraGrid(objRera.EmplID, 0);

            #endregion
        }
        if (e.CommandName == "Reject")
        {
            int id = Convert.ToInt32(e.CommandArgument);
            var objRera = dataContext.Empl_ReraDocs.Single(doc => doc.ID == id);


            GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            TextBox txtComment = (TextBox)gvr.FindControl("txtComment");
            if (txtComment.Text == "")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Comment is required for rejection');</script>");
                return;
            }
            objRera.Comment = txtComment.Text;
            objRera.Status = "Reject";

            //objGST.BrokerID = brokerid;
            //dataContext.SubmitChanges();
            //var objcontact = dataContext.tbl_EmpanelmentContacts.Single(doc => doc.EmplID == id);

            var Empldetail = dataContext.tbl_Empanelments.Single(doc => doc.EmplID == objRera.EmplID);
            Empldetail.Status = "R";
            dataContext.SubmitChanges();
            //SendRejectMail(Empldetail.FirstName + ' ' + Empldetail.FirstName, Empldetail.Email);
            //objcontact.Status = "Reject";
            //dataContext.SubmitChanges();
            ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Record rejected successfully');</script>");
            BindReraGrid(objRera.EmplID, 0);
            lblStatus.Text = "R";
            lblDisplayStatus.Text = "Rejected";
        }
    }

    public Empanelment GetCPProperties(int emplId)
    {
        Empanelment empObj = new Empanelment();
        SqlConnection con = new SqlConnection(ConStr);
        try
        {
            //string StoreProcedure = ConfigurationManager.AppSettings["GetCPData"].ToString();
            SqlCommand cmd = new SqlCommand("SPGetEmpanelmentDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@emplId", emplId);

            con.Open();

            SqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                empObj.Communication_Street = rdr["Communication_Street"].ToString();
                empObj.Communication_City = rdr["Communication_City"].ToString();
                empObj.Communication_State = rdr["Communication_State"].ToString();
                empObj.Communication_Zip = rdr["Communication_Zip"].ToString();
                empObj.Communication_Country = rdr["Communication_Country"].ToString();
                empObj.Registered_City = rdr["Registered_City"].ToString();
                empObj.Registered_Country = rdr["Registered_Country"].ToString();
                empObj.Registered_State = rdr["Registered_State"].ToString();
                empObj.Registered_Street = rdr["Registered_Street"].ToString();
                empObj.Registered_Zip = rdr["Registered_Zip"].ToString();
                empObj.Billing_City = rdr["Billing_City"].ToString();
                empObj.Billing_Country = rdr["Billing_Country"].ToString();
                empObj.Billing_State = rdr["Billing_State"].ToString();
                empObj.Billing_Street = rdr["Billing_Street"].ToString();
                empObj.Billing_Zip = rdr["Billing_Zip"].ToString();
                empObj.BrokerAccountID = rdr["BrokerAccountID"].ToString();
            }

        }
        catch (Exception ex)
        {

        }
        return empObj;
    }

    public int UpdateAccountContactId(int emplId, string brokerAccId, string brokerContId)
    {
        int ResponseCode = 0;
        SqlConnection con = new SqlConnection(ConStr);
        try
        {
            //string StoreProcedure = ConfigurationManager.AppSettings["UpdateBrokerAccConId"].ToString();
            SqlCommand cmd = new SqlCommand("UpdateBrokerAccConId", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@EmpanelmentID", emplId);
            cmd.Parameters.AddWithValue("@brokerAccId", brokerAccId);
            cmd.Parameters.AddWithValue("@brokerContId", brokerContId);
            con.Open();

            ResponseCode = (int)cmd.ExecuteScalar();
        }
        catch (Exception ex)
        {

        }
        return ResponseCode;
    }

    private string createOrUpdateContact(string FirstName, string LastName, string BrokerId, string Email, string Mobile, string countryCode,
        string Communication_City, string Communication_Zip, string Communication_Street, string Communication_State, string Communication_Country)
    {
        string createContact = ConfigurationManager.AppSettings["SFDCURL"] + "/services/apexrest/RestBrokerContactCreation";
        //string updateContact = //"https://godrej--test.cs5.my.salesforce.com/services/apexrest/RestBrokerContactUpdate";
        if (Communication_Street.Length > 39)
        {
            Communication_Street = Communication_Street.Substring(0, 39);
        }
        var brokerCreateBody = new
        {
            ei = new
            {
                brokerid = brokerId,
                LastName = FirstName + ' ' + LastName,
                Email = Email,
                Mobile = Mobile,
                CountryCode = countryCode,
                BrokerContactType = "Proprietor",
                MailingCity = Communication_City,
                MailingStreet = Communication_Street,
                MailingStateProvince = Communication_State,
                MailingCountry = Communication_Country,
                MailingZipPostal = Communication_Zip
            }
        };
        var createString = JsonConvert.SerializeObject(brokerCreateBody);
        var createResponse = sendRequest(createString, createContact);
        return createResponse;
    }

    private string sendRequest(string body, string url)
    {
        ServicePointManager.Expect100Continue = true;
        ServicePointManager.DefaultConnectionLimit = 9999;
        ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

        ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

        using (var client = new WebClient())
        {
            client.Headers[HttpRequestHeader.ContentType] = "application/json";
            client.Headers[HttpRequestHeader.Authorization] = "Bearer " + Session["access_token"];
            //WLog.AdminWriteLog("access_token Bearer " + Session["access_token"], "sendRequest");
            //WLog.AdminWriteLog("Call client.UploadString Url " + url + " body " + body, "sendRequest");

            var response = client.UploadString(url, "POST", body);
            return response;
        }
    }

    public int UpdateInsertTblEmpanelments(tbl_EmpanelmentContact empl)
    {
        int ResponseCode = 0;
        SqlConnection con = new SqlConnection(ConStr);
        try
        {
            //string StoreProcedure = ConfigurationManager.AppSettings["UpdateInsertEmpanelment"].ToString();
            string StoreProcedure = "SPUpdateInsertEmpanelment";
            SqlCommand cmd = new SqlCommand(StoreProcedure, con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@name", empl.Name);
            cmd.Parameters.AddWithValue("@mobile", empl.Mobile);
            cmd.Parameters.AddWithValue("@email", empl.Email);
            cmd.Parameters.AddWithValue("@panno", empl.Pan_No);
            cmd.Parameters.AddWithValue("@companyname", empl.Company_Name);
            cmd.Parameters.AddWithValue("@username", empl.username);
            cmd.Parameters.AddWithValue("@password", empl.password);
            cmd.Parameters.AddWithValue("@status", empl.Status);
            cmd.Parameters.AddWithValue("@EmpanelmentID", empl.EmpanelmentID);
            con.Open();

            ResponseCode = (int)cmd.ExecuteScalar();
        }
        catch (Exception ex)
        {

        }
        return ResponseCode;
    }
    public string GenerateRandomString(int length)
    {
        string Nik = "";
        //It will generate string with combination of small,capital letters and numbers
        char[] charArr = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".ToCharArray();
        string randomString = string.Empty;
        Random objRandom = new Random();
        for (int i = 0; i < length; i++)
        {
            //Don't Allow Repetation of Characters
            int x = objRandom.Next(1, charArr.Length);
            if (!randomString.Contains(charArr.GetValue(x).ToString()))
                randomString += charArr.GetValue(x);
            else
                i--;
        }
        return Nik = randomString;


    }
    private string RandomStr()
    {
        string text = "";
        string possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (int i = 0; i < 5; i++)
        {
            //text += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return text;
    }

    public void SendRejectMail(int EmplID, string name, string Email)
    {
        string Doc = "<table cellpadding='3' cellspaceing='0' border='1'>";
        Doc += "<tr><td>Document Name</td><td>Document No.</td><td>Comment</td></tr>";
        var objReraEmpl = from obj in dataContext.Empl_ReraDocs
                          where obj.EmplID == Convert.ToInt32(EmplID) && obj.Status == "Reject"
                          select new
                          {
                              obj.State,
                              obj.Rera_no,
                              obj.Rera_Certi,
                              obj.EmplID,
                              obj.ID,
                              obj.Status,
                              obj.Comment
                          };
        var arrReraEmpl = System.Linq.Enumerable.ToArray(objReraEmpl);
        for (int i = 0; i < arrReraEmpl.Length; i++)
        {
            Doc += "<tr><td>RERA</td><td>" + arrReraEmpl[0].Rera_no + "</td><td>" + arrReraEmpl[0].Comment + "</td></tr>";
        }

        var objGstEmpl = from obj in dataContext.Empl_GstDocs
                         where obj.EmplID == Convert.ToInt32(EmplID) && obj.Status == "Reject"
                         select new
                         {
                             obj.State,
                             obj.Gst_no,
                             obj.Gst_Certi,
                             obj.EmplID,
                             obj.ID,
                             obj.Status,
                             obj.Comment
                         };
        var arrGstEmpl = System.Linq.Enumerable.ToArray(objGstEmpl);
        for (int i = 0; i < arrGstEmpl.Length; i++)
        {
            Doc += "<tr><td>Gst</td><td>" + arrGstEmpl[0].Gst_no + "</td><td>" + arrGstEmpl[0].Comment + "</td></tr>";
        }
        Doc += "</table>";
        EmailService es = new EmailService();
        MailTemplateController mailCon = new MailTemplateController();
        mMailTemplate mail = mailCon.GetMailTempleteByCode("EMPREJ");
        es.properties.AddToEmail(Email, name);
        es.properties.MailSubject = mail.Subject;
        string Body = mail.Body;
        Body = Body.Replace("#EmpanelmentName#", name);
        Body = Body.Replace("#DocumentList#", Doc);
        es.properties.MailBody = Body;
        es.SendMail();
    }


    private void SendApprovalMail(string name, string Email, string username, string pwd)
    {
        try
        {
            EmailService es = new EmailService();
            MailTemplateController mailCon = new MailTemplateController();
            mMailTemplate mail = mailCon.GetMailTempleteByCode("EMPAPP");
            es.properties.AddToEmail(Email, name);
            es.properties.MailSubject = mail.Subject;
            string Body = mail.Body;
            Body = Body.Replace("#EmpanelmentName#", name);
            Body = Body.Replace("#username#", username);
            Body = Body.Replace("#password#", pwd);
            es.properties.MailBody = Body;
            es.SendMail();

            /*
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["ClientHost"]);
            mail.From = new MailAddress(ConfigurationManager.AppSettings["ClientFrom"]);
            mail.To.Add(email);
            mail.Subject = "Godrej Partner Connect APP | Login credentials";
            mail.IsBodyHtml = true;
            string path = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
            StreamReader sr = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Mailer/empanelment_form.html"));
            mail.Body = sr.ReadToEnd().Replace("user_name", name).Replace("user_email", email).Replace("user_pwd", pwd);
            SmtpServer.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
            // SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
            // SmtpServer.UseDefaultCredentials = false;
            SmtpServer.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["ClientFrom"], ConfigurationManager.AppSettings["ClientPwd"]);
            SmtpServer.EnableSsl = true;
            SmtpServer.Send(mail);
            sr.Close();
            */
            //MailMessage mail = new MailMessage();
            //mail.From = "partnerconnect@godrejproperties.com";
            //mail.To = email;
            //mail.BodyFormat = MailFormat.Html;
            //mail.Subject = "Password Mailer";
            //mail.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
            ////mail.IsBodyHtml = true;
            //StreamReader sr = new StreamReader(Server.MapPath("~/Mailer/empanelment_form.html"));
            //mail.Body = sr.ReadToEnd().Replace("user_name", name).Replace("user_email", email).Replace("user_pwd", pwd);
            ////mail.Subject = "Password Request for Partner Connect App";
            ////string BodyMessage = "<html><body><table><tr><td>Dear "+ name +", </td></tr><tr><td></td></tr><tr><td></td></tr><tr><td></td></tr><tr><td>" +
            ////    "This is with regards to your request for Password</td></tr><tr><td></td></tr><tr><td>" +
            ////    "Plase find below details for Login" +
            ////    "<tr><td>Username : "+ email +"</td></tr>" +
            ////    "<tr><td>Password : " + pwd + "</td></tr></table></body></html><html><body></body></html>";
            ////mail.Body = BodyMessage;
            //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
            //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "info@netbizlabs.com"); //set your username here
            //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "w*F%t?JRuEO0");	//set your password here
            ////StreamReader sr = new StreamReader(Server.MapPath(""));
            ////mail.Body = sr.ReadToEnd().Replace("Email", email);
            //SmtpMail.SmtpServer = "mail.netbizlabs.com";
            //SmtpMail.Send(mail);
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }
    //private void SendRejectMail(string name, string email)
    //{
    //    try
    //    {
    //        MailMessage mail = new MailMessage();
    //        SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["ClientHost"]);
    //        mail.From = new MailAddress(ConfigurationManager.AppSettings["ClientFrom"]);
    //        mail.To.Add(email);
    //        mail.Subject = "Godrej Partner Connect APP| CP empanelment rejection";
    //        mail.IsBodyHtml = true;
    //        string path = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
    //        StreamReader sr = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Mailer/rejection.html"));
    //        mail.Body = sr.ReadToEnd().Replace("user_name", name).Replace("user_email", email);
    //        SmtpServer.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
    //        // SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
    //        // SmtpServer.UseDefaultCredentials = false;
    //        SmtpServer.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["ClientFrom"], ConfigurationManager.AppSettings["ClientPwd"]);
    //        SmtpServer.EnableSsl = true;
    //        SmtpServer.Send(mail);
    //        sr.Close();
    //    }
    //    catch (Exception ex)
    //    {
    //        Response.Write(ex.Message);
    //    }
    //}
    protected void gdvRera_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lnkBtn = (LinkButton)e.Row.FindControl("lnkBtnApprove");
            LinkButton lnkBtnR = (LinkButton)e.Row.FindControl("lnkBtnReject");
            Label lblStatus = (Label)e.Row.FindControl("lblStatus");
            TextBox txtComment = (TextBox)e.Row.FindControl("txtComment");
            Label lblComment = (Label)e.Row.FindControl("lblComment");

            if (lblStatus.Text.Trim() == "Approve" || lblStatus.Text.Trim() == "Reject")
            {
                lnkBtn.Style.Add("display", "none");
                lnkBtnR.Style.Add("display", "none");
                txtComment.Visible = false;
                lblComment.Visible = true;
            }
            else
            {
                txtComment.Visible = true;
                lblComment.Visible = false;
                //lnkBtn.Text = "Activate";
            }
            if (lblStatus.Text.Trim() == "Reject")
            {
                btnSendRejectMail.Visible = true;
            }
        }
    }
    protected void gdvRera_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        BindReraGrid(Convert.ToInt32(lblEmplID.Text), e.NewPageIndex);
    }
    protected void gdvGst_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        BindGstGrid(Convert.ToInt32(lblEmplID.Text), e.NewPageIndex);
    }

    protected void gdvGst_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lnkBtn = (LinkButton)e.Row.FindControl("lnkBtnApprove");
            LinkButton lnkBtnR = (LinkButton)e.Row.FindControl("lnkBtnReject");
            Label lblStatus = (Label)e.Row.FindControl("lblStatus");
            TextBox txtComment = (TextBox)e.Row.FindControl("txtComment");
            Label lblComment = (Label)e.Row.FindControl("lblComment");

            if (lblStatus.Text.Trim() == "Approve" || lblStatus.Text.Trim() == "Reject")
            {
                lnkBtn.Style.Add("display", "none");
                lnkBtnR.Style.Add("display", "none");
                txtComment.Visible = false;
                lblComment.Visible = true;
            }
            else
            {
                txtComment.Visible = true;
                lblComment.Visible = false;
                //lnkBtn.Text = "Activate";
            }
            if (lblStatus.Text.Trim() == "Reject")
            {
                btnSendRejectMail.Visible = true;
            }

        }
    }

    protected void grdDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //LinkButton lnkPushSales = (LinkButton)e.Row.FindControl("lnkPushSales");
            Label lblStatus = (Label)e.Row.FindControl("lblStatus");
            Label lblStatusText = (Label)e.Row.FindControl("lblStatusText");
            switch (lblStatus.Text)
            {
                case "A":
                    lblStatusText.Text = "Approved";
                    break;
                case "R":
                    lblStatusText.Text = "Rejected";
                    break;
                case "P":
                    lblStatusText.Text = "Pending";
                    break;
                default:
                    lblStatusText.Text = "Pending";
                    break;
            }
        }
    }
    void Showfrom(int EmplID)
    {
        divForm.Visible = true;
        divGrid.Visible = false;
        BindGstGrid(EmplID, 0);
        BindReraGrid(EmplID, 0);

        tbl_Empanelment Empldetails = dataContext.tbl_Empanelments.Single(doc => doc.EmplID == EmplID);
        lblEmpanelmentID.Text = "EMP" + Empldetails.EmplID;
        lblEmplID.Text = Empldetails.EmplID.ToString();
        lblCompany_Name.Text = Empldetails.Company_Name.ToString();
        lblEntityType.Text = Empldetails.EntityType.ToString();
        lblFirstName.Text = Empldetails.FirstName.ToString();
        lblLastName.Text = Empldetails.LastName.ToString();
        lblMobile.Text = Empldetails.Mobile.ToString();
        lblEmail.Text = Empldetails.Email.ToString();
        lblBrokerAccountID.Text = Empldetails.BrokerAccountID;
        lblPan_No.Text = Empldetails.Pan_No.ToString();
        aPan_No.HRef = "/data/empanelment/" + Empldetails.Pan_Certi.ToString();
        lblStatus.Text = Empldetails.Status;
        switch (Empldetails.Status)
        {
            case "A":
                lblDisplayStatus.Text = "Approved";
                break;
            case "R":
                lblDisplayStatus.Text = "Rejected";
                break;
            case "P":
                lblDisplayStatus.Text = "Pending";
                break;
            default:
                lblDisplayStatus.Text = "Pending";
                break;
        }
        lblBilling_City.Text = Empldetails.Billing_City.ToString();
        lblBilling_Zip.Text = Empldetails.Billing_Zip.ToString();
        lblBilling_Street.Text = Empldetails.Billing_Street.ToString();
        lblBilling_State.Text = Empldetails.Billing_State.ToString();
        lblBilling_Country.Text = Empldetails.Billing_Country.ToString();
        lblRegistered_City.Text = Empldetails.Registered_City.ToString();
        lblRegistered_Zip.Text = Empldetails.Registered_Zip.ToString();
        lblRegistered_Street.Text = Empldetails.Registered_Street.ToString();
        lblRegistered_State.Text = Empldetails.Registered_State.ToString();
        lblRegistered_Country.Text = Empldetails.Registered_Country.ToString();
        lblCommunication_City.Text = Empldetails.Communication_City.ToString();
        lblCommunication_Zip.Text = Empldetails.Communication_Zip.ToString();
        lblCommunication_Street.Text = Empldetails.Communication_Street.ToString();
        lblCommunication_State.Text = Empldetails.Communication_State.ToString();
        lblCommunication_Country.Text = Empldetails.Communication_Country.ToString();
        if (lblStatus.Text == "A")
        {
            btnRegister.Visible = false;
        }

    }
    public class EmplReraGSTDocs
    {
        public string State { get; set; }
        public string Rera_no { get; set; }
        public string Rera_Certi { get; set; }
        public string EmplID { get; set; }
        public string Status { get; set; }
        public string Gst_no { get; set; }
        public string Gst_Certi { get; set; }

    }

    protected void btnRegister_Click(object sender, EventArgs e)
    {

        #region Code For Push to Sales Force and email
        Boolean flag = true;
        int EmplID = Convert.ToInt32(lblEmplID.Text);
        WLog.AdminWriteLog("Push Sales Started EmplID " + EmplID, "ViewEmpanelment");
        var Empldetails = dataContext.tbl_Empanelments.Single(doc => doc.EmplID == EmplID);
        var objGstEmpl = from obj in dataContext.Empl_GstDocs
                         where obj.EmplID == Convert.ToInt32(EmplID)
                         select new
                         {
                             obj.State,
                             obj.Gst_no,
                             obj.Gst_Certi,
                             obj.EmplID,
                             obj.ID,
                             obj.Status,
                             obj.SFDCRegisted
                         };
        if (objGstEmpl.Count() > 0)
        {
            var arrGstEmpl = System.Linq.Enumerable.ToArray(objGstEmpl);
            for (int i = 0; i < arrGstEmpl.Length; i++)
            {
                if (arrGstEmpl[i].Status == "Pending" || arrGstEmpl[i].Status == "Reject")
                {
                    flag = false;
                    break;
                }
            }
        }
        WLog.AdminWriteLog("objGstEmpl.Count = " + objGstEmpl.Count(), "ViewEmpanelment");
        var objReraEmpl = from obj in dataContext.Empl_ReraDocs
                          where obj.EmplID == Convert.ToInt32(EmplID)
                          select new
                          {
                              obj.State,
                              obj.Rera_no,
                              obj.Rera_Certi,
                              obj.EmplID,
                              obj.ID,
                              obj.Status,
                              obj.SFDCRegisted
                          };

        WLog.AdminWriteLog("objReraEmpl.Count = " + objReraEmpl.Count(), "ViewEmpanelment");
        if (objReraEmpl.Count() > 0)
        {
            var arrReraEmpl = System.Linq.Enumerable.ToArray(objReraEmpl);
            for (int i = 0; i < arrReraEmpl.Length; i++)
            {
                if (arrReraEmpl[i].Status == "Pending" || arrReraEmpl[i].Status == "Reject")
                {
                    flag = false;
                    break;
                }
            }
        }
        else
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('There has to atleast one Rera');</script>");
            return;

        }
        if (flag)
        {
            var listReraEmpl = System.Linq.Enumerable.ToList(objReraEmpl);
            List<EmplReraGSTDocs> list = new List<EmplReraGSTDocs>();
            foreach (var ReraEmpl in listReraEmpl)
            {
                if (ReraEmpl.Status == "Approve")
                {

                    var objStateGstEmpl = from obj in dataContext.Empl_GstDocs
                                          where obj.EmplID == Convert.ToInt32(EmplID) && obj.State == ReraEmpl.State && obj.Status == "Approve"
                                          select new
                                          {
                                              obj.Gst_no,
                                              obj.Gst_Certi,
                                          };

                    var arrStateGSTEmpl = System.Linq.Enumerable.ToArray(objStateGstEmpl);
                    EmplReraGSTDocs objemp = new EmplReraGSTDocs();
                    objemp.State = ReraEmpl.State;
                    objemp.Rera_no = ReraEmpl.Rera_no;
                    if (arrStateGSTEmpl.Length > 0)
                        objemp.Gst_no = arrStateGSTEmpl[0].Gst_no;
                    else
                        objemp.Gst_no = "";
                    list.Add(objemp);
                }

            }

            //Push to Sales Force
            WLog.AdminWriteLog("flag = true", "ViewEmpanelment");
            Empanelment emp = GetCPProperties(Empldetails.EmplID);
            string sendURL, updateURL;
            if (emp.BrokerAccountID == "")
            {
                var additionalParam = GetAdditionalParameters(Empldetails.EmplID);                                          // TCC Changes
                if (additionalParam.RequestType == "TCC")                                                                   // TCC Changes
                {
                    List<string> val;
                    sendURL = ConfigurationManager.AppSettings["SFDCURL"] + "/services/apexrest/BrokerAccountCreationRCP";
                    updateURL = ConfigurationManager.AppSettings["SFDCURL"] + "/services/apexrest/RestBrokerUpdate";
                    var body = new
                    {
                        ei = new
                        {
                            Name = Empldetails.Company_Name,
                            Pannumber = Empldetails.Pan_No,
                            GSTIN = "",// objGST.Gst_no,
                            GSTINState = "",//objGST.State,
                            BrokerType = "",
                            BrokerCategory = "RCP",
                            CompanyCategory = "",
                            MobileNo = Empldetails.Mobile,
                            Phone = Empldetails.Mobile,
                            BrokerEntityType = Empldetails.EntityType,
                            RegionName = "",
                            EmailId = Empldetails.Email,
                            Empanelment = "EMPL" + Empldetails.EmplID.ToString(),
                            BillingCity = emp.Billing_City,
                            BillingZipPostal = emp.Billing_Zip,
                            BillingStreet = emp.Billing_Street,
                            BillingStateProvince = emp.Billing_State,
                            BillingCountry = emp.Billing_Country,
                            RegisteredCity = emp.Registered_City,
                            RegisteredZipPostal = emp.Registered_Zip,
                            RegisteredStreet = emp.Registered_Street,
                            RegisteredStateProvince = emp.Registered_State,
                            RegisteredCountry = emp.Registered_Country,
                            CommunicationCity = emp.Communication_City,
                            CommunicationZipPostal = emp.Communication_Zip,
                            CommunicationStreet = emp.Communication_Street,
                            CommunicationStateProvince = emp.Communication_State,
                            CommunicationCountry = emp.Communication_Country,
                            sourcedBy = "CC - SEC"

                        }
                    };
                    var dataString = JsonConvert.SerializeObject(body);
                    WLog.AdminWriteLog("line 444 Call dataString " + dataString, "EmpanelmentAPI");
                    //dataString = "{\"ei\":{\"Name\":\"REM ITOTALITY SOLUTIONS PVT LTD\",\"Pannumber\":\"AAFCR6061A\",\"GSTIN\":\"\",\"GSTINState\":\"\",\"BrokerType\":\"\",\"BrokerCategory\":\"RCP\",\"CompanyCategory\":\"\",\"MobileNo\":\"9820663141\",\"Phone\":\"9820663141\",\"BrokerEntityType\":\"Company\",\"RegionName\":\"\",\"EmailId\":\"bookgains@gmail.com\",\"Empanelment\":\"EMPL3607\",\"BillingCity\":\"Navi Mumbai \",\"BillingZipPostal\":\"400053\",\"BillingStreet\":\"RAHEJA CLASSIQUE , BLDG # 2B , FLAT # 414/415 , NEW LINK ROAD , ANDHERI WEST\",\"BillingStateProvince\":\"Maharashtra\",\"BillingCountry\":\"India\",\"RegisteredCity\":\"Navi Mumbai \",\"RegisteredZipPostal\":\"400053\",\"RegisteredStreet\":\"BLRAHEJA CLASSIQUE , BLDG # 2B , FLAT # 414\/415 , NEW LINK ROAD , ANDHERI WEST\",\"RegisteredStateProvince\":\"Maharashtra\",\"RegisteredCountry\":\"India\",\"CommunicationCity\":\"Navi Mumbai \",\"CommunicationZipPostal\":\"400053\",\"CommunicationStreet\":\"BLRAHEJA CLASSIQUE , BLDG # 2B , FLAT # 414\/415 , NEW LINK ROAD , ANDHERI WEST\",\"CommunicationStateProvince\":\"Maharashtra\",\"CommunicationCountry\":\"India\"}}";
                    var response = sendRequest(dataString, sendURL);

                    WLog.AdminWriteLog("line 448 Call ", "ViewEmpanelment");
                    val = jsSerializer.Deserialize<List<string>>(response);
                    brokerId = val[1];
                    WLog.AdminWriteLog("line 452 brokerId " + brokerId, "ViewEmpanelment");

                    if (val[0].Contains("already"))
                    {
                        var newBody = new
                        {
                            ei = new
                            {
                                brokerid = brokerId,
                                Name = Empldetails.Company_Name,
                                Pannumber = Empldetails.Pan_No,
                                GSTIN = "",// objGST.Gst_no,
                                GSTINState = "",//objGST.State,
                                BrokerType = "",
                                BrokerCategory = "RCP",
                                CompanyCategory = "",
                                MobileNo = Empldetails.Mobile,
                                Phone = Empldetails.Mobile,
                                BrokerEntityType = Empldetails.EntityType,
                                RegionName = "",
                                EmailId = Empldetails.Email,
                                Empanelment = "EMPL" + Empldetails.EmplID.ToString(),
                                BillingCity = emp.Billing_City,
                                BillingZipPostal = emp.Billing_Zip,
                                BillingStreet = emp.Billing_Street,
                                BillingStateProvince = emp.Billing_State,
                                BillingCountry = emp.Billing_Country,
                                RegisteredCity = emp.Registered_City,
                                RegisteredZipPostal = emp.Registered_Zip,
                                RegisteredStreet = emp.Registered_Street,
                                RegisteredStateProvince = emp.Registered_State,
                                RegisteredCountry = emp.Registered_Country,
                                CommunicationCity = emp.Communication_City,
                                CommunicationZipPostal = emp.Communication_Zip,
                                CommunicationStreet = emp.Communication_Street,
                                CommunicationStateProvince = emp.Communication_State,
                                CommunicationCountry = emp.Communication_Country
                            }
                        };
                        var newDataString = JsonConvert.SerializeObject(newBody);
                        var newResponse = sendRequest(newDataString, updateURL);
                        WLog.AdminWriteLog("line 463 Call ", "ViewEmpanelment");
                    }
                }
                if (additionalParam.RequestType == "OCP" && additionalParam.SourceCallCenterId != "" && additionalParam.BrokerContactId != "" && additionalParam.SourcedBy != "")                                                                   // TCC Changes
                {
                    List<string> val;
                    sendURL = ConfigurationManager.AppSettings["SFDCURL"] + "/services/apexrest/BrokerAccountCreationRCP";
                    updateURL = ConfigurationManager.AppSettings["SFDCURL"] + "/services/apexrest/RestBrokerUpdate";
                    var body = new
                    {
                        ei = new
                        {
                            Name = Empldetails.Company_Name,
                            Pannumber = Empldetails.Pan_No,
                            GSTIN = "",// objGST.Gst_no,
                            GSTINState = "",//objGST.State,
                            BrokerType = "",
                            BrokerCategory = "RCP",
                            CompanyCategory = "",
                            MobileNo = Empldetails.Mobile,
                            Phone = Empldetails.Mobile,
                            BrokerEntityType = Empldetails.EntityType,
                            RegionName = "",
                            EmailId = Empldetails.Email,
                            Empanelment = "EMPL" + Empldetails.EmplID.ToString(),
                            BillingCity = emp.Billing_City,
                            BillingZipPostal = emp.Billing_Zip,
                            BillingStreet = emp.Billing_Street,
                            BillingStateProvince = emp.Billing_State,
                            BillingCountry = emp.Billing_Country,
                            RegisteredCity = emp.Registered_City,
                            RegisteredZipPostal = emp.Registered_Zip,
                            RegisteredStreet = emp.Registered_Street,
                            RegisteredStateProvince = emp.Registered_State,
                            RegisteredCountry = emp.Registered_Country,
                            CommunicationCity = emp.Communication_City,
                            CommunicationZipPostal = emp.Communication_Zip,
                            CommunicationStreet = emp.Communication_Street,
                            CommunicationStateProvince = emp.Communication_State,
                            CommunicationCountry = emp.Communication_Country,
                            sourceCallCenterId = additionalParam.SourceCallCenterId,
                            brokerContactId = additionalParam.BrokerContactId,
                            sourcedBy = "CC - SEC"

                        }
                    };
                    var dataString = JsonConvert.SerializeObject(body);
                    WLog.AdminWriteLog("BrokerAccountCreationRCP Request : " + dataString, "OnlineViewEmpanelment");
                    var response = sendRequest(dataString, sendURL);

                    WLog.AdminWriteLog("BrokerAccountCreationRCP Response : " + response, "OnlineViewEmpanelment");
                    val = jsSerializer.Deserialize<List<string>>(response);
                    brokerId = val[1];
                    WLog.AdminWriteLog("BrokerAccountCreationRCP brokerId : " + brokerId, "OnlineViewEmpanelment");

                    if (val[0].Contains("already"))
                    {
                        var newBody = new
                        {
                            ei = new
                            {
                                brokerid = brokerId,
                                Name = Empldetails.Company_Name,
                                Pannumber = Empldetails.Pan_No,
                                GSTIN = "",// objGST.Gst_no,
                                GSTINState = "",//objGST.State,
                                BrokerType = "",
                                BrokerCategory = "RCP",
                                CompanyCategory = "",
                                MobileNo = Empldetails.Mobile,
                                Phone = Empldetails.Mobile,
                                BrokerEntityType = Empldetails.EntityType,
                                RegionName = "",
                                EmailId = Empldetails.Email,
                                Empanelment = "EMPL" + Empldetails.EmplID.ToString(),
                                BillingCity = emp.Billing_City,
                                BillingZipPostal = emp.Billing_Zip,
                                BillingStreet = emp.Billing_Street,
                                BillingStateProvince = emp.Billing_State,
                                BillingCountry = emp.Billing_Country,
                                RegisteredCity = emp.Registered_City,
                                RegisteredZipPostal = emp.Registered_Zip,
                                RegisteredStreet = emp.Registered_Street,
                                RegisteredStateProvince = emp.Registered_State,
                                RegisteredCountry = emp.Registered_Country,
                                CommunicationCity = emp.Communication_City,
                                CommunicationZipPostal = emp.Communication_Zip,
                                CommunicationStreet = emp.Communication_Street,
                                CommunicationStateProvince = emp.Communication_State,
                                CommunicationCountry = emp.Communication_Country
                            }
                        };
                        var newDataString = JsonConvert.SerializeObject(newBody);
                        var newResponse = sendRequest(newDataString, updateURL);
                        WLog.AdminWriteLog("line 463 Call ", "ViewEmpanelment");
                    }
                }
                else  // Mobile App
                {

                    List<string> val;
                    sendURL = ConfigurationManager.AppSettings["SFDCURL"] + "/services/apexrest/RestWebToBrokerCreation";
                    updateURL = ConfigurationManager.AppSettings["SFDCURL"] + "/services/apexrest/RestBrokerUpdate";
                    var body = new
                    {
                        ei = new
                        {
                            Name = Empldetails.Company_Name,
                            Pannumber = Empldetails.Pan_No,
                            GSTIN = "",// objGST.Gst_no,
                            GSTINState = "",//objGST.State,
                            BrokerType = "",
                            BrokerCategory = "RCP",
                            CompanyCategory = "",
                            MobileNo = Empldetails.Mobile,
                            Phone = Empldetails.Mobile,
                            BrokerEntityType = Empldetails.EntityType,
                            RegionName = "",
                            EmailId = Empldetails.Email,
                            Empanelment = "EMPL" + Empldetails.EmplID.ToString(),
                            BillingCity = emp.Billing_City,
                            BillingZipPostal = emp.Billing_Zip,
                            BillingStreet = emp.Billing_Street,
                            BillingStateProvince = emp.Billing_State,
                            BillingCountry = emp.Billing_Country,
                            RegisteredCity = emp.Registered_City,
                            RegisteredZipPostal = emp.Registered_Zip,
                            RegisteredStreet = emp.Registered_Street,
                            RegisteredStateProvince = emp.Registered_State,
                            RegisteredCountry = emp.Registered_Country,
                            CommunicationCity = emp.Communication_City,
                            CommunicationZipPostal = emp.Communication_Zip,
                            CommunicationStreet = emp.Communication_Street,
                            CommunicationStateProvince = emp.Communication_State,
                            CommunicationCountry = emp.Communication_Country,

                        }
                    };
                    var dataString = JsonConvert.SerializeObject(body);
                    WLog.AdminWriteLog("line 444 Call dataString " + dataString, "EmpanelmentAPI");
                    var response = sendRequest(dataString, sendURL);

                    WLog.AdminWriteLog("line 448 Call ", "ViewEmpanelment");
                    val = jsSerializer.Deserialize<List<string>>(response);
                    brokerId = val[1];
                    WLog.AdminWriteLog("line 452 brokerId " + brokerId, "ViewEmpanelment");

                    if (val[0].Contains("already"))
                    {
                        var newBody = new
                        {
                            ei = new
                            {
                                brokerid = brokerId,
                                Name = Empldetails.Company_Name,
                                Pannumber = Empldetails.Pan_No,
                                GSTIN = "",// objGST.Gst_no,
                                GSTINState = "",//objGST.State,
                                BrokerType = "",
                                BrokerCategory = "RCP",
                                CompanyCategory = "",
                                MobileNo = Empldetails.Mobile,
                                Phone = Empldetails.Mobile,
                                BrokerEntityType = Empldetails.EntityType,
                                RegionName = "",
                                EmailId = Empldetails.Email,
                                Empanelment = "EMPL" + Empldetails.EmplID.ToString(),
                                BillingCity = emp.Billing_City,
                                BillingZipPostal = emp.Billing_Zip,
                                BillingStreet = emp.Billing_Street,
                                BillingStateProvince = emp.Billing_State,
                                BillingCountry = emp.Billing_Country,
                                RegisteredCity = emp.Registered_City,
                                RegisteredZipPostal = emp.Registered_Zip,
                                RegisteredStreet = emp.Registered_Street,
                                RegisteredStateProvince = emp.Registered_State,
                                RegisteredCountry = emp.Registered_Country,
                                CommunicationCity = emp.Communication_City,
                                CommunicationZipPostal = emp.Communication_Zip,
                                CommunicationStreet = emp.Communication_Street,
                                CommunicationStateProvince = emp.Communication_State,
                                CommunicationCountry = emp.Communication_Country
                            }
                        };
                        var newDataString = JsonConvert.SerializeObject(newBody);
                        var newResponse = sendRequest(newDataString, updateURL);
                        WLog.AdminWriteLog("line 463 Call ", "ViewEmpanelment");
                    }
                }

            }
            else
            {
                brokerId = emp.BrokerAccountID;
            }

            string FirstName = Empldetails.FirstName;
            string LastName = Empldetails.LastName;
            WLog.AdminWriteLog("dataContext.tbl_EmpanelmentContacts.ToList().Count " + dataContext.tbl_EmpanelmentContacts.ToList().Count, "ViewEmpanelment");
            if (dataContext.tbl_EmpanelmentContacts.ToList().Count > 0)
            {
                var EmplContact = dataContext.tbl_EmpanelmentContacts.Single(doc => doc.EmpanelmentID == Empldetails.EmplID);
                WLog.AdminWriteLog("EmplContact.BrokerContactID '" + EmplContact.BrokerContactID + "'", "ViewEmpanelment");
                if (EmplContact.BrokerContactID == "" || EmplContact.BrokerContactID == null)
                {
                    List<string> contactVal;

                    var contactResponse = createOrUpdateContact(FirstName, LastName, brokerId, Empldetails.Email, Empldetails.Mobile, emp.Registered_Country,
                        emp.Communication_City, emp.Communication_Zip, emp.Communication_Street, emp.Communication_State, emp.Communication_Country);
                    WLog.AdminWriteLog("contactResponse = '" + contactResponse + "'", "UpdateEmpanelmentEmplContact");
                    contactVal = jsSerializer.Deserialize<List<string>>(contactResponse);
                    string brokerContactId = contactVal[1];

                    WLog.AdminWriteLog("line 484 createOrUpdateContact Call Empldetails.EmplID = " + Empldetails.EmplID + ", brokerId = " + brokerId + ", brokerContactId=" + brokerContactId + ",", "UpdateEmpanelment");
                    UpdateAccountContactId(Empldetails.EmplID, brokerId, brokerContactId);
                }
            }

            //string sendReraURL = ConfigurationManager.AppSettings["SFDCURL"] + "/services/apexrest/api/BrokerReradetail";
            string sendReraURL = ConfigurationManager.AppSettings["SFDCURL"] + "/services/apexrest/api/updateBrokerReradetail";
            var arrReraEmpl = System.Linq.Enumerable.ToArray(objReraEmpl);
            WLog.AdminWriteLog("EmplReraGSTDocs list.Count = " + list.Count, "EmplReraGSTDocs");
            foreach (EmplReraGSTDocs oEmp in list)
            {
                var Rerabody = new
                {
                    ei = new
                    {
                        Pan_Number = Empldetails.Pan_No,
                        Rera_Number = oEmp.Rera_no,
                        state_code = oEmp.State,
                        GSTIN = oEmp.Gst_no.ToString(),
                        //SAPBrokerCode = ""
                    }
                };

                WLog.AdminWriteLog("Empldetails.Pan_No = " + Empldetails.Pan_No + ", oEmp.Rera_no = " + oEmp.Rera_no + ", oEmp.State = " + oEmp.State + ", oEmp.Gst_no = " + oEmp.Gst_no + "", "EmplReraGSTDocs");

                var ReradataString = JsonConvert.SerializeObject(Rerabody);
                var Reraresponse = sendRequest(ReradataString, sendReraURL);
                if (oEmp.Gst_no != "")
                {
                    var objStateGstEmpl = dataContext.Empl_GstDocs.Single(doc => doc.EmplID == EmplID && doc.State == oEmp.State && doc.Status == "Approve");
                    objStateGstEmpl.SFDCRegisted = "Y";
                }
                if (oEmp.Rera_no != "")
                {
                    var objStateReraEmpl = dataContext.Empl_ReraDocs.Single(doc => doc.EmplID == EmplID && doc.State == oEmp.State && doc.Status == "Approve");
                    objStateReraEmpl.SFDCRegisted = "Y";
                }
                WLog.AdminWriteLog(Reraresponse, "Reraresponse");
            }

            if (Empldetails.Status != "A")
            {
                Empldetails.Status = "A";
                Empldetails.BrokerAccountID = brokerId;
                Empldetails.RegisteredOn = System.DateTime.Now;
                WLog.AdminWriteLog("Empldetails Updated ", "ViewEmpanelment");
                string[] name = Empldetails.FirstName.ToString().Split(' ');
                string password = name[0] + GenerateRandomString(5);
                tbl_EmpanelmentContact objEmpl = new tbl_EmpanelmentContact();
                objEmpl.Name = Empldetails.FirstName + ' ' + Empldetails.LastName;
                objEmpl.Mobile = Empldetails.Mobile;
                objEmpl.Email = Empldetails.Email;

                objEmpl.Pan_No = Empldetails.Pan_No;
                objEmpl.Company_Name = Empldetails.Company_Name;
                objEmpl.username = Empldetails.Email;
                objEmpl.password = password;
                objEmpl.createddate = DateTime.Now;
                objEmpl.Status = "A";
                objEmpl.EmpanelmentID = Empldetails.EmplID;

                UpdateInsertTblEmpanelments(objEmpl);
                SendApprovalMail(Empldetails.FirstName + ' ' + Empldetails.LastName, objEmpl.Email, objEmpl.username, objEmpl.password);
                WLog.AdminWriteLog("UpdateInsertTblEmpanelments ", "ViewEmpanelment");

            }
            ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Record successfully Registered ');</script>");
            dataContext.SubmitChanges();
            Showfrom(EmplID);

        }
        else
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Please approve all GST and Rera');</script>");
        }

        #endregion Code For Push to Sales Force and email
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        divForm.Visible = false;
        divGrid.Visible = true;
    }

    protected void btnSendRejectMail_Click(object sender, EventArgs e)
    {

        int EmplID = Convert.ToInt32(lblEmplID.Text);
        WLog.AdminWriteLog("Push Sales Started EmplID " + EmplID, "ViewEmpanelment");
        var Empldetails = dataContext.tbl_Empanelments.Single(doc => doc.EmplID == EmplID);
        SendRejectMail(EmplID, Empldetails.FirstName + ' ' + Empldetails.LastName, Empldetails.Email);
    }

    // TCC Changes
    public AdditionalParameter GetAdditionalParameters(int emplID)
    {
        DataTable dt = new DataTable();
        SqlConnection con = new SqlConnection(ConStr);
        try
        {
            SqlCommand cmd = new SqlCommand("usp_EmplRoleWise", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmplID", Convert.ToInt32(emplID));
            cmd.Parameters.AddWithValue("@flag", "GET_ADDP");
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            con.Close();
            da.Dispose();

        }
        catch (Exception ex)
        {

        }
        var lst = (from rw in dt.AsEnumerable()
                   select new AdditionalParameter()
                   {
                       RequestType = rw["RequestType"].ToString(),
                       SubmittedByUser = rw["SubmittedByUser"].ToString(),
                       SourceCallCenterId = rw["SourceCallCenterId"].ToString(),
                       BrokerContactId = rw["BrokerContactId"].ToString(),
                       SourcedBy = rw["SourcedBy"].ToString(),
                   }).ToList();
        return lst[0];
    }

    public class AdditionalParameter
    {
        public string RequestType { get; set; }
        public string SubmittedByUser { get; set; }
        public string SourceCallCenterId { get; set; }
        public string BrokerContactId { get; set; }
        public string SourcedBy { get; set; }
    }
}