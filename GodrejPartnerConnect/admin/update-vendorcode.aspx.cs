﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Data.SqlClient;
using System.Data;

public partial class Admin_update_vendorcode : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["godrejCPConnectionString"].ToString());
    GodejCPDataContext dataContext = new GodejCPDataContext();
    Guid random = Guid.NewGuid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        if (Convert.ToString(Session["type"]) == "admin")
        {
            HtmlControl Pwd = (HtmlControl)Menu.FindControl("PwdLi");
            Pwd.Visible = false;
            HtmlControl User = (HtmlControl)Menu.FindControl("UserLi");
            User.Visible = false;
            HtmlControl Launch = (HtmlControl)Menu.FindControl("BannerLi");
            Launch.Visible = false;
            HtmlControl Master = (HtmlControl)Menu.FindControl("MasterLi");
            Master.Visible = false;
            HtmlControl Projects = (HtmlControl)Menu.FindControl("ProjectsLi");
            Projects.Visible = false;
            HtmlControl Enquiry = (HtmlControl)Menu.FindControl("EnquiryLi");
            Enquiry.Visible = false;
            HtmlControl Events = (HtmlControl)Menu.FindControl("EventsLi");
            Events.Visible = false;
            HtmlControl News = (HtmlControl)Menu.FindControl("NewsLi");
            News.Visible = false;
            HtmlControl Offers = (HtmlControl)Menu.FindControl("OffersLi");
            Offers.Visible = false;
            HtmlControl Property = (HtmlControl)Menu.FindControl("PropertyLi");
            Property.Visible = false;
        }
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["brokerId"])))
            {
                FillData(Convert.ToString(Request.QueryString["brokerId"]));
            }
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("Vendoranchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("VendorLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

    }

    private void FillData(string id)
    {
        var objroom = dataContext.tbl_EmpanelmentContacts.Single(doc => doc.EmplContID == Convert.ToInt32(id));
        txtBroker.Text = objroom.BrokerID;
    }

    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewVendorCode.aspx");
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        #region Code For add SAP vendor code
        int id = Convert.ToInt32(Session["BrokerID"]);

        #region to check if entered pan no matches or not

        var objChk = (from obj in dataContext.tbl_EmpanelmentContacts
                      where obj.Pan_No == txtPan.Text
                      where obj.EmplContID == id
                      select obj).FirstOrDefault();

        #endregion
        if (objChk == null)
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter valid correct PAN no');</script>");
            return;
        }
        else
        {
            var objMenu = dataContext.tbl_EmpanelmentContacts.Single(doc => doc.EmplContID == id);
            objMenu.SAP_Vendor_Code = txtCode.Text.Trim();
            dataContext.SubmitChanges();
            Session["BrokerID"] = null;
            Response.Redirect("viewVendorCode.aspx?saved=1");
        #endregion
        }
        Div1.Style.Add("display", "block");
    }
}
