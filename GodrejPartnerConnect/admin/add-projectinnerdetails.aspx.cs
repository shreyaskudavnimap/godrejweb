﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;
//using System.Transactions;

public partial class Admin_add_projectinnerdetails : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    Guid random = Guid.NewGuid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");

        if (!IsPostBack)
        {
            BindProject();
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["innerprojectId"])))
            {
                FillData(Convert.ToString(Request.QueryString["innerprojectId"]));
            }
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("ProjectAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("ProjectInnerDetailsLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

    }

    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewprojectinnerdetails.aspx");
    }

    private void BindProject()
    {
        #region Code for Project
        var objProject = (from obj in dataContext.Projects
                          join objDesgn in dataContext.CityMasters
                          on obj.cityid equals objDesgn.cityid
                          join objPlace in dataContext.PlaceMasters
                          on obj.placeid equals objPlace.placeid into defaultPlace
                          from c in defaultPlace.DefaultIfEmpty()
                          where obj.status.Trim() == "A"
                          orderby obj.projectid descending
                          select new
                          {
                              obj.projectid,
                              projectname = (c == null ? obj.title + ", " + objDesgn.cityname : obj.title + ", " + c.placename + ", " + objDesgn.cityname)

                          }).Distinct().ToList();
        ddlProject.DataTextField = "projectname";
        ddlProject.DataValueField = "projectid";
        ddlProject.DataSource = objProject;
        ddlProject.DataBind();
        ddlProject.Items.Insert(0, new ListItem("--Select--", "0"));
        #endregion
    }

    private void FillData(string id)
    {
        var objProject = dataContext.ProjectsInnerDetails.Single(doc => doc.ID == Convert.ToInt32(id));
        ddlProject.SelectedValue = Convert.ToString(objProject.projectid);
        BindMenuList(Convert.ToInt32(objProject.projectid));
        ddlMenu.SelectedValue = Convert.ToString(objProject.menuid);
        txtDescription.Text = objProject.description;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["innerprojectId"])))
        {
            #region to check if same city exists

            var objChk = (from obj in dataContext.ProjectsInnerDetails
                          where obj.projectid == Convert.ToInt32(ddlProject.SelectedValue) && obj.menuid==Convert.ToInt32(ddlMenu.SelectedValue)
                          select obj).FirstOrDefault();

            #endregion
            if (objChk != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Project Detail already exists.');</script>");
                return;
            }
            else
            {
                #region add new record
                ProjectsInnerDetail objproject = new ProjectsInnerDetail();
                if (ddlMenu.SelectedValue != "3")
                {
                    #region Save Image
                    if (FileUpload1.HasFile)
                    {
                        if (FileUpload1.PostedFile.ContentLength > 5242880)
                        {
                            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual Image size should not be more than 5MB');</script>");
                            return;
                        }
                        string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                        if (imgext.ToLower() == ".gif" || imgext.ToLower() == ".jpg" || imgext.ToLower() == ".png")
                        {
                            string fileName = Path.GetFileName(FileUpload1.FileName);
                            string fileNameEdited = random.ToString() + "_" + fileName;

                            float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
                            float height = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
                            if (width == 800 && height == 960)
                            {
                                if (fileNameEdited.Contains('&'))
                                    fileNameEdited = fileNameEdited.Replace('&', 'a');
                                fileNameEdited = fileNameEdited.Replace("-", "");

                                FileUpload1.SaveAs(Server.MapPath("~/images/Projects/") + fileNameEdited);
                                //FixedSize(Convert.ToInt32(width), Convert.ToInt32(height), 331, 233, true, Server.MapPath("~/images/Projects/") + fileNameEdited, Server.MapPath("~/images/Projects/resize/") + fileNameEdited);
                                objproject.image = fileNameEdited;
                            }
                            else
                            {
                                ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Image minimum dimensions should be 800 * 960');</script>");
                                return;
                            }
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif or .jpg or .png file');</script>");
                            return;
                        }
                    }
                    //else
                    //{
                    //    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please upload Image');</script>");
                    //    return;
                    //}
                    #endregion
                }

                objproject.projectid = Convert.ToInt32(ddlProject.SelectedValue);
                objproject.menuid = Convert.ToInt32(ddlMenu.SelectedValue);
                objproject.description = txtDescription.Text;
                objproject.status = "A";
                objproject.createddate = DateTime.Now;
                dataContext.ProjectsInnerDetails.InsertOnSubmit(objproject);
                dataContext.SubmitChanges();
                Response.Redirect("viewprojectinnerdetails.aspx?save=1");
                #endregion

            }
        }
        else
        {
            int id = Convert.ToInt32(Session["innerprojectId"]);
             #region to check if same city exists

            var objChk = (from obj in dataContext.ProjectsInnerDetails
                          where obj.ID!= id && obj.projectid == Convert.ToInt32(ddlProject.SelectedValue) && obj.menuid==Convert.ToInt32(ddlMenu.SelectedValue)
                          select obj).FirstOrDefault();

            #endregion
            if (objChk != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Project Detail already exists.');</script>");
                return;
            }
            else
            {
                #region Code For Updation
                var objproject = dataContext.ProjectsInnerDetails.Single(doc => doc.ID == id);
                if (objproject.menuid != 3)
                {
                    #region Update Image
                    if (FileUpload1.HasFile)
                    {
                        if (FileUpload1.PostedFile.ContentLength > 5242880)
                        {
                            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual File size should not be more than 5MB');</script>");
                            return;
                        }
                        string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                        if (imgext.ToLower() == ".gif" || imgext.ToLower() == ".jpg" || imgext.ToLower() == ".png")
                        {
                            string fileName = Path.GetFileName(FileUpload1.FileName);
                            string fileNameEdited = random.ToString() + "_" + fileName;

                            float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
                            float height = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
                            if (width == 800 && height == 960)
                            {
                                if (fileNameEdited.Contains('&'))
                                    fileNameEdited = fileNameEdited.Replace('&', 'a');
                                fileNameEdited = fileNameEdited.Replace("-", "");

                                FileUpload1.SaveAs(Server.MapPath("~/images/Projects/") + fileNameEdited);
                                //FixedSize(Convert.ToInt32(width), Convert.ToInt32(height), 331, 233, true, Server.MapPath("~/images/Projects/") + fileNameEdited, Server.MapPath("~/images/Projects/resize/") + fileNameEdited);
                                objproject.image = fileNameEdited;
                            }
                            else
                            {
                                ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Image minimum dimensions should be 800 * 960');</script>");
                                return;
                            }
                            //}
                            //else
                            //{
                            //    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Upload Image of Size 297 x 35 px');</script>");
                            //    return;
                            //}
                            try
                            {
                                if (ViewState["Image1"] != null && ViewState["Image1"].ToString() != "")
                                    File.Delete(Server.MapPath(@"images/Projects/" + ViewState["Image1"].ToString()));
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif or .jpg or .png file');</script>");
                            return;
                        }
                    }
                    #endregion
                }
                objproject.projectid = Convert.ToInt32(ddlProject.SelectedValue);
                objproject.menuid = Convert.ToInt32(ddlMenu.SelectedValue);
                objproject.description = txtDescription.Text;
                objproject.createddate = DateTime.Now;
                dataContext.SubmitChanges();
                Session["innerprojectId"] = null;
                Response.Redirect("viewprojectinnerdetails.aspx?saved=1");
                #endregion

            }
        }
        Div1.Style.Add("display", "block");
    }

    public void FixedSize(int srcwidth, int srcheight, int Width, int Height, bool needToFill, string srcpath, string targetpath)
    {
        try
        {
            System.Drawing.Image imgPhoto = System.Drawing.Image.FromFile(srcpath);
            int sourceWidth = srcwidth;
            int sourceHeight = srcheight;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)Width / (float)sourceWidth);
            nPercentH = ((float)Height / (float)sourceHeight);
            if (!needToFill)
            {
                if (nPercentH < nPercentW)
                {
                    nPercent = nPercentH;
                }
                else
                {
                    nPercent = nPercentW;
                }
            }
            else
            {
                if (nPercentH > nPercentW)
                {
                    nPercent = nPercentH;
                    destX = (int)Math.Round((Width -
                        (sourceWidth * nPercent)) / 2);
                }
                else
                {
                    nPercent = nPercentW;
                    destY = (int)Math.Round((Height -
                        (sourceHeight * nPercent)) / 2);
                }
            }

            if (nPercent > 1)
                nPercent = 1;

            int destWidth = (int)Math.Round(sourceWidth * nPercent);
            int destHeight = (int)Math.Round(sourceHeight * nPercent);

            System.Drawing.Bitmap bmPhoto = new System.Drawing.Bitmap(
                destWidth <= Width ? destWidth : Width,
                destHeight < Height ? destHeight : Height,
                              PixelFormat.Format32bppRgb);
            //bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
            //                 imgPhoto.VerticalResolution);

            System.Drawing.Graphics grPhoto = System.Drawing.Graphics.FromImage(bmPhoto);
            grPhoto.Clear(System.Drawing.Color.White);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
            //InterpolationMode.HighQualityBicubic;
            grPhoto.CompositingQuality = CompositingQuality.Default;
            grPhoto.SmoothingMode = SmoothingMode.Default;

            grPhoto.DrawImage(imgPhoto,
                new System.Drawing.Rectangle(destX, destY, destWidth, destHeight),
                new System.Drawing.Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                System.Drawing.GraphicsUnit.Pixel);
            bmPhoto.Save(targetpath);
            grPhoto.Dispose();
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }


    }

    private void BindMenuList(int projectid)
    {
        #region Code for Services
        var objMenu = from obj in dataContext.Menus
                      join prj in dataContext.ProjectMenus on obj.menuid equals prj.menuid
                      where prj.projectid == projectid
                      orderby obj.Priority
                      select new { obj.menuid, obj.menutitle };
        ddlMenu.DataTextField = "menutitle";
        ddlMenu.DataValueField = "menuid";
        ddlMenu.DataSource = objMenu;
        ddlMenu.DataBind();
        ddlMenu.Items.Insert(0, new ListItem("--Select--", "0"));
        #endregion
    }

    protected void ddlProject_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlProject.SelectedValue != "0")
        {
            BindMenuList(Convert.ToInt32(ddlProject.SelectedValue));
        }
        else
        {
            ddlMenu.DataSource = null;
            ddlMenu.DataBind();
        }
    }
}
