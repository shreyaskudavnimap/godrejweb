﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;
//using System.Transactions;

public partial class Admin_reply_expert : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    Guid random = Guid.NewGuid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");

        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["ExpertID"])))
            {
                FillData(Convert.ToString(Request.QueryString["ExpertID"]));
            }
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("EnquiryAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("ExpertLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

    }

    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("ViewAskExpert.aspx");
    }

    private void FillData(string id)
    {
        var objFaq = (from obj in dataContext.Faqs
                      join pj in dataContext.tbl_EmpanelmentContacts
                      on obj.userid equals pj.BrokerID
                      where obj.id == Convert.ToInt32(id)
                      select new
                      {
                          pj.username,
                          obj.question,
                          obj.answer
                      }).SingleOrDefault();


        txtUser.Text = objFaq.username;
        txtQuestion.Text = objFaq.question;
        if (objFaq.answer != "")
        {
          
            txtAnswer.Text = objFaq.answer;
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        var test = "";
        if (string.IsNullOrEmpty(Convert.ToString(Session["ExpertID"])))
        {
           
            #region add new record
            Faq objFaq = new Faq();
            test = System.Text.RegularExpressions.Regex.Replace(txtAnswer.Text, "<[^>]*>", "");
            objFaq.answer = test.Trim();
            //objFaq.answer = txtAnswer.Text;
            objFaq.ReplyDate = DateTime.Now;
            dataContext.Faqs.InsertOnSubmit(objFaq);
            dataContext.SubmitChanges();
            Response.Redirect("ViewAskExpert.aspx?save=1");
            #endregion
        }
        else
        {
            int id = Convert.ToInt32(Session["ExpertID"]);
            #region Code For Updation
            var objFaq = dataContext.Faqs.Single(doc => doc.id == id);
            test = System.Text.RegularExpressions.Regex.Replace(txtAnswer.Text, "<[^>]*>", "");
            objFaq.answer = test.Trim();
            //objFaq.answer = txtAnswer.Text;
            objFaq.ReplyDate = DateTime.Now;
            dataContext.SubmitChanges();
            Session["ExpertID"] = null;
            Response.Redirect("ViewAskExpert.aspx?saved=1");
            #endregion
        }
        Div1.Style.Add("display", "block");
    }
}
