﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Transactions;

public partial class Admin_add_projectdetails : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    Guid random = Guid.NewGuid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");

        if (!IsPostBack)
        {
            BindCity();
            BindMenuList();

            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["projectId"])))
            {
                FillData(Convert.ToString(Request.QueryString["projectId"]));
            }
        }

        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("ProjectAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("ProjectDetailsLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

    }

    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewprojectdetails.aspx");
    }

    private void BindCity()
    {
        var query = (from obj in dataContext.CityMasters
                     where obj.status == "A"
                     orderby obj.cityname
                     select new
                     {
                         obj.cityid,
                         obj.cityname
                     });

        if (query.Count() > 0)
        {
            ddlCity.DataTextField = "cityname";
            ddlCity.DataValueField = "cityid";
            ddlCity.DataSource = query;
            ddlCity.DataBind();
            ddlCity.Items.Insert(0, new ListItem("Select City", "0"));
        }
    }

    private void BindPlace(int cityid)
    {
        var query = (from obj in dataContext.PlaceMasters
                     where obj.cityid == cityid && obj.status == "A"
                     select new
                     {
                         obj.placeid,
                         obj.placename
                     });

        ddlPlace.DataTextField = "placename";
        ddlPlace.DataValueField = "placeid";
        ddlPlace.DataSource = query;
        ddlPlace.DataBind();
        ddlPlace.Items.Insert(0, new ListItem("Select Place", "0"));
    }

    private void FillData(string projectid)
    {
        var objProject = dataContext.Projects.Single(doc => doc.projectid == Convert.ToInt32(projectid));
        txtTitle.Text = objProject.title;
        txtSfdc.Text = objProject.SFDC;
        txtSFDCProjectID.Text = objProject.SFDCProjectID;
        txtLocation.Text = objProject.Location;
        ddlType.SelectedValue = objProject.projecttype;
        ddlStatus.SelectedValue = objProject.projectstatus;
        ddlCity.SelectedValue = Convert.ToString(objProject.cityid);
        BindPlace(Convert.ToInt32(objProject.cityid));
        ddlPlace.SelectedValue = Convert.ToString(objProject.placeid);
        var objGetobjProjID = (from obj in dataContext.ProjectMenus
                               where obj.projectid == Convert.ToInt32(projectid)
                               select new { obj.menuid });
        foreach (var oItem in objGetobjProjID)
        {
            lstMenu.Items.FindByValue(oItem.menuid.ToString()).Selected = true;
        }

    }

    private void BindMenuList()
    {
        #region Code for Services
        var objMenu = from obj in dataContext.Menus orderby obj.Priority select new { obj.menuid, obj.menutitle };
        lstMenu.DataTextField = "menutitle";
        lstMenu.DataValueField = "menuid";
        lstMenu.DataSource = objMenu;
        lstMenu.DataBind();
        #endregion
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        int ichecked = 0;
        int count = lstMenu.Items.Count;
        for (int i = 0; i < count; i++)
        {
            if (lstMenu.Items[i].Selected == true)
            {
                ichecked = 1;
            }
        }
        //if (ichecked != 1)
        //{
        //    ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Please Select Internal Project Menu.');</script>");

        //    return;
        //}
        if (string.IsNullOrEmpty(Convert.ToString(Session["projectid"])))
        {
            #region add new record
            Project objproject = new Project();
            #region Save Image
            //if (FileUpload1.HasFile)
            //{
            //    if (FileUpload1.PostedFile.ContentLength > 5242880)
            //    {
            //        ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual Image size should not be more than 5MB');</script>");
            //        return;
            //    }
            //    string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
            //    if (imgext.ToLower() == ".gif" || imgext.ToLower() == ".jpg" || imgext.ToLower() == ".png")
            //    {
            //        string fileName = Path.GetFileName(FileUpload1.FileName);
            //        string fileNameEdited = random.ToString() + "_" + fileName;

            //        float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
            //        float height = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
            //        Response.Write(Server.MapPath("~/images/Projects/"));
            //        FileUpload1.SaveAs(Server.MapPath("~/images/Projects/") + fileNameEdited);
            //        objproject.image = fileNameEdited;
            //    }
            //    else
            //    {
            //        ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif or .jpg or .png file');</script>");
            //        return;
            //    }
            //}
            //else
            //{
            //    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload Image');</script>");
            //    return;
            //}
            #endregion
            objproject.title = txtTitle.Text;
            objproject.cityid = Convert.ToInt32(ddlCity.SelectedValue);
            objproject.placeid = Convert.ToInt32(ddlPlace.SelectedValue);
            objproject.SFDC = txtSfdc.Text;
            objproject.status = "A";
            objproject.SFDCProjectID = txtSFDCProjectID.Text;
            objproject.Location = txtLocation.Text;
            objproject.createddate = DateTime.Now;
            objproject.createdby = Convert.ToInt32(Session["User"]);
            objproject.projectstatus = ddlStatus.SelectedItem.Text;
            objproject.projecttype = ddlType.SelectedItem.Text;
            using (TransactionScope ts = new TransactionScope())
            {
                dataContext.Projects.InsertOnSubmit(objproject);
                dataContext.SubmitChanges();

                var objGetHotelID = (from obj in dataContext.Projects
                                     orderby obj.projectid descending
                                     select new { obj.projectid }).Take(1).SingleOrDefault();

                foreach (System.Web.UI.WebControls.ListItem oItem in lstMenu.Items)
                {
                    if (oItem.Selected)
                    {
                        ProjectMenu objPM = new ProjectMenu();
                        objPM.projectid = objGetHotelID.projectid;
                        objPM.menuid = Convert.ToInt32(oItem.Value);
                        dataContext.ProjectMenus.InsertOnSubmit(objPM);
                        dataContext.SubmitChanges();
                    }
                }
                ts.Complete();    // now both inserts are committed to the db              
            }
            Response.Redirect("viewprojectdetails.aspx?save=1");
            #endregion
        }
        else
        {
            #region Code For Updation
            int id = Convert.ToInt32(Session["projectid"]);
            var objProject = dataContext.Projects.Single(doc => doc.projectid == id);
            #region Update Image
            //if (FileUpload1.HasFile)
            //{
            //    if (FileUpload1.PostedFile.ContentLength > 5242880)
            //    {
            //        ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual File size should not be more than 5MB');</script>");
            //        return;
            //    }
            //    string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
            //    if (imgext.ToLower() == ".gif" || imgext.ToLower() == ".jpg" || imgext.ToLower() == ".png")
            //    {
            //        string fileName = Path.GetFileName(FileUpload1.FileName);
            //        string fileNameEdited = random.ToString() + "_" + fileName;

            //        float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
            //        float height = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
            //        //width : 297px height : 35px
            //        //if (width == 297 && height == 35)
            //        //{
            //        FileUpload1.SaveAs(Server.MapPath("images/Projects/") + fileNameEdited);
            //        objProject.image = fileNameEdited;
            //        //}
            //        //else
            //        //{
            //        //    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Upload Image of Size 297 x 35 px');</script>");
            //        //    return;
            //        //}
            //        try
            //        {
            //            if (ViewState["Image1"] != null && ViewState["Image1"].ToString() != "")
            //                File.Delete(Server.MapPath(@"images/Projects/" + ViewState["Image1"].ToString()));
            //        }
            //        catch (Exception ex)
            //        {
            //        }
            //    }
            //    else
            //    {
            //        ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif or .jpg or .png file');</script>");
            //        return;
            //    }
            //}
            #endregion
            objProject.title = txtTitle.Text;
            objProject.cityid = Convert.ToInt32(ddlCity.SelectedValue);
            objProject.placeid = Convert.ToInt32(ddlPlace.SelectedValue);
            objProject.SFDC = txtSfdc.Text;
            objProject.SFDCProjectID = txtSFDCProjectID.Text;
            objProject.Location = txtLocation.Text;
            objProject.createddate = DateTime.Now;
            objProject.createdby = Convert.ToInt32(Session["UserID"]);
            objProject.projectstatus = ddlStatus.SelectedItem.Text;
            objProject.projecttype = ddlType.SelectedItem.Text;
            using (TransactionScope ts = new TransactionScope())
            {
                dataContext.SubmitChanges();

                var objProjMen = from d in dataContext.ProjectMenus where (d.projectid == id) select d;
                foreach (ProjectMenu item in objProjMen)
                {
                    dataContext.ProjectMenus.DeleteOnSubmit(item);
                    dataContext.SubmitChanges();
                }

                foreach (System.Web.UI.WebControls.ListItem oItem in lstMenu.Items)
                {
                    if (oItem.Selected)
                    {
                        ProjectMenu objPM = new ProjectMenu();
                        objPM.projectid = id;
                        objPM.menuid = Convert.ToInt32(oItem.Value);
                        dataContext.ProjectMenus.InsertOnSubmit(objPM);
                        dataContext.SubmitChanges();
                    }
                    else
                    {
                        var objHS = from d in dataContext.ProjectMenus where (d.projectid == id && d.menuid == Convert.ToInt32(oItem.Value)) select d;
                        dataContext.ProjectMenus.DeleteAllOnSubmit(objHS);
                    }
                }
                ts.Complete();    // now both inserts are committed to the db
            }
            Session["projectid"] = null;
            Response.Redirect("viewprojectdetails.aspx?saved=1");
            #endregion
        }
        Div1.Style.Add("display", "block");
    }

    protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCity.SelectedValue != "0")
        {
            txtSfdc.Text = txtTitle.Text + ", " + ddlCity.SelectedItem.Text;
            BindPlace(Convert.ToInt32(ddlCity.SelectedValue));
        }
        else
        {
            ddlPlace.DataSource = null;
            ddlPlace.DataBind();
        }
    }
}
