﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmpanelmentForm.aspx.cs" Inherits="Admin_EmpanelmentForm" MaintainScrollPositionOnPostback="true"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Admin/MenuControl.ascx" TagName="Menu" TagPrefix="UC1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
    <!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
    <!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
    <!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
    <!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
    <!--[if gt IE 8]><!-->
    <!--<![endif]-->
    <meta charset="utf-8">
    <link rel="dns-prefetch" href="http://fonts.googleapis.com" />
    <link rel="dns-prefetch" href="http://themes.googleusercontent.com" />
    <link rel="dns-prefetch" href="http://ajax.googleapis.com" />
    <link rel="dns-prefetch" href="http://cdnjs.cloudflare.com" />
    <link rel="dns-prefetch" href="http://agorbatchev.typepad.com" />
    <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/b/378 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Godrej Channel Partner :: Admin</title>
    <meta name="description" content="Godrej Channel Partner App">
    <meta name="author" content="Godrej Channel Partner App">
    <!-- Mobile viewport optimized: h5bp.com/viewport -->
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
    <!-- iPhone: Don't render numbers as call links -->
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" href="favicon.ico" />
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory: mathiasbynens.be/notes/touch-icons -->
    <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->
    <!-- The Styles -->
    <!-- ---------- -->
    <!-- Layout Styles -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/layout.css">
    <!-- Icon Styles -->
    <link rel="stylesheet" href="css/icons.css">
    <link rel="stylesheet" href="css/fonts/font-awesome.css">
    <!--[if IE 8]><link rel="stylesheet" href="css/fonts/font-awesome-ie7.css"><![endif]-->
    <!-- External Styles -->
    <link rel="stylesheet" href="css/external/jquery-ui-1.9.1.custom.css">
    <link rel="stylesheet" href="css/external/jquery.chosen.css">
    <link rel="stylesheet" href="css/external/jquery.cleditor.css">
    <link rel="stylesheet" href="css/external/jquery.colorpicker.css">
    <link rel="stylesheet" href="css/external/jquery.elfinder.css">
    <link rel="stylesheet" href="css/external/jquery.fancybox.css">
    <link rel="stylesheet" href="css/external/jquery.jgrowl.css">
    <link rel="stylesheet" href="css/external/jquery.plupload.queue.css">
    <link rel="stylesheet" href="css/external/syntaxhighlighter/shCore.css" />
    <link rel="stylesheet" href="css/external/syntaxhighlighter/shThemeDefault.css" />
    <!-- Elements -->
    <link rel="stylesheet" href="css/elements.css">
    <link rel="stylesheet" href="css/forms.css">
    <!-- OPTIONAL: Print Stylesheet for Invoice -->
    <link rel="stylesheet" href="css/print-invoice.css">
    <!-- Typographics -->
    <link rel="stylesheet" href="css/typographics.css">
    <!-- Responsive Design -->
    <link rel="stylesheet" href="css/media-queries.css">
    <!-- Bad IE Styles -->
    <link rel="stylesheet" href="css/ie-fixes.css">
    <!-- The Scripts -->
    <!-- ----------- -->
    <!-- JavaScript at the top (will be cached by browser) -->
    <!-- Grab frameworks from CDNs -->
    <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.js"></script>

    <script>        window.jQuery || document.write('<script src="js/libs/jquery-1.8.2.js"><\/script>')</script>

    <!-- Do the same with jQuery UI -->

    <script src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>

    <script>        window.jQuery.ui || document.write('<script src="js/libs/jquery-ui-1.9.1.js"><\/script>')</script>

    <!-- Do the same with Lo-Dash.js -->
    <!--[if gt IE 8]><!-->

    <script src="http://cdnjs.cloudflare.com/ajax/libs/lodash.js/0.8.2/lodash.js"></script>

    <script>        window._ || document.write('<script src="js/libs/lo-dash.js"><\/script>')</script>

    <!--<![endif]-->
    <!-- IE8 doesn't like lodash -->
    <!--[if lt IE 9]><script src="http://documentcloud.github.com/underscore/underscore.js"></script><![endif]-->
    <!-- Do the same with require.js -->

    <script src="http://cdnjs.cloudflare.com/ajax/libs/require.js/2.0.6/require.js"></script>

    <script>        window.require || document.write('<script src="js/libs/require-2.0.6.min.js"><\/script>')</script>

    <!-- Load Webfont loader -->

    <script type="text/javascript">
        window.WebFontConfig = {
            google: { families: ['PT Sans:400,700'] },
            active: function () { $(window).trigger('fontsloaded') }
        };

        function doClick(buttonName, e) {
            //the purpose of this function is to allow the enter key to 
            //point to the correct button to click.
            var key;

            if (window.event)
                key = window.event.keyCode;     //IE
            else
                key = e.which;     //firefox

            if (key == 13) {
                //Get the button the user wants to have clicked
                var btn = document.getElementById(buttonName);
                if (btn != null) { //If we find the button click it
                    btn.click();
                    event.keyCode = 0
                }
            }
        }
        // To clear default value 
        function clearDefault(ele) {
            if (ele.defaultValue == ele.value) {
                ele.value = ''
            }
        }

        // To restore default value 
        function restore(ele) {
            if (ele.value == '') {
                ele.value = ele.defaultValue;
            }
        }
        //$(".close_div_Gst").click(function () {
        function closeGst() {
            $('#div_Gst').css('display', 'none');
            $('#div_Rera').css('display', 'none');

        }
        //$(".close_div_Rera").click(function () {
        function closeRera() {
            $('#div_Gst').css('display', 'none');
            $('#div_Rera').css('display', 'none');

        }

        $(document).ready(function (e) {
            $("#AddReraPanel").hide();
            $("#AddGstPanel").hide();
            setTimeout(function () {
                $("#IsSameRegisCommAddress").removeClass("hidden");
            }, 1000);
        });

        function showReraBtn() {
            $("#AddReraPanel").toggle();
        }

        function showGstBtn() {
            $("#AddGstPanel").toggle();
        }

        function validateJunkChar(pwd) {
            if (pwd.includes("–")) {
                alert("Invalid character (–) found");
            }
        }
    </script>

    <script defer async src="https://ajax.googleapis.com/ajax/libs/webfont/1.0.28/webfont.js"></script>

    <!-- Essential polyfills -->

    <script src="js/mylibs/polyfills/modernizr-2.6.1.min.js"></script>

    <script src="js/mylibs/polyfills/respond.js"></script>

    <script src="js/mylibs/polyfills/matchmedia.js"></script>

    <!--[if lt IE 9]><script src="js/mylibs/polyfills/selectivizr.js"></script><![endif]-->
    <!--[if lt IE 10]><script src="js/mylibs/polyfills/excanvas.js"></script><![endif]-->
    <!--[if lt IE 10]><script src="js/mylibs/polyfills/classlist.js"></script><![endif]-->
    <!-- scripts concatenated and minified via build script -->
    <!-- Scripts required everywhere -->

    <script src="js/mylibs/jquery.hashchange.js"></script>

    <script src="js/mylibs/jquery.idle-timer.js"></script>

    <script src="js/mylibs/jquery.plusplus.js"></script>

    <script src="js/mylibs/jquery.scrollTo.js"></script>

    <script src="js/mylibs/jquery.ui.touch-punch.js"></script>

    <script src="js/mylibs/jquery.ui.multiaccordion.js"></script>

    <script src="js/mylibs/number-functions.js"></script>

    <script src="js/mylibs/fullstats/jquery.css-transform.js"></script>

    <script src="js/mylibs/fullstats/jquery.animate-css-rotate-scale.js"></script>

    <script src="js/mylibs/forms/jquery.validate.js"></script>

    <!-- Do not touch! -->

    <script src="js/mango.js"></script>

    <script src="js/plugins.js"></script>

    <script src="js/script.js"></script>

    <!-- Your custom JS goes here -->

    <script src="js/app.js"></script>

    <!-- end scripts -->

</head>
<style>
    .hand-cursor{
        cursor: pointer;
    }
</style>
<body>
    <form id="form1" runat="server">
        <!-- The loading box -->
        <div id="loading-overlay">
        </div>
        <div id="loading">
            <span>Loading...</span>
        </div>
        <!-- End of loading box -->
        <!--------------------------------->
        <!-- Now, the page itself begins -->
        <!--------------------------------->
        <!-- The toolbar at the top -->
        <section id="toolbar">
        <div class="container_12">
            <!-- Right side -->
            <div class="right">
                <ul>
                    <li class="red"><a href="AdminLogin.aspx">Logout</a></li>
                </ul>
            </div>
            <!-- End of .right -->
            <!-- Phone only items -->
            <div class="phone">
                <!-- Navigation -->
                <li><a class="navigation" href="#"><span class="icon icon-list"></span></a></li>
            </div>
            <!-- End of phone items -->
        </div>
        <!-- End of .container_12 -->
    </section>
        <!-- End of #toolbar -->
        <!-- The header containing the logo -->
        <header class="container_12">	
		<div class="container">
			<!-- Your logos -->
			<a href="viewcity.aspx"><img src="img/logo.png" alt="Godrej Channel Partner App"></a>
			<a class="phone-title" href="viewbanner.aspx"><img src="img/logo-mobile.png" alt="Godrej Channel Partner App" height="30" /></a>
		</div><!-- End of .container -->
	</header>
        <!-- End of header -->
        <!-- The container of the sidebar and content box -->
        <div role="main" id="main" class="container_12 clearfix">
            <!-- The blue toolbar stripe -->
            <section class="toolbar">
            <asp:TextBox ID="txtSearch" onfocus="clearDefault(this)" Text="search by name" onblur="restore(this)" runat="server" CssClass="tooltip"></asp:TextBox>
            <asp:Button ID="btnSubmit" runat="server" CssClass="submit go" Text="Go"
            onclick="btnSubmit_Click" />
        </section>
            <!-- End of .toolbar-->
            <!-- The sidebar -->
            <UC1:Menu ID="Menu" runat="server" />
            <!-- End of sidebar -->
            <!-- Here goes the content. -->
            <section id="content" class="container_12 clearfix" data-sort="true">
            <h1 class="grid_12">
                Empanelment Form</h1>
            <div class="grid_12">
                <div class="box">
                    <div id="divPan" runat="server">
                        <div class="header">
                            <h2>
                                Empanelment Details</h2>
                            <asp:Label ID="Label3" runat="server"  Visible="false"></asp:Label>
                            <asp:Label ID="Label4" runat="server"  Visible="false"></asp:Label>
                        </div>
                        <div class="full validate">
                            <div class="row">
                                <label for="txtTitle">
                                    <strong style="width: 121px;">Enter PAN Number</strong>
                                </label>
                                <div class="TextBox">
                                    <asp:TextBox ID="txt_PanNo" runat="server" width="300" style='text-transform:uppercase' ></asp:TextBox>
                                </div>
                            </div>
                        </div>
                            
                        <div class="actions">
                                <div class="right">
                                    <asp:Button ID="Button2" runat="server"
                                        CssClass="submit" Text="Submit" OnClick="btnPanSubmit_Click"  />
                                </div>
                            </div>
                        <!-- End of .content -->
                    </div>
                    <!-- End of #divGrid -->
                    <div id="divForm" runat="server">
                        <div class="header">
                            <h2>
                                Empanelment Details</h2>
                            <asp:Label ID="lblEmplID" runat="server"  Visible="false"></asp:Label>
                            <asp:Label ID="lblStatus" runat="server"  Visible="false"></asp:Label>
                        </div>
                        <div class="content">
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Company Name</strong>
                                    </label>
                                    <div class="TextBox">
                                        <asp:TextBox ID="input_CompanyName" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Type of entity</strong>
                                    </label>
                                    <div class="TextBox">
                                        <asp:DropDownList ID="input_Entitytype" runat="server">
                                        <asp:ListItem Value="notselected">Type of entity</asp:ListItem>
                                        <asp:ListItem Value="Partnership">Partnership</asp:ListItem>
                                        <asp:ListItem Value="Individual">Individual</asp:ListItem>
                                        <asp:ListItem Value="Proprietor">Proprietor</asp:ListItem>
                                        <asp:ListItem Value="Company">Company</asp:ListItem>
                                        <asp:ListItem Value="LLP">LLP</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">First Name</strong>
                                    </label>
                                    <div class="TextBox">
                                        <asp:TextBox ID="input_FirstName" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Last Name</strong>
                                    </label>
                                    <div class="TextBox">
                                        <asp:TextBox ID="input_LastName" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Mobile</strong>
                                    </label>
                                    <div class="TextBox">
                                        <asp:TextBox ID="input_Mobile" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Email</strong>
                                    </label>
                                    <div class="TextBox">
                                        <asp:TextBox ID="input_Email" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">PAN Card</strong>
                                    </label>
                                    <div class="TextBox">
                                        <asp:Panel ID="panFileUploadPanel" runat="server">
                                            <asp:FileUpload ID="panFileUpload" onchange="this.form.submit()" runat="server" />
                                        </asp:Panel>
                                        <asp:Label ID="input_PanCerti" runat="server">No file selected</asp:Label>
                                        <asp:HyperLink ID="aPan_No" runat="server" Target="_blank">View Doc</asp:HyperLink>
                                    </div>
                                </div>
                            </div>

                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Billing Street</strong>
                                    </label>
                                    <div class="TextBox">
                                        <asp:TextBox ID="input_BillingStreet" runat="server" onkeyup="validateJunkChar(this.value)"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Billing Country</strong>
                                    </label>
                                    <div class="TextBox">
                                        <%--<asp:TextBox ID="input_BillingCountry" runat="server"></asp:TextBox>--%>
                                        <asp:DropDownList ID="input_BillingCountry" OnSelectedIndexChanged="FillBillingState"  AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Billing State</strong>
                                    </label>
                                    <div class="TextBox">
                                        <asp:DropDownList ID="input_BillingState" OnSelectedIndexChanged="FillBillingCity"  AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                        <asp:TextBox ID="inputTxt_BillingState" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Billing City</strong>
                                    </label>
                                    <div class="TextBox">
                                        <asp:DropDownList ID="input_BillingCity" runat="server">
                                        </asp:DropDownList>
                                        <asp:TextBox ID="inputTxt_BillingCity" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="TextBox">
                                        <strong style="width: 121px;">Billing Zip</strong>
                                    </label>
                                    <div class="txtTitle">
                                        <asp:TextBox ID="input_BillingZip" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            

                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Registered Street</strong>
                                    </label>
                                    <div class="TextBox">
                                        <asp:TextBox ID="input_RegisteredStreet" runat="server" onkeyup="validateJunkChar(this.value)"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Registered Country</strong>
                                    </label>
                                    <div class="TextBox">
                                        <asp:DropDownList ID="input_RegisteredCountry" OnSelectedIndexChanged="FillRegisteredState"  AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Registered State</strong>
                                    </label>
                                    <div class="TextBox">
                                        <asp:DropDownList ID="input_RegisteredState" OnSelectedIndexChanged="FillRegisteredCity"  AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                        <asp:TextBox ID="inputTxt_RegisteredState" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Registered City</strong>
                                    </label>
                                    <div class="TextBox">
                                        <asp:DropDownList ID="input_RegisteredCity" runat="server">
                                        </asp:DropDownList>
                                        <asp:TextBox ID="inputTxt_RegisteredCity" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Registered Zip</strong>
                                    </label>
                                    <div class="TextBox">
                                        <asp:TextBox ID="input_RegisteredZip" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="full validate">
                                <div class="row">
                                    <asp:CheckBox ID="IsSameRegisCommAddress" runat="server" AutoPostBack="true" OnCheckedChanged="UpdateRegisCommAddress" />
                                    Check if Registered and Communication Address are the same.
                                </div>
                            </div>

                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Communication Street</strong>
                                    </label>
                                    <div class="TextBox">
                                        <asp:TextBox ID="input_CommunicationStreet" runat="server"  onkeyup="validateJunkChar(this.value)"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Communication Country</strong>
                                    </label>
                                    <div class="TextBox">
                                        <asp:DropDownList ID="input_CommunicationCountry" OnSelectedIndexChanged="FillCommunicationState"  AutoPostBack="true"  runat="server">
                                        </asp:DropDownList> 
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Communication State</strong>
                                    </label>
                                    <div class="TextBox">
                                        <asp:DropDownList ID="input_CommunicationState" OnSelectedIndexChanged="FillCommunicationCity"  AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                        <asp:TextBox ID="inputTxt_CommunicationState" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Communication City</strong>
                                    </label>
                                    <div class="TextBox">
                                        <asp:DropDownList ID="input_CommunicationCity" runat="server">
                                        </asp:DropDownList>
                                        <asp:TextBox ID="inputTxt_CommunicationCity" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Communication Zip</strong>
                                    </label>
                                    <div class="TextBox">
                                        <asp:TextBox ID="input_CommunicationZip" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End of .content -->
                        <div class="header">
                            <h2> View Rera Details <span class="hand-cursor" style="float: right;" onclick="showReraBtn()">Add / Remove</span></h2>
                        </div>
                        <div class="content">
                            <asp:GridView ID="gdvRera" runat="server" AutoGenerateColumns="false" 
                                AllowPaging="true" PageSize="10" OnRowCommand="gdvRera_RowCommand" OnRowDataBound="gdvRera_RowDataBound"
                                CssClass="dynamic styled with-prev-next" OnPageIndexChanging="gdvRera_PageIndexChanging" onrowdeleting="gdvRera_RowDeleting">
                                <Columns>
                                    <asp:BoundField HeaderText="ID" DataField="ID" Visible="false">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="State" DataField="State">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Rera No" DataField="Rera_no">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Rera Certi" DataField="Rera_Certi">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Status" DataField="Status" Visible="false">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="ViewReraDoc" NavigateUrl= <%# "/data/empanelment/" + Eval("Rera_Certi") %> runat="server" Target="_blank">View Doc</asp:HyperLink>
                                            |
                                            <asp:LinkButton ID="DeleteButton" runat="server" CssClass="DeleteButton" CommandName="Delete" CommandArgument='<%# Container.DataItemIndex %>' Text="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                            <div id="AddReraPanel" runat="server">
                                <table cellpadding="10">
                                    <tr>
                                        <%--<td><asp:TextBox ID="input_ReraState" runat="server" placeholder="Enter State"/></td>--%>
                                        <td>
                                            <asp:DropDownList ID="ReraStateDD" runat="server">
                                               <asp:ListItem Value="notselected">SELECT STATE</asp:ListItem>
                                               <asp:ListItem Value="Andhra Pradesh">Andhra Pradesh</asp:ListItem>
                                               <asp:ListItem Value="Arunachal Pradesh">Arunachal Pradesh</asp:ListItem>
                                               <asp:ListItem Value="Assam">Assam</asp:ListItem>
                                               <asp:ListItem Value="Bihar">Bihar</asp:ListItem>
                                               <asp:ListItem Value="Goa">Goa</asp:ListItem>
                                               <asp:ListItem Value="Gujarat">Gujarat</asp:ListItem>
                                               <asp:ListItem Value="Haryana">Haryana</asp:ListItem>
                                               <asp:ListItem Value="Himachal Pradesh">Himachal Pradesh</asp:ListItem>
                                               <asp:ListItem Value="Jammu and Kashmir">Jammu and Kashmir</asp:ListItem>
                                               <asp:ListItem Value="Karnataka">Karnataka</asp:ListItem>
                                               <asp:ListItem Value="Kerala">Kerala</asp:ListItem>
                                               <asp:ListItem Value="Madhya Pradesh">Madhya Pradesh</asp:ListItem>
                                               <asp:ListItem Value="Maharashtra">Maharashtra</asp:ListItem>
                                               <asp:ListItem Value="Manipur">Manipur</asp:ListItem>
                                               <asp:ListItem Value="Meghalaya">Meghalaya</asp:ListItem>
                                               <asp:ListItem Value="Mizoram">Mizoram</asp:ListItem>
                                               <asp:ListItem Value="Nagaland">Nagaland</asp:ListItem>
                                               <asp:ListItem Value="Orissa">Orissa</asp:ListItem>
                                               <asp:ListItem Value="Punjab">Punjab</asp:ListItem>
                                               <asp:ListItem Value="Rajasthan">Rajasthan</asp:ListItem>
                                               <asp:ListItem Value="Sikkim">Sikkim</asp:ListItem>
                                               <asp:ListItem Value="Tamil Nadu">Tamil Nadu</asp:ListItem>
                                               <asp:ListItem Value="Tripura">Tripura</asp:ListItem>
                                               <asp:ListItem Value="Uttar Pradesh">Uttar Pradesh</asp:ListItem>
                                               <asp:ListItem Value="West Bengal">West Bengal</asp:ListItem>
                                               <asp:ListItem Value="Andaman and Nico.In.">Andaman and Nico.In.</asp:ListItem>
                                               <asp:ListItem Value="Chandigarh">Chandigarh</asp:ListItem>
                                               <asp:ListItem Value="Dadra and Nagar Hav.">Dadra and Nagar Hav.</asp:ListItem>
                                               <asp:ListItem Value="Daman and Diu">Daman and Diu</asp:ListItem>
                                               <asp:ListItem Value="Delhi">Delhi</asp:ListItem>
                                               <asp:ListItem Value="Lakshadweep">Lakshadweep</asp:ListItem>
                                               <asp:ListItem Value="Pondicherry">Pondicherry</asp:ListItem>
                                               <asp:ListItem Value="Chhattisgarh">Chhattisgarh</asp:ListItem>
                                               <asp:ListItem Value="Jharkhand">Jharkhand</asp:ListItem>
                                               <asp:ListItem Value="Uttarakhand">Uttarakhand</asp:ListItem>
                                               <asp:ListItem Value="Telangana">Telangana</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td><asp:TextBox ID="input_ReraNo" runat="server" placeholder="Enter Rera Number"/></td>
                                        <td>
                                            <asp:Panel ID="ReraFilePanel" runat="server">
                                            <asp:FileUpload ID="ReraFileUpload" runat="server" />
                                            </asp:Panel>
                                        </td>
                                        <td><asp:Button ID="btnAddRera" runat="server" Text="Save" OnClick="AddRera" /></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- End of .content -->
                        <div class="header">
                            <h2> View Gst Details <span class="hand-cursor" style="float: right;" onclick="showGstBtn()">Add / Remove</span></h2>
                        </div>
                        <div class="content">
                            <asp:GridView ID="gdvGst" runat="server" AutoGenerateColumns="false"
                                AllowPaging="true" PageSize="10" OnRowCommand="gdvGst_RowCommand" OnRowDataBound="gdvGst_RowDataBound"
                                CssClass="dynamic styled with-prev-next" OnPageIndexChanging="gdvGst_PageIndexChanging" onrowdeleting="gdvGst_RowDeleting">
                                <Columns>
                                    <asp:BoundField HeaderText="ID" DataField="ID" Visible="false">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="State" DataField="State">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="GST No" DataField="Gst_no">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="GST Certi" DataField="Gst_Certi">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Status" DataField="Status" Visible="false">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:BoundField>
                                     <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="ViewGstDoc" NavigateUrl= <%# "/data/empanelment/" + Eval("Gst_Certi") %> runat="server" Target="_blank">View Doc</asp:HyperLink>
                                            |
                                            <asp:LinkButton ID="DeleteButton" runat="server" CssClass="DeleteButton" CommandName="Delete" CommandArgument='<%# Container.DataItemIndex %>' Text="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                            <div id="AddGstPanel">
                                <table cellpadding="10">
                                    <tr>
                                        <%--<td><asp:TextBox ID="input_GstState" runat="server" placeholder="Enter State"/></td>--%>
                                        <td>
                                        <asp:DropDownList ID="GstStateDD" runat="server">
                                               <asp:ListItem Value="notselected">SELECT STATE</asp:ListItem>
                                               <asp:ListItem Value="Andhra Pradesh">Andhra Pradesh</asp:ListItem>
                                               <asp:ListItem Value="Arunachal Pradesh">Arunachal Pradesh</asp:ListItem>
                                               <asp:ListItem Value="Assam">Assam</asp:ListItem>
                                               <asp:ListItem Value="Bihar">Bihar</asp:ListItem>
                                               <asp:ListItem Value="Goa">Goa</asp:ListItem>
                                               <asp:ListItem Value="Gujarat">Gujarat</asp:ListItem>
                                               <asp:ListItem Value="Haryana">Haryana</asp:ListItem>
                                               <asp:ListItem Value="Himachal Pradesh">Himachal Pradesh</asp:ListItem>
                                               <asp:ListItem Value="Jammu and Kashmir">Jammu and Kashmir</asp:ListItem>
                                               <asp:ListItem Value="Karnataka">Karnataka</asp:ListItem>
                                               <asp:ListItem Value="Kerala">Kerala</asp:ListItem>
                                               <asp:ListItem Value="Madhya Pradesh">Madhya Pradesh</asp:ListItem>
                                               <asp:ListItem Value="Maharashtra">Maharashtra</asp:ListItem>
                                               <asp:ListItem Value="Manipur">Manipur</asp:ListItem>
                                               <asp:ListItem Value="Meghalaya">Meghalaya</asp:ListItem>
                                               <asp:ListItem Value="Mizoram">Mizoram</asp:ListItem>
                                               <asp:ListItem Value="Nagaland">Nagaland</asp:ListItem>
                                               <asp:ListItem Value="Orissa">Orissa</asp:ListItem>
                                               <asp:ListItem Value="Punjab">Punjab</asp:ListItem>
                                               <asp:ListItem Value="Rajasthan">Rajasthan</asp:ListItem>
                                               <asp:ListItem Value="Sikkim">Sikkim</asp:ListItem>
                                               <asp:ListItem Value="Tamil Nadu">Tamil Nadu</asp:ListItem>
                                               <asp:ListItem Value="Tripura">Tripura</asp:ListItem>
                                               <asp:ListItem Value="Uttar Pradesh">Uttar Pradesh</asp:ListItem>
                                               <asp:ListItem Value="West Bengal">West Bengal</asp:ListItem>
                                               <asp:ListItem Value="Andaman and Nico.In.">Andaman and Nico.In.</asp:ListItem>
                                               <asp:ListItem Value="Chandigarh">Chandigarh</asp:ListItem>
                                               <asp:ListItem Value="Dadra and Nagar Hav.">Dadra and Nagar Hav.</asp:ListItem>
                                               <asp:ListItem Value="Daman and Diu">Daman and Diu</asp:ListItem>
                                               <asp:ListItem Value="Delhi">Delhi</asp:ListItem>
                                               <asp:ListItem Value="Lakshadweep">Lakshadweep</asp:ListItem>
                                               <asp:ListItem Value="Pondicherry">Pondicherry</asp:ListItem>
                                               <asp:ListItem Value="Chhattisgarh">Chhattisgarh</asp:ListItem>
                                               <asp:ListItem Value="Jharkhand">Jharkhand</asp:ListItem>
                                               <asp:ListItem Value="Uttarakhand">Uttarakhand</asp:ListItem>
                                               <asp:ListItem Value="Telangana">Telangana</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td><asp:TextBox ID="input_GstNo" runat="server" placeholder="Enter Gst Number"/></td>
                                        <td>
                                            <asp:Panel ID="GstFilePanel" runat="server">
                                            <asp:FileUpload ID="GstFileUpload" runat="server" />
                                            </asp:Panel>
                                        </td>
                                        <td><asp:Button ID="btnAddGst" runat="server" Text="Save" OnClick="AddGst" /></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="actions">
                                <div class="right">
                                    <asp:Button ID="btnRegister" runat="server"
                                        CssClass="submit" Text="Register" OnClick="btnRegister_Click"  />
                                    <asp:Button ID="btnCancel" runat="server"
                                        CssClass="submit" Text="Cancel" OnClick="btnCancel_Click"  />
                                </div>
                            </div>
                        </div>
                        <!-- End of .content -->

                    </div>
                    <!-- End of #divForm -->

                    <!-- End of #divGrid -->
                    <div id="divEdit" runat="server">
                        <div class="header">
                            <h2>
                                Edit Details</h2>
                            <asp:Label ID="Label1" runat="server"  Visible="false"></asp:Label>
                            <asp:Label ID="Label2" runat="server"  Visible="false"></asp:Label>
                        </div>
                        <div class="content">

                            <div class="full validate">
                                <div class="row">
                                    <table cellpadding="20" width="100%">
                                        <tr>
                                            <td><strong style="width: 121px;">Email</strong></td>
                                            <td><asp:TextBox ID="txtEmail" runat="server" width="300"></asp:TextBox></td>
                                            <td><asp:HiddenField ID="txtEmpId" runat="server" /></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>  
                            <div class="actions">
                                <div class="right">
                                    <asp:Button ID="saveEdit" runat="server" CssClass="submit" Text="Submit" OnClick="editSubmit_Click"/>
                                </div>
                           </div>
                        </div>
                    <!-- End of #divForm -->
                    </div>

                    <div id="divSummary" runat="server" style="text-align:center;">
                        <h2>Your request has been submitted.</h2>
                    </div>
        </section>
            <!-- End of #content -->
        </div>
        <!-- End of #main -->
        <!-- The footer -->
        <footer class="container_12">
        <span class="grid_12">Copyright &copy; 2013 Netbiz Systems </span>
    </footer>
        <!-- End of footer -->
        <!-- Spawn $$.loaded -->

        <script>
            $$.loaded();
        </script>

        <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
        <!--[if lt IE 7 ]>
    <script defer src="http://ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
    <script defer>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
    <![endif]-->
    </form>
</body>
</html>
