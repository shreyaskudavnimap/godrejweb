﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;

public partial class Admin_add_city : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["cityId"])))
            {
                FillData(Convert.ToString(Request.QueryString["cityId"]));
            }
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("MasterAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("CityLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

    }

    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewcity.aspx");
    }

    private void FillData(string cityid)
    {
        var objCity = dataContext.CityMasters.Single(doc => doc.cityid == Convert.ToInt32(cityid));
        txtTitle.Text = objCity.cityname;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["cityId"])))
        {
            #region to check if same city exists

            var objChk = (from obj in dataContext.CityMasters
                          where (obj.cityname).ToLower() == txtTitle.Text.ToLower()
                          select obj).FirstOrDefault();

            #endregion
            if (objChk != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('City Name already exists.');</script>");
                return;
            }
            else
            {
                #region For Adding the Record
                CityMaster objCity = new CityMaster();

                objCity.cityname = txtTitle.Text.Trim();
                objCity.createddate = DateTime.Now;
                objCity.status = "A";
                dataContext.CityMasters.InsertOnSubmit(objCity);
                dataContext.SubmitChanges();
                Response.Redirect("viewcity.aspx?save=1");
                #endregion
            }
        }
        else
        {
            #region Code For Updation
            int id = Convert.ToInt32(Session["cityId"]);
            var objCity = dataContext.CityMasters.Single(doc => doc.cityid == id);
            objCity.cityname = txtTitle.Text.Trim();
            dataContext.SubmitChanges();
            Session["cityId"] = null;
            Response.Redirect("viewcity.aspx?saved=1");
            #endregion
        }
        //Div1.Style.Add("display", "block");
    }
}
