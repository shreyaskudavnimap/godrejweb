﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_TestMail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void btnSend_Click(object sender, EventArgs e)
    {
        try
        {
            EmailService es = new EmailService();
            es.properties.AddToEmail("jipsonk@gmail.com", "Jipson Kuttikkadan");
            es.properties.MailSubject = "Godrej Partner Connect APP | Login credentials";
            string path = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
            StreamReader sr = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Mailer/empanelment_form.html"));
            es.properties.MailBody = sr.ReadToEnd().Replace("user_name", "jipson").Replace("user_email", "jipsonk@gmail.com").Replace("user_pwd", "password");
            es.SendMail();


        }
        catch (Exception ex)
        {
            WLog.AdminWriteLog(ex.ToString(), "TestMail");

        }
    }
}