﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;

public partial class Admin_add_gplpolicy : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    Guid random = Guid.NewGuid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["policyId"])))
            {
                FillData(Convert.ToString(Request.QueryString["policyId"]));
            }
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("GPLPolicyAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("GPLPolicycLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

    }

    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewgplpolicy.aspx");
    }

    private void FillData(string policyId)
    {
        var objGallery = dataContext.GPLPolicies.Single(doc => doc.ID == Convert.ToInt32(policyId));
        txtTitle.Text = objGallery.title;
        FileUpload1.Attributes.Add("maxlength", "1");
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        if (string.IsNullOrEmpty(Convert.ToString(Session["policyId"])))
        {
            #region add new record
            GPLPolicy objOffer = new GPLPolicy();
            #region Save Image
            if (FileUpload1.HasFile)
            {
                //if (FileUpload1.PostedFile.ContentLength > 1048576)
                //{
                //    //ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual Image size should not be more than 1MB');</script>");
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Actual Image size should not be more than 1MB.');</script>", false);
                //    return;
                //}
                string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                if (imgext.ToLower() == ".pdf")
                {
                    string fileName = Path.GetFileName(FileUpload1.FileName);
                    string fileNameEdited = random.ToString() + "_" + fileName;
                    fileNameEdited = fileNameEdited.Replace(" ", "-");
                    //float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
                    //float height = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
                    //if (width == 800 && height == 563)
                    //{
                    FileUpload1.SaveAs(Server.MapPath("~/images/Policy/") + fileNameEdited);
                    objOffer.Image = fileNameEdited;
                    // }
                    //else
                    //{
                    //    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please upload image with minimum dimensions 800 x 563px');</script>");
                    //    return;
                    //}
                }
                else
                {
                    // ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif or .jpg or .png file');</script>");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload only .pdf file.');</script>", false);
                    return;
                }
            }
            else
            {
                // ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload Image');</script>");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload Image');</script>", false);
                return;
            }
            #endregion

            objOffer.title = txtTitle.Text;
            objOffer.status = "A";
            objOffer.createdate = DateTime.Now;
            dataContext.GPLPolicies.InsertOnSubmit(objOffer);
            dataContext.SubmitChanges();
            Response.Redirect("viewgplpolicy.aspx?save=1");
            #endregion
        }
        else
        {
            int id = Convert.ToInt32(Session["policyId"]);
            #region Code For Updation
            var objOffer = dataContext.GPLPolicies.Single(doc => doc.ID == id);
            #region Update Image
            if (FileUpload1.HasFile)
            {
                //if (FileUpload1.PostedFile.ContentLength > 1048576)
                //{
                //    //ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual Image size should not be more than 1MB');</script>");
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Actual Image size should not be more than 1MB.');</script>", false);

                //    return;
                //}
                string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                if (imgext.ToLower() == ".pdf")
                {
                    string fileName = Path.GetFileName(FileUpload1.FileName);
                    string fileNameEdited = random.ToString() + "_" + fileName;
                    FileUpload1.SaveAs(Server.MapPath("~/images/Policy/") + fileNameEdited);
                    objOffer.Image = fileNameEdited;
                    try
                    {
                        if (ViewState["Image1"] != null && ViewState["Image1"].ToString() != "")
                            File.Delete(Server.MapPath(@"img/" + ViewState["Image1"].ToString()));
                    }
                    catch (Exception ex)
                    {
                    }
                }
                else
                {
                    //  ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif or .jpg or .png file');</script>");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload only .jpg, .gif and .png file.');</script>", false);

                    return;
                }
            }
            #endregion

            objOffer.title = txtTitle.Text;
            dataContext.SubmitChanges();
            Session["policyId"] = null;
            Response.Redirect("viewgplpolicy.aspx?saved=1");
            #endregion
        }
        Div1.Style.Add("display", "block");
    }
}
