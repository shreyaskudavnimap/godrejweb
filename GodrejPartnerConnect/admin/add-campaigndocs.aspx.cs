﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;

public partial class Admin_add_campaigndocs : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    Guid random = Guid.NewGuid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        if (!IsPostBack)
        {
            BindCampaign();
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["campaigndocId"])))
            {
                FillData(Convert.ToString(Request.QueryString["campaigndocId"]));
            }
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("CampaignDocAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("CampaignDocLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

    }

    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewcampaigndocs.aspx");
    }

    private void BindCampaign()
    {
        #region Code for Campaign
        var objProject = (from obj in dataContext.Campaigns
                          where obj.status.Trim() == "A"
                          orderby obj.campaignid descending
                          select new
                          {
                              obj.campaignid,
                              obj.title

                          }).Distinct().ToList();
        ddlCampaign.DataTextField = "title";
        ddlCampaign.DataValueField = "campaignid";
        ddlCampaign.DataSource = objProject;
        ddlCampaign.DataBind();
        ddlCampaign.Items.Insert(0, new ListItem("--Select--", "0"));
        #endregion
    }

    private void FillData(string campaigndocId)
    {
        var objGallery = dataContext.CampaignDocs.Single(doc => doc.ID == Convert.ToInt32(campaigndocId));
        ddlCampaign.SelectedValue = Convert.ToString(objGallery.campaignid);
        txtTitle.Text = objGallery.title;
        FileUpload1.Attributes.Add("maxlength", "1");
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        if (string.IsNullOrEmpty(Convert.ToString(Session["campaigndocId"])))
        {
           // #region to check if same offer exists

            //var objChk = (from obj in dataContext.CampaignDocs
            //              where obj.campaignid == Convert.ToInt32(ddlCampaign.SelectedValue)
            //              select obj).FirstOrDefault();

            //#endregion
            //if (objChk != null)
            //{
            //    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Campaign already exists.');</script>");
            //    return;
            //}
            //else
            //{
                #region add new record
                CampaignDoc objOffer = new CampaignDoc();
                #region Save Image
                if (FileUpload1.HasFile)
                {
                    if (FileUpload1.PostedFile.ContentLength > 1048576)
                    {
                        //ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual Image size should not be more than 1MB');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Actual Image size should not be more than 1MB.');</script>", false);
                        return;
                    }
                    string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                    if (imgext.ToLower() == ".jpg" || imgext.ToLower() == ".gif" || imgext.ToLower() == ".png" || imgext.ToLower() == ".pdf" || imgext.ToLower() == ".xls" || imgext.ToLower() == ".doc" || imgext.ToLower() == ".pptx" || imgext == ".docx" || imgext.ToLower() == ".xlsx")
                    {
                        string fileName = Path.GetFileName(FileUpload1.FileName);
                        string fileNameEdited = random.ToString() + "_" + fileName;
                        fileNameEdited=fileNameEdited.Replace(" ", "-");
                        //float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
                        //float height = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
                        //if (width == 800 && height == 563)
                        //{
                        FileUpload1.SaveAs(Server.MapPath("~/images/Campaign/") + fileNameEdited);
                        objOffer.Image = fileNameEdited;
                        // }
                        //else
                        //{
                        //    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please upload image with minimum dimensions 800 x 563px');</script>");
                        //    return;
                        //}
                    }
                    else
                    {
                        // ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif or .jpg or .png file');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload only .jpg,.gif,.png file.');</script>", false);
                        return;
                    }
                }
                else
                {
                    // ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload Image');</script>");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload Image');</script>", false);
                    return;
                }
                #endregion

                objOffer.campaignid = Convert.ToInt32(ddlCampaign.SelectedValue);
                objOffer.title = txtTitle.Text;
                objOffer.status = "A";
                objOffer.createdate = DateTime.Now;
                dataContext.CampaignDocs.InsertOnSubmit(objOffer);
                dataContext.SubmitChanges();
                Response.Redirect("viewcampaigndocs.aspx?save=1");
                #endregion

            //}
        }
        else
        {
            int id = Convert.ToInt32(Session["campaigndocId"]);
            //#region to check if same offers exists

            //var objChk = (from obj in dataContext.CampaignDocs
            //              where obj.ID != id && obj.campaignid == Convert.ToInt32(ddlCampaign.SelectedValue)
            //              select obj).FirstOrDefault();

            //#endregion
            //if (objChk != null)
            //{
            //    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('campaign already exists.');</script>");
            //    return;
            //}
            //else
            //{
                #region Code For Updation
                var objOffer = dataContext.CampaignDocs.Single(doc => doc.ID == id);
                #region Update Image
                if (FileUpload1.HasFile)
                {
                    if (FileUpload1.PostedFile.ContentLength > 1048576)
                    {
                        //ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual Image size should not be more than 1MB');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Actual Image size should not be more than 1MB.');</script>", false);

                        return;
                    }
                    string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                    if (imgext.ToLower() == ".jpg" || imgext.ToLower() == ".gif" || imgext.ToLower() == ".png" || imgext.ToLower() == ".pdf" || imgext.ToLower() == ".xls" || imgext.ToLower() == ".doc" || imgext.ToLower() == ".pptx" || imgext == ".docx" || imgext.ToLower() == ".xlsx")
                    {
                        string fileName = Path.GetFileName(FileUpload1.FileName);
                        string fileNameEdited = random.ToString() + "_" + fileName;

                        //float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
                        //float height = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
                        //if (width == 800 && height == 563)
                        //{
                            FileUpload1.SaveAs(Server.MapPath("~/images/Campaign/") + fileNameEdited);
                            objOffer.Image = fileNameEdited;
                        //}
                        //else
                        //{
                        //    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please upload image with minimum dimensions 800 x 563px');</script>");
                        //    return;
                        //}
                        try
                        {
                            if (ViewState["Image1"] != null && ViewState["Image1"].ToString() != "")
                                File.Delete(Server.MapPath(@"img/" + ViewState["Image1"].ToString()));
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    else
                    {
                        //  ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif or .jpg or .png file');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload only .jpg, .gif and .png file.');</script>", false);

                        return;
                    }
                }
                #endregion
                objOffer.campaignid = Convert.ToInt32(ddlCampaign.SelectedValue);
                objOffer.title = txtTitle.Text;
                dataContext.SubmitChanges();
                Session["campaigndocId"] = null;
                Response.Redirect("viewcampaigndocs.aspx?saved=1");
                #endregion

            //}
        }
        Div1.Style.Add("display", "block");
    }
}
