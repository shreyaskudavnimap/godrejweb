﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;
//using System.Transactions;

public partial class Admin_add_property : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    Guid random = Guid.NewGuid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");

        if (!IsPostBack)
        {
            BindProject();
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["PropertyId"])))
            {
                FillData(Convert.ToString(Request.QueryString["PropertyId"]));
            }
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("PropertyAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("PropertyLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

    }

    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewproperty.aspx");
    }

    private void FillData(string id)
    {
        var objProperty = dataContext.Properties.Single(doc => doc.id == Convert.ToInt32(id));
        ddlProject.SelectedValue = Convert.ToString(objProperty.projectid);
        txtDescription.Text = objProperty.description;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["PropertyId"])))
        {
            #region to check if same offer exists

            var objChk = (from obj in dataContext.Properties
                          where obj.projectid == Convert.ToInt32(ddlProject.SelectedValue)
                          select obj).FirstOrDefault();

            #endregion
            if (objChk != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Property already exists.');</script>");
                return;
            }
            else
            {
                #region add new record
                Property objProperty = new Property();
                #region Save Image
                if (FileUpload1.HasFile)
                {
                    if (FileUpload1.PostedFile.ContentLength > 1048576)
                    {
                        //ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual Image size should not be more than 1MB');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Actual Image size should not be more than 1MB.');</script>", false);
                        return;
                    }
                    string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                    if (imgext.ToLower() == ".jpg" || imgext.ToLower() == ".gif" || imgext.ToLower() == ".png")
                    {
                        string fileName = Path.GetFileName(FileUpload1.FileName);
                        string fileNameEdited = random.ToString() + "_" + fileName;
                        float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
                        float height = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
                        if (width == 800 && height == 563)
                        {
                            FileUpload1.SaveAs(Server.MapPath("~/images/Property/") + fileNameEdited);
                            objProperty.image = fileNameEdited;
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please upload image with minimum dimensions 800 x 563px');</script>");
                            return;
                        }
                    }
                    else
                    {
                        // ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif or .jpg or .png file');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload only .jpg,.gif,.png file.');</script>", false);
                        return;
                    }
                }
                else
                {
                    // ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload Image');</script>");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload Image');</script>", false);
                    return;
                }
                #endregion

                objProperty.projectid = Convert.ToInt32(ddlProject.SelectedValue);
                objProperty.description = txtDescription.Text;
                objProperty.status = "A";
                objProperty.createddate = DateTime.Now;
                dataContext.Properties.InsertOnSubmit(objProperty);
                dataContext.SubmitChanges();
                Response.Redirect("viewproperty.aspx?save=1");
                #endregion

            }
        }
        else
        {
            int id = Convert.ToInt32(Session["PropertyId"]);
             #region to check if same offers exists

            var objChk = (from obj in dataContext.Properties
                          where obj.id != id && obj.projectid == Convert.ToInt32(ddlProject.SelectedValue)
                          select obj).FirstOrDefault();

            #endregion
            if (objChk != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('property already exists.');</script>");
                return;
            }
            else
            {
                #region Code For Updation
                var objProperty = dataContext.Properties.Single(doc => doc.id == id);
                #region Update Image
                if (FileUpload1.HasFile)
                {
                    if (FileUpload1.PostedFile.ContentLength > 1048576)
                    {
                        //ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual Image size should not be more than 1MB');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Actual Image size should not be more than 1MB.');</script>", false);

                        return;
                    }
                    string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                    if (imgext.ToLower() == ".jpg" || imgext.ToLower() == ".gif" || imgext.ToLower() == ".png")
                    {
                        string fileName = Path.GetFileName(FileUpload1.FileName);
                        string fileNameEdited = random.ToString() + "_" + fileName;

                        float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
                        float height = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
                        if (width == 800 && height == 563)
                        {
                            FileUpload1.SaveAs(Server.MapPath("~/images/Property/") + fileNameEdited);
                            objProperty.image = fileNameEdited;
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please upload image with minimum dimensions 800 x 563px');</script>");
                            return;
                        }
                        try
                        {
                            if (ViewState["Image1"] != null && ViewState["Image1"].ToString() != "")
                                File.Delete(Server.MapPath(@"img/" + ViewState["Image1"].ToString()));
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    else
                    {
                        //  ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif or .jpg or .png file');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload only .jpg, .gif and .png file.');</script>", false);

                        return;
                    }
                }
                #endregion
                objProperty.projectid = Convert.ToInt32(ddlProject.SelectedValue);
                objProperty.description = txtDescription.Text;
                dataContext.SubmitChanges();
                Session["PropertyId"] = null;
                Response.Redirect("viewproperty.aspx?saved=1");
                #endregion

            }
        }
        Div1.Style.Add("display", "block");
    }

    private void BindProject()
    {
        #region Code for Project
        var objProject = (from obj in dataContext.Projects
                          join objDesgn in dataContext.CityMasters
                          on obj.cityid equals objDesgn.cityid
                          join objPlace in dataContext.PlaceMasters
                          on obj.placeid equals objPlace.placeid into defaultPlace
                          from c in defaultPlace.DefaultIfEmpty()
                          where obj.status.Trim() == "A"
                          orderby obj.projectid descending
                          select new
                          {
                              obj.projectid,
                              projectname = (c == null ? obj.title + ", " + objDesgn.cityname : obj.title + ", " + c.placename + ", " + objDesgn.cityname)

                          }).Distinct().ToList();
        ddlProject.DataTextField = "projectname";
        ddlProject.DataValueField = "projectid";
        ddlProject.DataSource = objProject;
        ddlProject.DataBind();
        ddlProject.Items.Insert(0, new ListItem("--Select--", "0"));
        #endregion
    }
}
