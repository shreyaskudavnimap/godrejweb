﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;
//using System.Transactions;

public partial class Admin_add_projecthighlights : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    Guid random = Guid.NewGuid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");

        if (!IsPostBack)
        {
            BindProject();
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["highlightId"])))
            {
                FillData(Convert.ToString(Request.QueryString["highlightId"]));
            }
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("ProjectAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("ProjectHighlights");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

    }

    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewprojecthighlights.aspx");
    }

    private void BindProject()
    {
        #region Code for Project
        var objProject = (from obj in dataContext.Projects
                          join objDesgn in dataContext.CityMasters
                          on obj.cityid equals objDesgn.cityid
                          join objPlace in dataContext.PlaceMasters
                          on obj.placeid equals objPlace.placeid into defaultPlace
                          from c in defaultPlace.DefaultIfEmpty()
                          where obj.status.Trim() == "A"
                          orderby obj.projectid descending
                          select new
                          {
                              obj.projectid,
                              projectname = (c == null ? obj.title + ", " + objDesgn.cityname : obj.title + ", " + c.placename + ", " + objDesgn.cityname)

                          }).Distinct().ToList();
        ddlProject.DataTextField = "projectname";
        ddlProject.DataValueField = "projectid";
        ddlProject.DataSource = objProject;
        ddlProject.DataBind();
        ddlProject.Items.Insert(0, new ListItem("--Select--", "0"));
        #endregion
    }

    private void FillData(string id)
    {
        var objProject = dataContext.ProjectHighlights.Single(doc => doc.id == Convert.ToInt32(id));
        ddlProject.SelectedValue = Convert.ToString(objProject.projectid);
        BindMenuList(Convert.ToInt32(objProject.projectid));
        ddlMenu.SelectedValue = Convert.ToString(objProject.menuid);
        txtTitle.Text = objProject.description;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["highlightId"])))
        {
            #region to check if same city exists

            var objChk = (from obj in dataContext.ProjectHighlights
                          where obj.projectid == Convert.ToInt32(ddlProject.SelectedValue) && obj.menuid == Convert.ToInt32(ddlMenu.SelectedValue) && obj.description == txtTitle.Text
                          select obj).FirstOrDefault();

            #endregion
            if (objChk != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Project highlight already exists.');</script>");
                return;
            }
            else
            {
                #region add new record
                ProjectHighlight objproject = new ProjectHighlight();
                #region Save Image
                if (FileUpload1.HasFile)
                {
                    if (FileUpload1.PostedFile.ContentLength > 1048576)
                    {
                        //ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual Image size should not be more than 1MB');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Actual Image size should not be more than 1MB.');</script>", false);
                        return;
                    }
                    string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                    if (imgext.ToLower() == ".jpg" || imgext.ToLower() == ".gif" || imgext.ToLower() == ".png")
                    {
                        string fileName = Path.GetFileName(FileUpload1.FileName);
                        string fileNameEdited = random.ToString() + "_" + fileName;
                        float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
                        float height = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
                        if (width >= 28 && height >= 28)
                        {
                            FileUpload1.SaveAs(Server.MapPath("~/images/Highlights/") + fileNameEdited);
                            objproject.icon = fileNameEdited;
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please upload icon with minimum dimensions 28 x 28px');</script>");
                            return;
                        }
                    }
                    else
                    {
                        // ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif or .jpg or .png file');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload only .jpg,.gif,.png file.');</script>", false);
                        return;
                    }
                }
                //else
                //{
                //    // ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload Image');</script>");
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload Image');</script>", false);
                //    return;
                //}
                #endregion

                objproject.projectid = Convert.ToInt32(ddlProject.SelectedValue);
                objproject.menuid = Convert.ToInt32(ddlMenu.SelectedValue);
                objproject.description = txtTitle.Text;
                objproject.status = "A";
                objproject.createddate = DateTime.Now;
                dataContext.ProjectHighlights.InsertOnSubmit(objproject);
                dataContext.SubmitChanges();
                Response.Redirect("viewprojecthighlights.aspx?save=1");
                #endregion

            }
        }
        else
        {
            int id = Convert.ToInt32(Session["highlightId"]);
             #region to check if same city exists

            var objChk = (from obj in dataContext.ProjectHighlights
                          where obj.id!= id && obj.projectid == Convert.ToInt32(ddlProject.SelectedValue) && obj.menuid==Convert.ToInt32(ddlMenu.SelectedValue) && obj.description==txtTitle.Text
                          select obj).FirstOrDefault();

            #endregion
            if (objChk != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Project highlight already exists.');</script>");
                return;
            }
            else
            {
                #region Code For Updation
                var objproject = dataContext.ProjectHighlights.Single(doc => doc.id == id);
                #region Update Image
                if (FileUpload1.HasFile)
                {
                    if (FileUpload1.PostedFile.ContentLength > 1048576)
                    {
                        //ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual Image size should not be more than 1MB');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Actual Image size should not be more than 1MB.');</script>", false);

                        return;
                    }
                    string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                    if (imgext.ToLower() == ".jpg" || imgext.ToLower() == ".gif" || imgext.ToLower() == ".png")
                    {
                        string fileName = Path.GetFileName(FileUpload1.FileName);
                        string fileNameEdited = random.ToString() + "_" + fileName;

                        float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
                        float height = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
                        if (width >= 28 && height >= 28)
                        {
                            FileUpload1.SaveAs(Server.MapPath("~/images/Highlights/") + fileNameEdited);
                            objproject.icon = fileNameEdited;
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please upload icon with minimum dimensions 28 x 28px');</script>");
                            return;
                        }
                        try
                        {
                            if (ViewState["Image1"] != null && ViewState["Image1"].ToString() != "")
                                File.Delete(Server.MapPath(@"img/" + ViewState["Image1"].ToString()));
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    else
                    {
                        //  ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif or .jpg or .png file');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload only .jpg, .gif and .png file.');</script>", false);

                        return;
                    }
                }
                #endregion
                objproject.projectid = Convert.ToInt32(ddlProject.SelectedValue);
                objproject.menuid = Convert.ToInt32(ddlMenu.SelectedValue);
                objproject.description = txtTitle.Text;
                objproject.createddate = DateTime.Now;
                
                dataContext.SubmitChanges();
                Session["highlightId"] = null;
                Response.Redirect("viewprojecthighlights.aspx?saved=1");
                #endregion

            }
        }
        Div1.Style.Add("display", "block");
    }

    private void BindMenuList(int projectid)
    {
        #region Code for Services
        var objMenu = from obj in dataContext.Menus
                      join prj in dataContext.ProjectMenus on obj.menuid equals prj.menuid
                      where prj.projectid == projectid
                      orderby obj.Priority
                      select new { obj.menuid, obj.menutitle };
        ddlMenu.DataTextField = "menutitle";
        ddlMenu.DataValueField = "menuid";
        ddlMenu.DataSource = objMenu;
        ddlMenu.DataBind();
        ddlMenu.Items.Insert(0, new ListItem("--Select--", "0"));
        #endregion
    }

    protected void ddlProject_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlProject.SelectedValue != "0")
        {
            BindMenuList(Convert.ToInt32(ddlProject.SelectedValue));
        }
        else
        {
            ddlMenu.DataSource = null;
            ddlMenu.DataBind();
        }
    }
}
