﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;
//using System.Transactions;

public partial class Admin_add_news : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    Guid random = Guid.NewGuid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");

        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["newsId"])))
            {
                FillData(Convert.ToString(Request.QueryString["newsId"]));
            }
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("NewsAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("NewsLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

    }

    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewnewsfeed.aspx");
    }

    private void FillData(string id)
    {
        var objNews = dataContext.NewsFeeds.Single(doc => doc.id == Convert.ToInt32(id));
        txtTitle.Text = objNews.title;
        txtDescription.Text = objNews.description;
        txtDate.Text = Convert.ToDateTime(objNews.date).ToShortDateString();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["newsId"])))
        {
            #region to check if same offer exists

            var objChk = (from obj in dataContext.NewsFeeds
                          where obj.title == txtTitle.Text
                          select obj).FirstOrDefault();

            #endregion
            if (objChk != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('News already exists.');</script>");
                return;
            }
            else
            {
                #region add new record
                NewsFeed objNews = new NewsFeed();
                #region Save Image
                if (FileUpload1.HasFile)
                {
                    if (FileUpload1.PostedFile.ContentLength > 1048576)
                    {
                        //ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual Image size should not be more than 1MB');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Actual Image size should not be more than 1MB.');</script>", false);
                        return;
                    }
                    string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                    if (imgext.ToLower() == ".jpg" || imgext.ToLower() == ".gif" || imgext.ToLower() == ".png")
                    {
                        string fileName = Path.GetFileName(FileUpload1.FileName);
                        string fileNameEdited = random.ToString() + "_" + fileName;
                        float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
                        float height = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
                        if (width == 300 && height == 300)
                        {
                            FileUpload1.SaveAs(Server.MapPath("~/images/News/") + fileNameEdited);
                            objNews.image = fileNameEdited;
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please upload image with minimum dimensions 300 x 300px');</script>");
                            return;
                        }
                    }
                    else
                    {
                        // ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif or .jpg or .png file');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload only .jpg,.gif,.png file.');</script>", false);
                        return;
                    }
                }
                else
                {
                    // ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload Image');</script>");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload Image');</script>", false);
                    return;
                }
                #endregion

                objNews.title = txtTitle.Text;
                objNews.description = txtDescription.Text.Replace("\n", "").Replace("\r", "").Replace("\t", "").Replace("</p>", "").Replace("<p>", "");
                objNews.date = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", null);
                objNews.status = "A";
                objNews.createdon = DateTime.Now;
                dataContext.NewsFeeds.InsertOnSubmit(objNews);
                dataContext.SubmitChanges();
                Response.Redirect("viewnewsfeed.aspx?save=1");
                #endregion

            }
        }
        else
        {
            int id = Convert.ToInt32(Session["newsId"]);
             #region to check if same offers exists

            var objChk = (from obj in dataContext.NewsFeeds
                          where obj.id != id && obj.title == txtTitle.Text
                          select obj).FirstOrDefault();

            #endregion
            if (objChk != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('News already exists.');</script>");
                return;
            }
            else
            {
                #region Code For Updation
                var objNews = dataContext.NewsFeeds.Single(doc => doc.id == id);
                #region Update Image
                if (FileUpload1.HasFile)
                {
                    if (FileUpload1.PostedFile.ContentLength > 1048576)
                    {
                        //ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual Image size should not be more than 1MB');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Actual Image size should not be more than 1MB.');</script>", false);

                        return;
                    }
                    string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                    if (imgext.ToLower() == ".jpg" || imgext.ToLower() == ".gif" || imgext.ToLower() == ".png")
                    {
                        string fileName = Path.GetFileName(FileUpload1.FileName);
                        string fileNameEdited = random.ToString() + "_" + fileName;

                        float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
                        float height = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
                        if (width == 300 && height == 300)
                        {
                            FileUpload1.SaveAs(Server.MapPath("~/images/News/") + fileNameEdited);
                            objNews.image = fileNameEdited;
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please upload image with minimum dimensions 300 x 300px');</script>");
                            return;
                        }
                        try
                        {
                            if (ViewState["Image1"] != null && ViewState["Image1"].ToString() != "")
                                File.Delete(Server.MapPath(@"img/" + ViewState["Image1"].ToString()));
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    else
                    {
                        //  ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif or .jpg or .png file');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload only .jpg, .gif and .png file.');</script>", false);

                        return;
                    }
                }
                #endregion
                objNews.title = txtTitle.Text;
                objNews.description = txtDescription.Text.Replace("\n", "").Replace("\r", "").Replace("\t", "").Replace("</p>", "").Replace("<p>", "");
                objNews.date = Convert.ToDateTime(txtDate.Text);
                dataContext.SubmitChanges();
                Session["newsId"] = null;
                Response.Redirect("viewnewsfeed.aspx?saved=1");
                #endregion

            }
        }
        Div1.Style.Add("display", "block");
    }

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        if (Cal1.Visible == false)
        {
            Cal1.Visible = true;
        }
        else
        {
            Cal1.Visible = false;
        }
    }

    protected void Cal1_SelectionChanged(object sender, EventArgs e)
    {
        txtDate.Text = Cal1.SelectedDate.ToString("dd/MM/yyyy");
        Cal1.Visible = false;
    }
}
