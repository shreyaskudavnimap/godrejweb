﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ScheduleNotification.aspx.cs" Inherits="admin_ScheduleNotification" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Admin/MenuControl.ascx" TagName="Menu" TagPrefix="UC1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
    <!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
    <!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
    <!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
    <!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
    <!--[if gt IE 8]><!-->
    <!--<![endif]-->
    <meta charset="utf-8">
    <link rel="dns-prefetch" href="http://fonts.googleapis.com" />
    <link rel="dns-prefetch" href="http://themes.googleusercontent.com" />
    <link rel="dns-prefetch" href="http://ajax.googleapis.com" />
    <link rel="dns-prefetch" href="http://cdnjs.cloudflare.com" />
    <link rel="dns-prefetch" href="http://agorbatchev.typepad.com" />
    <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/b/378 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Godrej Channel Partner :: Admin</title>
    <meta name="description" content="Godrej Channel Partner App">
    <meta name="author" content="Godrej Channel Partner App">
    <!-- Mobile viewport optimized: h5bp.com/viewport -->
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
    <!-- iPhone: Don't render numbers as call links -->
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" href="favicon.ico" />
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory: mathiasbynens.be/notes/touch-icons -->
    <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->
    <!-- The Styles -->
    <!-- ---------- -->
    <!-- Layout Styles -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/layout.css">
    <!-- Icon Styles -->
    <link rel="stylesheet" href="css/icons.css">
    <link rel="stylesheet" href="css/fonts/font-awesome.css">
    <!--[if IE 8]><link rel="stylesheet" href="css/fonts/font-awesome-ie7.css"><![endif]-->
    <!-- External Styles -->
    <link rel="stylesheet" href="css/external/jquery-ui-1.9.1.custom.css">
    <link rel="stylesheet" href="css/external/jquery.chosen.css">
    <link rel="stylesheet" href="css/external/jquery.cleditor.css">
    <link rel="stylesheet" href="css/external/jquery.colorpicker.css">
    <link rel="stylesheet" href="css/external/jquery.elfinder.css">
    <link rel="stylesheet" href="css/external/jquery.fancybox.css">
    <link rel="stylesheet" href="css/external/jquery.jgrowl.css">
    <link rel="stylesheet" href="css/external/jquery.plupload.queue.css">
    <link rel="stylesheet" href="css/external/syntaxhighlighter/shCore.css" />
    <link rel="stylesheet" href="css/external/syntaxhighlighter/shThemeDefault.css" />
    <!-- Elements -->
    <link rel="stylesheet" href="css/elements.css">
    <link rel="stylesheet" href="css/forms.css">
    <!-- OPTIONAL: Print Stylesheet for Invoice -->
    <link rel="stylesheet" href="css/print-invoice.css">
    <!-- Typographics -->
    <link rel="stylesheet" href="css/typographics.css">
    <!-- Responsive Design -->
    <link rel="stylesheet" href="css/media-queries.css">
    <!-- Bad IE Styles -->
    <link rel="stylesheet" href="css/ie-fixes.css">


    <%--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--%>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.css" />


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.js"></script>

    <script type="text/javascript">
        $(function () {
            $("#txtNotificationDate").datetimepicker();
        });
    </script>
</head>
<body>

    <section id="toolbar">
        <div class="container_12">
            <!-- Right side -->
            <div class="right">
                <ul>
                    <li class="red"><a href="AdminLogin.aspx">Logout</a></li>
                </ul>
            </div>
            <!-- End of .right -->
            <!-- Phone only items -->
            <div class="phone">
                <!-- Navigation -->
                <li><a class="navigation" href="#"><span class="icon icon-list"></span></a></li>
            </div>
            <!-- End of phone items -->
        </div>
        <!-- End of .container_12 -->
    </section>
    <!-- End of #toolbar -->
    <!-- The header containing the logo -->
    <header class="container_12">	
		<div class="container">
			<!-- Your logos -->
			<a href="viewcity.aspx"><img src="img/logo.png" alt="Godrej Channel Partner App"></a>
			<a class="phone-title" href="viewbanner.aspx"><img src="img/logo-mobile.png" alt="Godrej Channel Partner App" height="30" /></a>
		</div><!-- End of .container -->
	</header>
    <!-- End of header -->
    <!-- The container of the sidebar and content box -->
    <div role="main" id="main">
        <!-- The blue toolbar stripe -->

        <!-- End of .toolbar-->
        <!-- The sidebar -->
        <UC1:Menu ID="Menu" runat="server" />
        <!-- End of sidebar -->
        <!-- Here goes the content. -->


        <section id="content" class="container_12 clearfix" data-sort="true">
            <div class="grid_12">
                <div class="box">

        <form id="form1" runat="server">
        <div id="divgrid" runat="server">
            <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" />
            <asp:GridView ID="grView" runat="server" AutoGenerateColumns="false" DataKeyNames="NotificationID"
                AllowPaging="true" PageSize="10" OnPageIndexChanging="grView_PageIndexChanging"
                OnRowCommand="grView_RowCommand" Width="100%" OnRowDeleting="grView_RowDeleting">
                <Columns>
                    <asp:BoundField DataField="NotificationTitle" HeaderText="Title" />
                    <asp:BoundField DataField="SubTitle" HeaderText="Subtitle" />
                    <asp:BoundField DataField="NotificationBody" HeaderText="Body" />
                    <asp:BoundField DataField="NotificationDate" HeaderText="NotificationDate" />
                    <asp:BoundField DataField="ScheduleDate" HeaderText="ScheduleDate" />
                    <asp:BoundField DataField="SentOn" HeaderText="SentOn" />
                    <asp:TemplateField HeaderText="Action">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkBtnView" Text="View" runat="server" CommandName="View"
                                CommandArgument='<%# Eval("NotificationID") %>'></asp:LinkButton>|  
                            <asp:LinkButton ID="LinkButton1" Text="Delete" runat="server" CommandName="Delete"
                                CommandArgument='<%# Eval("NotificationID") %>'></asp:LinkButton>
                        <%--<asp:LinkButton ID="lnkBtnSend" Text="Send" runat="server" CommandName="Send"
                            CommandArgument='<%# Eval("NotificationID") %>' Visible='<%# Convert.ToBoolean(Eval("nHasSend")) %>'></asp:LinkButton>--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div id="divDetail" runat="server">
            <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>

            <h4>Schedule Notification <span id="spTitle" runat="server"></span></h4>
            <div class="form-horizontal">

                <asp:Panel ID="Panel1" runat="server">
                    <asp:DropDownList ID="stateDropDOwn"  OnSelectedIndexChanged="getStateDropDown_SelectedIndexChanged"  runat="server" AutoPostBack="true"></asp:DropDownList><br />
                </asp:Panel>
                <br />

                <asp:Panel ID="Panel2" runat="server">
                    <asp:DropDownList ID="cityDropDown" runat="server" AutoPostBack="true"></asp:DropDownList><br />
                </asp:Panel>
                <br />

                <asp:Panel ID="hideTemplate" runat="server">
                    Template<br />
                    <br />
                    <asp:DropDownList ID="templateDropDown" OnSelectedIndexChanged="templateDropDown_SelectedIndexChanged" runat="server" AutoPostBack="true"></asp:DropDownList><br />
                    <br />
                </asp:Panel>
                Notification Title<br />
                <br />
                <asp:TextBox ID="txtNotificationTitle" runat="server" Width="30%" /><br />
                <br />
                Notification Sub Title<br />
                <br />
                <asp:TextBox ID="txtSubTitle" runat="server" /><br />
                <br />
                Notification Body<br />
                <br />
                <asp:TextBox ID="txtNotificationBody" runat="server" TextMode="MultiLine" Width="50%" CssClass="text" /><br />
                <br />
                Notification Date<br />
                <br />
                <asp:TextBox ID="txtNotificationDate" runat="server" autocomplete="off" /><br />
                <br />
                <asp:Panel ID="hideScheduleDate" runat="server">
                Schedule Date<br />
                <br />
                <asp:TextBox ID="txtScheduleDate" runat="server" autocomplete="off" /><br />
                <br />
                </asp:Panel>

                <asp:Panel ID="hideCheckbox" runat="server">
                    Schedule  &nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="schedule7Days" runat="server" />
                    7 days before &nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="schedule4Days" runat="server" />
                    4 days before &nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="schedule1Day" runat="server" />
                    1 day before &nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="schedule2Hrs" runat="server" />
                    2 hours before &nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="scheduleLast" runat="server" />
                    Final date
                </asp:Panel>
                <br />
                Image
                <br />
                <br />
                <asp:Panel ID="hideFileUpload" runat="server">
                    <asp:FileUpload ID="fileUpload" runat="server" /><br />
                </asp:Panel>
                <br />
                <asp:Image ID="Image1" runat="server" ImageUrl="" /> 
                <br />
                <asp:Button ID="btnSubmit" runat="server" OnClientClick="javascript:return validate();"
                    CssClass="submit" Text="Submit" OnClick="btnSubmit_Click" />
            </div>
        </div>
    </form>


                </div>
        </section>
    </div>
    <footer class="container_12">
        <span class="grid_12">Copyright &copy; 2013 Netbiz Systems </span>
    </footer>

</body>
</html>
