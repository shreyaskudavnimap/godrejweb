﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using System.Net.Mail;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using GPLPartnerConnect.Model.Empanel;
using GPLPartnerConnect.Controller.Empanel;

public partial class ProfileChangeRequest : System.Web.UI.Page
{
    string brokerId;
    ProfileDataController objCon = new ProfileDataController();
    JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
    GodejCPDataContext dataContext = new GodejCPDataContext();
    string ConStr = ConfigurationManager.ConnectionStrings["godrejCPConnectionString"].ConnectionString;

    string updateURL = ConfigurationManager.AppSettings["SFDCURL"] + "/services/apexrest/RestBrokerUpdate";
    string sendReraURL = ConfigurationManager.AppSettings["SFDCURL"] + "/services/apexrest/api/updateBrokerReradetail";

    public class Empanelment
    {
        public string Password
        { get; set; }

        public string Company_Name
        { get; set; }

        public string entity_type
        { get; set; }

        public string Name
        { get; set; }

        public string Mobile
        { get; set; }

        public string Email
        { get; set; }

        public string communication_address
        { get; set; }

        public string registered_address
        { get; set; }

        public string Pan_No
        { get; set; }

        public string pancerti
        { get; set; }


        public string BrokerID
        { get; set; }


        public string BrokerContactID
        { get; set; }

        public string status
        { get; set; }


        public string Billing_City
        { get; set; }


        public string Billing_Zip
        { get; set; }


        public string Billing_Street
        { get; set; }


        public string Billing_State
        { get; set; }


        public string Billing_Country
        { get; set; }


        public string Registered_City
        { get; set; }


        public string Registered_Zip
        { get; set; }


        public string Registered_Street
        { get; set; }

        public string Registered_State
        { get; set; }


        public string Registered_Country
        { get; set; }


        public string Communication_City
        { get; set; }


        public string Communication_Zip
        { get; set; }


        public string Communication_Street
        { get; set; }


        public string Communication_State
        { get; set; }


        public string Communication_Country
        { get; set; }
        public string BrokerAccountID
        { get; set; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        divRegAddr.Visible = false;
        //divGst.Visible = false;
        divRera.Visible = false;

        /////////////

        //EmailService es = new EmailService();
        //es.properties.AddToEmail("shreyaskudav@nimapinfotech.com", "Shreyas");
        //es.properties.MailSubject = "Test Subject";
        //es.properties.MailBody = "Test Body";
        //es.SendMail();

        /////////////


        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        if (Convert.ToString(Session["type"]) == "admin")
        {
            HtmlControl User = (HtmlControl)Menu.FindControl("UserLi");
            User.Visible = false;
            HtmlControl Launch = (HtmlControl)Menu.FindControl("BannerLi");
            Launch.Visible = false;
            HtmlControl Master = (HtmlControl)Menu.FindControl("MasterLi");
            Master.Visible = false;
            HtmlControl Projects = (HtmlControl)Menu.FindControl("ProjectsLi");
            Projects.Visible = false;
            HtmlControl Enquiry = (HtmlControl)Menu.FindControl("EnquiryLi");
            Enquiry.Visible = false;
            HtmlControl Events = (HtmlControl)Menu.FindControl("EventsLi");
            Events.Visible = false;
            HtmlControl News = (HtmlControl)Menu.FindControl("NewsLi");
            News.Visible = false;
            HtmlControl Offers = (HtmlControl)Menu.FindControl("OffersLi");
            Offers.Visible = false;
            HtmlControl Property = (HtmlControl)Menu.FindControl("PropertyLi");
            Property.Visible = false;
            HtmlControl SubAdmin = (HtmlControl)Menu.FindControl("SubAdminLi");
            SubAdmin.Visible = false;
            HtmlControl DataAnchor = (HtmlControl)Menu.FindControl("DataDetailsLi");
            DataAnchor.Visible = false;
        }

        if (!IsPostBack)
        {
            BindGrid("");
            BindStatusDropDown();
            if (Convert.ToString(Request.QueryString["saved"]) == "1")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record updated successfully.');</script>");
            }
            if (Convert.ToString(Request.QueryString["save"]) == "1")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record added successfully.');</script>");
            }
        }

        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("EmplAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("EmplLi");

        txtSearch.Attributes.Add("onKeyPress", "doClick('" + btnSubmit.ClientID + "',event)");

    }



    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (txtSearch.Text != "" && txtSearch.Text != "search by name")
        {
            BindGrid(txtSearch.Text);
        }
    }

    protected void submitRegAddr_Click(object sender, EventArgs e)
    {
        string rowId = txtRegAddrRowId.Value;
        string reg_addr = txtNew_RegisteredAddress.Text;
        string reg_country = txtNew_RegisteredCountry.Text;
        string reg_state = txtNew_RegisteredState.Text;
        string reg_city = txtNew_RegisteredCity.Text;
        string reg_zipcode = txtNew_RegisteredZipCode.Text;
        string status = statusRegAddrDropDown.SelectedItem.Value;
        string reg_Comment = txtRegAddressComment.Text;
        string emplId = txtEmplId.Value;

        var data = objCon.GetUserDetailsForAlert(emplId);
        mEmpanelmentContact empl = data.Tables[0].AsEnumerable()
                                            .Select(x => new mEmpanelmentContact()
                                            {
                                                Email = x.Field<string>("Email"),
                                                Name = x.Field<string>("Name"),
                                                Registered_City = x.Field<string>("Registered_City"),
                                                Registered_Zip = x.Field<string>("Registered_Zip"),
                                                Registered_Street = x.Field<string>("Registered_Street"),
                                                Registered_State = x.Field<string>("Registered_State"),
                                                Registered_Country = x.Field<string>("Registered_Country"),
                                            }).ToList()[0];

        DataTable dataTable = new DataTable();
        List<mProfileChangeRequest> list = new List<mProfileChangeRequest>();
        SqlConnection con = new SqlConnection(ConStr);
        try
        {
            SqlCommand cmd = new SqlCommand("usp_UpdateProfileSubmit", con);
            cmd.Parameters.AddWithValue("@rowId", rowId);
            cmd.Parameters.AddWithValue("@Status", status);
            cmd.Parameters.AddWithValue("@EmplID", emplId);
            cmd.Parameters.AddWithValue("@flag", "REG_ADDR");
            cmd.Parameters.AddWithValue("@Reg_addr", reg_addr);
            cmd.Parameters.AddWithValue("@Reg_country", reg_country);
            cmd.Parameters.AddWithValue("@Reg_state", reg_state);
            cmd.Parameters.AddWithValue("@Reg_city", reg_city);
            cmd.Parameters.AddWithValue("@Reg_zipcode", reg_zipcode);
            cmd.Parameters.AddWithValue("@Comment", reg_Comment);

            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dataTable);
            con.Close();
            da.Dispose();
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Error while submitting data. Please contact support team. [Err:" + ex.Message + "]'); window.location='ProfileChangeRequest.aspx';</script>");
        }

        if (status == "A")
        {
            // SDFC UPDATE
            var Empldetails = dataContext.tbl_Empanelments.Single(doc => doc.EmplID == Convert.ToInt32(emplId));
            var newBody = new
            {
                ei = new
                {
                    brokerid = Empldetails.BrokerAccountID,
                    Name = Empldetails.Company_Name,
                    Pannumber = Empldetails.Pan_No,
                    GSTIN = "",// objGST.Gst_no,
                    GSTINState = "",//objGST.State,
                    BrokerType = "",
                    BrokerCategory = "RCP",
                    CompanyCategory = "",
                    MobileNo = Empldetails.Mobile,
                    Phone = Empldetails.Mobile,
                    BrokerEntityType = Empldetails.EntityType,
                    RegionName = "",
                    EmailId = Empldetails.Email,
                    Empanelment = "EMPL" + Empldetails.EmplID.ToString(),
                    BillingCity = Empldetails.Billing_City,
                    BillingZipPostal = Empldetails.Billing_Zip,
                    BillingStreet = Empldetails.Billing_Street,
                    BillingStateProvince = Empldetails.Billing_State,
                    BillingCountry = Empldetails.Billing_Country,
                    RegisteredCity = Empldetails.Registered_City,
                    RegisteredZipPostal = Empldetails.Registered_Zip,
                    RegisteredStreet = Empldetails.Registered_Street,
                    RegisteredStateProvince = Empldetails.Registered_State,
                    RegisteredCountry = Empldetails.Registered_Country,
                    CommunicationCity = Empldetails.Communication_City,
                    CommunicationZipPostal = Empldetails.Communication_Zip,
                    CommunicationStreet = Empldetails.Communication_Street,
                    CommunicationStateProvince = Empldetails.Communication_State,
                    CommunicationCountry = Empldetails.Communication_Country
                }
            };
            var newDataString = JsonConvert.SerializeObject(newBody);
            WLog.AdminWriteLog("Request Body : " + newDataString, "REGADDR_UPDATE");

            var newResponse = sendRequest(newDataString, updateURL);
            WLog.AdminWriteLog("Response : " + newResponse, "REGADDR_UPDATE");

            if (newResponse.Contains("Successfully Update"))
            {
                string email_template = "<div>Dear " + empl.Name + ",</div> <div>&nbsp;</div> <div>As per your request, your communication address associated with your account has been changed from " + empl.Registered_City + ", " + empl.Registered_State + ", " + empl.Registered_Country + ", " + empl.Registered_Street + ", " + empl.Registered_Zip + " to " + reg_city + ", " + reg_state + ", " + reg_country + ", " + reg_zipcode + ", " + reg_addr + " and we will be using the new details for further communication.</div> <div>&nbsp;</div> <div>This is an auto-generated mail, please do not reply to this message.</div> <div><br />Thanks &amp; Regards<br />Godrej Partner Connect Team</div>";
                SendMail(empl.Email, "Registered Address Change Request", email_template);

                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Data updated successfully.'); window.location='ProfileChangeRequest.aspx';</script>");
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Error while updating SFDC. Please contact support team. [Err:" + newResponse + "]'); window.location='ProfileChangeRequest.aspx';</script>");
            }
        }
        if (status == "R")
        {
            string email_template = "<div>Dear " + empl.Name + ",</div> <div>&nbsp;</div> <div> Your request to change your registered address has been rejected. </div> <div>&nbsp;</div> " + reg_Comment + "<div>&nbsp;</div> <div>This is an auto-generated mail, please do not reply to this message.</div> <div><br />Thanks &amp; Regards<br />Godrej Partner Connect Team</div>";
            SendMail(empl.Email, "Registered Address Change Request", email_template);
        }
        return;
    }

    private void SendMail(string toEmail, string title, string body)
    {
        EmailService es = new EmailService();
        es.properties.AddToEmail(toEmail, "Godrej Connect App");
        es.properties.MailSubject = title;
        es.properties.MailBody = body;
        es.SendMail();
    }

    private string sendRequest(string body, string url)
    {
        ServicePointManager.Expect100Continue = true;
        ServicePointManager.DefaultConnectionLimit = 9999;
        ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

        ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

        using (var client = new WebClient())
        {
            client.Headers[HttpRequestHeader.ContentType] = "application/json";
            client.Headers[HttpRequestHeader.Authorization] = "Bearer " + Session["access_token"];
            //WLog.AdminWriteLog("access_token Bearer " + Session["access_token"], "sendRequest");
            //WLog.AdminWriteLog("Call client.UploadString Url " + url + " body " + body, "sendRequest");

            var response = client.UploadString(url, "POST", body);
            return response;
        }
    }

    //protected void submitGst_Click(object sender, EventArgs e)
    //{
    //    string rowId = txtGstRowId.Value;
    //    string gstNo = txtGstNo.Text;
    //    string gstState = txtGstState.Text;
    //    string gstComment = txtGstComment.Text;
    //    string gstCerti = txtGstCerti.HRef.Replace("/data/empanelment/", "").ToString();
    //    string status = statusGstDropDown.SelectedItem.Value;
    //    string emplId = txtEmplId.Value;

    //    var data = objCon.GetUserDetailsForAlert(emplId);
    //    mEmpanelmentContact empl = data.Tables[0].AsEnumerable()
    //                                        .Select(x => new mEmpanelmentContact()
    //                                        {
    //                                            Email = x.Field<string>("Email"),
    //                                            Name = x.Field<string>("Name")
    //                                        }).ToList()[0];

    //    if (status == "R")
    //    {
    //        DataTable dataTable = new DataTable();
    //        List<mProfileChangeRequest> list = new List<mProfileChangeRequest>();
    //        SqlConnection con = new SqlConnection(ConStr);
    //        try
    //        {
    //            SqlCommand cmd = new SqlCommand("usp_UpdateProfileSubmit", con);
    //            cmd.Parameters.AddWithValue("@rowId", rowId);
    //            cmd.Parameters.AddWithValue("@Status", status);
    //            cmd.Parameters.AddWithValue("@EmplID", emplId);
    //            cmd.Parameters.AddWithValue("@GstNo", gstNo);
    //            cmd.Parameters.AddWithValue("@GstState", gstState);
    //            cmd.Parameters.AddWithValue("@GstCerti", gstCerti);
    //            cmd.Parameters.AddWithValue("@Comment", gstComment);
    //            cmd.Parameters.AddWithValue("@flag", "GST");

    //            cmd.CommandType = CommandType.StoredProcedure;
    //            con.Open();

    //            SqlDataAdapter da = new SqlDataAdapter(cmd);
    //            da.Fill(dataTable);
    //            con.Close();
    //            da.Dispose();
    //        }
    //        catch (Exception ex)
    //        {
    //            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Error while submitting data. Please contact support team. [Err:" + ex.Message + "]'); window.location='ProfileChangeRequest.aspx';</script>");
    //        }

    //        string email_template = "<div>Dear " + empl.Name + ",</div> <div>&nbsp;</div> <div> Your request to change your GST details has been rejected. </div> <div>&nbsp;</div> Reason : " + gstComment + "<div>&nbsp;</div><div>This is an auto-generated mail, please do not reply to this message.</div> <div><br />Thanks &amp; Regards<br />Godrej Partner Connect Team</div>";
    //        SendMail(empl.Email, "GST Details Change Request", email_template);

    //        ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Data updated successfully.'); window.location='ProfileChangeRequest.aspx';</script>");
    //        return;
    //    }

    //    else if (status == "A")
    //    {
    //        DataSet pands = new DataSet();
    //        List<mProfileChangeRequest> list = new List<mProfileChangeRequest>();
    //        SqlConnection con1 = new SqlConnection(ConStr);
    //        try
    //        {
    //            SqlCommand cmd = new SqlCommand("usp_UpdateProfileSubmit", con1);
    //            cmd.Parameters.AddWithValue("@rowId", rowId);
    //            cmd.Parameters.AddWithValue("@EmplID", emplId);
    //            cmd.Parameters.AddWithValue("@flag", "GET_REQ_GST");
    //            cmd.CommandType = CommandType.StoredProcedure;
    //            con1.Open();

    //            SqlDataAdapter da = new SqlDataAdapter(cmd);
    //            da.Fill(pands);
    //            con1.Close();
    //            da.Dispose();
    //        }
    //        catch (Exception ex)
    //        {
    //            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Error while submitting data. Please contact support team. [Err:" + ex.Message + "]'); window.location='ProfileChangeRequest.aspx';</script>");
    //        }

    //        string Panno = pands.Tables[0].Rows[0]["Pan_No"].ToString();

    //        List<Empl_ReraDoc> objReraEmpl = pands.Tables[1].AsEnumerable().Select(row =>
    //                                new Empl_ReraDoc
    //                                {
    //                                    State = row.Field<string>("State"),
    //                                    Rera_no = row.Field<string>("Rera_no"),
    //                                    EmplID = row.Field<int>("EmplID"),
    //                                    ID = row.Field<int>("ID"),
    //                                    Status = "Approve",
    //                                    SFDCRegisted = null
    //                                }).ToList();

    //        Empl_ReraDoc firstReraEmpl = new Empl_ReraDoc();
    //        if (objReraEmpl.Count() > 0)
    //        {
    //            firstReraEmpl = objReraEmpl[0];
    //        }
    //        else
    //        {
    //            firstReraEmpl.Rera_no = "";
    //            firstReraEmpl.State = "";
    //        }

    //        //if (objReraEmpl.Count() > 0)
    //        //{
    //        //    List<Empl_ReraDoc> firstReraOnly = new List<Empl_ReraDoc>();
    //        //    firstReraOnly.Add(objReraEmpl[0]);

    //        //var listReraEmpl = System.Linq.Enumerable.ToList(firstReraOnly);
    //        //List<EmplReraGSTDocs> lst = new List<EmplReraGSTDocs>();

    //        //foreach (var ReraEmpl in listReraEmpl)
    //        //{
    //        //    if (ReraEmpl.Status == "Approve")
    //        //    {

    //        //        var objStateGstEmpl = from obj in dataContext.Empl_GstDocs
    //        //                              where obj.EmplID == Convert.ToInt32(emplId) && obj.State == ReraEmpl.State && obj.Status == "Approve"
    //        //                              select new
    //        //                              {
    //        //                                  obj.Gst_no,
    //        //                                  obj.Gst_Certi,
    //        //                              };

    //        //        var arrStateGSTEmpl = System.Linq.Enumerable.ToArray(objStateGstEmpl);
    //        //        EmplReraGSTDocs objemp = new EmplReraGSTDocs();
    //        //        objemp.State = ReraEmpl.State;
    //        //        objemp.Rera_no = ReraEmpl.Rera_no;
    //        //        if (arrStateGSTEmpl.Length > 0)
    //        //            objemp.Gst_no = arrStateGSTEmpl[0].Gst_no;
    //        //        else
    //        //            objemp.Gst_no = "";
    //        //        objemp.Rera_no = ReraEmpl.Rera_no;
    //        //        objemp.State = ReraEmpl.State;
    //        //        lst.Add(objemp);
    //        //    }

    //        //}

    //        //var arrReraEmpl = System.Linq.Enumerable.ToArray(objReraEmpl);

    //        //foreach (EmplReraGSTDocs oEmp in lst)
    //        //{
    //        var Rerabody = new
    //        {
    //            ei = new
    //            {
    //                Pan_Number = Panno,
    //                Rera_Number = firstReraEmpl.Rera_no,
    //                state_code = firstReraEmpl.State,
    //                GSTIN = gstNo,
    //                //SAPBrokerCode = ""
    //            }
    //        };
    //        string Reraresponse = string.Empty;
    //        var ReradataString = JsonConvert.SerializeObject(Rerabody);
    //        WLog.AdminWriteLog("Request Body : " + ReradataString, "RERA_UPDATE");
    //        Reraresponse = sendRequest(ReradataString, sendReraURL);
    //        WLog.AdminWriteLog("Response : " + Reraresponse, "RERA_UPDATE");
    //        //}
    //        if (Reraresponse.Contains("Broker RERA Details successfully created"))  // Update msg
    //        {
    //            DataTable dataTable = new DataTable();
    //            SqlConnection con = new SqlConnection(ConStr);
    //            try
    //            {
    //                SqlCommand cmd = new SqlCommand("usp_UpdateProfileSubmit", con);
    //                cmd.Parameters.AddWithValue("@rowId", rowId);
    //                cmd.Parameters.AddWithValue("@Status", status);
    //                cmd.Parameters.AddWithValue("@EmplID", emplId);
    //                cmd.Parameters.AddWithValue("@flag", "GST");
    //                cmd.Parameters.AddWithValue("@GstNo", gstNo);
    //                cmd.Parameters.AddWithValue("@GstState", gstState);
    //                cmd.Parameters.AddWithValue("@GstCerti", gstCerti);

    //                cmd.CommandType = CommandType.StoredProcedure;
    //                con.Open();

    //                SqlDataAdapter da = new SqlDataAdapter(cmd);
    //                da.Fill(dataTable);
    //                con.Close();
    //                da.Dispose();
    //            }
    //            catch (Exception ex)
    //            {
    //                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Error while submitting data. Please contact support team. [Err:" + ex.Message + "]'); window.location='ProfileChangeRequest.aspx';</script>");
    //            }

    //            string email_template = "<div>Dear " + empl.Name + ",</div> <div>&nbsp;</div> <div> Your request to add new GST Details has been approved. </div> <div>&nbsp;</div> <div>This is an auto-generated mail, please do not reply to this message.</div> <div><br />Thanks &amp; Regards<br />Godrej Partner Connect Team</div>";
    //            SendMail(empl.Email, "GST Details Change Request", email_template);

    //            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Data updated successfully.'); window.location='ProfileChangeRequest.aspx';</script>");
    //            return;
    //        }

    //        else
    //        {
    //            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Error while updating SFDC. Please contact support team. [Err:" + Reraresponse + "]');</script>");
    //            return;
    //        }

    //    }
    //}


    protected void submitRera_Click(object sender, EventArgs e)
    {
        string reraRowId = txtReraRowId.Value;
        string reraNo = txtReraNo.Text;
        string reraState = txtReraState.Text;
        string Comment = txtReraComment.Text;
        string gstRowId = txtGstRowId.Value;
        string gstNumber = txtGstNumberNew.Text;
        string gstState = txtGstStateNew.Text;
        string gstCerti = txtGstCertiNew.HRef.Replace("/data/empanelment/", "").ToString();

        string reraCerti = txtReraCerti.HRef.Replace("/data/empanelment/", "").ToString();
        string status = statusReraDropDown.SelectedItem.Value;
        string emplId = txtEmplId.Value;

        var data = objCon.GetUserDetailsForAlert(emplId);
        mEmpanelmentContact empl = data.Tables[0].AsEnumerable()
                                            .Select(x => new mEmpanelmentContact()
                                            {
                                                Email = x.Field<string>("Email"),
                                                Name = x.Field<string>("Name")
                                            }).ToList()[0];

        if (status == "R")
        {
            // RERA 
            DataTable dataTable = new DataTable();
            List<mProfileChangeRequest> list = new List<mProfileChangeRequest>();
            SqlConnection con = new SqlConnection(ConStr);
            try
            {
                SqlCommand cmd = new SqlCommand("usp_UpdateProfileSubmit", con);
                cmd.Parameters.AddWithValue("@rowId", reraRowId);
                cmd.Parameters.AddWithValue("@Status", status);
                cmd.Parameters.AddWithValue("@EmplID", emplId);
                cmd.Parameters.AddWithValue("@ReraNo", reraNo);
                cmd.Parameters.AddWithValue("@ReraState", reraState);
                cmd.Parameters.AddWithValue("@Comment", Comment);
                cmd.Parameters.AddWithValue("@ReraCerti", reraCerti);
                cmd.Parameters.AddWithValue("@flag", "RERA");

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dataTable);
                da.Dispose();

                dataTable = new DataTable();
                SqlCommand cmd1 = new SqlCommand("usp_UpdateProfileSubmit", con);
                cmd1.Parameters.AddWithValue("@rowId", gstRowId);
                cmd1.Parameters.AddWithValue("@Status", status);
                cmd1.Parameters.AddWithValue("@EmplID", emplId);
                cmd1.Parameters.AddWithValue("@GstNo", gstNumber);
                cmd1.Parameters.AddWithValue("@GstState", gstState);
                cmd1.Parameters.AddWithValue("@GstCerti", gstCerti);
                cmd1.Parameters.AddWithValue("@flag", "GST");

                cmd1.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                da1.Fill(dataTable);
                da1.Dispose();

                con.Close();
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Error while submitting data. Please contact support team. [Err:" + ex.Message + "]'); window.location='ProfileChangeRequest.aspx';</script>");
            }

            string email_template = "<div>Dear " + empl.Name + ",</div> <div>&nbsp;</div> <div> Your request to change your RERA details has been rejected. </div> <div>&nbsp;</div> Reason : " + Comment + "<div>&nbsp;</div> <div>This is an auto-generated mail, please do not reply to this message.</div> <div><br />Thanks &amp; Regards<br />Godrej Partner Connect Team</div>";
            SendMail(empl.Email, "RERA & GST Details Change Request", email_template);

            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Data updated successfully.'); window.location='ProfileChangeRequest.aspx';</script>");
            return;
        }

        else if (status == "A")
        {
            DataSet pands = new DataSet();
            List<mProfileChangeRequest> list = new List<mProfileChangeRequest>();
            SqlConnection con1 = new SqlConnection(ConStr);
            try
            {
                SqlCommand cmd = new SqlCommand("usp_UpdateProfileSubmit", con1);
                cmd.Parameters.AddWithValue("@rowId", reraRowId);
                cmd.Parameters.AddWithValue("@EmplID", emplId);
                cmd.Parameters.AddWithValue("@flag", "GET_REQ_RERA");
                cmd.CommandType = CommandType.StoredProcedure;
                con1.Open();

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(pands);
                con1.Close();
                da.Dispose();
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Error while submitting data. Please contact support team. [Err:" + ex.Message + "]'); window.location='ProfileChangeRequest.aspx';</script>");
            }

            string Panno = pands.Tables[0].Rows[0]["Pan_No"].ToString();

            List<mReraGst> objReraEmpl = pands.Tables[1].AsEnumerable().Select(row =>
                                    new mReraGst
                                    {
                                        ReraState = row.Field<string>("ReraState"),
                                        ReraNumber = row.Field<string>("ReraNumber"),
                                        ReraCerti = row.Field<string>("ReraCerti"),
                                        GstNumber = row.Field<string>("GstNumber"),
                                        EmplID = Convert.ToInt32(row.Field<string>("EmplID")),
                                        ReraRowId = row.Field<int>("ReraRowId")
                                    }).ToList();

            mReraGst firstReraEmpl = new mReraGst();
            if (objReraEmpl.Count() > 0)
            {
                firstReraEmpl = objReraEmpl[0];
            }

            //if (objReraEmpl.Count() > 0)
            //{
            //    var listReraEmpl = System.Linq.Enumerable.ToList(objReraEmpl);
            //    List<EmplReraGSTDocs> lst = new List<EmplReraGSTDocs>();

            //    foreach (var ReraEmpl in listReraEmpl)
            //    {
            //        if (ReraEmpl.Status == "Approve")
            //        {

            //            var objStateGstEmpl = from obj in dataContext.Empl_GstDocs
            //                                  where obj.EmplID == Convert.ToInt32(emplId) && obj.State == ReraEmpl.State && obj.Status == "Approve"
            //                                  select new
            //                                  {
            //                                      obj.Gst_no,
            //                                      obj.Gst_Certi,
            //                                  };

            //            var arrStateGSTEmpl = System.Linq.Enumerable.ToArray(objStateGstEmpl);
            //            EmplReraGSTDocs objemp = new EmplReraGSTDocs();
            //            objemp.State = ReraEmpl.State;
            //            objemp.Rera_no = ReraEmpl.Rera_no;
            //            if (arrStateGSTEmpl.Length > 0)
            //                objemp.Gst_no = arrStateGSTEmpl[0].Gst_no;
            //            else
            //                objemp.Gst_no = "";
            //            lst.Add(objemp);
            //        }

            //    }

            //var arrReraEmpl = System.Linq.Enumerable.ToArray(objReraEmpl);
            //string Reraresponse = string.Empty;
            //foreach (EmplReraGSTDocs oEmp in lst)
            //{
            //    var Rerabody = new
            //    {
            //        ei = new
            //        {
            //            Pan_Number = Panno,
            //            Rera_Number = oEmp.Rera_no,
            //            state_code = oEmp.State,
            //            GSTIN = oEmp.Gst_no.ToString(),
            //            //SAPBrokerCode = ""
            //        }
            //    };

            //    var ReradataString = JsonConvert.SerializeObject(Rerabody);
            //    WLog.AdminWriteLog("Request Body : " + ReradataString, "RERA_UPDATE");
            //    Reraresponse = sendRequest(ReradataString, sendReraURL);
            //    WLog.AdminWriteLog("Response : " + Reraresponse, "RERA_UPDATE");
            //}

            var arrReraEmpl = System.Linq.Enumerable.ToArray(objReraEmpl);
            string Reraresponse = string.Empty;

            var Rerabody = new
            {
                ei = new
                {
                    Pan_Number = Panno,
                    Rera_Number = firstReraEmpl.ReraNumber,
                    state_code = firstReraEmpl.ReraState,
                    GSTIN = firstReraEmpl.GstNumber,
                }
            };

            var ReradataString = JsonConvert.SerializeObject(Rerabody);
            WLog.AdminWriteLog("Request Body : " + ReradataString, "RERA_UPDATE");
            Reraresponse = sendRequest(ReradataString, sendReraURL);
            WLog.AdminWriteLog("Response : " + Reraresponse, "RERA_UPDATE");


            if (Reraresponse.Contains("Broker RERA Details successfully created"))
            {
                try
                {
                    DataTable dataTable = new DataTable();
                    SqlConnection con = new SqlConnection(ConStr);
                    SqlCommand cmd = new SqlCommand("usp_UpdateProfileSubmit", con);
                    cmd.Parameters.AddWithValue("@rowId", reraRowId);
                    cmd.Parameters.AddWithValue("@Status", status);
                    cmd.Parameters.AddWithValue("@EmplID", emplId);
                    cmd.Parameters.AddWithValue("@flag", "RERA");
                    cmd.Parameters.AddWithValue("@ReraNo", reraNo);
                    cmd.Parameters.AddWithValue("@ReraState", reraState);
                    cmd.Parameters.AddWithValue("@ReraCerti", reraCerti);

                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dataTable);
                    con.Close();
                    da.Dispose();
                }
                catch (Exception ex)
                {
                    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Error while submitting data. Please contact support team. [Err:" + ex.Message + "]'); window.location='ProfileChangeRequest.aspx';</script>");
                }

                try
                {
                    DataTable dataTable = new DataTable();
                    SqlConnection con2 = new SqlConnection(ConStr);
                    SqlCommand cmd = new SqlCommand("usp_UpdateProfileSubmit", con2);
                    cmd.Parameters.AddWithValue("@rowId", gstRowId);
                    cmd.Parameters.AddWithValue("@Status", status);
                    cmd.Parameters.AddWithValue("@EmplID", emplId);
                    cmd.Parameters.AddWithValue("@flag", "GST");
                    cmd.Parameters.AddWithValue("@GstNo", gstNumber);
                    cmd.Parameters.AddWithValue("@GstState", gstState);
                    cmd.Parameters.AddWithValue("@GstCerti", gstCerti);

                    cmd.CommandType = CommandType.StoredProcedure;
                    con2.Open();

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dataTable);
                    con2.Close();
                    da.Dispose();
                }
                catch (Exception ex)
                {
                    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Error while submitting data. Please contact support team. [Err:" + ex.Message + "]'); window.location='ProfileChangeRequest.aspx';</script>");
                }

                string email_template = "<div>Dear " + empl.Name + ",</div> <div>&nbsp;</div> <div> Your request to add new RERA Details has been approved. </div> <div>&nbsp;</div> <div>This is an auto-generated mail, please do not reply to this message.</div> <div><br />Thanks &amp; Regards<br />Godrej Partner Connect Team</div>";
                SendMail(empl.Email, "RERA Details Change Request", email_template);

                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('RERA & GST updated successfully.');</script>");
                return;
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Error while updating SFDC. Please contact support team. [Err:" + Reraresponse + "]');</script>");
                return;
            }
            //}
        }


        ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Data updated successfully.'); window.location='ProfileChangeRequest.aspx';</script>");
        return;
    }

    public string getToken()
    {
        using (WebClient client = new WebClient())
        {
            var reqparm = new System.Collections.Specialized.NameValueCollection();
            reqparm.Add("param1", "");
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

            Uri myUri = new Uri(ConfigurationManager.AppSettings["SFDCURL"] + ConfigurationManager.AppSettings["GRANTSERVICE"] + "&client_id="
                + ConfigurationManager.AppSettings["CLIENTID"] + "&client_secret=" + ConfigurationManager.AppSettings["CLIENTSECRET"] + "&username="
                + ConfigurationManager.AppSettings["USERNAME"] + "&password=" + ConfigurationManager.AppSettings["Password"], UriKind.Absolute);

            //Uri myUri = new Uri("https://godrej.my.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9Y6d_Btp4xp7lt5FL02Cc.bHBCI_vpcrJYvpeBev1Ob5nXpobkxkmhygUekoOvbQMscya0i3r7EacuDWz&client_secret=332639414523432900&username=external.app@godrejproperties.com&password=Nov@12345Svgw3cZob0A7HWlJs8I07Jcdb", UriKind.Absolute);

            byte[] responsebytes = client.UploadValues(myUri, reqparm);

            string responseBody = System.Text.Encoding.UTF8.GetString(responsebytes);
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            var result = jsSerializer.DeserializeObject(responseBody);
            Dictionary<string, dynamic> obj2 = new Dictionary<string, dynamic>();
            obj2 = (Dictionary<string, dynamic>)(result);
            return obj2["access_token"].ToString();
        }
    }

    private void BindGrid(string searchtext)
    {

        divGrid.Visible = true;
        var profileData = GetProfileChangeReq();

        if (searchtext != "" && searchtext != "search by name")
        {
            IList<mProfileChangeRequest> profileList = profileData.AsEnumerable().Select(row =>
                                    new mProfileChangeRequest
                                    {
                                        ID = row.Field<int>("ID"),
                                        EmplId = row.Field<string>("EmplID"),
                                        Email = row.Field<string>("Email"),
                                        Pan_No = row.Field<string>("Pan_No"),
                                        Status = row.Field<string>("Status"),
                                        RequestType = row.Field<string>("RequestType"),
                                        RequestTypeId = row.Field<int>("RequestTypeId"),
                                        RejectionComment = row.Field<string>("RejectionComment")
                                    }).ToList();
            var filterProfileList = profileList.AsEnumerable().Where(item => item.Email.Contains(searchtext) || item.Pan_No.Contains(searchtext)
                                                                    || item.Status.Contains(convertStatus(searchtext))
                                                                    ).ToList();
            profileData = ToDataTable<mProfileChangeRequest>(filterProfileList);
        }

        if (profileData.Rows.Count > 0)
        {
            lblMsg.Visible = false;
            lblMsg.Text = "";
            reqDetails.DataSource = profileData;
            reqDetails.DataBind();
        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "<br/>No records found.";
            reqDetails.DataSource = profileData;
            reqDetails.DataBind();
        }
    }


    public static DataTable ToDataTable<T>(List<T> items)
    {
        DataTable dataTable = new DataTable(typeof(T).Name);

        //Get all the properties
        System.Reflection.PropertyInfo[] Props = typeof(T).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
        foreach (System.Reflection.PropertyInfo prop in Props)
        {
            //Defining type of data column gives proper data table 
            var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
            //Setting column names as Property names
            dataTable.Columns.Add(prop.Name, type);
        }
        foreach (T item in items)
        {
            var values = new object[Props.Length];
            for (int i = 0; i < Props.Length; i++)
            {
                //inserting property values to datatable rows
                values[i] = Props[i].GetValue(item, null);
            }
            dataTable.Rows.Add(values);
        }
        //put a breakpoint here and check datatable
        return dataTable;
    }

    private string convertStatus(string searchtext)
    {
        if (searchtext == "Approved")
        {
            return "A";
        }
        else if (searchtext == "Rejected")
        {
            return "R";
        }
        else if (searchtext == "Pending")
        {
            return "P";
        }
        else
        {
            return searchtext;
        }
    }

    protected void reqDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        reqDetails.PageIndex = e.NewPageIndex;
        BindGrid(txtSearch.Text);
    }


    protected void reqDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {
            reqDetails.Visible = false;

            var data = e.CommandArgument.ToString().Split(',');
            int rowId = Convert.ToInt32(data[0]);
            int reqTypeId = Convert.ToInt32(data[1]);
            string emplId = data[2];

            txtEmplId.Value = emplId;
            if (reqTypeId == 1)
            {
                IList<mRegisteredAddress> regAddrObj = GetProfileChangeReqDetails(rowId, reqTypeId).AsEnumerable().Select(row =>
                                   new mRegisteredAddress
                                   {
                                       ID = row.Field<int>("ID"),
                                       New_RegisteredAddress = row.Field<string>("New_RegisteredAddress"),
                                       New_RegisteredCountry = row.Field<string>("New_RegisteredCountry"),
                                       New_RegisteredState = row.Field<string>("New_RegisteredState"),
                                       New_RegisteredCity = row.Field<string>("New_RegisteredCity"),
                                       New_RegisteredZipCode = row.Field<string>("New_RegisteredZipCode"),
                                       AddressProof = row.Field<string>("AddressProof"),
                                       IsApproved = row.Field<string>("IsApproved"),
                                       Comment = row.Field<string>("Comment")
                                   }).ToList();

                var details = regAddrObj.AsEnumerable().Where(item => item.ID == rowId).ToList();

                divRegAddr.Visible = true;
                divRera.Visible = false;
                //divGst.Visible = false;
                divGrid.Visible = false;
                txtNew_RegisteredAddress.Text = details[0].New_RegisteredAddress;
                txtNew_RegisteredCountry.Text = details[0].New_RegisteredCountry;
                txtNew_RegisteredState.Text = details[0].New_RegisteredState;
                txtNew_RegisteredCity.Text = details[0].New_RegisteredCity;
                txtNew_RegisteredZipCode.Text = details[0].New_RegisteredZipCode;
                txtRegAddressComment.Text = details[0].Comment;
                if (details[0].IsApproved != "P")
                    txtRegAddressComment.Enabled = false;
                else
                    txtRegAddressComment.Enabled = true;
                if (details[0].AddressProof != "undefined")
                {
                    txtAddressProof.HRef = "/data/empanelment/" + details[0].AddressProof;
                }
                txtRegAddrRowId.Value = details[0].ID.ToString();
                statusRegAddrDropDown.SelectedValue = details[0].IsApproved;
                if (statusRegAddrDropDown.SelectedValue == "P")
                {
                    statusRegAddrDropDown.Enabled = true;
                }
                else
                {
                    statusRegAddrDropDown.Enabled = false;
                }
            }

            else if (reqTypeId == 2)
            {
                IList<mReraGst> reraObj = GetProfileChangeReqDetails(rowId, reqTypeId).AsEnumerable().Select(row =>
                                   new mReraGst
                                   {
                                       EmplID = Convert.ToInt32(row.Field<string>("EmplID")),
                                       ReraRowId = row.Field<int?>("ReraRowId"),
                                       ReraNumber = row.Field<string>("ReraNumber"),
                                       ReraState = row.Field<string>("ReraState"),
                                       ReraCerti = row.Field<string>("ReraCerti"),
                                       GstRowId = row.Field<int?>("GstRowId"),
                                       GstNumber = row.Field<string>("GstNumber"),
                                       GstState = row.Field<string>("GstState"),
                                       GstCerti = row.Field<string>("GstCerti"),
                                       IsApproved = row.Field<string>("IsApproved"),
                                       Comment = row.Field<string>("Comment")
                                   }).ToList();

                var details = reraObj.AsEnumerable().Where(item => item.ReraRowId == rowId).ToList();

                //divGst.Visible = false;
                divRegAddr.Visible = false;
                divRera.Visible = true;
                divGrid.Visible = false;
                txtReraNo.Text = details[0].ReraNumber;
                txtReraState.Text = details[0].ReraState;
                txtReraComment.Text = details[0].Comment;
                txtGstNumberNew.Text = details[0].GstNumber;
                txtGstStateNew.Text = details[0].GstState;
                if (details[0].IsApproved != "P")
                    txtReraComment.Enabled = false;
                else
                    txtReraComment.Enabled = true;
                if (details[0].ReraCerti != "undefined")
                {
                    txtReraCerti.HRef = "/data/empanelment/" + details[0].ReraCerti;
                }
                if (details[0].GstCerti != "undefined")
                {
                    txtGstCertiNew.HRef = "/data/empanelment/" + details[0].GstCerti;
                }
                
                statusReraDropDown.SelectedValue = details[0].IsApproved;
                txtReraRowId.Value = details[0].ReraRowId.ToString();
                txtGstRowId.Value = details[0].GstRowId.ToString();
                if (statusReraDropDown.SelectedValue == "P")
                {
                    statusReraDropDown.Enabled = true;
                }
                else
                {
                    statusReraDropDown.Enabled = false;
                }

            }

            //else if (reqTypeId == 3)
            //{
            //    IList<mGst> gstObj = GetProfileChangeReqDetails(rowId, reqTypeId).AsEnumerable().Select(row =>
            //                       new mGst
            //                       {
            //                           ID = row.Field<int>("ID"),
            //                           GstNumber = row.Field<string>("GstNumber"),
            //                           State = row.Field<string>("State"),
            //                           GstCerti = row.Field<string>("GstCerti"),
            //                           IsApproved = row.Field<string>("IsApproved"),
            //                           Comment = row.Field<string>("Comment")
            //                       }).ToList();

            //    var details = gstObj.AsEnumerable().Where(item => item.ID == rowId).ToList();

            //    divGst.Visible = true;
            //    divRegAddr.Visible = false;
            //    divRera.Visible = false;
            //    txtGstNo.Text = details[0].GstNumber;
            //    txtGstState.Text = details[0].State;
            //    txtGstComment.Text = details[0].Comment;
            //    if (details[0].IsApproved != "P")
            //        txtGstComment.Enabled = false;
            //    else
            //        txtGstComment.Enabled = true;
            //    if (details[0].GstCerti != "undefined")
            //    {
            //        txtGstCerti.HRef = "/data/empanelment/" + details[0].GstCerti;
            //    }
            //    statusGstDropDown.SelectedValue = details[0].IsApproved;
            //    txtGstRowId.Value = details[0].ID.ToString();
            //    if (statusGstDropDown.SelectedValue == "P")
            //    {
            //        statusGstDropDown.Enabled = true;
            //    }
            //    else
            //    {
            //        statusGstDropDown.Enabled = false;
            //    }
            //}
        }
    }

    private void BindStatusDropDown()
    {
        statusRegAddrDropDown.Items.Add(new ListItem("Pending", "P"));
        statusRegAddrDropDown.Items.Add(new ListItem("Approved", "A"));
        statusRegAddrDropDown.Items.Add(new ListItem("Rejected", "R"));

        //statusGstDropDown.Items.Add(new ListItem("Pending", "P"));
        //statusGstDropDown.Items.Add(new ListItem("Approved", "A"));
        //statusGstDropDown.Items.Add(new ListItem("Rejected", "R"));

        statusReraDropDown.Items.Add(new ListItem("Pending", "P"));
        statusReraDropDown.Items.Add(new ListItem("Approved", "A"));
        statusReraDropDown.Items.Add(new ListItem("Rejected", "R"));
    }

    public DataTable GetProfileChangeReqDetails(int rowId, int flag)
    {
        DataTable dataTable = new DataTable();
        List<mProfileChangeRequest> list = new List<mProfileChangeRequest>();
        SqlConnection con = new SqlConnection(ConStr);
        try
        {
            SqlCommand cmd = new SqlCommand("usp_ProfileChangeRequestDetails", con);
            cmd.Parameters.AddWithValue("@rowId", rowId);
            cmd.Parameters.AddWithValue("@flag", flag);
            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            // this will query your database and return the result to your datatable
            da.Fill(dataTable);
            con.Close();
            da.Dispose();
        }
        catch (Exception ex)
        {

        }

        return dataTable;
    }

    public DataTable GetProfileChangeReq()
    {
        DataTable dataTable = new DataTable();
        List<mProfileChangeRequest> list = new List<mProfileChangeRequest>();
        SqlConnection con = new SqlConnection(ConStr);
        try
        {
            SqlCommand cmd = new SqlCommand("usp_ProfileChangeRequest", con);
            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            // this will query your database and return the result to your datatable
            da.Fill(dataTable);
            con.Close();
            da.Dispose();
        }
        catch (Exception ex)
        {

        }

        return dataTable;
    }

    protected void gdvRera_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }


    protected void gdvGst_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lnkBtn = (LinkButton)e.Row.FindControl("lnkBtnApprove");
            LinkButton lnkBtnR = (LinkButton)e.Row.FindControl("lnkBtnReject");
            Label lblStatus = (Label)e.Row.FindControl("lblStatus");
            TextBox txtComment = (TextBox)e.Row.FindControl("txtComment");
            Label lblComment = (Label)e.Row.FindControl("lblComment");

            if (lblStatus.Text.Trim() == "Approve" || lblStatus.Text.Trim() == "Reject")
            {
                lnkBtn.Style.Add("display", "none");
                lnkBtnR.Style.Add("display", "none");
                txtComment.Visible = false;
                lblComment.Visible = true;
            }
            else
            {
                txtComment.Visible = true;
                lblComment.Visible = false;
                //lnkBtn.Text = "Activate";
            }

        }
    }

    protected void reqDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblStatus = (Label)e.Row.FindControl("lblStatus");
            Label lblStatusText = (Label)e.Row.FindControl("lblStatusText");
            switch (lblStatus.Text)
            {
                case "A":
                    lblStatusText.Text = "Approved";
                    break;
                case "R":
                    lblStatusText.Text = "Rejected";
                    break;
                case "P":
                    lblStatusText.Text = "Pending";
                    break;
                default:
                    lblStatusText.Text = "Pending";
                    break;
            }
        }
    }

    public class EmplReraGSTDocs
    {
        public string State { get; set; }
        public string Rera_no { get; set; }
        public string Rera_Certi { get; set; }
        public string EmplID { get; set; }
        public string Status { get; set; }
        public string Gst_no { get; set; }
        public string Gst_Certi { get; set; }

    }


    public class mProfileChangeRequest
    {
        public int ID { get; set; }
        public string EmplId { get; set; }
        public string Email { get; set; }
        public string Pan_No { get; set; }
        public string Status { get; set; }
        public int RequestTypeId { get; set; }
        public string RequestType { get; set; }
        public string RejectionComment { get; set; }
    }

    public class mRegisteredAddress
    {
        public int ID { get; set; }
        public string New_RegisteredAddress { get; set; }
        public string New_RegisteredCountry { get; set; }
        public string New_RegisteredState { get; set; }
        public string New_RegisteredCity { get; set; }
        public string AddressProof { get; set; }
        public string IsApproved { get; set; }
        public string New_RegisteredZipCode { get; set; }
        public string Comment { get; internal set; }
    }

    public class mReraGst
    {
        public int? EmplID { get; set; }
        public int? ReraRowId { get; set; }
        public string ReraNumber { get; set; }
        public string ReraState { get; set; }
        public string ReraCerti { get; set; }
        public string IsApproved { get; set; }
        public int? GstRowId { get; set; }
        public string GstNumber { get; set; }
        public string GstCerti { get; set; }
        public string Comment { get; set; }
        public string GstState { get; set; }
    }

    //public class mRera
    //{
    //    public int ID { get; set; }
    //    public string ReraNumber { get; set; }
    //    public string State { get; set; }
    //    public string ReraCerti { get; set; }
    //    public string IsApproved { get; set; }
    //    public string Comment { get; set; }
    //}

    public class mGst
    {
        public int ID { get; set; }
        public string GstNumber { get; set; }
        public string State { get; set; }
        public string GstCerti { get; set; }
        public string IsApproved { get; set; }
        public string Comment { get; internal set; }
    }

    public enum ChangeReqType
    {
        RegisteredAddress = 1,
        Rera = 2,
        Gst = 3
    }
}