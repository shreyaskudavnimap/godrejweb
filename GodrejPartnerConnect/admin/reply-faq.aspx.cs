﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;
//using System.Transactions;

public partial class Admin_add_projectinnerdetails : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    Guid random = Guid.NewGuid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");

        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["faqId"])))
            {
                FillData(Convert.ToString(Request.QueryString["faqId"]));
            }
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("ProjectAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("ProjectInnerDetailsLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

    }

    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("faq.aspx");
    }

    private void FillData(string id)
    {
        var objFaq = (from obj in dataContext.Faqs
                      join empl in dataContext.tbl_EmpanelmentContacts
                      on obj.userid equals empl.BrokerID
                      orderby obj.id descending
                      select new
                      {
                          empl.Name,
                          obj.question
                      }).SingleOrDefault();

        txtName.Text = objFaq.Name;
        txtQues.Text = objFaq.question;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["faqId"])))
        {
            #region add new record
            Faq objFaq = new Faq();
            objFaq.ReplyDate = DateTime.Now;
            objFaq.answer = txtDescription.Text;
            dataContext.Faqs.InsertOnSubmit(objFaq);
            dataContext.SubmitChanges();
            Response.Redirect("faq.aspx?save=1");
            #endregion
        }
        else
        {
            int id = Convert.ToInt32(Session["faqId"]);
            #region Code For Updation
            var objFaq = dataContext.Faqs.Single(doc => doc.id == id);
            objFaq.ReplyDate = DateTime.Now;
            objFaq.answer = txtDescription.Text;
            dataContext.SubmitChanges();
            Session["faqId"] = null;
            Response.Redirect("faq.aspx?saved=1");
            #endregion
        }
        Div1.Style.Add("display", "block");
    }
}
