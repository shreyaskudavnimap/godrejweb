﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class Admin_viewprojecthighlights : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        if (!IsPostBack)
        {
            BindGrid("");

            if (Convert.ToString(Request.QueryString["saved"]) == "1")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record updated successfully.');</script>");
            }
            if (Convert.ToString(Request.QueryString["save"]) == "1")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record added successfully.');</script>");
            }
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("ProjectAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("ProjectHighlights");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

        txtSearch.Attributes.Add("onKeyPress", "doClick('" + btnSubmit.ClientID + "',event)");
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (txtSearch.Text != "" && txtSearch.Text != "search by project")
        {
            BindGrid(txtSearch.Text);
        }
    }

    private void BindGrid(string searchtext)
    {
        var objProject = from obj in dataContext.ProjectHighlights
                         join prj in dataContext.Projects
                         on obj.projectid equals prj.projectid
                         join cty in dataContext.CityMasters
                         on prj.cityid equals cty.cityid
                         join menu in dataContext.Menus
                         on obj.menuid equals menu.menuid
                         orderby obj.id descending
                         select new
                         {
                             obj.id,
                             title = prj.title + "," + cty.cityname,
                             menu.menutitle,
                             obj.icon,
                             obj.description,
                             obj.status
                         };

        if (searchtext != "" && searchtext != "search by project")
            objProject = objProject.Where(d => d.title.Contains(searchtext));

        if (objProject.Count() > 0)
        {
            lblMsg.Visible = false;
            lblMsg.Text = "";
            grdDetails.DataSource = objProject;
            grdDetails.DataBind();
        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "<br/>No records found.";
            grdDetails.DataSource = objProject;
            grdDetails.DataBind();
        }
    }

    protected void lnkBtnAddContent_Click(object sender, EventArgs e)
    {
        Session["highlightId"] = null;
        Response.Redirect("add-projecthighlights.aspx");
    }

    protected void grdDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdDetails.PageIndex = e.NewPageIndex;
        BindGrid(txtSearch.Text);
    }

    protected void grdDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditTest")
        {
            #region Code For Updation
            int id = Convert.ToInt32(e.CommandArgument);
            Session["highlightId"] = Convert.ToString(e.CommandArgument);
            Response.Redirect("add-projecthighlights.aspx?highlightId=" + id + "");
            #endregion
        }
        if (e.CommandName == "DeleteText")
        {
            #region
            int id = Convert.ToInt32(e.CommandArgument);
            var objProject = from d in dataContext.ProjectHighlights where (d.id == id) select d;
            foreach (ProjectHighlight item in objProject)
            {
                dataContext.ProjectHighlights.DeleteOnSubmit(item);
                dataContext.SubmitChanges();
            }
            ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data deleted successfully');</script>");
            BindGrid("");
            #endregion
        }
        if (e.CommandName == "Active")
        {
            #region code for Activation
            int id = Convert.ToInt32(e.CommandArgument);
            var objProject = dataContext.ProjectHighlights.Single(doc => doc.id == id);
            if (objProject.status != null)
            {
                if (objProject.status.Trim() == "A")
                {
                    objProject.status = "D";
                    dataContext.SubmitChanges();
                    ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data DeActivated successfully');</script>");
                }
                else
                {
                    objProject.status = "A";
                    dataContext.SubmitChanges();
                    ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data Activated successfully');</script>");
                }
            }
            else
            {
                objProject.status = "A";
                dataContext.SubmitChanges();
                ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data Activated successfully');</script>");
            }
            BindGrid("");
            #endregion
        }
    }

    protected void grdDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lnkBtn = (LinkButton)e.Row.FindControl("lnkBtnActive");
            Label lbls = (Label)e.Row.FindControl("lblStatus");

            Image image = (Image)e.Row.FindControl("imgPage");
            string[] str = image.ImageUrl.Split('/');
            int leghth = str.Length - 1;
            if (str[leghth] == "")
            {
                image.ImageUrl = "";
            }

            if (lbls.Text.Trim() == "A")
            {
                lnkBtn.Text = "DeActivate";
            }
            else
            {
                lnkBtn.Text = "Activate";
            }
            if (e.Row.DataItemIndex >= 0)
            {
                foreach (Control control in e.Row.Cells[4].Controls)
                {
                    LinkButton DeleteButton = control as LinkButton;
                    if (DeleteButton != null && DeleteButton.Text == "Delete")
                    {
                        DeleteButton.OnClientClick = "return(confirm('Are you sure you want to delete this record?'))";
                    }
                    if (DeleteButton != null && DeleteButton.Text == "Activate")
                    {
                        DeleteButton.OnClientClick = "return(confirm('Are you sure you want to Activate this record?'))";
                    }
                    if (DeleteButton != null && DeleteButton.Text == "DeActivate")
                    {
                        DeleteButton.OnClientClick = "return(confirm('Are you sure you want to DeActivate this record?'))";
                    }
                }
            }
        }
    }
}
