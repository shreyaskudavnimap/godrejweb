﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;
//using System.Transactions;

public partial class Admin_add_events : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    Guid random = Guid.NewGuid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");

        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["eventId"])))
            {
                FillData(Convert.ToString(Request.QueryString["eventId"]));
            }
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("EventsAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("EventsLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

    }

    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewevents.aspx");
    }

    private void FillData(string id)
    {
        var objEvents = dataContext.Events.Single(doc => doc.id == Convert.ToInt32(id));
        txtTitle.Text = objEvents.title;
        txtDescription.Text = objEvents.description;
        txtDate.Text = Convert.ToDateTime(objEvents.date).ToString("dd-MMM-yyyy");
        txtPlace.Text = objEvents.place;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["eventId"])))
        {
            #region to check if same events exists

            var objChk = (from obj in dataContext.Events
                          where obj.title == txtTitle.Text && obj.place == txtPlace.Text
                          select obj).FirstOrDefault();

            #endregion
            if (objChk != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Event already exists.');</script>");
                return;
            }
            else
            {
                #region add new record
                Event objEvents = new Event();
                #region Save Image
                if (FileUpload1.HasFile)
                {
                    if (FileUpload1.PostedFile.ContentLength > 5242880)
                    {
                        ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual Image size should not be more than 1MB');</script>");
                        return;
                    }
                    string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                    if (imgext.ToLower() == ".gif" || imgext.ToLower() == ".jpg" || imgext.ToLower() == ".png")
                    {
                        string fileName = Path.GetFileName(FileUpload1.FileName);
                        string fileNameEdited = random.ToString() + "_" + fileName;

                        float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
                        float height = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
                        if (width == 800 && height == 563)
                        {
                            if (fileNameEdited.Contains('&'))
                                fileNameEdited = fileNameEdited.Replace('&', 'a');
                            fileNameEdited = fileNameEdited.Replace("-", "");

                            FileUpload1.SaveAs(Server.MapPath("~/images/Events/") + fileNameEdited);
                            FixedSize(Convert.ToInt32(width), Convert.ToInt32(height), 331, 233, true, Server.MapPath("~/images/Events/") + fileNameEdited, Server.MapPath("~/images/Events/resize/") + fileNameEdited);
                            objEvents.image = fileNameEdited;
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Image minimum dimensions should be 800 * 563px');</script>");
                            return;
                        }
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif or .jpg or .png file');</script>");
                        return;
                    }
                }
                #endregion
                objEvents.title = txtTitle.Text;
                objEvents.description = txtDescription.Text; ;
                objEvents.place = txtPlace.Text;
                objEvents.date = Convert.ToDateTime(txtDate.Text);
                objEvents.status = "A";
                objEvents.createdon = DateTime.Now;
                dataContext.Events.InsertOnSubmit(objEvents);
                dataContext.SubmitChanges();
                Response.Redirect("viewevents.aspx?save=1");
                #endregion

            }
        }
        else
        {
            int id = Convert.ToInt32(Session["eventId"]);
            #region to check if same city exists

            var objChk = (from obj in dataContext.Events
                          where obj.id != id && obj.title == txtTitle.Text && obj.place == txtPlace.Text
                          select obj).FirstOrDefault();

            #endregion
            if (objChk != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Event already exists.');</script>");
                return;
            }
            else
            {
                #region Code For Updation
                var objEvent = dataContext.Events.Single(doc => doc.id == id);
                #region Update Image
                if (FileUpload1.HasFile)
                {
                    if (FileUpload1.PostedFile.ContentLength > 5242880)
                    {
                        ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual File size should not be more than 5MB');</script>");
                        return;
                    }
                    string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                    if (imgext.ToLower() == ".gif" || imgext.ToLower() == ".jpg" || imgext.ToLower() == ".png")
                    {
                        string fileName = Path.GetFileName(FileUpload1.FileName);
                        string fileNameEdited = random.ToString() + "_" + fileName;

                        float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
                        float height = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
                        if (width == 800 && height == 563)
                        {
                            if (fileNameEdited.Contains('&'))
                                fileNameEdited = fileNameEdited.Replace('&', 'a');
                            fileNameEdited = fileNameEdited.Replace("-", "");

                            FileUpload1.SaveAs(Server.MapPath("~/images/Projects/") + fileNameEdited);
                            FixedSize(Convert.ToInt32(width), Convert.ToInt32(height), 331, 233, true, Server.MapPath("~/images/Projects/") + fileNameEdited, Server.MapPath("~/images/Projects/resize/") + fileNameEdited);
                            objEvent.image = fileNameEdited;
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Image minimum dimensions should be 800 * 563px');</script>");
                            return;
                        }
                        try
                        {
                            if (ViewState["Image1"] != null && ViewState["Image1"].ToString() != "")
                                File.Delete(Server.MapPath(@"images/Projects/" + ViewState["Image1"].ToString()));
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif or .jpg or .png file');</script>");
                        return;
                    }
                }
                #endregion
                objEvent.title = txtTitle.Text;
                objEvent.description = txtDescription.Text;
                objEvent.place = txtPlace.Text;
                objEvent.date = Convert.ToDateTime(txtDate.Text);
                objEvent.createdon = DateTime.Now;
                dataContext.SubmitChanges();
                Session["innerprojectId"] = null;
                Response.Redirect("viewevents.aspx?saved=1");
                #endregion

            }
        }
        Div1.Style.Add("display", "block");
    }

    public void FixedSize(int srcwidth, int srcheight, int Width, int Height, bool needToFill, string srcpath, string targetpath)
    {
        try
        {
            System.Drawing.Image imgPhoto = System.Drawing.Image.FromFile(srcpath);
            int sourceWidth = srcwidth;
            int sourceHeight = srcheight;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)Width / (float)sourceWidth);
            nPercentH = ((float)Height / (float)sourceHeight);
            if (!needToFill)
            {
                if (nPercentH < nPercentW)
                {
                    nPercent = nPercentH;
                }
                else
                {
                    nPercent = nPercentW;
                }
            }
            else
            {
                if (nPercentH > nPercentW)
                {
                    nPercent = nPercentH;
                    destX = (int)Math.Round((Width -
                        (sourceWidth * nPercent)) / 2);
                }
                else
                {
                    nPercent = nPercentW;
                    destY = (int)Math.Round((Height -
                        (sourceHeight * nPercent)) / 2);
                }
            }

            if (nPercent > 1)
                nPercent = 1;

            int destWidth = (int)Math.Round(sourceWidth * nPercent);
            int destHeight = (int)Math.Round(sourceHeight * nPercent);

            System.Drawing.Bitmap bmPhoto = new System.Drawing.Bitmap(
                destWidth <= Width ? destWidth : Width,
                destHeight < Height ? destHeight : Height,
                              PixelFormat.Format32bppRgb);
            //bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
            //                 imgPhoto.VerticalResolution);

            System.Drawing.Graphics grPhoto = System.Drawing.Graphics.FromImage(bmPhoto);
            grPhoto.Clear(System.Drawing.Color.White);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
            //InterpolationMode.HighQualityBicubic;
            grPhoto.CompositingQuality = CompositingQuality.Default;
            grPhoto.SmoothingMode = SmoothingMode.Default;

            grPhoto.DrawImage(imgPhoto,
                new System.Drawing.Rectangle(destX, destY, destWidth, destHeight),
                new System.Drawing.Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                System.Drawing.GraphicsUnit.Pixel);
            bmPhoto.Save(targetpath);
            grPhoto.Dispose();
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        if (Cal1.Visible == false)
        {
            Cal1.Visible = true;
        }
        else
        {
            Cal1.Visible = false;
        }
    }
    protected void Cal1_SelectionChanged(object sender, EventArgs e)
    {
        txtDate.Text = Cal1.SelectedDate.ToString("dd/MM/yyyy");
        Cal1.Visible = false;
    }
}
