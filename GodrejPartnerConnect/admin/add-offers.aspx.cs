﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;
//using System.Transactions;

public partial class Admin_add_offers : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    Guid random = Guid.NewGuid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");

        if (!IsPostBack)
        {
            BindProject();
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["offerId"])))
            {
                FillData(Convert.ToString(Request.QueryString["offerId"]));
            }
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("OffersAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("OffersLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

    }

    private void BindProject()
    {
        #region Code for Project
        var query = (from obj in dataContext.Projects
                     join cty in dataContext.CityMasters
                     on obj.cityid equals cty.cityid
                     join objPlace in dataContext.PlaceMasters
                     on obj.placeid equals objPlace.placeid into defaultPlace
                     from c in defaultPlace.DefaultIfEmpty()
                     where obj.status == "A"
                     select new
                     {
                         obj.projectid,
                         projectname = (c == null ? obj.title + ", " + cty.cityname : obj.title + ", " + c.placename + ", " + cty.cityname)
                     });

       // if (query.Count() > 0)
        ddlProject.DataTextField = "projectname";
        ddlProject.DataValueField = "projectid";
        ddlProject.DataSource = query;
        ddlProject.DataBind();
        ddlProject.Items.Insert(0, new ListItem("--Select--", "0"));
        #endregion
    }
    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewoffers.aspx");
    }

    private void FillData(string id)
    {
        var objProject = dataContext.OfferAndSchemes.Single(doc => doc.id == Convert.ToInt32(id));
        txtDescription.Text = objProject.description;
        ddlProject.SelectedValue = Convert.ToString(objProject.projectid); ;
       
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["offerId"])))
        {
            #region to check if same offer exists

            var objChk = (from obj in dataContext.OfferAndSchemes
                          where obj.projectid == Convert.ToInt32(ddlProject.SelectedValue)
                          select obj).FirstOrDefault();  

            #endregion
            if (objChk != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Offer already exists.');</script>");
                return;
            }
            else
            {
                #region add new record
                OfferAndScheme objOffer = new OfferAndScheme();
                #region Save Image
                if (FileUpload1.HasFile)
                {
                    if (FileUpload1.PostedFile.ContentLength > 1048576)
                    {
                        //ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual Image size should not be more than 1MB');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Actual Image size should not be more than 1MB.');</script>", false);
                        return;
                    }
                    string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                    if (imgext.ToLower() == ".jpg" || imgext.ToLower() == ".gif" || imgext.ToLower() == ".png")
                    {
                        string fileName = Path.GetFileName(FileUpload1.FileName);
                        string fileNameEdited = random.ToString() + "_" + fileName;
                        float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
                        float height = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
                        if (width == 800 && height == 563)
                        {
                            FileUpload1.SaveAs(Server.MapPath("~/images/Offers/") + fileNameEdited);
                            objOffer.image = fileNameEdited;
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please upload image with minimum dimensions 800 x 563px');</script>");
                            return;
                        }
                    }
                    else
                    {
                        // ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif or .jpg or .png file');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload only .jpg,.gif,.png file.');</script>", false);
                        return;
                    }
                }
                else
                {
                    // ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload Image');</script>");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload Image');</script>", false);
                    return;
                }
                #endregion

                objOffer.description = txtDescription.Text;
                objOffer.projectid = Convert.ToInt32(ddlProject.SelectedValue);
                objOffer.status = "A";
                objOffer.craetedon = DateTime.Now;
                dataContext.OfferAndSchemes.InsertOnSubmit(objOffer);
                dataContext.SubmitChanges();
                Response.Redirect("viewoffers.aspx?save=1");
                #endregion

            }
        }
        else
        {
            int id = Convert.ToInt32(Session["offerId"]);
             #region to check if same offers exists

            var objChk = (from obj in dataContext.OfferAndSchemes
                          where obj.id != id && obj.projectid == Convert.ToInt32(ddlProject.SelectedValue)
                          select obj).FirstOrDefault();

            #endregion
            if (objChk != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Offer already exists.');</script>");
                return;
            }
            else
            {
                #region Code For Updation
                var objOffer = dataContext.OfferAndSchemes.Single(doc => doc.id == id);
                #region Update Image
                if (FileUpload1.HasFile)
                {
                    if (FileUpload1.PostedFile.ContentLength > 1048576)
                    {
                        //ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual Image size should not be more than 1MB');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Actual Image size should not be more than 1MB.');</script>", false);

                        return;
                    }
                    string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                    if (imgext.ToLower() == ".jpg" || imgext.ToLower() == ".gif" || imgext.ToLower() == ".png")
                    {
                        string fileName = Path.GetFileName(FileUpload1.FileName);
                        string fileNameEdited = random.ToString() + "_" + fileName;

                        float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
                        float height = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
                        if (width == 800 && height == 563)
                        {
                            FileUpload1.SaveAs(Server.MapPath("~/images/Offers/") + fileNameEdited);
                            objOffer.image = fileNameEdited;
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please upload image with minimum dimensions 800 x 563px');</script>");
                            return;
                        }
                        try
                        {
                            if (ViewState["Image1"] != null && ViewState["Image1"].ToString() != "")
                                File.Delete(Server.MapPath(@"img/" + ViewState["Image1"].ToString()));
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    else
                    {
                        //  ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif or .jpg or .png file');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload only .jpg, .gif and .png file.');</script>", false);

                        return;
                    }
                }
                #endregion
                objOffer.description = txtDescription.Text;
                objOffer.projectid = Convert.ToInt32(ddlProject.SelectedValue);
                dataContext.SubmitChanges();
                Session["offerId"] = null;
                Response.Redirect("viewoffers.aspx?saved=1");
                #endregion

            }
        }
        Div1.Style.Add("display", "block");
    }
}
