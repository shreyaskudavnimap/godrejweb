﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class Admin_ViewProjectVSRooms : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        BindGrid("");
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("ProjectAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("ProjectVSRoomsLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

        if (Convert.ToString(Request.QueryString["saved"]) == "1")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record updated successfully.');</script>");
        }
        if (Convert.ToString(Request.QueryString["save"]) == "1")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record added successfully.');</script>");
        }
        txtSearch.Attributes.Add("onKeyPress", "doClick('" + btnSubmit.ClientID + "',event)");
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (txtSearch.Text != "" && txtSearch.Text != "search by project")
        {
            BindGrid(txtSearch.Text);
        }
    }
    private void BindGrid(string searchtext)
    {

        var objProjectVS = (from obj in dataContext.SP_ProjectVSRoom()

                            select new
                            {
                                obj.Roomtype,
                                obj.status,
                                obj.title,
                                obj.Createddate,
                                obj.projectid,

                            }).ToList();
        
      
       
        if (searchtext != "" && searchtext != "search by project") {
            var items = (from li in objProjectVS
                         where li.title.Contains(searchtext)
                         select new
                            {
                                li.Roomtype,
                                li.status,
                                li.title,
                                li.Createddate,
                                li.projectid,

                            }).ToList();
            if (items.Count() > 0)
            {
                lblMsg.Visible = false;
                lblMsg.Text = "";
                grdDetails.DataSource = items;
                grdDetails.DataBind();
            }
            else
            {
                lblMsg.Visible = true;
                lblMsg.Text = "<br/>No records found.";
                grdDetails.DataSource = items;
                grdDetails.DataBind();
            }
        }
        else
        { 
           // objProjectVSRooms = objProjectVSRooms.Where(objProjectVSRoomsd => objProjectVSRooms.projecttype.Contains equals(searchtext));

         if (objProjectVS.Count() > 0)
        {
            lblMsg.Visible = false;
            lblMsg.Text = "";
            grdDetails.DataSource = objProjectVS;
            grdDetails.DataBind();
        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "<br/>No records found.";
            grdDetails.DataSource = objProjectVS;
            grdDetails.DataBind();
        }
            }
    }

    protected void lnkBtnAddContent_Click(object sender, EventArgs e)
    {
        Session["galleryId"] = null;
        Response.Redirect("addProjectVSRooms.aspx");
    }

    protected void grdDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdDetails.PageIndex = e.NewPageIndex;
        BindGrid(txtSearch.Text);
    }

    protected void grdDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditTest")
        {
            #region Code For Updation
            int id = Convert.ToInt32(e.CommandArgument);
            Session["galleryId"] = Convert.ToString(e.CommandArgument);
            Response.Redirect("addProjectVSRooms.aspx?galleryId=" + id + "");
            #endregion
        }
        if (e.CommandName == "DeleteText")
        {
            #region
            int id = Convert.ToInt32(e.CommandArgument);
            var objGallery = from d in dataContext.Galleries where (d.GalleryID == id) select d;
            foreach (Gallery item in objGallery)
            {
                dataContext.Galleries.DeleteOnSubmit(item);
                dataContext.SubmitChanges();
            }
            ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data deleted successfully');</script>");
            BindGrid("");
            #endregion
        }
        if (e.CommandName == "Active")
        {
            #region code for Activation
            int id = Convert.ToInt32(e.CommandArgument);
            var objGallery = dataContext.ProjectVSRooms.Single(doc => doc.Id == id);
            if (objGallery.Status != null)
            {
                if (objGallery.Status.Trim() == "A")
                {
                    objGallery.Status = "D";
                    dataContext.SubmitChanges();
                    ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data DeActivated successfully');</script>");
                }
                else
                {
                    objGallery.Status = "A";
                    dataContext.SubmitChanges();
                    ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data Activated successfully');</script>");
                }
            }
            else
            {
                objGallery.Status = "A";
                dataContext.SubmitChanges();
                ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data Activated successfully');</script>");
            }
            BindGrid("");
            #endregion
        }
    }

    protected void grdDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lnkBtn = (LinkButton)e.Row.FindControl("lnkBtnActive");
            Label lbls = (Label)e.Row.FindControl("lblStatus");

            if (lbls.Text.Trim() == "A")
            {
                lnkBtn.Text = "DeActivate";
            }
            else
            {
                lnkBtn.Text = "Activate";
            }
            if (e.Row.DataItemIndex >= 0)
            {
                foreach (Control control in e.Row.Cells[3].Controls)
                {
                    LinkButton DeleteButton = control as LinkButton;
                    if (DeleteButton != null && DeleteButton.Text == "Delete")
                    {
                        DeleteButton.OnClientClick = "return(confirm('Are you sure you want to delete this record?'))";
                    }
                    if (DeleteButton != null && DeleteButton.Text == "Activate")
                    {
                        DeleteButton.OnClientClick = "return(confirm('Are you sure you want to Activate this record?'))";
                    }
                    if (DeleteButton != null && DeleteButton.Text == "DeActivate")
                    {
                        DeleteButton.OnClientClick = "return(confirm('Are you sure you want to DeActivate this record?'))";
                    }
                }
            }
        }
    }
    protected void btnSubmit_Click1(object sender, EventArgs e)
    {
        if (txtSearch.Text != "" && txtSearch.Text != "search by project")
        {
            BindGrid(txtSearch.Text);
        }
    }
}
