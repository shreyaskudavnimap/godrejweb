﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;

public partial class Admin_add_city : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        if (!IsPostBack)
        {
            BindCity();
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["placeId"])))
            {
                FillData(Convert.ToString(Request.QueryString["placeId"]));
            }
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("MasterAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("PlaceLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

    }

    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewplace.aspx");
    }

    private void BindCity()
    {
        var query = (from obj in dataContext.CityMasters
                     where obj.status == "A"
                     orderby obj.cityname
                     select new
                     {
                         obj.cityid,
                         obj.cityname
                     });

        if (query.Count() > 0)
        {
            ddlCity.DataTextField = "cityname";
            ddlCity.DataValueField = "cityid";
            ddlCity.DataSource = query;
            ddlCity.DataBind();
            ddlCity.Items.Insert(0, new ListItem("Select City", "0"));
        }
    }

    private void FillData(string placeid)
    {
        var objPlace = dataContext.PlaceMasters.Single(doc => doc.placeid == Convert.ToInt32(placeid));
        txtTitle.Text = objPlace.placename;
        ddlCity.SelectedValue = Convert.ToString(objPlace.cityid);
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["placeId"])))
        {
             #region to check if same place exists

            var objChk = (from obj in dataContext.PlaceMasters
                          where obj.cityid == Convert.ToInt32(ddlCity.SelectedValue) && obj.placename == txtTitle.Text
                          select obj).FirstOrDefault();

            #endregion
            if (objChk != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Place Name already exists.');</script>");
                return;
            }
            else
            {
                #region For Adding the Record
                PlaceMaster objPlace = new PlaceMaster();

                objPlace.placename = txtTitle.Text.Trim();
                objPlace.cityid = Convert.ToInt32(ddlCity.SelectedValue);
                objPlace.createddate = DateTime.Now;
                objPlace.status = "A";
                dataContext.PlaceMasters.InsertOnSubmit(objPlace);
                dataContext.SubmitChanges();
                Response.Redirect("viewplace.aspx?save=1");
                #endregion
            }
        }
        else
        { 
            int id = Convert.ToInt32(Session["placeId"]);
            #region to check if same place exists

            var objChk = (from obj in dataContext.PlaceMasters
                          where obj.placeid !=id &&  obj.cityid == Convert.ToInt32(ddlCity.SelectedValue) && obj.placename == txtTitle.Text
                          select obj).FirstOrDefault();

            #endregion
            if (objChk != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Place Name already exists.');</script>");
                return;
            }
            else
            {
                #region Code For Updation
                var objPlace = dataContext.PlaceMasters.Single(doc => doc.placeid == id);
                objPlace.placename = txtTitle.Text.Trim();
                objPlace.cityid = Convert.ToInt32(ddlCity.SelectedValue);
                dataContext.SubmitChanges();
                Session["placeId"] = null;
                Response.Redirect("viewplace.aspx?saved=1");
                #endregion
            }
        }
        Div1.Style.Add("display", "block");
    }
}
