﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;

public partial class Admin_AddProjectVSRooms : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        if (!IsPostBack)
        {
            BindProject();
            FillCheckBoxList();

            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["galleryId"])))
            {
                FillData(Convert.ToString(Request.QueryString["galleryId"]));

            }
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("ProjectAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("ProjectVSRoomsLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");
        
    }


    private void FillCheckBoxList()
    {
        var objRooms = from obj in dataContext.Rooms                        
                         orderby obj.RoomType ascending
                         select new
                         {
                             obj.RoomID,                             
                             obj.RoomType,
                             obj.Status,
                              

                         };
        chlist.DataSource = objRooms;
        chlist.DataBind();
     
    }
    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewplace.aspx");
    }

    private void BindProject()
    {
        #region Code for Project
        var objProject = (from obj in dataContext.Projects
                          join objDesgn in dataContext.CityMasters
                          on obj.cityid equals objDesgn.cityid
                          join objPlace in dataContext.PlaceMasters
                          on obj.placeid equals objPlace.placeid into defaultPlace
                          from c in defaultPlace.DefaultIfEmpty()
                          where obj.status.Trim() == "A"
                          orderby obj.title ascending
                          select new
                          {
                              obj.projectid,
                              projectname = (c == null ? obj.title + "," + objDesgn.cityname : obj.title + "," + objDesgn.cityname + " " + c.placename)

                          }).Distinct().ToList();
        ddlProject.DataTextField = "projectname";
        ddlProject.DataValueField = "projectid";
        ddlProject.DataSource = objProject;
        ddlProject.DataBind();
        ddlProject.Items.Insert(0, new ListItem("--Select--", "0"));
        #endregion
    }

    private void FillData(string galleryid)
    {
        ddlProject.SelectedValue = Convert.ToString(galleryid);

        var objProjectVSRooms = from d in dataContext.ProjectVSRooms
                                join room in dataContext.Rooms on d.RoomID equals (room.RoomID)
                                where (d.ProjectID.Equals(galleryid))
                                select new
                                {
                                    d.RoomID,
                                    room.RoomType,                                     

                                };



        var objroom = from d in dataContext.ProjectVSRooms  where (d.ProjectID.Equals(galleryid))
                      select d;

        var objroomMas = from db in dataContext.Rooms                                              
                         select new
                         {
                             db.RoomID,
                             db.RoomType,

                         };


        chlist.DataSource = objroomMas;
        chlist.DataBind();
        int intcount;

        for (int i = 0; i < chlist.Items.Count; i++)
        {
            foreach (ProjectVSRoom item in objroom)
            {
                intcount = Convert.ToInt32(item.RoomID) - 2;
                if (intcount == i)
                {
                    chlist.Items[intcount].Selected = true;
                }
              
            }
        }
        
        
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string s = string.Empty;
       Int32 intprojectid =  Convert.ToInt32(ddlProject.SelectedValue);
       Int32 intRoomID;

       if (intprojectid != 0)
       {
           var objroom = from d in dataContext.ProjectVSRooms where (d.ProjectID.Equals(intprojectid)) select d;
           foreach (ProjectVSRoom item in objroom)
           {
               dataContext.ProjectVSRooms.DeleteOnSubmit(item);
               dataContext.SubmitChanges();
           }

          
           for (int i = 0; i < chlist.Items.Count; i++)
           {
               if (chlist.Items[i].Selected)
               {
                   intRoomID = Convert.ToInt32(chlist.Items[i].Value);                 

                       ProjectVSRoom obj = new ProjectVSRoom();
                       obj.RoomID = Convert.ToInt32(chlist.Items[i].Value);
                       obj.ProjectID = intprojectid;
                       obj.CreatedDate = System.DateTime.Now;
                       obj.Status = "A";
                       dataContext.ProjectVSRooms.InsertOnSubmit(obj);
                       dataContext.SubmitChanges();
                    
               }

           }
           Response.Redirect("ViewProjectVSRooms.aspx?saved=1");
       }
    }
}


       
   
