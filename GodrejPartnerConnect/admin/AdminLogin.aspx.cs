﻿using GodrejCP;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Script.Serialization;

public partial class Admin_AdminLogin : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        var admin = from u in dataContext.tblAdmins
                    where
                        (u.username == txtUserName.Text.Trim() && u.password == txtPwd.Text.Trim())
                    select u;
        if (admin.Count() > 0)
        {
            foreach (var item in admin)
            {
                Session.Add("User", item.userid);
                Session.Add("type", item.type);
                Session.Add("region", item.region);
            }
            if (Convert.ToString(Session["type"]) == "SA")
            {
                getToken();
                Response.Redirect("viewbanner.aspx");
            }
            if (Convert.ToString(Session["type"]) == "admin")
            {
                Response.Redirect("viewrequest.aspx");
            }
            if (Convert.ToString(Session["type"]) == "subadmin")
            {
                Response.Redirect("View-Registration.aspx");
            }
            if (Convert.ToString(Session["type"]) == "callcentre")      // Role_dev
            {
                Response.Redirect("Home.aspx");
            }
        }
        else
            ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('UserName and Password Mismatch');</script>");
    }

    protected void getToken()
    {
        using (WebClient client = new WebClient())
        {
            var reqparm = new System.Collections.Specialized.NameValueCollection();
            reqparm.Add("param1", "");
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

            Uri myUri = new Uri(ConfigurationManager.AppSettings["SFDCURL"] + ConfigurationManager.AppSettings["GRANTSERVICE"] + "&client_id=" 
                + ConfigurationManager.AppSettings["CLIENTID"] + "&client_secret=" + ConfigurationManager.AppSettings["CLIENTSECRET"] + "&username=" 
                + ConfigurationManager.AppSettings["USERNAME"] + "&password=" + ConfigurationManager.AppSettings["Password"], UriKind.Absolute);

            //Uri myUri = new Uri("https://godrej.my.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9Y6d_Btp4xp7lt5FL02Cc.bHBCI_vpcrJYvpeBev1Ob5nXpobkxkmhygUekoOvbQMscya0i3r7EacuDWz&client_secret=332639414523432900&username=external.app@godrejproperties.com&password=Nov@12345Svgw3cZob0A7HWlJs8I07Jcdb", UriKind.Absolute);

            byte[] responsebytes = client.UploadValues(myUri, reqparm);

            string responseBody = System.Text.Encoding.UTF8.GetString(responsebytes);
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            var result = jsSerializer.DeserializeObject(responseBody);
            Dictionary<string, dynamic> obj2 = new Dictionary<string, dynamic>();
            obj2 = (Dictionary<string, dynamic>)(result);
            string val = obj2["access_token"].ToString();
            WLog.AdminWriteLog("SESSION: " + val, "SESSION");
            Session["access_token"] = val;
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>console.log(val);</script>");
        }
    }
}
