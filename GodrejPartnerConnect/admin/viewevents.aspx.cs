﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class Admin_viewevents : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        BindGrid("");
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("EventsAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("EventsLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

        if (Convert.ToString(Request.QueryString["saved"]) == "1")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record updated successfully.');</script>");
        }
        if (Convert.ToString(Request.QueryString["save"]) == "1")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record added successfully.');</script>");
        }
        txtSearch.Attributes.Add("onKeyPress", "doClick('" + btnSubmit.ClientID + "',event)");
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (txtSearch.Text != "" && txtSearch.Text != "search by events")
        {
            BindGrid(txtSearch.Text);
        }
    }

    private void BindGrid(string searchtext)
    {
        var objProject = from obj in dataContext.Events
                         orderby obj.id descending
                         select new
                         {
                             obj.id,
                             obj.title,
                             obj.date,
                             obj.image,
                             obj.place,
                             obj.description,
                             obj.status,
                             obj.createdon
                         };

        if (searchtext != "" && searchtext != "search by events")
            objProject = objProject.Where(d => d.title.Contains(searchtext));

        if (objProject.Count() > 0)
        {
            lblMsg.Visible = false;
            lblMsg.Text = "";
            grdDetails.DataSource = objProject;
            grdDetails.DataBind();
        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "<br/>No records found.";
            grdDetails.DataSource = objProject;
            grdDetails.DataBind();
        }
    }

    protected void lnkBtnAddContent_Click(object sender, EventArgs e)
    {
        Session["eventId"] = null;
        Response.Redirect("add-events.aspx");
    }

    protected void grdDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdDetails.PageIndex = e.NewPageIndex;
        BindGrid(txtSearch.Text);
    }

    protected void grdDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditTest")
        {
            #region Code For Updation
            int id = Convert.ToInt32(e.CommandArgument);
            Session["eventId"] = Convert.ToString(e.CommandArgument);
            Response.Redirect("add-events.aspx?eventId=" + id + "");
            #endregion
        }
        if (e.CommandName == "DeleteText")
        {
            #region
            int id = Convert.ToInt32(e.CommandArgument);
            var objEvents = from d in dataContext.Events where (d.id == id) select d;
            foreach (Event item in objEvents)
            {
                dataContext.Events.DeleteOnSubmit(item);
                dataContext.SubmitChanges();
            }
            ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data deleted successfully');</script>");
            BindGrid("");
            #endregion
        }
        if (e.CommandName == "Active")
        {
            #region code for Activation
            int id = Convert.ToInt32(e.CommandArgument);
            var objEvents = dataContext.Events.Single(doc => doc.id == id);
            if (objEvents.status != null)
            {
                if (objEvents.status.Trim() == "A")
                {
                    objEvents.status = "D";
                    dataContext.SubmitChanges();
                    ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data DeActivated successfully');</script>");
                }
                else
                {
                    objEvents.status = "A";
                    dataContext.SubmitChanges();
                    ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data Activated successfully');</script>");
                }
            }
            else
            {
                objEvents.status = "A";
                dataContext.SubmitChanges();
                ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data Activated successfully');</script>");
            }
            BindGrid("");
            #endregion
        }
    }

    protected void grdDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lnkBtn = (LinkButton)e.Row.FindControl("lnkBtnActive");
            Label lbls = (Label)e.Row.FindControl("lblStatus");

            if (lbls.Text.Trim() == "A")
            {
                lnkBtn.Text = "DeActivate";
            }
            else
            {
                lnkBtn.Text = "Activate";
            }
            if (e.Row.DataItemIndex >= 0)
            {
                foreach (Control control in e.Row.Cells[5].Controls)
                {
                    LinkButton DeleteButton = control as LinkButton;
                    if (DeleteButton != null && DeleteButton.Text == "Delete")
                    {
                        DeleteButton.OnClientClick = "return(confirm('Are you sure you want to delete this record?'))";
                    }
                    if (DeleteButton != null && DeleteButton.Text == "Activate")
                    {
                        DeleteButton.OnClientClick = "return(confirm('Are you sure you want to Activate this record?'))";
                    }
                    if (DeleteButton != null && DeleteButton.Text == "DeActivate")
                    {
                        DeleteButton.OnClientClick = "return(confirm('Are you sure you want to DeActivate this record?'))";
                    }
                }
            }
        }
    }
}
