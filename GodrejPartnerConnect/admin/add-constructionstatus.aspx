﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="add-constructionstatus.aspx.cs" Inherits="Admin_add_constructionstatus" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Admin/MenuControl.ascx" TagName="Menu" TagPrefix="UC1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <link rel="dns-prefetch" href="http://fonts.googleapis.com" />
    <link rel="dns-prefetch" href="http://themes.googleusercontent.com" />
    <link rel="dns-prefetch" href="http://ajax.googleapis.com" />
    <link rel="dns-prefetch" href="http://cdnjs.cloudflare.com" />
    <link rel="dns-prefetch" href="http://agorbatchev.typepad.com" />
    <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/b/378 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Godrej Channel Partner :: Admin</title>
    <meta name="description" content="Godrej Channel Partner App">
    <meta name="author" content="Godrej Channel Partner App">
    <!-- Mobile viewport optimized: h5bp.com/viewport -->
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
    <!-- iPhone: Don't render numbers as call links -->
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" href="favicon.ico" />
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory: mathiasbynens.be/notes/touch-icons -->
    <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->
    <!-- The Styles -->
    <!-- ---------- -->
    <!-- Layout Styles -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/layout.css">
    <!-- Icon Styles -->
    <link rel="stylesheet" href="css/icons.css">
    <link rel="stylesheet" href="css/fonts/font-awesome.css">
    <!--[if IE 8]><link rel="stylesheet" href="css/fonts/font-awesome-ie7.css"><![endif]-->
    <!-- External Styles -->
    <link rel="stylesheet" href="css/external/jquery-ui-1.9.1.custom.css">
    <link rel="stylesheet" href="css/external/jquery.chosen.css">
    <link rel="stylesheet" href="css/external/jquery.cleditor.css">
    <link rel="stylesheet" href="css/external/jquery.colorpicker.css">
    <link rel="stylesheet" href="css/external/jquery.elfinder.css">
    <link rel="stylesheet" href="css/external/jquery.fancybox.css">
    <link rel="stylesheet" href="css/external/jquery.jgrowl.css">
    <link rel="stylesheet" href="css/external/jquery.plupload.queue.css">
    <link rel="stylesheet" href="css/external/syntaxhighlighter/shCore.css" />
    <link rel="stylesheet" href="css/external/syntaxhighlighter/shThemeDefault.css" />
    <!-- Elements -->
    <link rel="stylesheet" href="css/elements.css">
    <link rel="stylesheet" href="css/forms.css">
    <!-- OPTIONAL: Print Stylesheet for Invoice -->
    <link rel="stylesheet" href="css/print-invoice.css">
    <!-- Typographics -->
    <link rel="stylesheet" href="css/typographics.css">
    <!-- Responsive Design -->
    <link rel="stylesheet" href="css/media-queries.css">
    <!-- Bad IE Styles -->
    <link rel="stylesheet" href="css/ie-fixes.css">
    <!-- The Scripts -->
    <!-- ----------- -->
    <!-- JavaScript at the top (will be cached by browser) -->
    <!-- Grab frameworks from CDNs -->
    <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->

    <%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.js"></script>--%>

    <script>        window.jQuery || document.write('<script src="js/libs/jquery-1.8.2.js"><\/script>')</script>

    <!-- Do the same with jQuery UI -->

    <script src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>

    <script>        window.jQuery.ui || document.write('<script src="js/libs/jquery-ui-1.9.1.js"><\/script>')</script>

    <!-- Do the same with Lo-Dash.js -->
    <!--[if gt IE 8]><!-->

    <script src="http://cdnjs.cloudflare.com/ajax/libs/lodash.js/0.8.2/lodash.js"></script>

    <script>        window._ || document.write('<script src="js/libs/lo-dash.js"><\/script>')</script>

    <!--<![endif]-->
    <!-- IE8 doesn't like lodash -->
    <!--[if lt IE 9]><script src="http://documentcloud.github.com/underscore/underscore.js"></script><![endif]-->
    <!-- Do the same with require.js -->

    <script src="http://cdnjs.cloudflare.com/ajax/libs/require.js/2.0.6/require.js"></script>

    <script>        window.require || document.write('<script src="js/libs/require-2.0.6.min.js"><\/script>')</script>

    <!-- Load Webfont loader -->

    <script type="text/javascript">
        function validate() {

            if (document.getElementById('ddlProject').value == "0") {
                alert('Please Select Project');
                document.getElementById('ddlProject').focus();
                return false;
            }
            if (document.getElementById('txtTitle').value == "") {
                alert('Please enter title');
                document.getElementById('txtTitle').focus();
                return false;
            }            

            if (document.getElementById('ddlMonth').value == "0") {
                alert('Please Select Month');
                document.getElementById('ddlMonth').focus();
                return false;
            }

            if (document.getElementById('ddlYear').value == "0") {
                alert('Please Select Year');
                document.getElementById('ddlYear').focus();
                return false;
            }
        }
    </script>

    <script defer async src="https://ajax.googleapis.com/ajax/libs/webfont/1.0.28/webfont.js"></script>

    <!-- Essential polyfills -->

    <script src="js/mylibs/polyfills/modernizr-2.6.1.min.js"></script>

    <script src="js/mylibs/polyfills/respond.js"></script>

    <script src="js/mylibs/polyfills/matchmedia.js"></script>

    <!--[if lt IE 9]><script src="js/mylibs/polyfills/selectivizr.js"></script><![endif]-->
    <!--[if lt IE 10]><script src="js/mylibs/polyfills/excanvas.js"></script><![endif]-->
    <!--[if lt IE 10]><script src="js/mylibs/polyfills/classlist.js"></script><![endif]-->
    <!-- scripts concatenated and minified via build script -->
    <!-- Scripts required everywhere -->

    <script src="js/mylibs/jquery.hashchange.js"></script>

    <script src="js/mylibs/jquery.idle-timer.js"></script>

    <script src="js/mylibs/jquery.plusplus.js"></script>

    <script src="js/mylibs/jquery.scrollTo.js"></script>

    <script src="js/mylibs/jquery.ui.touch-punch.js"></script>

    <script src="js/mylibs/jquery.ui.multiaccordion.js"></script>

    <script src="js/mylibs/number-functions.js"></script>

    <script src="js/mylibs/fullstats/jquery.css-transform.js"></script>

    <script src="js/mylibs/fullstats/jquery.animate-css-rotate-scale.js"></script>

    <script src="js/mylibs/forms/jquery.validate.js"></script>

    <!-- Do not touch! -->

    <script src="js/mango.js"></script>

    <script src="js/plugins.js"></script>

    <script src="js/script.js"></script>

    <!-- Your custom JS goes here -->

    <script src="js/app.js"></script>

    <!-- end scripts -->

     <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.js"></script>

    <script src="js/jquery/jquery.MultiFile.js" type="text/javascript"></script>

</head>
<body>
    <form id="form1" runat="server">
        <!-- The loading box -->
        <input type="hidden" runat="server" id="hidSummary" />
        <div id="loading-overlay">
        </div>
        <div id="loading">
            <span>Loading...</span>
        </div>
        <!-- End of loading box -->
        <div style="display: none;" id="Div1" runat="server" title="Add Client Example Dialog">
            <div class="row">
                Data Saved Successfully
            <asp:Button ID="btnDataSaved" runat="server" CssClass="submit" OnClick="btnDataSaved_Click" />
            </div>
        </div>
        <!-- End if #dialog_add_client -->
        <!--------------------------------->
        <!-- Now, the page itself begins -->
        <!--------------------------------->
        <!-- The toolbar at the top -->
        <section id="toolbar">
        <div class="container_12">
            <!-- Right side -->
            <div class="right">
                <ul>
                    <li class="red"><a href="AdminLogin.aspx">Logout</a></li>
                </ul>
            </div>
            <!-- End of .right -->
            <!-- Phone only items -->
            <div class="phone">
                <!-- Navigation -->
                <li><a class="navigation" href="#"><span class="icon icon-list"></span></a></li>
            </div>
            <!-- End of phone items -->
        </div>
        <!-- End of .container_12 -->
    </section>
        <!-- End of #toolbar -->
        <!-- The header containing the logo -->
        <header class="container_12">
        <div class="container">
            <!-- Your logos -->
            <a href="viewusers.aspx">
                <img src="img/logo.png" alt="Godrej Channel Partner App"></a> <a class="phone-title" href="viewbanner.aspx">
                    <img src="img/logo-mobile.png" alt="Godrej Channel Partner App" height="30" /></a>
        </div>
        <!-- End of .container -->
    </header>
        <!-- End of header -->
        <!-- The container of the sidebar and content box -->
        <div role="main" id="main" class="container_12 clearfix">
            <!-- The blue toolbar stripe -->
            <section class="toolbar">
            <%--<input type="search" data-source="extras/search.php" placeholder="Search..." autocomplete="off"
                class="tooltip" title="" data-gravity="s">--%>
        </section>
            <!-- End of .toolbar-->
            <!-- The sidebar -->
            <UC1:Menu ID="Menu" runat="server" />
            <!-- End of sidebar -->
            <!-- Here goes the content. -->
            <section id="content" class="container_12 clearfix" data-sort="true">
            <h1 class="grid_12">
                Construction Status</h1>
            <!-- Here you see an example of two long .grid_6 colums filled with content -->
            <div class="grid_12">
                <div class="box">
                    <div class="header">
                        <h2>
                            Add Construction Status</h2>
                    </div>
                    <div class="content">
                        <div id="dialog_add_client" title="Add Client Example Dialog">
                            <div class="full validate">
                                <div class="row">
                                    <label for="f2_select1">
                                        <strong style="width: 80px;">Project<span style="color:red"> *</span></strong>
                                    </label>
                                    <div>
                                        <asp:DropDownList ID="ddlProject" runat="server"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="f2_select1">
                                        <strong style="width: 80px;">Title<span style="color:red"> *</span></strong>
                                    </label>
                                    <div>
                                        <asp:TextBox ID="txtTitle" runat="server" CssClass="inp-form" MaxLength="100"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="f2_select1">
                                        <strong style="width: 80px;">Month<span style="color:red"> *</span></strong>
                                    </label>
                                    <div>
                                        <asp:DropDownList ID="ddlMonth" runat="server">
                                            <asp:ListItem Value="0">select month</asp:ListItem>
                                            <asp:ListItem Value="01">January</asp:ListItem>
                                            <asp:ListItem Value="02">February</asp:ListItem>
                                            <asp:ListItem Value="03">March</asp:ListItem>
                                            <asp:ListItem Value="04">April</asp:ListItem>
                                            <asp:ListItem Value="05">May</asp:ListItem>
                                            <asp:ListItem Value="06">June</asp:ListItem>
                                            <asp:ListItem Value="07">July</asp:ListItem>
                                            <asp:ListItem Value="08">August</asp:ListItem>
                                            <asp:ListItem Value="09">September</asp:ListItem>
                                            <asp:ListItem Value="10">October</asp:ListItem>
                                            <asp:ListItem Value="11">November</asp:ListItem>
                                            <asp:ListItem Value="12">December</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="f2_select1">
                                        <strong style="width: 80px;">Year<span style="color:red"> *</span></strong>
                                    </label>
                                    <div>
                                        <asp:DropDownList ID="ddlYear" runat="server">
                                            <asp:ListItem Value="0">select year</asp:ListItem>
                                            <asp:ListItem Value="2016">2016</asp:ListItem>
                                            <asp:ListItem Value="2017">2017</asp:ListItem>
                                            <asp:ListItem Value="2018">2018</asp:ListItem>
                                            <asp:ListItem Value="2019">2019</asp:ListItem>
                                            <asp:ListItem Value="2020">2020</asp:ListItem>
                                            <asp:ListItem Value="2021">2021</asp:ListItem>
                                            <asp:ListItem Value="2022">2022</asp:ListItem>
                                            <asp:ListItem Value="2023">2023</asp:ListItem>
                                            <asp:ListItem Value="2024">2024</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                 <div class="row">
                                     <label for="f2_select1">
                                        <strong style="width: 80px;">Image<span style="color:red"> *</span></strong>
                                     </label>
                                      <div>
                                          <asp:FileUpload ID="FileUpload1"  maxlength="5" class="multi" style="float:left; width:200px;" runat="server" accept="jpg|jpeg"/>
                                      <div id="div_pic" runat="server">
                                            jpeg, jpg 1MB max per image, Max 5 images
                                        </div>
                                      </div>
                                  </div>       
                             </div>
                            <div class="actions">
                                <div class="right">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="submit" Text="Submit" OnClientClick="javascript:return validate();" OnClick="btnSubmit_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of .content -->
                </div>
                <!-- End of .box -->
            </div>
            <!-- End of .grid_6 -->
        </section>
            <!-- End of #content -->
        </div>
        <!-- End of #main -->
        <!-- The footer -->
        <footer class="container_12">
        <span class="grid_12">Copyright &copy; 2013 Netbiz Systems </span>
    </footer>
        <!-- End of footer -->
        <!-- Spawn $$.loaded -->

        <script>
            $$.loaded();
        </script>

        <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
        <!--[if lt IE 7 ]>
    <script defer src="http://ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
    <script defer>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
    <![endif]-->
    </form>
    </form>
</body>
</html>

