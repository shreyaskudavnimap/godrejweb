﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;

public partial class admin_uploaddata : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    Guid random = Guid.NewGuid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");

        if (!IsPostBack)
        {
            BindProject();
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["projectId"])))
            {
                FillData(Convert.ToString(Request.QueryString["projectId"]));
            }
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("DataAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("DataDetailsLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

    }

    private void BindProject()
    {
        #region Code for Project
        var objProject = (from obj in dataContext.Projects
                          join objDesgn in dataContext.CityMasters
                          on obj.cityid equals objDesgn.cityid
                          join objPlace in dataContext.PlaceMasters
                          on obj.placeid equals objPlace.placeid into defaultPlace
                          from c in defaultPlace.DefaultIfEmpty()
                          where obj.status.Trim() == "A"
                          orderby obj.projectid descending
                          select new
                          {
                              obj.projectid,
                              projectname = (c == null ? obj.title + ", " + objDesgn.cityname : obj.title + ", " + c.placename + ", " + objDesgn.cityname)

                          }).Distinct().ToList();
        ddlProject.DataTextField = "projectname";
        ddlProject.DataValueField = "projectid";
        ddlProject.DataSource = objProject;
        ddlProject.DataBind();
        ddlProject.Items.Insert(0, new ListItem("--Select--", "0"));
        #endregion
    }

    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("datadetails.aspx");
    }

    private void FillData(string id)
    {
        var objDocument = dataContext.Documents.Single(doc => doc.id == Convert.ToInt32(id));
        txtTitle.Text = objDocument.title;
        ddlType.SelectedValue = Convert.ToString(objDocument.type);
        ddlProject.SelectedValue = Convert.ToString(objDocument.projectid);
        if (ddlType.SelectedValue == "Text")
        {
            div_doc.Visible = false;
            txtDescription.Text = objDocument.text;
        }
        else
        {
            div_text.Visible = false;
            //div_doc.Visible = true;
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["dataId"])))
        {
            string text = "";
            #region add new record
            Document objDocument = new Document();
            if (ddlType.SelectedValue == "Text")
            {
                text = txtDescription.Text;
            }

            if (ddlType.SelectedValue == "Document")
            {
                #region Save Image
                if (FileUpload1.HasFile)
                {
                    //if (FileUpload1.PostedFile.ContentLength > 5242880)
                    //{
                    //    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual Image size should not be more than 5MB');</script>");
                    //    return;
                    //}
                    string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                    if (imgext.ToLower() == ".pdf" || imgext.ToLower() == ".xls" || imgext.ToLower() == ".doc" || imgext.ToLower() == ".pptx" || imgext == ".docx" || imgext.ToLower() == ".xlsx")
                    {
                        string fileName = Path.GetFileName(FileUpload1.FileName);
                        string fileNameEdited = random.ToString() + "_" + fileName;

                        if (fileNameEdited.Contains('&'))
                            fileNameEdited = fileNameEdited.Replace('&', 'a');
                        fileNameEdited = fileNameEdited.Replace(" ", "-");
                        FileUpload1.SaveAs(Server.MapPath("~/images/Document/") + fileNameEdited);
                        //FixedSize(Convert.ToInt32(width), Convert.ToInt32(height), 331, 233, true, Server.MapPath("~/images/Projects/") + fileNameEdited, Server.MapPath("~/images/Projects/resize/") + fileNameEdited);
                        objDocument.data = fileNameEdited;

                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .pdf or .doc or .xls or .ppt .docx .xlsx .pptx file');</script>");
                        return;
                    }
                }

                #endregion
            }
            var test = "";
            test = System.Text.RegularExpressions.Regex.Replace(text, "<[^>]*>", "");
            objDocument.type = ddlType.SelectedValue;
            objDocument.title = txtTitle.Text;
            objDocument.text = test;
            objDocument.projectid = ddlProject.SelectedValue;
            objDocument.status = "A";
            objDocument.createdon = DateTime.Now;
            dataContext.Documents.InsertOnSubmit(objDocument);
            dataContext.SubmitChanges();
            Response.Redirect("datadetails.aspx?save=1");
            #endregion
        }
        else
        {
            string text = "";
            int id = Convert.ToInt32(Session["dataId"]);
            #region Code For Updation
            var objDocument = dataContext.Documents.Single(doc => doc.id == id);
            if (ddlType.SelectedValue == "Text")
            {
                text = txtDescription.Text;
            }

            if (ddlType.SelectedValue == "Document")
            {
                #region Update Image
                if (FileUpload1.HasFile)
                {
                   // if (FileUpload1.PostedFile.ContentLength > 5242880)
                    //{
                    //    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual File size should not be more than 5MB');</script>");
                    //    return;
                    //}
                    string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                    if (imgext.ToLower() == ".pdf" || imgext.ToLower() == ".xls" || imgext.ToLower() == ".xlsx" || imgext.ToLower() == ".doc" || imgext.ToLower() == ".docx" || imgext.ToLower() == ".ppt" || imgext.ToLower() == ".pptx")
                    {
                        string fileName = Path.GetFileName(FileUpload1.FileName);
                        string fileNameEdited = random.ToString() + "_" + fileName;

                        if (fileNameEdited.Contains('&'))
                            fileNameEdited = fileNameEdited.Replace('&', 'a');
                        fileNameEdited = fileNameEdited.Replace(" ", "-");

                        FileUpload1.SaveAs(Server.MapPath("~/images/Document/") + fileNameEdited);
                        //FixedSize(Convert.ToInt32(width), Convert.ToInt32(height), 331, 233, true, Server.MapPath("~/images/Projects/") + fileNameEdited, Server.MapPath("~/images/Projects/resize/") + fileNameEdited);
                        objDocument.data = fileNameEdited;


                        try
                        {
                            if (ViewState["Image1"] != null && ViewState["Image1"].ToString() != "")
                                File.Delete(Server.MapPath(@"images/Projects/" + ViewState["Image1"].ToString()));
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .pdf or .doc or .xls or .ppt .docx .xlsx .pptx file'');</script>");
                        return;
                    }
                }
                #endregion
            }
            var test = "";
            test = System.Text.RegularExpressions.Regex.Replace(text, "<[^>]*>", "");
            objDocument.type = ddlType.SelectedValue;
            objDocument.title = txtTitle.Text;
            objDocument.text = test;
            objDocument.projectid = ddlProject.SelectedValue;
            dataContext.SubmitChanges();
            Session["dataId"] = null;
            Response.Redirect("datadetails.aspx?saved=1");
            #endregion
        }
        Div1.Style.Add("display", "block");
    }

    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlType.SelectedValue == "Text")
        {
            div_text.Visible = true;
            div_doc.Visible = false;
        }
        else
        {
            div_text.Visible = false;
            div_doc.Visible = true;
        }
    }
}