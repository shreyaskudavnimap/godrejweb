﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net.Mail;

public partial class admin_SubAdmin : System.Web.UI.Page
{
    private Random random = new Random();
    GodejCPDataContext dataContext = new GodejCPDataContext();
    string Nik = "";
    protected void Page_Load(object sender, EventArgs e)
    {
       // BindRegion();
       
    }

    //public void BindRegion()
    //{
    //   var query = (from obj in dataContext.Regions
                     
    //                 select new
    //                 {               
    //                     obj.Region1
    //                 });

    //    if (query.Count() > 0)
    //    {
    //        ddlRegion.DataTextField = "Region1";
    //        //ddlRegion.DataValueField = query;
    //        ddlRegion.DataSource = query;
    //        ddlRegion.DataBind();
    //        ddlRegion.Items.Insert(0, new ListItem("Select Zone", "0"));
   
    //    }


    //}

    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("ViewSubAdmin.aspx");
    }
 

    public  string GenerateRandomString(int length)
    {
        //It will generate string with combination of small,capital letters and numbers
        char[] charArr = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".ToCharArray();
        string randomString = string.Empty;
        Random objRandom = new Random();
        for (int i = 0; i < length; i++)
        {
            //Don't Allow Repetation of Characters
            int x = objRandom.Next(1, charArr.Length);
            if (!randomString.Contains(charArr.GetValue(x).ToString()))
                randomString += charArr.GetValue(x);
            else
                i--;
        }
        return Nik=randomString;
        

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        #region add new record
        tblAdmin objsubadmin = new tblAdmin();
        GenerateRandomString(5);
        objsubadmin.Name = txtName.Text;
        objsubadmin.username = txtUsername.Text;
        objsubadmin.region = ddlRegion.SelectedItem.Text;
        objsubadmin.password = txtUsername.Text + '@' + Nik;
        string pswd = objsubadmin.password.Trim();
        string id = objsubadmin.username;
        objsubadmin.emailid = txtEmail.Text;
        objsubadmin.type = "admin";
        objsubadmin.createddate = DateTime.Now;
        dataContext.tblAdmins.InsertOnSubmit(objsubadmin);
        dataContext.SubmitChanges();
       // Clear();
        sendEmail(pswd,id);
        Response.Redirect("ViewSubAdmin.aspx?save=1");
        #endregion
    }
    //public void Clear()
    //{

    //    txtUsername.Text = "";
    //    txtName.Text = "";
    //    ddlRegion.SelectedIndex = 0;
    //    txtEmail.Text = "";
    //}

    public void sendEmail(string pswd,string id)
    {
        try
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("mail.netbizlabs.com");
            mail.From = new MailAddress("info@netbizlabs.com");
            mail.To.Add(txtEmail.Text);
            mail.Subject = "Account Credential";
            mail.Body = "Your UserName" + " " + id +" and Password is "+ " " + pswd ;
            SmtpServer.Port = 25;
            //SmtpServer.Credentials = new System.Net.NetworkCredential("umeshpida@gmail.com", "umesh@1234");
            SmtpServer.Credentials = new System.Net.NetworkCredential("info@netbizlabs.com", "info321");
            SmtpServer.EnableSsl = true;
            SmtpServer.Send(mail);

        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
    }

}
