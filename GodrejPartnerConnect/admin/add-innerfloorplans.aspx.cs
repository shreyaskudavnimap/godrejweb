﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;
//using System.Transactions;

public partial class Admin_add_innerfloorplans : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    Guid random = Guid.NewGuid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");

        if (!IsPostBack)
        {
            BindProject();
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["innerplanId"])))
            {
                FillData(Convert.ToString(Request.QueryString["innerplanId"]));
            }
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("ProjectAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("InnerFloorPlansLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

    }

    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewfloorplans.aspx");
    }

    private void BindProject()
    {
        #region Code for Project
        var objProject = (from obj in dataContext.Projects
                          join objDesgn in dataContext.CityMasters
                          on obj.cityid equals objDesgn.cityid
                          join objPlace in dataContext.PlaceMasters
                          on obj.placeid equals objPlace.placeid into defaultPlace
                          from c in defaultPlace.DefaultIfEmpty()
                          where obj.status.Trim() == "A"
                          orderby obj.projectid descending
                          select new
                          {
                              obj.projectid,
                              projectname = (c == null ? obj.title + ", " + objDesgn.cityname : obj.title + ", " + c.placename + ", " + objDesgn.cityname)

                          }).Distinct().ToList();
        ddlProject.DataTextField = "projectname";
        ddlProject.DataValueField = "projectid";
        ddlProject.DataSource = objProject;
        ddlProject.DataBind();
        ddlProject.Items.Insert(0, new ListItem("--Select--", "0"));
        #endregion
    }

    private void FillData(string id)
    {
        var objProject = dataContext.InnerFloorPlans.Single(doc => doc.id == Convert.ToInt32(id));
        ddlProject.SelectedValue = Convert.ToString(objProject.projectid);
        BindPlanList(Convert.ToInt32(objProject.projectid));
        ddlPlan.SelectedValue = Convert.ToString(objProject.planid);
        txtTitle.Text = objProject.title;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["innerplanId"])))
        {
            #region to check if same inner floor plan exists

            var objChk = (from obj in dataContext.InnerFloorPlans
                          where obj.projectid == Convert.ToInt32(ddlProject.SelectedValue) && obj.planid == Convert.ToInt32(ddlPlan.SelectedValue) && obj.title.ToLower() == txtTitle.Text.ToLower()
                          select obj).FirstOrDefault();

            #endregion
            if (objChk != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Inner Floor Plan already exists.');</script>");
                return;
            }
            else
            {
                #region add new record
                InnerFloorPlan objplan = new InnerFloorPlan();
                #region Save Image
                if (FileUpload1.HasFile)
                {
                    if (FileUpload1.PostedFile.ContentLength > 1048576)
                    {
                        //ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual Image size should not be more than 1MB');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Actual Image size should not be more than 1MB.');</script>", false);
                        return;
                    }
                    string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                    if (imgext.ToLower() == ".jpg" || imgext.ToLower() == ".gif" || imgext.ToLower() == ".png")
                    {
                        string fileName = Path.GetFileName(FileUpload1.FileName);
                        string fileNameEdited = random.ToString() + "_" + fileName;
                        float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
                        float height = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
                        if (width >= 28 && height >= 28)
                        {
                            FileUpload1.SaveAs(Server.MapPath("~/images/FloorPlans/") + fileNameEdited);
                            objplan.image = fileNameEdited;
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please upload image with minimum dimensions 28 x 28px');</script>");
                            return;
                        }
                    }
                    else
                    {
                        // ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif or .jpg or .png file');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload only .jpg,.gif,.png file.');</script>", false);
                        return;
                    }
                }
                else
                {
                    // ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload Image');</script>");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload Image');</script>", false);
                    return;
                }
                #endregion

                objplan.projectid = Convert.ToInt32(ddlProject.SelectedValue);
                objplan.planid = Convert.ToInt32(ddlPlan.SelectedValue);
                objplan.title = txtTitle.Text;
                objplan.status = "A";
                objplan.createddate = DateTime.Now;
                dataContext.InnerFloorPlans.InsertOnSubmit(objplan);
                dataContext.SubmitChanges();
                Response.Redirect("viewinnerfloorplans.aspx?save=1");
                #endregion

            }
        }
        else
        {
            int id = Convert.ToInt32(Session["innerplanId"]);
             #region to check if same inner floor plan exists

            var objChk = (from obj in dataContext.InnerFloorPlans
                          where obj.id != id && obj.projectid == Convert.ToInt32(ddlProject.SelectedValue) && obj.planid == Convert.ToInt32(ddlPlan.SelectedValue) && obj.title.ToLower() == txtTitle.Text.ToLower()
                          select obj).FirstOrDefault();

            #endregion
            if (objChk != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Inner Floor Plan already exists.');</script>");
                return;
            }
            else
            {
                #region Code For Updation
                var objplan = dataContext.InnerFloorPlans.Single(doc => doc.id == id);
                #region Update Image
                if (FileUpload1.HasFile)
                {
                    if (FileUpload1.PostedFile.ContentLength > 1048576)
                    {
                        //ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual Image size should not be more than 1MB');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Actual Image size should not be more than 1MB.');</script>", false);

                        return;
                    }
                    string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                    if (imgext.ToLower() == ".jpg" || imgext.ToLower() == ".gif" || imgext.ToLower() == ".png")
                    {
                        string fileName = Path.GetFileName(FileUpload1.FileName);
                        string fileNameEdited = random.ToString() + "_" + fileName;

                        float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
                        float height = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
                        if (width >= 28 && height >= 28)
                        {
                            FileUpload1.SaveAs(Server.MapPath("~/images/FloorPlans/") + fileNameEdited);
                            objplan.image = fileNameEdited;
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please upload icon with minimum dimensions 28 x 28px');</script>");
                            return;
                        }
                        try
                        {
                            if (ViewState["Image1"] != null && ViewState["Image1"].ToString() != "")
                                File.Delete(Server.MapPath(@"img/" + ViewState["Image1"].ToString()));
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    else
                    {
                        //  ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please Upload only .gif or .jpg or .png file');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "<script languange='javascript'>alert('Please upload only .jpg,.gif,.png file.');</script>", false);


                        return;
                    }
                }
                #endregion
                objplan.projectid = Convert.ToInt32(ddlProject.SelectedValue);
                objplan.planid = Convert.ToInt32(ddlPlan.SelectedValue);
                objplan.title = txtTitle.Text;
                objplan.createddate = DateTime.Now;
                dataContext.SubmitChanges();
                Session["innerplanId"] = null;
                Response.Redirect("viewinnerfloorplans.aspx?saved=1");
                #endregion

            }
        }
        Div1.Style.Add("display", "block");
    }

    private void BindPlanList(int projectid)
    {
        #region Code for Services
        var objMenu = from obj in dataContext.FloorPlans
                      where obj.projectid == projectid && obj.status=="A"
                      select new { obj.title, obj.id };
        ddlPlan.DataTextField = "title";
        ddlPlan.DataValueField = "id";
        ddlPlan.DataSource = objMenu;
        ddlPlan.DataBind();
        ddlPlan.Items.Insert(0, new ListItem("--Select--", "0"));
        #endregion
    }

    protected void ddlProject_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlProject.SelectedValue != "0")
        {
            BindPlanList(Convert.ToInt32(ddlProject.SelectedValue));
        }
        else
        {
            ddlPlan.DataSource = null;
            ddlPlan.DataBind();
        }
    }
}
