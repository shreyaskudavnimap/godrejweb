﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;

public partial class Admin_Add_Room : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        if (IsPostBack) return;
        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["roomid"])))
        {
            FillData(Convert.ToString(Request.QueryString["roomid"]));
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("MasterAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("RoomTypeLi1");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");
    }

    private void FillData(string roomid)
    {
        var objroom = dataContext.Rooms.Single(doc => doc.RoomID == Convert.ToInt32(roomid));
        txtRoom.Text = objroom.RoomType;
        //txtPriority.Text = Convert.ToString(objMenu.Priority);
    }
    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewRooms.aspx");
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["roomid"])))
        {
            #region to check if same Room exists

            var objChk = (from obj in dataContext.Rooms
                          where (obj.RoomType).ToLower() == txtRoom.Text.ToLower()
                          select obj).FirstOrDefault();

            #endregion
            if (objChk != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Room Type already exists.');</script>");
                return;
            }
            else
            {
                #region For Adding the Record

                Room objRoom = new Room();
                objRoom.RoomType = txtRoom.Text.Trim();
                objRoom.Status = "A";
                objRoom.CreatedDate = DateTime.Now;
                dataContext.Rooms.InsertOnSubmit(objRoom);
                dataContext.SubmitChanges();
                Response.Redirect("viewRooms.aspx?save=1");

                #endregion
            }
        }
        else
        {
            #region Code For Updation
            int id = Convert.ToInt32(Session["roomid"]);
            var objroom = dataContext.Rooms.Single(doc => doc.RoomID == id);
            objroom.RoomType = txtRoom.Text.Trim();
            dataContext.SubmitChanges();
            Session["roomid"] = null;
            Response.Redirect("viewRooms.aspx?saved=1");
            #endregion
        }
        Div1.Style.Add("display", "block");
    }
}