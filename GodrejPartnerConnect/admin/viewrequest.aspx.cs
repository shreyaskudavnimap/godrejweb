﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.IO;

public partial class Admin_viewrequest : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        if (Convert.ToString(Session["type"]) == "superadmin")
            BindGridAll("");
        else
            BindGrid("");
        if (Convert.ToString(Session["type"]) == "admin")
        {
            HtmlControl User = (HtmlControl)Menu.FindControl("UserLi");
            User.Visible = false;
            HtmlControl Launch = (HtmlControl)Menu.FindControl("BannerLi");
            Launch.Visible = false;
            HtmlControl Master = (HtmlControl)Menu.FindControl("MasterLi");
            Master.Visible = false;
            HtmlControl Projects = (HtmlControl)Menu.FindControl("ProjectsLi");
            Projects.Visible = false;
            HtmlControl Enquiry = (HtmlControl)Menu.FindControl("EnquiryLi");
            Enquiry.Visible = false;
            HtmlControl Events = (HtmlControl)Menu.FindControl("EventsLi");
            Events.Visible = false;
            HtmlControl News = (HtmlControl)Menu.FindControl("NewsLi");
            News.Visible = false;
            HtmlControl Offers = (HtmlControl)Menu.FindControl("OffersLi");
            Offers.Visible = false;
            HtmlControl Property = (HtmlControl)Menu.FindControl("PropertyLi");
            Property.Visible = false;
            HtmlControl SubAdmin = (HtmlControl)Menu.FindControl("SubAdminLi");
            SubAdmin.Visible = false;
            HtmlControl DataAnchor = (HtmlControl)Menu.FindControl("DataDetailsLi");
            DataAnchor.Visible = false;
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("ApptAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("ApptLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

        if (Convert.ToString(Request.QueryString["saved"]) == "1")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record updated successfully.');</script>");
        }
        if (Convert.ToString(Request.QueryString["save"]) == "1")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record added successfully.');</script>");
        }
        txtSearch.Attributes.Add("onKeyPress", "doClick('" + btnSubmit.ClientID + "',event)");
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (txtSearch.Text != "" && txtSearch.Text != "search by name")
        {
            BindGrid(txtSearch.Text);
        }
    }

    private void BindGrid(string searchtext)
    {
        var objAppt = from obj in dataContext.BoookAppointments
                      join prj in dataContext.Projects
                      on Convert.ToInt32(obj.location) equals prj.projectid
                      join ct in dataContext.CityMasters
                      on prj.cityid equals ct.cityid
                      join cty in dataContext.CityMasters
                      on obj.city equals cty.cityid
                      join empl in dataContext.tbl_EmpanelmentContacts
                      on obj.brokerid equals empl.BrokerID
                      join reg in dataContext.Regions
                      on empl.Region equals reg.Region1
                      orderby obj.appointmentID descending
                      where reg.Region1 == Convert.ToString(Session["region"])
                      select new
                      {
                          obj.appointmentID,
                          obj.name,
                          obj.mobile,
                          obj.email,
                          obj.company,
                          title = prj.title + "," + ct.cityname,
                          obj.appointmentDate,
                          cty.cityname,
                          obj.comments,
                          obj.approve
                      };

        if (searchtext != "" && searchtext != "search by name")
            objAppt = objAppt.Where(d => d.name.Contains(searchtext));
      
        if (objAppt.Count() > 0)
        {
            lblMsg.Visible = false;
            lblMsg.Text = "";
            grdDetails.DataSource = objAppt;
            grdDetails.DataBind();
        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "<br/>No records found.";
            grdDetails.DataSource = objAppt;
            grdDetails.DataBind();
        }
    }

    private void BindGridAll(string searchtext)
    {
        var objAppt = from obj in dataContext.BoookAppointments
                      join prj in dataContext.Projects
                      on Convert.ToInt32(obj.location) equals prj.projectid
                      join ct in dataContext.CityMasters
                      on prj.cityid equals ct.cityid
                      join cty in dataContext.CityMasters
                      on obj.city equals cty.cityid
                      join empl in dataContext.tbl_EmpanelmentContacts
                      on obj.brokerid equals empl.BrokerID

                      orderby obj.appointmentID descending
                      select new
                      {
                          obj.appointmentID,
                          obj.name,
                          obj.mobile,
                          obj.email,
                          obj.company,
                          title=prj.title+","+ct.cityname,
                          obj.appointmentDate,
                          cty.cityname,
                          obj.comments,
                          approve=obj.approve == "W"? "Waiting For Approval": obj.approve 
                      };

        if (searchtext != "" && searchtext != "search by name")
            objAppt = objAppt.Where(d => d.name.Contains(searchtext));
      
        if (objAppt.Count() > 0)
        {
            lblMsg.Visible = false;
            lblMsg.Text = "";
            grdDetails.DataSource = objAppt;
            grdDetails.DataBind();
        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "<br/>No records found.";
            grdDetails.DataSource = objAppt;
            grdDetails.DataBind();
        }
    }

    protected void grdDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdDetails.PageIndex = e.NewPageIndex;
        if (Convert.ToString(Session["type"]) == "superadmin")
            BindGridAll(txtSearch.Text);
        else
            BindGrid(txtSearch.Text);
        //BindGrid(txtSearch.Text);
    }

    protected void grdDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Approve")
        {
            #region code for Activation
            int id = Convert.ToInt32(e.CommandArgument);
            var objProject = dataContext.BoookAppointments.Single(doc => doc.appointmentID == id);
            objProject.approve = "A";
            dataContext.SubmitChanges();

            var objAppt = (from obj in dataContext.BoookAppointments
                           join prj in dataContext.Projects
                           on Convert.ToInt32(obj.location) equals prj.projectid
                           join ct in dataContext.CityMasters
                           on prj.cityid equals ct.cityid
                           join cty in dataContext.CityMasters
                           on obj.city equals cty.cityid
                           join empl in dataContext.tbl_EmpanelmentContacts
                           on obj.brokerid equals empl.BrokerID
                           join reg in dataContext.Regions
                           on empl.Region equals reg.Region1
                           where obj.appointmentID == Convert.ToInt32(id)
                           select new
                           {
                               obj.name,
                               obj.email,
                               createdDate = Convert.ToDateTime(obj.createdDate).ToShortDateString(),
                               obj.appointmentDate,
                               title = prj.title + "," + ct.cityname,
                               cty.cityname,
                               reg.emailid
                           }).SingleOrDefault();

            SendMail(objAppt.name, objAppt.createdDate.ToString(),"confirmed",objAppt.title,objAppt.cityname,objAppt.email, objAppt.appointmentDate.ToString(), objAppt.emailid.ToString());
            ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Appointment Approved.');</script>");
            if (Convert.ToString(Session["type"]) == "superadmin")
                BindGridAll("");
            else
                BindGrid("");
            #endregion
        }
        if (e.CommandName == "Reject")
        {
            #region code for Activation
            int id = Convert.ToInt32(e.CommandArgument);
            var objProject = dataContext.BoookAppointments.Single(doc => doc.appointmentID == id);
            objProject.approve = "R";
            dataContext.SubmitChanges();

            var objAppt = (from obj in dataContext.BoookAppointments
                           join prj in dataContext.Projects
                           on Convert.ToInt32(obj.location) equals prj.projectid
                           join ct in dataContext.CityMasters
                           on prj.cityid equals ct.cityid
                           join cty in dataContext.CityMasters
                           on obj.city equals cty.cityid
                           join empl in dataContext.tbl_EmpanelmentContacts
                           on obj.brokerid equals empl.BrokerID
                           join reg in dataContext.Regions
                           on empl.Region equals reg.Region1
                           where obj.appointmentID == Convert.ToInt32(id)
                           select new
                           {
                               obj.name,
                               obj.email,
                               obj.createdDate,
                               obj.appointmentDate,
                               title = prj.title + "," + ct.cityname,
                               cty.cityname,
                               reg.emailid
                           }).SingleOrDefault();
            SendMail(objAppt.name, objAppt.createdDate.ToString(), "rejected", objAppt.title, objAppt.cityname, objAppt.email, objAppt.appointmentDate.ToString(), objAppt.emailid.ToString());
            ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Appointment Rejected.');</script>");
            if (Convert.ToString(Session["type"]) == "superadmin")
                BindGridAll("");
            else
                BindGrid("");
            #endregion
        }
    }

    //protected Boolean IsVisible(string approve )
    //{
    //    return approve != "Waiting For Approval";
    //}
    protected void grdDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[8].Text != "Waiting For Approval")
            {
                LinkButton lnkBtnApprove = (LinkButton)e.Row.FindControl("lnkBtnApprove");
                LinkButton lnkBtnReject = (LinkButton)e.Row.FindControl("lnkBtnReject");
                lnkBtnApprove.Visible = false;
                lnkBtnReject.Visible = false;
                e.Row.Cells[9].Text =null;

            }

        }
       
    }
    private void SendMail(string name, string reqdate,string status,string prj,string location, string toemail,string aptdate,string frmemail)
    {
        try
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.office365.com");
            mail.From = new MailAddress("marketing@godrejproperties.com");
            mail.To.Add(toemail);
            mail.Subject = "Book A Site Visit Status";
            mail.IsBodyHtml = true;
            string path = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
            StreamReader sr = new StreamReader(Server.MapPath("~/Mailer/book_a_site_visit_confirmatin.html"));
            mail.Body = sr.ReadToEnd().Replace("user_name", name).Replace("user_reqdate", reqdate).Replace("user_status", status).Replace("user_proj", prj).Replace("user_loc", location).Replace("user_date", aptdate);
            SmtpServer.Port = Convert.ToInt32(25);
            SmtpServer.Credentials = new System.Net.NetworkCredential("support_it@godrejproperties.com", "IT@outlook.com");
            SmtpServer.EnableSsl = true;
            SmtpServer.Send(mail);
            sr.Close();


            //MailMessage mail = new MailMessage();
            //mail.From = "marketing@godrejproperties.com";
            //mail.To = toemail;
            //mail.BodyFormat = MailFormat.Html;
            //mail.Subject = "Book A Site Visit Status";
            //StreamReader sr = new StreamReader(Server.MapPath("~/Mailer/book_a_site_visit_confirmatin.html"));
            //mail.Body = sr.ReadToEnd().Replace("user_name", name).Replace("user_reqdate", reqdate).Replace("user_status", status).Replace("user_proj", prj).Replace("user_loc", location).Replace("user_date", aptdate);
            ////mail.Subject = "Password Request for Partner Connect App";
            ////string BodyMessage = "<html><body><table><tr><td>Dear "+ name +", </td></tr><tr><td></td></tr><tr><td></td></tr><tr><td></td></tr><tr><td>" +
            ////    "This is with regards to your request for Password</td></tr><tr><td></td></tr><tr><td>" +
            ////    "Plase find below details for Login" +
            ////    "<tr><td>Username : "+ email +"</td></tr>" +
            ////    "<tr><td>Password : " + pwd + "</td></tr></table></body></html><html><body></body></html>";
            ////mail.Body = BodyMessage;
            //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
            //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "info@netbizlabs.com"); //set your username here
            //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "info321");	//set your password here
            ////StreamReader sr = new StreamReader(Server.MapPath(""));
            ////mail.Body = sr.ReadToEnd().Replace("Email", email);
            //SmtpMail.SmtpServer = "mail.netbiz.in";
            //SmtpMail.Send(mail);
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }
}
