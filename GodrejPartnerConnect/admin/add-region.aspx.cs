﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;

public partial class Admin_add_region : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["regionid"])))
            {
                FillData(Convert.ToString(Request.QueryString["regionId"]));
            }
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("MasterAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("RegionLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

    }

    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewregion.aspx");
    }

    private void FillData(string regionid)
    {
        var objRegion = dataContext.RegionInfos.Single(doc => doc.ID == Convert.ToInt32(regionid));
        txtTitle.Text = objRegion.Region;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["regionid"])))
        {
            #region to check if same region exists

            var objChk = (from obj in dataContext.RegionInfos
                          where (obj.Region).ToLower() == txtTitle.Text.ToLower()
                          select obj).FirstOrDefault();

            #endregion
            if (objChk != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Region already exists.');</script>");
                return;
            }
            else
            {
                #region For Adding the Record
                RegionInfo objRegion = new RegionInfo();

                objRegion.Region = txtTitle.Text.Trim();
                objRegion.createdon = DateTime.Now;
                objRegion.status = "A";
                dataContext.RegionInfos.InsertOnSubmit(objRegion);
                dataContext.SubmitChanges();
                Response.Redirect("viewregion.aspx?save=1");
                #endregion
            }
        }
        else
        {
            #region Code For Updation
            int id = Convert.ToInt32(Session["regionId"]);
            var objRegion = dataContext.RegionInfos.Single(doc => doc.ID == id);
            objRegion.Region = txtTitle.Text.Trim();
            dataContext.SubmitChanges();
            Session["regionId"] = null;
            Response.Redirect("viewregion.aspx?saved=1");
            #endregion
        }
        Div1.Style.Add("display", "block");
    }
}
