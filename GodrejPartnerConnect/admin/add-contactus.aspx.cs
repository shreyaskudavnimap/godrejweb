﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;
//using System.Transactions;

public partial class Admin_add_contactus : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    Guid random = Guid.NewGuid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");

        if (!IsPostBack)
        {
            BindProject();
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["contactId"])))
            {
                FillData(Convert.ToString(Request.QueryString["contactId"]));
            }
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("ProjectAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("ContactsLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

    }

    private void BindProject()
    {
        #region Code for Project
        var query = (from obj in dataContext.Projects
                     join cty in dataContext.CityMasters
                     on obj.cityid equals cty.cityid
                     join objPlace in dataContext.PlaceMasters
                     on obj.placeid equals objPlace.placeid into defaultPlace
                     from c in defaultPlace.DefaultIfEmpty()
                     where obj.status == "A"
                     select new
                     {
                         obj.projectid,
                         projectname = (c == null ? obj.title + ", " + cty.cityname : obj.title + ", " + c.placename + ", " + cty.cityname)
                     });

       // if (query.Count() > 0)
        ddlProject.DataTextField = "projectname";
        ddlProject.DataValueField = "projectid";
        ddlProject.DataSource = query;
        ddlProject.DataBind();
        ddlProject.Items.Insert(0, new ListItem("--Select--", "0"));
        #endregion
    }
    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewcontactus.aspx");
    }

    private void FillData(string id)
    {
        var objProject = dataContext.ContactUs.Single(doc => doc.id == Convert.ToInt32(id));
        txtDescription.Text = objProject.description;
        ddlProject.SelectedValue = Convert.ToString(objProject.projectid); ;
       
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["contactId"])))
        {
            #region to check if same details exists

            var objChk = (from obj in dataContext.ContactUs
                          where obj.projectid == Convert.ToInt32(ddlProject.SelectedValue)
                          select obj).FirstOrDefault();  

            #endregion
            if (objChk != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Contact details already exists.');</script>");
                return;
            }
            else
            {
                #region add new record
                ContactUs objContact= new ContactUs();

                objContact.description = txtDescription.Text;
                objContact.projectid = Convert.ToInt32(ddlProject.SelectedValue);
                objContact.status = "A";
                objContact.createdon = DateTime.Now;
                dataContext.ContactUs.InsertOnSubmit(objContact);
                dataContext.SubmitChanges();
                Response.Redirect("viewcontactus.aspx?save=1");
                #endregion

            }
        }
        else
        {
            int id = Convert.ToInt32(Session["contactId"]);
             #region to check if same details exists

            var objChk = (from obj in dataContext.ContactUs
                          where obj.id != id && obj.projectid == Convert.ToInt32(ddlProject.SelectedValue)
                          select obj).FirstOrDefault();

            #endregion
            if (objChk != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Contact details already exists.');</script>");
                return;
            }
            else
            {
                #region Code For Updation
                var objcontact = dataContext.ContactUs.Single(doc => doc.id == id);

                objcontact.description = txtDescription.Text;
                objcontact.projectid = Convert.ToInt32(ddlProject.SelectedValue);
                dataContext.SubmitChanges();
                Session["contactId"] = null;
                Response.Redirect("viewcontactus.aspx?saved=1");
                #endregion

            }
        }
        Div1.Style.Add("display", "block");
    }
}
