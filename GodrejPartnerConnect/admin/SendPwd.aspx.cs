﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Web.Mail;
using System.IO;

public partial class Admin_SendPwd : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        BindGrid();

        //HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("PwdAnchor");
        //HtmlControl selectMenu = (HtmlControl)Menu.FindControl("PwdLi");
        //openAnchor.Attributes.Add("class", "open");
        //selectMenu.Attributes.Add("class", "current");

        if (Convert.ToString(Request.QueryString["saved"]) == "1")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record updated successfully.');</script>");
        }
        if (Convert.ToString(Request.QueryString["save"]) == "1")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record added successfully.');</script>");
        }

    }

    private void BindGrid()
    {
        var objUser = from obj in dataContext.tbl_EmpanelmentContacts
                      where obj.Request_Pwd=="Yes"
                      orderby obj.EmplContID descending
                      select new
                      {
                          obj.EmplContID,
                          obj.Name,
                          obj.Email,
                          obj.Mobile
                      };

        if (objUser.Count() > 0)
        {
            lblMsg.Visible = false;
            lblMsg.Text = "";
            grdDetails.DataSource = objUser;
            grdDetails.DataBind();
        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "<br/>No records found.";
            grdDetails.DataSource = objUser;
            grdDetails.DataBind();
        }
    }

    protected void grdDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdDetails.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void grdDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Send")
        {
            #region code for Sending Password
            int id = Convert.ToInt32(e.CommandArgument);
            var objPwd = dataContext.tbl_EmpanelmentContacts.Single(doc => doc.EmplContID == id);
            objPwd.Request_Pwd = "";
            dataContext.SubmitChanges();

            var objEmpl = (from obj in dataContext.tbl_EmpanelmentContacts
                      where obj.EmplContID == Convert.ToInt32(id)
                      select new
                      {
                          obj.Name,
                          obj.Email,
                          obj.password
                      }).SingleOrDefault();

            SendMail(objEmpl.Name, objEmpl.Email, objEmpl.password);
            #endregion
        }
    }

    protected void grdDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            
        }
    }

    private void SendMail(string name,string email,string pwd)
    {
        try
        {
            MailMessage mail = new MailMessage();
            mail.From = "info@godrejproperties.com";
            mail.To = email;
            mail.BodyFormat = MailFormat.Html;
            mail.Subject = "Forgot Password Mailer";
            StreamReader sr = new StreamReader(Server.MapPath("~/Mailer/forget_mail.html"));
            mail.Body = sr.ReadToEnd().Replace("user_name", name).Replace("user_pwd", pwd);
            //mail.Subject = "Password Request for Partner Connect App";
            //string BodyMessage = "<html><body><table><tr><td>Dear "+ name +", </td></tr><tr><td></td></tr><tr><td></td></tr><tr><td></td></tr><tr><td>" +
            //    "This is with regards to your request for Password</td></tr><tr><td></td></tr><tr><td>" +
            //    "Plase find below details for Login" +
            //    "<tr><td>Username : "+ email +"</td></tr>" +
            //    "<tr><td>Password : " + pwd + "</td></tr></table></body></html><html><body></body></html>";
            //mail.Body = BodyMessage;
            mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
            mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "info@netbizlabs.com"); //set your username here
            mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "info321");	//set your password here
            //StreamReader sr = new StreamReader(Server.MapPath(""));
            //mail.Body = sr.ReadToEnd().Replace("Email", email);
            SmtpMail.SmtpServer = "mail.netbiz.in";
            SmtpMail.Send(mail);
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }
}