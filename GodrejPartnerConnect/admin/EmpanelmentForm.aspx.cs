﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using System.Net.Mail;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using GPLPartnerConnect.Model.Empanel;
using System.Text.RegularExpressions;

public partial class Admin_EmpanelmentForm : System.Web.UI.Page
{
    string brokerId;
    JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
    GodejCPDataContext dataContext = new GodejCPDataContext();
    string ConStr = ConfigurationManager.ConnectionStrings["godrejCPConnectionString"].ConnectionString;
    static string baseURL = "http://localhost:1000/";
    string baseURLEOI = baseURL + "gplservice/";
    string baseAPI = baseURL + "webapi/";
    string liveURL = baseURL;
    public mEmpanelment bookData;

    public class Empanelment
    {
        public string Password
        { get; set; }

        public string Company_Name
        { get; set; }

        public string entity_type
        { get; set; }

        public string Name
        { get; set; }

        public string Mobile
        { get; set; }

        public string Email
        { get; set; }

        public string communication_address
        { get; set; }

        public string registered_address
        { get; set; }

        public string Pan_No
        { get; set; }

        public string pancerti
        { get; set; }


        public string BrokerID
        { get; set; }


        public string BrokerContactID
        { get; set; }

        public string status
        { get; set; }


        public string Billing_City
        { get; set; }


        public string Billing_Zip
        { get; set; }


        public string Billing_Street
        { get; set; }


        public string Billing_State
        { get; set; }


        public string Billing_Country
        { get; set; }


        public string Registered_City
        { get; set; }


        public string Registered_Zip
        { get; set; }


        public string Registered_Street
        { get; set; }

        public string Registered_State
        { get; set; }


        public string Registered_Country
        { get; set; }


        public string Communication_City
        { get; set; }


        public string Communication_Zip
        { get; set; }


        public string Communication_Street
        { get; set; }


        public string Communication_State
        { get; set; }


        public string Communication_Country
        { get; set; }
        public string BrokerAccountID
        { get; set; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        if (Convert.ToString(Session["type"]) == "admin")
        {
            HtmlControl User = (HtmlControl)Menu.FindControl("UserLi");
            User.Visible = false;
            HtmlControl Launch = (HtmlControl)Menu.FindControl("BannerLi");
            Launch.Visible = false;
            HtmlControl Master = (HtmlControl)Menu.FindControl("MasterLi");
            Master.Visible = false;
            HtmlControl Projects = (HtmlControl)Menu.FindControl("ProjectsLi");
            Projects.Visible = false;
            HtmlControl Enquiry = (HtmlControl)Menu.FindControl("EnquiryLi");
            Enquiry.Visible = false;
            HtmlControl Events = (HtmlControl)Menu.FindControl("EventsLi");
            Events.Visible = false;
            HtmlControl News = (HtmlControl)Menu.FindControl("NewsLi");
            News.Visible = false;
            HtmlControl Offers = (HtmlControl)Menu.FindControl("OffersLi");
            Offers.Visible = false;
            HtmlControl Property = (HtmlControl)Menu.FindControl("PropertyLi");
            Property.Visible = false;
            HtmlControl SubAdmin = (HtmlControl)Menu.FindControl("SubAdminLi");
            SubAdmin.Visible = false;
            //HtmlControl PwdAnchor = (HtmlControl)Menu.FindControl("PwdLi");
            //PwdAnchor.Visible = false;
            HtmlControl DataAnchor = (HtmlControl)Menu.FindControl("DataDetailsLi");
            DataAnchor.Visible = false;
        }


        if (!IsPostBack)
        {
            //AddReraPanel.Visible = false;
            BindReraGst();
            LoadDiv();
            FillCountry();
            if (Convert.ToString(Request.QueryString["saved"]) == "1")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record updated successfully.');</script>");
            }
            if (Convert.ToString(Request.QueryString["save"]) == "1")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record added successfully.');</script>");
            }
        }

        if (IsPostBack && panFileUpload.PostedFile != null)
        {
            if (panFileUpload.PostedFile.FileName.Length > 0)
            {
                string[] supportFormts = { ".png", ".jpg", ".jpeg", ".pdf"};
                string newFileName;

                if (supportFormts.Contains(System.IO.Path.GetExtension(panFileUpload.FileName).ToLower()))
                {
                    string subfolder = "data\\empanelment\\";
                    string extn = System.IO.Path.GetExtension(panFileUpload.FileName);
                    string path = System.AppDomain.CurrentDomain.BaseDirectory;
                    newFileName = "PAN_" + DateTime.Now.ToString("yyyyMMddHHmmss");

                    string fullPath = path + subfolder + newFileName + extn;
                    panFileUpload.SaveAs(fullPath);

                    input_PanCerti.Text = newFileName + extn;
                    aPan_No.NavigateUrl = "/data/empanelment/" + newFileName + extn;
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Invalid File Format. (Supported: .png, .jpg, .jpeg, .pdf)');</script>");
                    return;
                }

            }
        }

        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("EmplAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("EmplLi");
        //openAnchor.Attributes.Add("class", "open");
        //selectMenu.Attributes.Add("class", "current");


        txtSearch.Attributes.Add("onKeyPress", "doClick('" + btnSubmit.ClientID + "',event)");

        //SendMail("test", "test", "anaghasht@gmail.com", "test123");
    }

    public void BindReraGst()
    {
        DataTable reraDt = new DataTable();
        reraDt.Columns.AddRange(new DataColumn[8] {new DataColumn("ID"), new DataColumn("State"), new DataColumn("Rera_no"), new DataColumn("Rera_Certi"), new DataColumn("Status"), new DataColumn("EmplID"), new DataColumn("Comment"), new DataColumn("IsDeleted") });
        ViewState["ReraView"] = reraDt;
        gdvRera.DataSource = reraDt;
        gdvRera.DataBind();

        DataTable gstDt = new DataTable();
        gstDt.Columns.AddRange(new DataColumn[8] {new DataColumn("ID"), new DataColumn("State"), new DataColumn("Gst_no"), new DataColumn("Gst_Certi"), new DataColumn("Status"), new DataColumn("EmplID"), new DataColumn("Comment"), new DataColumn("IsDeleted") });
        ViewState["GstView"] = gstDt;
        gdvGst.DataSource = gstDt;
        gdvGst.DataBind();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

    }

    protected void editSubmit_Click(object sender, EventArgs e)
    {
        var empId = txtEmpId.Value;
        var newEmail = txtEmail.Text;
        if (newEmail != "" && newEmail != null)
        {
            int ResponseCode = 0;
            SqlConnection con = new SqlConnection(ConStr);
            try
            {
                SqlCommand cmd = new SqlCommand("usp_EditViewPanelment", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmpId", empId);
                cmd.Parameters.AddWithValue("@Email", newEmail);
                con.Open();

                cmd.ExecuteScalar();
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record updated successfully.');</script>");
                return;
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Error while submitting changes.');</script>");
                return;
            }
        }
        else
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter correct email.');</script>");
            return;
        }
    }


    private void LoadDiv()
    {
        divForm.Visible = false;
        divPan.Visible = true;
        divEdit.Visible = false;
        divSummary.Visible = false;

        inputTxt_CommunicationState.Visible = false;
        inputTxt_CommunicationCity.Visible = false;
        inputTxt_RegisteredState.Visible = false;
        inputTxt_RegisteredCity.Visible = false;
        inputTxt_BillingState.Visible = false;
        inputTxt_BillingCity.Visible = false;
    }
    
    protected void grdDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }

    protected void gdvRera_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            DataTable dt = (DataTable)ViewState["ReraView"];
            
            int index = Convert.ToInt32(e.CommandArgument);
            dt.Rows[index]["Status"] = "D";
            dt.Rows[index]["IsDeleted"] = 1;
            var rows = dt.AsEnumerable().Where(r => r.Field<string>("Status") != "D");
            if (rows.Any())
            {
                dt.DefaultView.Sort = "IsDeleted ASC";
                dt = dt.DefaultView.ToTable();
                var dtChanges = rows.CopyToDataTable();
                gdvRera.DataSource = dtChanges;
            }
            else
            {
                gdvRera.DataSource = null;
            }

            gdvRera.DataBind();
            ViewState["ReraView"] = dt;
        }
    }

    public void validateJunkChar(Object sender, EventArgs e)
    {
        if (input_BillingStreet.Text.Contains("–"))
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Invalid character (–) found');</script>");
            return;
        }
    }

    protected void gdvGst_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            DataTable dt = (DataTable)ViewState["GstView"];

            int index = Convert.ToInt32(e.CommandArgument);
            dt.Rows[index]["Status"] = "D";
            dt.Rows[index]["IsDeleted"] = 1;
            var rows = dt.AsEnumerable().Where(r => r.Field<string>("Status") != "D");
            if (rows.Any())
            {
                dt.DefaultView.Sort = "IsDeleted ASC";
                dt = dt.DefaultView.ToTable();
                var dtChanges = rows.CopyToDataTable();
                gdvGst.DataSource = dtChanges;
            }
            else
            {
                gdvGst.DataSource = null;
            }
            gdvGst.DataBind();
            ViewState["GstView"] = dt;
        }
    }

    protected void gdvRera_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    protected void gdvGst_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    public Empanelment GetCPProperties(int emplId)
    {
        Empanelment empObj = new Empanelment();
        SqlConnection con = new SqlConnection(ConStr);
        try
        {
            //string StoreProcedure = ConfigurationManager.AppSettings["GetCPData"].ToString();
            SqlCommand cmd = new SqlCommand("SPGetEmpanelmentDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@emplId", emplId);

            con.Open();

            SqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                empObj.Communication_Street = rdr["Communication_Street"].ToString();
                empObj.Communication_City = rdr["Communication_City"].ToString();
                empObj.Communication_State = rdr["Communication_State"].ToString();
                empObj.Communication_Zip = rdr["Communication_Zip"].ToString();
                empObj.Communication_Country = rdr["Communication_Country"].ToString();
                empObj.Registered_City = rdr["Registered_City"].ToString();
                empObj.Registered_Country = rdr["Registered_Country"].ToString();
                empObj.Registered_State = rdr["Registered_State"].ToString();
                empObj.Registered_Street = rdr["Registered_Street"].ToString();
                empObj.Registered_Zip = rdr["Registered_Zip"].ToString();
                empObj.Billing_City = rdr["Billing_City"].ToString();
                empObj.Billing_Country = rdr["Billing_Country"].ToString();
                empObj.Billing_State = rdr["Billing_State"].ToString();
                empObj.Billing_Street = rdr["Billing_Street"].ToString();
                empObj.Billing_Zip = rdr["Billing_Zip"].ToString();
                empObj.BrokerAccountID = rdr["BrokerAccountID"].ToString();
            }

        }
        catch (Exception ex)
        {

        }
        return empObj;
    }

    public int UpdateAccountContactId(int emplId, string brokerAccId, string brokerContId)
    {
        int ResponseCode = 0;
        SqlConnection con = new SqlConnection(ConStr);
        try
        {
            //string StoreProcedure = ConfigurationManager.AppSettings["UpdateBrokerAccConId"].ToString();
            SqlCommand cmd = new SqlCommand("UpdateBrokerAccConId", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@EmpanelmentID", emplId);
            cmd.Parameters.AddWithValue("@brokerAccId", brokerAccId);
            cmd.Parameters.AddWithValue("@brokerContId", brokerContId);
            con.Open();

            ResponseCode = (int)cmd.ExecuteScalar();
        }
        catch (Exception ex)
        {

        }
        return ResponseCode;
    }


    public int UpdateInsertTblEmpanelments(tbl_EmpanelmentContact empl)
    {
        int ResponseCode = 0;
        SqlConnection con = new SqlConnection(ConStr);
        try
        {
            //string StoreProcedure = ConfigurationManager.AppSettings["UpdateInsertEmpanelment"].ToString();
            string StoreProcedure = "SPUpdateInsertEmpanelment";
            SqlCommand cmd = new SqlCommand(StoreProcedure, con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@name", empl.Name);
            cmd.Parameters.AddWithValue("@mobile", empl.Mobile);
            cmd.Parameters.AddWithValue("@email", empl.Email);
            cmd.Parameters.AddWithValue("@panno", empl.Pan_No);
            cmd.Parameters.AddWithValue("@companyname", empl.Company_Name);
            cmd.Parameters.AddWithValue("@username", empl.username);
            cmd.Parameters.AddWithValue("@password", empl.password);
            cmd.Parameters.AddWithValue("@status", empl.Status);
            cmd.Parameters.AddWithValue("@EmpanelmentID", empl.EmpanelmentID);
            con.Open();

            ResponseCode = (int)cmd.ExecuteScalar();
        }
        catch (Exception ex)
        {

        }
        return ResponseCode;
    }

    protected void gdvRera_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
    protected void gdvRera_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
    }
    protected void gdvGst_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        BindGstGrid(Convert.ToInt32(lblEmplID.Text), e.NewPageIndex);
    }

    protected void gdvGst_RowDataBound(object sender, GridViewRowEventArgs e)
    {


    }

    protected void grdDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //LinkButton lnkPushSales = (LinkButton)e.Row.FindControl("lnkPushSales");
            Label lblStatus = (Label)e.Row.FindControl("lblStatus");
            Label lblStatusText = (Label)e.Row.FindControl("lblStatusText");
            switch (lblStatus.Text)
            {
                case "A":
                    lblStatusText.Text = "Approved";
                    break;
                case "R":
                    lblStatusText.Text = "Rejected";
                    break;
                case "P":
                    lblStatusText.Text = "Pending";
                    break;
                default:
                    lblStatusText.Text = "Pending";
                    break;
            }
        }
    }

    public class EmplReraGSTDocs
    {
        public string State { get; set; }
        public string Rera_no { get; set; }
        public string Rera_Certi { get; set; }
        public string EmplID { get; set; }
        public string Status { get; set; }
        public string Gst_no { get; set; }
        public string Gst_Certi { get; set; }

    }


    public void FillCountry()
    {
        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("http://cp.godrejproperties.com/gplservice/" + "CPService.svc/GetCountry");
        request.ContentType = "application/json; charset=utf-8";
        request.Method = "GET";
        String jsonResponse = String.Empty;
        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        {
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            jsonResponse = reader.ReadToEnd();
            reader.Close();
            dataStream.Close();
        }

        input_BillingCountry.Items.Clear();
        input_RegisteredCountry.Items.Clear();
        input_CommunicationCountry.Items.Clear();
        CountryDropDown countrylst = JsonConvert.DeserializeObject<CountryDropDown>(jsonResponse);
        input_BillingCountry.Items.Insert(0, new ListItem("SELECT COUNTRY", "SELECT COUNTRY"));
        input_BillingCountry.Items.Insert(1, new ListItem("India", "1"));
        input_RegisteredCountry.Items.Insert(0, new ListItem("SELECT COUNTRY", "SELECT COUNTRY"));
        input_RegisteredCountry.Items.Insert(1, new ListItem("India", "1"));
        input_CommunicationCountry.Items.Insert(0, new ListItem("SELECT COUNTRY", "SELECT COUNTRY"));
        input_CommunicationCountry.Items.Insert(1, new ListItem("India", "1"));
        input_BillingState.Items.Insert(0, new ListItem("SELECT STATE", "SELECT STATE"));
        input_BillingCity.Items.Insert(0, new ListItem("SELECT CITY", "SELECT CITY"));
        input_RegisteredState.Items.Insert(0, new ListItem("SELECT STATE", "SELECT STATE"));
        input_RegisteredCity.Items.Insert(0, new ListItem("SELECT CITY", "SELECT CITY"));
        input_CommunicationState.Items.Insert(0, new ListItem("SELECT STATE", "SELECT STATE"));
        input_CommunicationCity.Items.Insert(0, new ListItem("SELECT CITY", "SELECT CITY"));

        foreach (var item in countrylst.GetCountryjsonResult)
        {
            input_BillingCountry.Items.Add(new ListItem(item.CountryName, item.CountryId));
        }
        foreach (var item in countrylst.GetCountryjsonResult)
        {
            input_RegisteredCountry.Items.Add(new ListItem(item.CountryName, item.CountryId));
        }
        foreach (var item in countrylst.GetCountryjsonResult)
        {
            input_CommunicationCountry.Items.Add(new ListItem(item.CountryName, item.CountryId));
        }
    }

    protected void FillBillingState(object sender, EventArgs e)
    {
        _FillBillingState();
    }

    public void _FillBillingState()
    {
        string country = input_BillingCountry.SelectedValue.ToString();
        if (input_BillingCountry.SelectedItem.ToString() == "India")        // India
        {
            inputTxt_BillingState.Visible = false;
            input_BillingState.Visible = true;
            inputTxt_BillingCity.Visible = false;
            input_BillingCity.Visible = true;

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("http://cp.godrejproperties.com/gplservice/" + "CPService.svc/GetStateData/" + country);
            request.ContentType = "application/json; charset=utf-8";
            request.Method = "GET";
            String jsonResponse = String.Empty;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                jsonResponse = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
            }
            input_BillingState.Items.Clear();
            StateDropDown statelst = JsonConvert.DeserializeObject<StateDropDown>(jsonResponse);
            foreach (var item in statelst.GetStateDatajsonResult)
            {
                input_BillingState.Items.Add(new ListItem(item.StateName, item.StateId));
            }
        }
        else
        {
            inputTxt_BillingState.Visible = true;
            input_BillingState.Visible = false;
            inputTxt_BillingCity.Visible = true;
            input_BillingCity.Visible = false;
        }
    }

    protected void FillBillingCity(object sender, EventArgs e)
    {
        _FillBillingCity();
    }

    public void _FillBillingCity()
    {
        string state = input_BillingState.SelectedValue.ToString();
        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("http://cp.godrejproperties.com/gplservice/" + "CPService.svc/GetCity/" + state);
        request.ContentType = "application/json; charset=utf-8";
        request.Method = "GET";
        String jsonResponse = String.Empty;
        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        {
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            jsonResponse = reader.ReadToEnd();
            reader.Close();
            dataStream.Close();
        }
        input_BillingCity.Items.Clear();
        CityDropDown citylst = JsonConvert.DeserializeObject<CityDropDown>(jsonResponse);
        foreach (var item in citylst.GetCityjsonResult)
        {
            input_BillingCity.Items.Add(new ListItem(item.CityName, item.CityId));
        }
    }

    protected void FillRegisteredState(object sender, EventArgs e)
    {
        _FillRegisteredState();
    }

    protected void _FillRegisteredState()
    {
        string country = input_RegisteredCountry.SelectedValue.ToString();
        if (input_RegisteredCountry.SelectedItem.ToString() == "India")        // India
        {
            inputTxt_RegisteredState.Visible = false;
            input_RegisteredState.Visible = true;
            inputTxt_RegisteredCity.Visible = false;
            input_RegisteredCity.Visible = true;

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("http://cp.godrejproperties.com/gplservice/" + "CPService.svc/GetStateData/" + country);
            request.ContentType = "application/json; charset=utf-8";
            request.Method = "GET";
            String jsonResponse = String.Empty;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                jsonResponse = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
            }
            input_RegisteredState.Items.Clear();
            StateDropDown statelst = JsonConvert.DeserializeObject<StateDropDown>(jsonResponse);
            foreach (var item in statelst.GetStateDatajsonResult)
            {
                input_RegisteredState.Items.Add(new ListItem(item.StateName, item.StateId));
            }
        }
        else
        {
            inputTxt_RegisteredState.Visible = true;
            input_RegisteredState.Visible = false;
            inputTxt_RegisteredCity.Visible = true;
            input_RegisteredCity.Visible = false;
        }
    }

    protected void FillRegisteredCity(object sender, EventArgs e)
    {
        _FillRegisteredCity();
    }

    protected void _FillRegisteredCity()
    {
        string state = input_RegisteredState.SelectedValue.ToString();
        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("http://cp.godrejproperties.com/gplservice/" + "CPService.svc/GetCity/" + state);
        request.ContentType = "application/json; charset=utf-8";
        request.Method = "GET";
        String jsonResponse = String.Empty;
        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        {
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            jsonResponse = reader.ReadToEnd();
            reader.Close();
            dataStream.Close();
        }
        input_RegisteredCity.Items.Clear();
        CityDropDown citylst = JsonConvert.DeserializeObject<CityDropDown>(jsonResponse);
        foreach (var item in citylst.GetCityjsonResult)
        {
            input_RegisteredCity.Items.Add(new ListItem(item.CityName, item.CityId));
        }
    }

    protected void FillCommunicationState(object sender, EventArgs e)
    {
        _FillCommunicationState();
    }

    protected void _FillCommunicationState()
    {
        string country = input_CommunicationCountry.SelectedValue.ToString();
        if (input_CommunicationCountry.SelectedItem.ToString() == "India")        // India
        {
            inputTxt_CommunicationState.Visible = false;
            input_CommunicationState.Visible = true;
            inputTxt_CommunicationCity.Visible = false;
            input_CommunicationCity.Visible = true;

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("http://cp.godrejproperties.com/gplservice/" + "CPService.svc/GetStateData/" + country);
            request.ContentType = "application/json; charset=utf-8";
            request.Method = "GET";
            String jsonResponse = String.Empty;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                jsonResponse = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
            }
            input_CommunicationState.Items.Clear();
            StateDropDown statelst = JsonConvert.DeserializeObject<StateDropDown>(jsonResponse);
            foreach (var item in statelst.GetStateDatajsonResult)
            {
                input_CommunicationState.Items.Add(new ListItem(item.StateName, item.StateId));
            }
        }
        else
        {
            inputTxt_CommunicationState.Visible = true;
            input_CommunicationState.Visible = false;
            inputTxt_CommunicationCity.Visible = true;
            input_CommunicationCity.Visible = false;
        }
    }

    protected void FillCommunicationCity(object sender, EventArgs e)
    {
        _FillCommunicationCity();
    }

    protected void _FillCommunicationCity()
    {
        string state = input_CommunicationState.SelectedValue.ToString();
        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("http://cp.godrejproperties.com/gplservice/" + "CPService.svc/GetCity/" + state);
        request.ContentType = "application/json; charset=utf-8";
        request.Method = "GET";
        String jsonResponse = String.Empty;
        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        {
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            jsonResponse = reader.ReadToEnd();
            reader.Close();
            dataStream.Close();
        }
        input_CommunicationCity.Items.Clear();
        CityDropDown citylst = JsonConvert.DeserializeObject<CityDropDown>(jsonResponse);
        foreach (var item in citylst.GetCityjsonResult)
        {
            input_CommunicationCity.Items.Add(new ListItem(item.CityName, item.CityId));
        }
    }

    protected void UpdateRegisCommAddress(object sender, EventArgs e)
    {
        if (IsSameRegisCommAddress.Checked == true)
        {
            input_CommunicationStreet.Text = String.Empty;
            input_CommunicationZip.Text = String.Empty;
            inputTxt_CommunicationCity.Text = String.Empty;
            inputTxt_CommunicationState.Text = String.Empty;

            input_CommunicationStreet.Enabled = false;
            input_CommunicationCountry.Enabled = false;
            input_CommunicationCity.Enabled = false;
            input_CommunicationState.Enabled = false;
            input_CommunicationZip.Enabled = false;
            inputTxt_CommunicationCity.Enabled = false;
            inputTxt_CommunicationState.Enabled = false;
        }
        else
        {
            //input_CommunicationStreet.Text = String.Empty;
            //input_CommunicationZip.Text = String.Empty;
            //inputTxt_CommunicationCity.Text = String.Empty;
            //inputTxt_CommunicationState.Text = String.Empty;

            input_CommunicationStreet.Enabled = true;
            input_CommunicationCountry.Enabled = true;
            input_CommunicationCity.Enabled = true;
            input_CommunicationState.Enabled = true;
            input_CommunicationZip.Enabled = true;
            inputTxt_CommunicationCity.Enabled = true;
            inputTxt_CommunicationState.Enabled = true;
        }

    }

    private string convertStatus(string searchtext)
    {
        if (searchtext == "Approved")
        {
            return "A";
        }
        else if (searchtext == "Rejected")
        {
            return "R";
        }
        else if (searchtext == "Pending")
        {
            return "P";
        }
        else
        {
            return searchtext;
        }
    }

    protected void btnRegister_Click(object sender, EventArgs e)
    {
        UserInputEmpanelment userInput = new UserInputEmpanelment();
        userInput.Company_Name = input_CompanyName.Text.Trim();
        userInput.entity_type = input_Entitytype.Text.Trim();
        userInput.FirstName = input_FirstName.Text.Trim();
        userInput.LastName = input_LastName.Text.Trim();
        userInput.Mobile = input_Mobile.Text.Trim();
        userInput.Email = input_Email.Text.Trim();
        userInput.Pan_No = Session["panno"].ToString().Trim();
        userInput.pancerti = input_PanCerti.Text.Trim();
        userInput.Billing_Street = input_BillingStreet.Text.Trim();
        userInput.Billing_Zip = input_BillingZip.Text.Trim();

        if (input_BillingCountry.SelectedItem.Text.ToUpper() == "INDIA")
        {
            userInput.Billing_State = input_BillingState.SelectedItem.Text;
            userInput.Billing_Country = input_BillingCountry.SelectedItem.Text;
            userInput.Billing_City = input_BillingCity.SelectedItem.Text;
        }
        else
        {
            userInput.Billing_State = inputTxt_BillingState.Text.Trim();
            userInput.Billing_Country = input_BillingCountry.SelectedItem.Text;
            userInput.Billing_City = inputTxt_BillingCity.Text.Trim();
        }

        userInput.Registered_Street = input_RegisteredStreet.Text.Trim();
        userInput.Registered_Zip = input_RegisteredZip.Text.Trim();

        if (input_RegisteredCountry.SelectedItem.Text.ToUpper() == "INDIA")
        {
            userInput.Registered_State = input_RegisteredState.SelectedItem.Text;
            userInput.Registered_Country = input_RegisteredCountry.SelectedItem.Text;
            userInput.Registered_City = input_RegisteredCity.SelectedItem.Text;
        }
        else
        {
            userInput.Registered_State = inputTxt_RegisteredState.Text.Trim();
            userInput.Registered_Country = input_RegisteredCountry.SelectedItem.Text;
            userInput.Registered_City = inputTxt_RegisteredCity.Text.Trim();
        }

        if (IsSameRegisCommAddress.Checked == false)
        {
            if (input_CommunicationCountry.SelectedItem.Text.ToUpper() == "INDIA")
            {
                userInput.Communication_Country = input_CommunicationCountry.SelectedItem.Text;
                userInput.Communication_City = input_CommunicationCity.SelectedItem.Text;
                userInput.Communication_State = input_CommunicationState.SelectedItem.Text;
            }
            else
            {
                userInput.Communication_State = inputTxt_CommunicationState.Text.Trim();
                userInput.Communication_Country = input_CommunicationCountry.SelectedItem.Text;
                userInput.Communication_City = inputTxt_CommunicationCity.Text.Trim();
            }
            userInput.Communication_Street = input_CommunicationStreet.Text.Trim();
            userInput.Communication_Zip = input_CommunicationZip.Text.Trim();
        }
        else
        {
            if (input_RegisteredCountry.SelectedItem.Text.ToUpper() == "INDIA")
            {
                userInput.Communication_Country = input_RegisteredCountry.SelectedItem.Text;
                userInput.Communication_City = input_RegisteredCity.SelectedItem.Text;
                userInput.Communication_State = input_RegisteredState.SelectedItem.Text;
            }
            else
            {
                userInput.Communication_State = inputTxt_RegisteredState.Text.Trim();
                userInput.Communication_Country = input_RegisteredCountry.SelectedItem.Text;
                userInput.Communication_City = inputTxt_RegisteredCity.Text.Trim();
            }
            userInput.Communication_Street = input_RegisteredStreet.Text.Trim();
            userInput.Communication_Zip = input_RegisteredZip.Text.Trim();
        }


        

        DataTable reraDt = (DataTable)ViewState["ReraView"];
        if (reraDt.Rows.Count != 0 && reraDt != null)
        {
            List<mReraDocs> reraList = reraDt.AsEnumerable().Select(row =>
                                                            new mReraDocs
                                                            {
                                                                id = row.Field<string>("ID"),
                                                                state = row.Field<string>("State"),
                                                                rerano = row.Field<string>("Rera_no").ToString().Trim(),
                                                                reracerti = row.Field<string>("Rera_Certi"),
                                                                status = convertStatus(row.Field<string>("Status"))
                                                            }).ToList();
            userInput.ReraDoc = reraList;
        }

        DataTable gstDt = (DataTable)ViewState["GstView"];
        if (gstDt.Rows.Count != 0 && gstDt != null)
        {

            List<mGstDocs> gstList = gstDt.AsEnumerable().Select(row =>
                                                        new mGstDocs
                                                        {
                                                            id = row.Field<string>("ID"),
                                                            state = row.Field<string>("State"),
                                                            gstno = row.Field<string>("Gst_no").ToString().Trim(),
                                                            gstcerti = row.Field<string>("Gst_Certi"),
                                                            status = convertStatus(row.Field<string>("Status"))
                                                        }).ToList();
            userInput.GstDoc = gstList;
        }




        string result = "";
        UserInputEmpanelment bookData = new UserInputEmpanelment();//(UserInputEmpanelment)Session["bookData"];
        using (WebClient Client = new WebClient())
        {
            var parameters = new System.Collections.Specialized.NameValueCollection();
            parameters.Add("email", userInput.Email);
            parameters.Add("EmplID", Session["EmplID"] != null ? Session["EmplID"].ToString() : "");
            var response = Client.UploadValues(baseAPI + "GetEmplDetailsEmail.ashx", "POST", parameters);
            result = System.Text.Encoding.UTF8.GetString(response);
        }
        var result_mEmpl = JsonConvert.DeserializeObject<mEmpanelment>(result);

        var flag = true;
        if (result_mEmpl.EmplID != "0")
        {
            flag = false;
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Email id already exist');</script>");
            return;
        }
        if (flag)
        {

            using (WebClient Client = new WebClient())
            {
                var parameters = new System.Collections.Specialized.NameValueCollection();
                parameters.Add("panno", Base64Encode(Session["panno"].ToString()));
                var response = Client.UploadValues(baseAPI + "GetEmplDetailsPanNo.ashx", "POST", parameters);
                result = System.Text.Encoding.UTF8.GetString(response);
            }
            var result_EmplPan = JsonConvert.DeserializeObject<mEmpanelment>(result);
            flag = true;
            if (result_EmplPan.EmplID != "0")
            {
                if (result_EmplPan.status == "A")
                {
                    flag = false;
                    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Partner account is already created with this PAN number.Please Login!!');</script>");
                    return;
                }
            }
            if (flag)
            {
                userInput.EmplID = result_EmplPan.EmplID;
                ValidateThenSubmit(bookData, userInput);
                divSummary.Visible = true;
                divForm.Visible = false;
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Data submitted successfully');</script>");
                return;
            }
        }

    }

    public string alphanumeric_checkname(string e)
    {
        Regex rg = new Regex(@"^[a-zA-Z\s,]*$");
        if (!rg.IsMatch(e))
        {
            return "NO";
        }
        else
            return "YES";
    }

    private void ValidateThenSubmit(UserInputEmpanelment bookData, UserInputEmpanelment userInput)
    {
        bookData.EmplID = userInput.EmplID;
        bookData.communication_address = userInput.Communication_Street;
        bookData.registered_address = userInput.Registered_Street;
        bookData.Pan_No = userInput.Pan_No;
        bookData.pancerti = userInput.pancerti;

        bookData.ReraDoc = userInput.ReraDoc;
        if (userInput.GstDoc == null)
        {
            bookData.GstDoc = new List<mGstDocs>();
        }
        else
        {
            bookData.GstDoc = userInput.GstDoc;
        }



        bookData.Company_Name = userInput.Company_Name;
        if (bookData.Company_Name == null || bookData.Company_Name == "")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter company name.');</script>");
            return;
        }
        if (bookData.Company_Name.Length > 100)
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Company name should be maximum 100 alphabets.');</script>");
            return;
        }


        bookData.entity_type = userInput.entity_type;
        if (bookData.entity_type == "Type of entity")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please select type of entity.');</script>");
            return;
        }

        bookData.FirstName = userInput.FirstName;
        if (bookData.FirstName == null || bookData.FirstName == "" || bookData.FirstName.Length > 100 || bookData.FirstName.Length < 1)
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter first name.');</script>");
            return;
        }
        //var chkname = alphanumeric_checkname(bookData.FirstName);

        if (alphanumeric_checkname(bookData.FirstName) == "NO")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter valid first name.');</script>");
            return;
        }

        bookData.LastName = userInput.LastName;
        if (bookData.LastName == null || bookData.LastName == "" || bookData.LastName.Length > 100 || bookData.LastName.Length < 3)
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter last name.');</script>");
            return;
        }

        if (alphanumeric_checkname(bookData.LastName) == "NO")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter valid last name.');</script>");
            return;
        }

        bookData.Mobile = userInput.Mobile;

        if (bookData.Mobile == null || bookData.Mobile == "")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter mobile number.');</script>");
            return;
        }

        if (bookData.Mobile.Length < 10 || bookData.Mobile.Length > 10)
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Mobile number Should be 10 digits.');</script>");
            return;
        }

        //var numPattern = "/\d + (,\d{ 1,3})?/";

        //if (!numPattern.test(bookData.Mobile))
        //{
        //    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter numeric value.');</script>");
        //}

        //Regex regex = new Regex("\\d + (,\\d{ 1,3})?");
        //Match match = regex.Match(bookData.Mobile);
        //if (!match.Success)
        //{
        //    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter numeric value.');</script>");
        //}


        bookData.Email = userInput.Email.ToLower();
        if (bookData.Email == null || bookData.Email == "" || bookData.Email.Length > 100)
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter email ID.');</script>");
            return;
        }
        //var filter = "/ ^\w + ([\.-] ?\w +)*@\w + ([\.-] ?\w +)*(\.\w{ 2,3})+$/";

        //if (!filter.test(bookData.Email))
        //{
        //    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter valid email id.');</script>");
        //}

        Regex regex1 = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");
        Match match1 = regex1.Match(bookData.Email);
        if (!match1.Success)
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter valid email id.');</script>");
            return;
        }



        //Billing Data
        bookData.Billing_Street = userInput.Billing_Street;
        bookData.Billing_Country = userInput.Billing_Country;
        bookData.Billing_State = userInput.Billing_State;
        bookData.Billing_City = userInput.Billing_City;
        //if (bookData.Billing_Country.ToUpper() == "INDIA")
        //{
        //    //bookData.Billing_State = $("#billing_state").find(":selected").text();
        //    //bookData.Billing_City = $("#billing_city").find(":selected").text();
        //}
        //else
        //{
        //    bookData.Billing_State = userInput.Billing_State;
        //    bookData.Billing_City = userInput.Billing_City;
        //}

        if (bookData.Billing_Street == null || bookData.Billing_Street == "")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter billing street.');</script>");
            return;
        }
        if (bookData.Billing_Country == "SELECT COUNTRY")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please select billing country.');</script>");
            return;
        }
        if (bookData.Billing_Country.ToUpper() == "INDIA")
        {
            if (bookData.Billing_State == "SELECT STATE")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please select billing state.');</script>");
                return;
            }
            if (bookData.Billing_City == "SELECT CITY")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please select billing city.');</script>");
                return;
            }
        }
        else
        {
            if (bookData.Billing_State == null || bookData.Billing_State == "")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter billing state.');</script>");
                return;
            }
            if (bookData.Billing_City == null || bookData.Billing_City == "")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter billing city.');</script>");
                return;
            }
        }

        bookData.Billing_Zip = userInput.Billing_Zip;
        if (bookData.Billing_Zip == null || bookData.Billing_Zip == "")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter billing zip code.');</script>");
            return;
        }


        //Communication data
        bookData.Communication_Street = userInput.Communication_Street;
        bookData.Communication_Country = userInput.Communication_Country;
        bookData.Communication_State = userInput.Communication_State;
        bookData.Communication_City = userInput.Communication_City;

        //if (bookData.Communication_Country.ToUpper() == "INDIA")
        //{
        //    //bookData.Communication_State = $("#communication_state").find(":selected").text();
        //    //bookData.Communication_City = $("#communication_city").find(":selected").text();
        //}
        //else
        //{
        //    bookData.Communication_State = userInput.Communication_State;
        //    bookData.Communication_City = userInput.Communication_City;
        //}

        if (bookData.Communication_Street == null || bookData.Communication_Street == "")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter communication street.');</script>");
            return;
        }
        if (bookData.Communication_Country == "SELECT COUNTRY")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please select communication country.');</script>");
            return;
        }
        if (bookData.Communication_Country.ToUpper() == "INDIA")
        {
            if (bookData.Communication_State == "SELECT STATE")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please select communication state.');</script>");
                return;
            }
            if (bookData.Communication_City == "SELECT CITY")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please select communication city.');</script>");
                return;
            }
        }
        else
        {
            if (bookData.Communication_State == null || bookData.Communication_State == "")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter communication state.');</script>");
                return;
            }
            if (bookData.Communication_City == null || bookData.Communication_City == "")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter communication city.');</script>");
                return;
            }
        }
        bookData.Communication_Zip = userInput.Communication_Zip;
        if (bookData.Communication_Zip == null || bookData.Communication_Zip == "")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter communication zip code.');</script>");
            return;
        }

        //Registered Data
        bookData.Registered_Street = userInput.Registered_Street;
        bookData.Registered_Country = userInput.Registered_Country;
        bookData.Registered_State = userInput.Registered_State;
        bookData.Registered_City = userInput.Registered_City;
        //bookData.Registered_Country = $("#registered_country").find(":selected").text();
        //if (bookData.Registered_Country.ToUpper() == "INDIA")
        //{
        //    bookData.Registered_State = $("#registered_state").find(":selected").text();
        //    bookData.Registered_City = $("#registered_city").find(":selected").text();
        //}
        //else
        //{
        //    bookData.Registered_State = userInput.Registered_State;
        //    bookData.Registered_City = userInput.Registered_City;
        //}


        if (bookData.Registered_Street == null || bookData.Registered_Street == "")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter registered street.');</script>");
            return;
        }
        if (bookData.Registered_Country == "SELECT COUNTRY")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please select registered country.');</script>");
            return;
        }
        if (bookData.Registered_Country.ToUpper() == "INDIA")
        {
            if (bookData.Registered_State == "SELECT STATE")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please select registered state.');</script>");
                return;
            }
            if (bookData.Registered_City == "SELECT CITY")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please select registered city.');</script>");
                return;
            }
        }
        else
        {
            if (bookData.Registered_State == null || bookData.Registered_State == "")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('');</script>");
                return;
            }
            if (bookData.Registered_City == null || bookData.Registered_City == "")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please select registered city.');</script>");
                return;
            }

        }

        bookData.Registered_Zip = userInput.Registered_Zip;
        if (bookData.Registered_Zip == null || bookData.Registered_Zip == "")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter registered zip code.');</script>");
            return;
        }


        Regex xd1 = new Regex(@"^\d+$");
        Match mb2 = xd1.Match(bookData.Billing_Zip);
        if (!mb2.Success)
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter valid Billing ZipCode.');</script>");
            return;
        }
        Match mb1 = xd1.Match(bookData.Registered_Zip);
        if (!mb1.Success)
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter valid Registered ZipCode.');</script>");
            return;
        }
        Match mb3 = xd1.Match(bookData.Communication_Zip);
        if (!mb3.Success)
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter valid Communication ZipCode.');</script>");
            return;
        }


        if (bookData.Pan_No == null || bookData.Pan_No == "" || bookData.Pan_No.Length > 50)
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter PAN No.');</script>");
            return;
        }


        //if (!bookData.Pan_No.match(/[A - Z]{ 5}[0-9]{4}[A-Z]{1}/)) 
        //{
        //    showMsg('Please enter valid PAN No.');
        //}

        bookData.Pan_No = userInput.Pan_No;
        Regex regex2 = new Regex("[A-Z]{5}[0-9]{4}[A-Z]{1}");
        Match match2 = regex2.Match(bookData.Pan_No);
        if (!match2.Success)
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter valid PAN No.');</script>");
            return;
        }


        if (bookData.pancerti == null || bookData.pancerti == "" || bookData.pancerti == "No file selected")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please upload pan certificate.');</script>");
            return;
        }

        if (bookData.ReraDoc == null || bookData.ReraDoc.Count == 0)
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter RERA details.');</script>");
            return;
        }

        bookData.RequestType = "TCC";
        bookData.SubmittedByUser = Convert.ToString(Session["User"]);

        string result = String.Empty;
        var jsonData = new JavaScriptSerializer().Serialize(bookData);
        using (WebClient Client = new WebClient())
        {
            var parameters = new System.Collections.Specialized.NameValueCollection();
            parameters.Add("empl", jsonData);
            var response = Client.UploadValues(baseAPI + "InsertEmpanelment.ashx", "POST", parameters);
            result = System.Text.Encoding.UTF8.GetString(response);
        }
        var result_mEmpl = JsonConvert.DeserializeObject<mEmpanelment>(result);
    }

    public static string Base64Encode(string plainText)
    {
        var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
        return System.Convert.ToBase64String(plainTextBytes);
    }

    protected void btnPanSubmit_Click(object sender, EventArgs e)
    {
        string URI = baseAPI + "GetEmplDetailsPanNo.ashx";
        string result = "";
        string panno = txt_PanNo.Text.ToString().ToUpper();
        Session["panno"] = panno;

        if (panno == null || panno == "" || panno.Length < 1)
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter PAN No.');</script>");
            return;
        }
        if (panno.Length != 10)
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter PAN No.');</script>");
            return;
        }

        Regex regex = new Regex("[A-Z]{5}[0-9]{4}[A-Z]{1}");
        Match match = regex.Match(panno);
        if (!match.Success)
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Invalid PAN');</script>");
            return;
        }

        using (WebClient Client = new WebClient())
        {
            var parameters = new System.Collections.Specialized.NameValueCollection();
            parameters.Add("panno", Base64Encode(panno));
            var response = Client.UploadValues(URI, "POST", parameters);
            result = System.Text.Encoding.UTF8.GetString(response);
        }

        bookData = JsonConvert.DeserializeObject<mEmpanelment>(result);
        Session["bookData"] = bookData;

        if (bookData.EmplID == "0")
        {
            divPan.Visible = false;
            divForm.Visible = true;
        }
        else
        {
            if (bookData.status == "A")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Partner account is already created with this PAN number.Please Login!!');</script>");
                return;
            }
            else if (bookData.status == "R")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Your empanelment request has been rejected. Please refer comments and resubmit');</script>");
                Session["EmplID"] = bookData.EmplID.ToString();
                divForm.Visible = true;
                divPan.Visible = false;
                FillForm();
            }
            else if (bookData.status == "P")
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Empanelment request in progress. Although you edit your empanelment request and resubmit');</script>");
                Session["EmplID"] = bookData.EmplID.ToString();
                divForm.Visible = true;
                divPan.Visible = false;
                FillForm();
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Empanelment request in progress. Please try after some time!!');</script>");
                return;
            }
        }
    }

    private void FillForm()
    {
        string Pan_no = txt_PanNo.Text;
        DataTable dt = new DataTable();
        SqlConnection con = new SqlConnection(ConStr);
        try
        {
            SqlCommand cmd = new SqlCommand("usp_EmplCallCenter", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Pan_No", Pan_no);
            cmd.Parameters.AddWithValue("@flag", "GET_PN");
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            con.Close();
            da.Dispose();
        }
        catch (Exception ex)
        {

        }

        int EmplId = Convert.ToInt32(dt.Rows[0]["EmplID"]);
        ViewEmpl(EmplId);
        BindGstGrid(EmplId, 0);
        BindReraGrid(EmplId, 0);
    }

    private void BindGstGrid(int EmplID, int pageIndex)
    {
        var objGstEmpl = dataContext.Empl_GstDocs.Select(obj => new {
                                                                    obj.ID,
                                                                    obj.State,
                                                                    obj.Gst_no,
                                                                    obj.Gst_Certi,
                                                                    obj.Status,
                                                                    obj.EmplID,
                                                                    obj.Comment,
                                                                    IsDeleted = 0
                                                                    }).OrderBy(s => s.Status)
                                                                    .Where(z => z.EmplID == Convert.ToInt32(EmplID) && z.Status != "Deleted")
                                                                    .ToList();

        if (objGstEmpl.Count() > 0)
        {
            DataTable gstDt = ToDataTable(objGstEmpl);
            foreach (DataRow row in gstDt.Rows)
            {
                row["Status"] = convertStatus(row["Status"].ToString());
            }
            ViewState["GstView"] = gstDt;
            gdvGst.DataSource = gstDt;
            gdvGst.PageIndex = pageIndex;
            gdvGst.DataBind();
        }
        else
        {
            gdvGst.DataSource = objGstEmpl;
            gdvGst.DataBind();
        }
    }

    private void BindReraGrid(int EmplID, int pageIndex)
    {
        var objReraEmpl = dataContext.Empl_ReraDocs.Select(obj => new {
                                                                    obj.ID,
                                                                    obj.State,
                                                                    obj.Rera_no,
                                                                    obj.Rera_Certi,
                                                                    obj.Status,
                                                                    obj.EmplID,
                                                                    obj.Comment,
                                                                    IsDeleted = 0
                                                                    }).OrderBy(s => s.Status)
                                                                    .Where(z => z.EmplID == Convert.ToInt32(EmplID) && z.Status != "Deleted")
                                                                    .ToList();

        if (objReraEmpl.Count() > 0)
        {
            DataTable reraDt = ToDataTable(objReraEmpl);
            foreach (DataRow row in reraDt.Rows)
            {
                row["Status"] = convertStatus(row["Status"].ToString());
            }
            ViewState["ReraView"] = reraDt;
            gdvRera.DataSource = objReraEmpl;
            gdvRera.PageIndex = pageIndex;
            gdvRera.DataBind();
        }
        else
        {
            gdvRera.DataSource = objReraEmpl;
            gdvRera.DataBind();
        }
    }

    public static DataTable ToDataTable<T>(List<T> items)
    {
        DataTable dataTable = new DataTable(typeof(T).Name);

        //Get all the properties
        System.Reflection.PropertyInfo[] Props = typeof(T).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
        foreach (System.Reflection.PropertyInfo prop in Props)
        {
            //Setting column names as Property names
            dataTable.Columns.Add(prop.Name);
        }
        foreach (T item in items)
        {
            var values = new object[Props.Length];
            for (int i = 0; i < Props.Length; i++)
            {
                //inserting property values to datatable rows
                values[i] = Props[i].GetValue(item, null);
            }
            dataTable.Rows.Add(values);
        }
        //put a breakpoint here and check datatable
        return dataTable;
    }

    public void ViewEmpl(int EmplID)
    {
        divForm.Visible = true;
        divPan.Visible = false;

        tbl_Empanelment Empldetails = dataContext.tbl_Empanelments.Single(doc => doc.EmplID == EmplID);
        input_CompanyName.Text = Empldetails.Company_Name.ToString();
        input_Entitytype.Text = Empldetails.EntityType.ToString();
        input_FirstName.Text = Empldetails.FirstName.ToString();
        input_LastName.Text = Empldetails.LastName.ToString();
        input_Mobile.Text = Empldetails.Mobile.ToString();
        input_Email.Text = Empldetails.Email.ToString();
        txt_PanNo.Text = Empldetails.Pan_No.ToString();
        aPan_No.NavigateUrl = "/data/empanelment/" + Empldetails.Pan_Certi.ToString();
        input_PanCerti.Text = Empldetails.Pan_Certi.ToString();

        FillCountry();
        
        if (Empldetails.Billing_Country.ToString() == "India")
        {
            inputTxt_BillingCity.Visible = false;
            inputTxt_BillingState.Visible = false;
            input_BillingState.Visible = true;
            input_BillingCity.Visible = true;
            input_BillingCountry.SelectedValue = "1";
            input_BillingCountry.SelectedItem.Text = "India";
            _FillBillingState();
            _FillBillingCity();
            input_BillingState.SelectedItem.Text = Empldetails.Billing_State.ToString();
            input_BillingCity.SelectedItem.Text = Empldetails.Billing_City.ToString();
        }
        else
        {
            input_BillingCountry.SelectedItem.Text = Empldetails.Billing_Country;
            inputTxt_BillingCity.Visible = true;
            inputTxt_BillingState.Visible = true;
            input_BillingState.Visible = false;
            input_BillingCity.Visible = false;
            inputTxt_BillingState.Text = Empldetails.Billing_State.ToString();
            inputTxt_BillingCity.Text = Empldetails.Billing_City.ToString();
            
        }

        if (Empldetails.Registered_Country.ToString() == "India")
        {
            inputTxt_RegisteredState.Visible = false;
            inputTxt_RegisteredCity.Visible = false;
            input_RegisteredState.Visible = true;
            input_RegisteredCity.Visible = true;
            input_RegisteredCountry.SelectedValue = "1";
            input_RegisteredCountry.SelectedItem.Text = "India";
            _FillRegisteredState();
            _FillRegisteredCity();
            input_RegisteredState.SelectedItem.Text = Empldetails.Registered_State.ToString();
            input_RegisteredCity.SelectedItem.Text = Empldetails.Registered_City.ToString();
        }
        else
        {
            input_RegisteredCountry.SelectedItem.Text = Empldetails.Registered_Country;
            inputTxt_RegisteredCity.Visible = true;
            inputTxt_RegisteredState.Visible = true;
            input_RegisteredState.Visible = false;
            input_RegisteredCity.Visible = false;
            inputTxt_RegisteredState.Text = Empldetails.Registered_State.ToString();
            inputTxt_RegisteredCity.Text = Empldetails.Registered_City.ToString();
        }

        if (Empldetails.Communication_Country.ToString() == Empldetails.Registered_Country.ToString()
            && Empldetails.Communication_State.ToString() == Empldetails.Registered_State.ToString()
            && Empldetails.Communication_City.ToString() == Empldetails.Registered_City.ToString()
            && Empldetails.Communication_Zip.ToString() == Empldetails.Registered_Zip.ToString()
            && Empldetails.Communication_Street.ToString() == Empldetails.Registered_Street.ToString())
        {
            IsSameRegisCommAddress.Checked = true;
        }
        

        if (Empldetails.Communication_Country.ToString() == "India")
        {
            inputTxt_CommunicationState.Visible = false;
            inputTxt_CommunicationCity.Visible = false;
            input_CommunicationState.Visible = true;
            input_CommunicationCity.Visible = true;
            input_CommunicationCountry.SelectedValue = "1";
            input_CommunicationCountry.SelectedItem.Text = "India";
            _FillCommunicationState();
            _FillCommunicationCity();
            input_CommunicationState.SelectedItem.Text = Empldetails.Communication_State.ToString();
            input_CommunicationCity.SelectedItem.Text = Empldetails.Communication_City.ToString();
        }
        else
        {
            input_CommunicationCountry.SelectedItem.Text = Empldetails.Communication_Country;
            inputTxt_CommunicationCity.Visible = true;
            inputTxt_CommunicationState.Visible = true;
            input_CommunicationState.Visible = false;
            input_CommunicationCity.Visible = false;
            inputTxt_CommunicationState.Text = Empldetails.Communication_State.ToString();
            inputTxt_CommunicationCity.Text = Empldetails.Communication_City.ToString();
        }


        input_BillingZip.Text = Empldetails.Billing_Zip.ToString();
        input_BillingStreet.Text = Empldetails.Billing_Street.ToString();
        
        input_RegisteredZip.Text = Empldetails.Registered_Zip.ToString();
        input_RegisteredStreet.Text = Empldetails.Registered_Street.ToString();
        
        input_CommunicationZip.Text = Empldetails.Communication_Zip.ToString();
        input_CommunicationStreet.Text = Empldetails.Communication_Street.ToString();


    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        divForm.Visible = false;
        divPan.Visible = true;
    }


    public class PanVerification
    {
        public string panno { get; set; }
    }

    //protected void ToggleAddRera(object sender, EventArgs e)
    //{
    //    AddReraPanel.Visible = !AddReraPanel.Visible;
    //    ReraStateDD.Items.Clear();
    //    ReraStateDD.Items.Add(new ListItem("SELECT STATE", "notselected"));
    //    ReraStateDD.Items.Add(new ListItem("Andhra Pradesh", "Andhra Pradesh"));
    //    ReraStateDD.Items.Add(new ListItem("Arunachal Pradesh", "Arunachal Pradesh"));
    //    ReraStateDD.Items.Add(new ListItem("Assam", "Assam"));
    //    ReraStateDD.Items.Add(new ListItem("Bihar", "Bihar"));
    //    ReraStateDD.Items.Add(new ListItem("Goa", "Goa"));
    //    ReraStateDD.Items.Add(new ListItem("Gujarat", "Gujarat"));
    //    ReraStateDD.Items.Add(new ListItem("Haryana", "Haryana"));
    //    ReraStateDD.Items.Add(new ListItem("Himachal Pradesh", "Himachal Pradesh"));
    //    ReraStateDD.Items.Add(new ListItem("Jammu and Kashmir", "Jammu and Kashmir"));
    //    ReraStateDD.Items.Add(new ListItem("Karnataka", "Karnataka"));
    //    ReraStateDD.Items.Add(new ListItem("Kerala", "Kerala"));
    //    ReraStateDD.Items.Add(new ListItem("Madhya Pradesh", "Madhya Pradesh"));
    //    ReraStateDD.Items.Add(new ListItem("Maharashtra", "Maharashtra"));
    //    ReraStateDD.Items.Add(new ListItem("Manipur", "Manipur"));
    //    ReraStateDD.Items.Add(new ListItem("Meghalaya", "Meghalaya"));
    //    ReraStateDD.Items.Add(new ListItem("Mizoram", "Mizoram"));
    //    ReraStateDD.Items.Add(new ListItem("Nagaland", "Nagaland"));
    //    ReraStateDD.Items.Add(new ListItem("Orissa", "Orissa"));
    //    ReraStateDD.Items.Add(new ListItem("Punjab", "Punjab"));
    //    ReraStateDD.Items.Add(new ListItem("Rajasthan", "Rajasthan"));
    //    ReraStateDD.Items.Add(new ListItem("Sikkim", "Sikkim"));
    //    ReraStateDD.Items.Add(new ListItem("Tamil Nadu", "Tamil Nadu"));
    //    ReraStateDD.Items.Add(new ListItem("Tripura", "Tripura"));
    //    ReraStateDD.Items.Add(new ListItem("Uttar Pradesh", "Uttar Pradesh"));
    //    ReraStateDD.Items.Add(new ListItem("West Bengal", "West Bengal"));
    //    ReraStateDD.Items.Add(new ListItem("Andaman and Nico.In.", "Andaman and Nico.In."));
    //    ReraStateDD.Items.Add(new ListItem("Chandigarh", "Chandigarh"));
    //    ReraStateDD.Items.Add(new ListItem("Dadra and Nagar Hav.", "Dadra and Nagar Hav."));
    //    ReraStateDD.Items.Add(new ListItem("Daman and Diu", "Daman and Diu"));
    //    ReraStateDD.Items.Add(new ListItem("Delhi", "Delhi"));
    //    ReraStateDD.Items.Add(new ListItem("Lakshadweep", "Lakshadweep"));
    //    ReraStateDD.Items.Add(new ListItem("Pondicherry", "Pondicherry"));
    //    ReraStateDD.Items.Add(new ListItem("Chhattisgarh", "Chhattisgarh"));
    //    ReraStateDD.Items.Add(new ListItem("Jharkhand", "Jharkhand"));
    //    ReraStateDD.Items.Add(new ListItem("Uttarakhand", "Uttarakhand"));
    //    ReraStateDD.Items.Add(new ListItem("Telangana", "Telangana"));

    //}

    protected void AddRera(object sender, EventArgs e)
    {
        DataTable dt = (DataTable)ViewState["ReraView"];

        if (ReraFileUpload.FileName == "")
        {
            ClearReraInputs();
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please select Rera certificate.');</script>");
            return;
        }

        //if (input_ReraState.Text.Trim() == "")
        //{
        //    ClearReraInputs();
        //    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please select state.');</script>");
        //    return;
        //}

        if (ReraStateDD.SelectedItem.Text == "SELECT STATE")
        {
            ClearReraInputs();
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please select RERA state.');</script>");
            return;
        }

        if (input_ReraNo.Text.Trim() == "")
        {
            ClearReraInputs();
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter Rera No.');</script>");
            return;
        }


        //bool duplCheckState = dt.AsEnumerable().Any(row => row.Field<String>("State") == input_ReraState.Text.Trim());
        //if (duplCheckState)
        //{
        //    ClearReraInputs();
        //    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('State already selected');</script>");
        //    return;
        //}

        bool duplCheckState = dt.AsEnumerable().Any(row => row.Field<String>("State") == ReraStateDD.SelectedValue && row.Field<String>("Status") != "D");
        if (duplCheckState)
        {
            ClearReraInputs();
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('State already selected');</script>");
            return;
        }

        bool duplCheckReraNo = dt.AsEnumerable().Any(row => row.Field<String>("Rera_no") == input_ReraNo.Text.Trim() && row.Field<String>("Status") != "D");
        if (duplCheckReraNo)
        {
            ClearReraInputs();
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Rera No already selected');</script>");
            return;
        }

        string[] supportFormts = { ".png", ".jpg", ".jpeg", ".pdf"};
        string newFileName, extn;

        if (supportFormts.Contains(System.IO.Path.GetExtension(ReraFileUpload.FileName).ToLower()))
        {
            string subfolder = "data\\empanelment\\";
            extn = System.IO.Path.GetExtension(ReraFileUpload.FileName);
            string path = System.AppDomain.CurrentDomain.BaseDirectory;
            newFileName = "RERA_" + DateTime.Now.ToString("yyyyMMddHHmmss");

            string fullPath = path + subfolder + newFileName + extn;
            ReraFileUpload.SaveAs(fullPath);
        }
        else
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Invalid File Format. (Supported: .png, .jpg, .jpeg, .pdf)');</script>");
            return;
        }
        //input_ReraState.Text.Trim()
        dt.Rows.Add("", ReraStateDD.SelectedValue, input_ReraNo.Text.Trim(), newFileName + extn, null, null, null, 0);
        ViewState["ReraView"] = dt;
        gdvRera.DataSource = dt.AsEnumerable()
                                .Where(r => r.Field<string>("Status") != "D")
                                .CopyToDataTable();

        gdvRera.DataBind();
        //input_ReraState.Text = string.Empty;
        ReraStateDD.SelectedValue = "notselected";
        input_ReraNo.Text = string.Empty;
    }

    public void ClearGstInputs()
    {
        //input_GstState.Text = string.Empty;
        GstStateDD.SelectedValue = "notselected";
        input_GstNo.Text = string.Empty;
    }

    public void ClearReraInputs()
    {
        //input_ReraState.Text = string.Empty;
        ReraStateDD.SelectedValue = "notselected";
        input_ReraNo.Text = string.Empty;
    }
    protected void AddGst(object sender, EventArgs e)
    {
        DataTable dt = (DataTable)ViewState["GstView"];

        if (GstFileUpload.FileName == "")
        {
            ClearGstInputs();
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please select GST certificate.');</script>");
            return;
        }

        //if (input_GstState.Text.Trim() == "")
        //{
        //    ClearGstInputs();
        //    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please select state.');</script>");
        //    return;
        //}

        if (GstStateDD.SelectedItem.Text == "SELECT STATE")
        {
            ClearGstInputs();
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please select GST state.');</script>");
            return;
        }

        if (input_GstNo.Text.Trim() == "")
        {
            ClearGstInputs();
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter GST No.');</script>");
            return;
        }

        Regex regex1 = new Regex(@"\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}");
        Match match1 = regex1.Match(input_GstNo.Text.Trim());
        if (!match1.Success)
        {
            ClearGstInputs();
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please enter valid GST No.');</script>");
            return;
        }

        //bool duplCheckState = dt.AsEnumerable().Any(row => row.Field<String>("State") == input_GstState.Text.Trim());
        //if(duplCheckState)
        //{
        //    ClearGstInputs();
        //    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('State already selected');</script>");
        //    return;
        //}

        bool duplCheckState = dt.AsEnumerable().Any(row => row.Field<String>("State") == GstStateDD.SelectedItem.Text.Trim());
        if (duplCheckState)
        {
            ClearGstInputs();
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('State already selected');</script>");
            return;
        }

        bool duplCheckGstNo = dt.AsEnumerable().Any(row => row.Field<String>("Gst_no") == input_GstNo.Text.Trim());
        if (duplCheckGstNo)
        {
            ClearGstInputs();
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('GST No already selected');</script>");
            return;
        }

        string[] supportFormts = { ".png", ".jpg", ".jpeg", ".pdf"};
        string newFileName, extn;

        if (supportFormts.Contains(System.IO.Path.GetExtension(GstFileUpload.FileName).ToLower()))
        {
            string subfolder = "data\\empanelment\\";
            extn = System.IO.Path.GetExtension(GstFileUpload.FileName);
            string path = System.AppDomain.CurrentDomain.BaseDirectory;
            newFileName = "GST_" + DateTime.Now.ToString("yyyyMMddHHmmss");

            string fullPath = path + subfolder + newFileName + extn;
            GstFileUpload.SaveAs(fullPath);
        }
        else
        {
            ClearGstInputs();
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Invalid File Format. (Supported: .png, .jpg, .jpeg, .pdf)');';</script>");
            return;
        }
        // input_GstState.Text.Trim()
        dt.Rows.Add("", GstStateDD.SelectedItem.Text, input_GstNo.Text.Trim(), newFileName + extn, null, null, null, 0);
        ViewState["GstView"] = dt;
        gdvGst.DataSource = dt.AsEnumerable()
                                 .Where(r => r.Field<string>("Status") != "D")
                                 .CopyToDataTable();

        gdvGst.DataBind();
        ClearGstInputs();
    }

    public class CountryDropDown
    {
        public List<GetCountryjsonResult> GetCountryjsonResult { get; set; }
    }

    public class GetCountryjsonResult
    {
        public string CountryId { get; set; }
        public string CountryName { get; set; }
        public object Status { get; set; }
    }

    public class GetStateDatajsonResult
    {
        public string StateId { get; set; }
        public string StateName { get; set; }
        public object Status { get; set; }
    }

    public class StateDropDown
    {
        public List<GetStateDatajsonResult> GetStateDatajsonResult { get; set; }
    }

    public class GetCityjsonResult
    {
        public string CityId { get; set; }
        public string CityName { get; set; }
        public object Status { get; set; }
    }

    public class CityDropDown
    {
        public List<GetCityjsonResult> GetCityjsonResult { get; set; }
    }

    public class UserInputEmpanelment
    {
        public string Communication_Street { get; set; }
        public string Communication_Zip { get; set; }
        public string Communication_City { get; set; }
        public string Registered_Country { get; set; }
        public string Registered_State { get; set; }
        public string Registered_Street { get; set; }
        public string Registered_Zip { get; set; }
        public string Registered_City { get; set; }
        public string Billing_Country { get; set; }
        public string Billing_State { get; set; }
        public string Billing_Street { get; set; }
        public string Billing_Zip { get; set; }
        public string Billing_City { get; set; }
        public string status { get; set; }
        public List<mGstDocs> GstDoc { get; set; }
        public List<mReraDocs> ReraDoc { get; set; }
        public string BrokerContactID { get; set; }
        public string BrokerID { get; set; }
        public string pancerti { get; set; }
        public string Pan_No { get; set; }
        public string registered_address { get; set; }
        public string communication_address { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string entity_type { get; set; }
        public string Company_Name { get; set; }
        public string EmplID { get; set; }
        public string Communication_State { get; set; }
        public string Communication_Country { get; set; }
        public string RequestType { get; set; }
        public string SubmittedByUser { get; set; }
    }
}
