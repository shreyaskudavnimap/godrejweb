﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class Admin_viewprojectinnerdetails : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
       
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("ProjectAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("ProjectInnerDetailsLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");
        if (!IsPostBack)
        { 
            BindGrid(""); 
        }
        if (Convert.ToString(Request.QueryString["saved"]) == "1")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record updated successfully.');</script>");
        }
        if (Convert.ToString(Request.QueryString["save"]) == "1")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record added successfully.');</script>");
        }
        txtSearch.Attributes.Add("onKeyPress", "doClick('" + btnSubmit.ClientID + "',event)");
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (txtSearch.Text != "" && txtSearch.Text != "search by project")
        {
            BindGrid(txtSearch.Text);
        }
    }

    private void BindGrid(string searchtext)
    {
        var objProject = from obj in dataContext.ProjectsInnerDetails
                         join prj in dataContext.Projects
                         on obj.projectid equals prj.projectid
                         join cty in dataContext.CityMasters
                         on prj.cityid equals cty.cityid
                         join menu in dataContext.Menus
                         on obj.menuid equals menu.menuid
                         orderby obj.ID descending
                         select new
                         {
                             obj.ID,
                             title = prj.title + "," + cty.cityname,
                             menu.menutitle,
                             obj.image,
                             obj.status
                         };

        if (searchtext != "" && searchtext != "search by project")
            objProject = objProject.Where(d => d.title.Contains(searchtext));

        if (objProject.Count() > 0)
        {
            lblMsg.Visible = false;
            lblMsg.Text = "";
            grdDetails.DataSource = objProject;
            grdDetails.DataBind();
        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "<br/>No records found.";
            grdDetails.DataSource = objProject;
            grdDetails.DataBind();
        }
    }

    protected void lnkBtnAddContent_Click(object sender, EventArgs e)
    {
        Session["innerprojectId"] = null;
        Response.Redirect("add-projectinnerdetails.aspx");
    }

    protected void grdDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdDetails.PageIndex = e.NewPageIndex;
        BindGrid(txtSearch.Text);
    }

    protected void grdDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditTest")
        {
            #region Code For Updation
            int id = Convert.ToInt32(e.CommandArgument);
            Session["innerprojectId"] = Convert.ToString(e.CommandArgument);
            Response.Redirect("add-projectinnerdetails.aspx?innerprojectId=" + id + "");
            #endregion
        }
        if (e.CommandName == "DeleteText")
        {
            #region
            int id = Convert.ToInt32(e.CommandArgument);
            var objProject = from d in dataContext.ProjectsInnerDetails where (d.ID == id) select d;
            foreach (ProjectsInnerDetail item in objProject)
            {
                dataContext.ProjectsInnerDetails.DeleteOnSubmit(item);
                dataContext.SubmitChanges();
            }
            ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data deleted successfully');</script>");
            BindGrid("");
            #endregion
        }
        if (e.CommandName == "Active")
        {
            #region code for Activation
            int id = Convert.ToInt32(e.CommandArgument);
            var objProject = dataContext.ProjectsInnerDetails.Single(doc => doc.ID == id);
            if (objProject.status != null)
            {
                if (objProject.status.Trim() == "A")
                {
                    objProject.status = "D";
                    dataContext.SubmitChanges();
                    ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data DeActivated successfully');</script>");
                }
                else
                {
                    objProject.status = "A";
                    dataContext.SubmitChanges();
                    ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data Activated successfully');</script>");
                }
            }
            else
            {
                objProject.status = "A";
                dataContext.SubmitChanges();
                ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data Activated successfully');</script>");
            }
            BindGrid("");
            #endregion
        }
    }

    protected void grdDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lnkBtn = (LinkButton)e.Row.FindControl("lnkBtnActive");
            Label lbls = (Label)e.Row.FindControl("lblStatus");

            if (lbls.Text.Trim() == "A")
            {
                lnkBtn.Text = "DeActivate";
            }
            else
            {
                lnkBtn.Text = "Activate";
            }
            if (e.Row.DataItemIndex >= 0)
            {
                foreach (Control control in e.Row.Cells[3].Controls)
                {
                    LinkButton DeleteButton = control as LinkButton;
                    if (DeleteButton != null && DeleteButton.Text == "Delete")
                    {
                        DeleteButton.OnClientClick = "return(confirm('Are you sure you want to delete this record?'))";
                    }
                    if (DeleteButton != null && DeleteButton.Text == "Activate")
                    {
                        DeleteButton.OnClientClick = "return(confirm('Are you sure you want to Activate this record?'))";
                    }
                    if (DeleteButton != null && DeleteButton.Text == "DeActivate")
                    {
                        DeleteButton.OnClientClick = "return(confirm('Are you sure you want to DeActivate this record?'))";
                    }
                }
            }
        }
    }
}
