﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class Admin_ViewAskExpert : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        BindGrid("");

        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("EnquiryAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("ExpertLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

        if (Convert.ToString(Request.QueryString["saved"]) == "1")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record updated successfully.');</script>");
        }
        if (Convert.ToString(Request.QueryString["save"]) == "1")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record added successfully.');</script>");
        }

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (txtSearch.Text != "" && txtSearch.Text != "search by question")
        {
            BindGrid(txtSearch.Text);
        }
    }

    private void BindGrid(string searchtext)
    {
        var objUser = from obj in dataContext.Faqs
                      join pj in dataContext.tbl_EmpanelmentContacts
                      on obj.userid equals pj.BrokerID
                      where obj.type == "Expert"
                      orderby obj.id descending
                      select new
                      {
                          obj.id,
                          pj.username,
                          obj.question,
                          obj.answer,
                          obj.ReplyDate,
                          obj.status
                      };

        if (searchtext != "" && searchtext != "search by question")
            objUser = objUser.Where(d => d.question.Contains(searchtext));

        if (objUser.Count() > 0)
        {
            lblMsg.Visible = false;
            lblMsg.Text = "";
            grdDetails.DataSource = objUser;
            grdDetails.DataBind();
        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "<br/>No records found.";
            grdDetails.DataSource = objUser;
            grdDetails.DataBind();
        }
    }

    protected void grdDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdDetails.PageIndex = e.NewPageIndex;
        BindGrid(txtSearch.Text);
    }

    protected void grdDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Reply")
        {
            #region code for Sending Reply
            int id = Convert.ToInt32(e.CommandArgument);
            Session["ExpertID"] = id;
            Response.Redirect("reply-expert.aspx?ExpertID=" + id + "");
            #endregion
        }
        if (e.CommandName == "Active")
        {
            #region code for Activation
            int id = Convert.ToInt32(e.CommandArgument);
            var objFaq = dataContext.Faqs.Single(doc => doc.id == id);
            if (objFaq.status != null)
            {
                if (objFaq.status.Trim() == "A")
                {
                    objFaq.status = "D";
                    dataContext.SubmitChanges();
                    ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data DeActivated successfully');</script>");
                }
                else
                {
                    objFaq.status = "A";
                    dataContext.SubmitChanges();
                    ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data Activated successfully');</script>");
                }
            }
            else
            {
                objFaq.status = "A";
                dataContext.SubmitChanges();
                ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data Activated successfully');</script>");
            }
            BindGrid("");
            #endregion
        }
    }

    protected void grdDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lnkBtn = (LinkButton)e.Row.FindControl("lnkBtnActive");
            Label lbls = (Label)e.Row.FindControl("lblStatus");

            if (lbls.Text.Trim() == "A")
            {
                lnkBtn.Text = "DeActivate";
            }
            else
            {
                lnkBtn.Text = "Activate";
            }
            if (e.Row.DataItemIndex >= 0)
            {
                foreach (Control control in e.Row.Cells[4].Controls)
                {
                    LinkButton DeleteButton = control as LinkButton;
                    if (DeleteButton != null && DeleteButton.Text == "Delete")
                    {
                        DeleteButton.OnClientClick = "return(confirm('Are you sure you want to delete this record?'))";
                    }
                    if (DeleteButton != null && DeleteButton.Text == "Activate")
                    {
                        DeleteButton.OnClientClick = "return(confirm('Are you sure you want to Activate this record?'))";
                    }
                    if (DeleteButton != null && DeleteButton.Text == "DeActivate")
                    {
                        DeleteButton.OnClientClick = "return(confirm('Are you sure you want to DeActivate this record?'))";
                    }
                }
            }
        }
    }

}