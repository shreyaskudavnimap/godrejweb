﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;

public partial class Admin_add_campaign : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["CampaignId"])))
            {
                FillData(Convert.ToString(Request.QueryString["CampaignId"]));
            }
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("MasterAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("CampaignLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

    }

    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewcampaign.aspx");
    }

    private void FillData(string campaignid)
    {
        var objCampaign = dataContext.Campaigns.Single(doc => doc.campaignid == Convert.ToInt32(campaignid));
        txtTitle.Text = objCampaign.title;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["CampaignId"])))
        {
            #region to check if same city exists

            var objChk = (from obj in dataContext.Campaigns
                          where (obj.title).ToLower() == txtTitle.Text.ToLower()
                          select obj).FirstOrDefault();

            #endregion
            if (objChk != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Campaign Name already exists.');</script>");
                return;
            }
            else
            {
                #region For Adding the Record
                Campaign objCampaign = new Campaign();

                objCampaign.title = txtTitle.Text.Trim();
                objCampaign.createddate = DateTime.Now;
                objCampaign.status = "A";
                dataContext.Campaigns.InsertOnSubmit(objCampaign);
                dataContext.SubmitChanges();
                Response.Redirect("viewcampaign.aspx?save=1");
                #endregion
            }
        }
        else
        {
            #region Code For Updation
            int id = Convert.ToInt32(Session["CampaignId"]);
            var objCity = dataContext.Campaigns.Single(doc => doc.campaignid == id);
            objCity.title = txtTitle.Text.Trim();
            dataContext.SubmitChanges();
            Session["CampaignId"] = null;
            Response.Redirect("viewcampaign.aspx?saved=1");
            #endregion
        }
        Div1.Style.Add("display", "block");
    }
}
