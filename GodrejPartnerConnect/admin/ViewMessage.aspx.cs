﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class Admin_ViewMessage : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        BindGrid("");

        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("EnquiryAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("MassageLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

        if (Convert.ToString(Request.QueryString["saved"]) == "1")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record updated successfully.');</script>");
        }
        if (Convert.ToString(Request.QueryString["save"]) == "1")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record added successfully.');</script>");
        }

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (txtSearch.Text != "" && txtSearch.Text != "search by name")
        {
            BindGrid(txtSearch.Text);
        }
    }

    private void BindGrid(string searchtext)
    {
        var objUser = from obj in dataContext.Messages
                      join pj in dataContext.tbl_EmpanelmentContacts
                      on obj.userid equals pj.BrokerID
                      orderby obj.id descending
                      select new
                      {
                          obj.id,
                          pj.Name ,
                          pj.Email,
                          obj.message1,
                          obj.mobile,
                          msg = (obj.msgtype == "Mobile" ? "Call Back" : "Message"),
                          obj.createdon
                      };

        if (searchtext != "" && searchtext != "search by name")
            objUser = objUser.Where(d => d.Name.Contains(searchtext));

        if (objUser.Count() > 0)
        {
            lblMsg.Visible = false;
            lblMsg.Text = "";
            grdDetails.DataSource = objUser;
            grdDetails.DataBind();
        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "<br/>No records found.";
            grdDetails.DataSource = objUser;
            grdDetails.DataBind();
        }
    }

    protected void grdDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdDetails.PageIndex = e.NewPageIndex;
        BindGrid(txtSearch.Text);
    }
}