﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class Admin_add_banner : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    Guid random = Guid.NewGuid();
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["godrejcpmsConnectionString"].ConnectionString);
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        if (Convert.ToString(Session["type"])=="admin")
        {

        }
        if (Convert.ToString(Request.QueryString["bannerId"]) !=null)
        {
            divPriority.Visible = true;
        }
        else 
        {
            divPriority.Visible = false;
        }
        if (!IsPostBack)
        {
            BindProject();
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["bannerId"])))
            {
                FillData(Convert.ToString(Request.QueryString["bannerId"]));
            }
        }
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("BannerAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("BannerLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

    }

    protected void btnDataSaved_Click(object sender, EventArgs e)
    {
        Response.Redirect("viewplace.aspx");
    }

    private void BindProject()
    {
        #region Code for Project
        var objProject = (from obj in dataContext.Projects
                          join city in dataContext.CityMasters on obj.cityid equals city.cityid
                          where obj.status.Trim() == "A"
                          orderby obj.projectid descending
                          select new
                          {
                              obj.projectid,
                              title = obj.title + ", " + city.cityname

                          }).Distinct().ToList();
        ddlProject.DataTextField = "title";
        ddlProject.DataValueField = "projectid";
        ddlProject.DataSource = objProject;
        ddlProject.DataBind();
        ddlProject.Items.Insert(0, new ListItem("--Select--", "0"));
        #endregion
    }

    private void FillData(string bannerId)
    {
        var objBanner = dataContext.HomeBanners.Single(doc => doc.id == Convert.ToInt32(bannerId));
        txtTitle.Text = objBanner.title;
        txtCaption.Text = objBanner.caption;
        txtPriority.Text =objBanner.Priority.ToString();
        if (objBanner.relatedLink != "")
        {
            ddlProject.SelectedValue = objBanner.relatedLink;
        }
        else
        {
            ddlProject.SelectedValue = "0";
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["bannerId"])))
        {
            #region For Adding the Record
            var priority = (from obj in dataContext.HomeBanners
                            select obj).Max(p => p.Priority);
           

            HomeBanner objBanner = new HomeBanner();
            #region Save Image
            if (FileUpload1.HasFile)
            {
                if (FileUpload1.PostedFile.ContentLength > 1048576)
                {
                    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual Image size should not be more than 1MB');</script>");
                    return;
                }
                string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                if (imgext.ToLower() == ".jpeg" || imgext.ToLower() == ".jpg")
                {
                    string fileName = Path.GetFileName(FileUpload1.FileName);
                    string fileNameEdited = random.ToString() + "_" + fileName;

                    float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
                    float hieght = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
                    if (width == 800 && hieght == 700)
                    {
                        if (fileNameEdited.Contains('&'))
                            fileNameEdited = fileNameEdited.Replace('&', 'a');
                        fileNameEdited = fileNameEdited.Replace("-", "");
                        FileUpload1.SaveAs(Server.MapPath("~/images/HomeImages/") + fileNameEdited);
                        //FixedSize(Convert.ToInt32(width), Convert.ToInt32(hieght), 287, 398, true, Server.MapPath("~/images/HomeImages/") + fileNameEdited, Server.MapPath("~/images/HomeImages/resize/") + fileNameEdited);
                        objBanner.ImageName = fileNameEdited;
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Image minimum dimensions should be 800 * 700');</script>");
                        return;
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please upload only .jpeg or .jpg file');</script>");
                    return;
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please upload banner image');</script>");
                return;
            }
            #endregion
            objBanner.title = txtTitle.Text.Trim();
            objBanner.caption = txtCaption.Text;
            if (ddlProject.SelectedValue != "0")
            {
                objBanner.relatedLink = ddlProject.SelectedValue;
            }
            objBanner.createddate = DateTime.Now;
            objBanner.status = "A";
            objBanner.Priority = Convert.ToInt32(priority.Value + 1);
            dataContext.HomeBanners.InsertOnSubmit(objBanner);

            dataContext.SubmitChanges();
            Response.Redirect("viewbanner.aspx?save=1");
            #endregion 
        }
        else
        {
           
            #region Code For Updation
            int id = Convert.ToInt32(Session["bannerId"]);
            var objBanner = dataContext.HomeBanners.Single(doc => doc.id == id);
            #region Update Image
            if (FileUpload1.HasFile)
            {
                if (FileUpload1.PostedFile.ContentLength > 1048576)
                {
                    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Actual File size should not be more than 1MB');</script>");
                    return;
                }
                string imgext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                if (imgext.ToLower() == ".jpeg" || imgext.ToLower() == ".jpg")
                {
                    string fileName = Path.GetFileName(FileUpload1.FileName);
                    string fileNameEdited = random.ToString() + "_" + fileName;
                    float width = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Width;
                    float hieght = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream).PhysicalDimension.Height;
                    if (width == 800 && hieght == 700)
                    {
                        if (fileNameEdited.Contains('&'))
                            fileNameEdited = fileNameEdited.Replace('&', 'a');
                        fileNameEdited = fileNameEdited.Replace("-", "");
                        FileUpload1.SaveAs(Server.MapPath("~/images/HomeImages/") + fileNameEdited);
                        //FixedSize(Convert.ToInt32(width), Convert.ToInt32(hieght), 287, 398, true, Server.MapPath("~/images/HomeImages/") + fileNameEdited, Server.MapPath("~/images/HomeImages/resize/") + fileNameEdited);
                        objBanner.ImageName = fileNameEdited;
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Image minimum dimensions should be 800 * 700');</script>");
                        return;
                    }
                    try
                    {
                        if (ViewState["Image1"] != null && ViewState["Image1"].ToString() != "")
                            File.Delete(Server.MapPath(@"~/images/HomeImages/" + ViewState["Image1"].ToString()));
                    }
                    catch (Exception ex)
                    {
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Please upload only .jpeg or .jpg file');</script>");
                    return;
                }
            }
            #endregion
            objBanner.title = txtTitle.Text.Trim();
            objBanner.caption = txtCaption.Text;
           // objBanner.Priority = Convert.ToInt32(txtPriority.Text);

            if (ddlProject.SelectedValue != "0")
            {
                objBanner.relatedLink = ddlProject.SelectedValue;
            }
            dataContext.SubmitChanges();
           
            var query = (from obj in dataContext.HomeBanners
                         where obj.id == id
                         select new
                         {
                             obj.Priority

                         }).SingleOrDefault();
            if (query != null)
            {
                int priority = Convert.ToInt32(query.Priority);
            }
            SqlCommand cmd = new SqlCommand("sp_priority_update", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@currentsrno", query.Priority);
            cmd.Parameters.AddWithValue("@newsrno", Convert.ToInt32(txtPriority.Text));
            cmd.Parameters.AddWithValue("@id", Convert.ToInt32(Session["bannerId"]));
            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();
            int k = cmd.ExecuteNonQuery();
            if (con.State != ConnectionState.Closed)
                con.Close();
            Session["bannerId"] = null;
            Response.Redirect("viewbanner.aspx?saved=1");
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message.ToString());
            }

           
            #endregion
        }
        Div1.Style.Add("display", "block");
    }

    public void FixedSize(int srcwidth, int srcheight, int Width, int Height, bool needToFill, string srcpath, string targetpath)
    {
        System.Drawing.Image imgPhoto = System.Drawing.Image.FromFile(srcpath);
        int sourceWidth = srcwidth;
        int sourceHeight = srcheight;
        int sourceX = 0;
        int sourceY = 0;
        int destX = 0;
        int destY = 0;

        float nPercent = 0;
        float nPercentW = 0;
        float nPercentH = 0;

        nPercentW = ((float)Width / (float)sourceWidth);
        nPercentH = ((float)Height / (float)sourceHeight);
        if (!needToFill)
        {
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
            }
            else
            {
                nPercent = nPercentW;
            }
        }
        else
        {
            if (nPercentH > nPercentW)
            {
                nPercent = nPercentH;
                destX = (int)Math.Round((Width -
                    (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = (int)Math.Round((Height -
                    (sourceHeight * nPercent)) / 2);
            }
        }

        if (nPercent > 1)
            nPercent = 1;

        int destWidth = (int)Math.Round(sourceWidth * nPercent);
        int destHeight = (int)Math.Round(sourceHeight * nPercent);

        System.Drawing.Bitmap bmPhoto = new System.Drawing.Bitmap(
            destWidth <= Width ? destWidth : Width,
            destHeight < Height ? destHeight : Height,
                          PixelFormat.Format32bppRgb);
        //bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
        //                 imgPhoto.VerticalResolution);

        System.Drawing.Graphics grPhoto = System.Drawing.Graphics.FromImage(bmPhoto);
        grPhoto.Clear(System.Drawing.Color.White);
        grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
        //InterpolationMode.HighQualityBicubic;
        grPhoto.CompositingQuality = CompositingQuality.Default;
        grPhoto.SmoothingMode = SmoothingMode.Default;

        grPhoto.DrawImage(imgPhoto,
            new System.Drawing.Rectangle(destX, destY, destWidth, destHeight),
            new System.Drawing.Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
            System.Drawing.GraphicsUnit.Pixel);
        bmPhoto.Save(targetpath);
        grPhoto.Dispose();

    }
}
