﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Net;
using System.Web.Script.Serialization;

public partial class Admin_viewbanner : System.Web.UI.Page
{
    GodejCPDataContext dataContext = new GodejCPDataContext();
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["godrejcpmsConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        getToken();
        if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
            Response.Redirect("AdminLogin.aspx");
        BindGrid();
        HtmlAnchor openAnchor = (HtmlAnchor)Menu.FindControl("BannerAnchor");
        HtmlControl selectMenu = (HtmlControl)Menu.FindControl("BannerLi");
        openAnchor.Attributes.Add("class", "open");
        selectMenu.Attributes.Add("class", "current");

        if (Convert.ToString(Request.QueryString["saved"]) == "1")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record updated successfully.');</script>");
        }
        if (Convert.ToString(Request.QueryString["save"]) == "1")
        {
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record added successfully.');</script>");
        }
    }

    protected void getToken() {
      using(WebClient client = new WebClient())
      {
        var reqparm = new System.Collections.Specialized.NameValueCollection();
        string PARM= "par1=value1&par=value2";
        ServicePointManager.Expect100Continue = true;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
        // string responsebytes = client.UploadString("https://test.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9Nvmjd9lcjRnoT4GG3E8o7ZbQcp3HqKaX6KsWkBg77OzU6SN.6oqr00W1pLR_P50oeF8xzGIk7RWT9TTA&client_secret=8491910721028248323&username=sachin_more@magicsoftware.com&password=Godrej@2018N96qqGVtAstcSut9a0dhqF2lo", PARM);
        // string responsebody = Encoding.UTF8.GetString(responsebytes);
        // var serializer = new JavaScriptSerializer();
        // var serializedResult = serializer.Serialize(responsebytes);
        // JObject json = JObject.Parse(str);
        // ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert(" + json + ");</script>");
      }
    }

    private void BindGrid()
    {
        var objBanner = from obj in dataContext.HomeBanners
                        orderby obj.id descending
                        select new
                        {
                            obj.id,
                            obj.ImageName,
                            obj.caption,
                            obj.status,
                            obj.Priority                        };

        if (objBanner.Count() > 0)
        {
            lblMsg.Visible = false;
            lblMsg.Text = "";
            grdDetails.DataSource = objBanner;
            grdDetails.DataBind();
        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "<br/>No records found.";
            grdDetails.DataSource = objBanner;
            grdDetails.DataBind();
        }
    }

    protected void lnkBtnAddContent_Click(object sender, EventArgs e)
    {
        Session["bannerId"] = null;
        Response.Redirect("add-banner.aspx");
    }

    protected void grdDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdDetails.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void grdDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditTest")
        {
            #region Code For Updation
            int id = Convert.ToInt32(e.CommandArgument);
            Session["bannerId"] = Convert.ToString(e.CommandArgument);
            Response.Redirect("add-banner.aspx?bannerId=" + id + "");
            #endregion
        }
        if (e.CommandName == "DeleteText")
        {
            #region
            int srno;
            int id = Convert.ToInt32(e.CommandArgument);
            srno = GetPriority(id);
            
            
            var objBanner = from d in dataContext.HomeBanners where (d.id == id) select d;
            foreach (HomeBanner item in objBanner)
            {
                dataContext.HomeBanners.DeleteOnSubmit(item);
                dataContext.SubmitChanges();
            }
            if (srno != 0)
            {
                SqlCommand cmd = new SqlCommand("update_after_delete", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@priority", srno);
                try
                {
                    if (con.State != ConnectionState.Open)
                        con.Open();
                    int j = cmd.ExecuteNonQuery();
                    if (con.State != ConnectionState.Closed)
                        con.Close();
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message.ToString());
                }
            }
            
           // GetPriority(id);
            BindGrid();
            ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data deleted successfully');</script>");
            #endregion
        }
        if (e.CommandName == "Active")
        {
            #region code for Activation
            int id = Convert.ToInt32(e.CommandArgument);
            var objBanner = dataContext.HomeBanners.Single(doc => doc.id == id);
            if (objBanner.status != null)
            {
                if (objBanner.status.Trim() == "A")
                {
                    objBanner.status = "D";
                    dataContext.SubmitChanges();
                    ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data DeActivated successfully');</script>");
                }
                else
                {
                    objBanner.status = "A";
                    dataContext.SubmitChanges();
                    ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data Activated successfully');</script>");
                }
            }
            else
            {
                objBanner.status = "A";
                dataContext.SubmitChanges();
                ClientScript.RegisterStartupScript(GetType(), "js", "<script languange='javascript'>alert('Data Activated successfully');</script>");
            }
            BindGrid();
            #endregion
        } 
    }

    public int GetPriority(int productid)
    {
        int priority = 0;

        var obj = from u in dataContext.HomeBanners where (u.id == productid) select u;

        foreach (var item in obj)
        {
            priority = Convert.ToInt32(item.Priority);
        }

        return priority;
    }
    protected void grdDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lnkBtn = (LinkButton)e.Row.FindControl("lnkBtnActive");
            Label lbls = (Label)e.Row.FindControl("lblStatus");

            if (lbls.Text.Trim() == "A")
            {
                lnkBtn.Text = "DeActivate";
            }
            else
            {
                lnkBtn.Text = "Activate";
            }
            if (e.Row.DataItemIndex >= 0)
            {
                foreach (Control control in e.Row.Cells[2].Controls)
                {
                    LinkButton DeleteButton = control as LinkButton;
                    if (DeleteButton != null && DeleteButton.Text == "Delete")
                    {
                        DeleteButton.OnClientClick = "return(confirm('Are you sure you want to delete this record?'))";
                    }
                    if (DeleteButton != null && DeleteButton.Text == "Activate")
                    {
                        DeleteButton.OnClientClick = "return(confirm('Are you sure you want to Activate this record?'))";
                    }
                    if (DeleteButton != null && DeleteButton.Text == "DeActivate")
                    {
                        DeleteButton.OnClientClick = "return(confirm('Are you sure you want to DeActivate this record?'))";
                    }
                }
            }
        }
    }
}
