<?php
	error_reporting(1);
	/*header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: PUT, GET, POST");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");*/

	//ini_set("soap.wsdl_cache_enabled", "0");
	//$pan_id = "AAZFS8386E";
	require_once ($_SERVER['DOCUMENT_ROOT'].'/GodrejPartnerConnect/org_config.php'); //configuration file containg login credentials
	require_once ($_SERVER['DOCUMENT_ROOT'].'/GodrejPartnerConnect/soapclient/SforcePartnerClient.php');
	$mySforceConnection = new SforcePartnerClient();
	$mySforceConnection->createConnection($_SERVER['DOCUMENT_ROOT']."/GodrejPartnerConnect/soapclient/Partnerwsdl_Production-14-3-17.jsp.xml");
	$mySforceConnection->login(USERNAME, PASSWORD.SECURITY_TOKEN);	

	$title = $_GET['title'];
	$firstName = $_GET['firstName'];
	$lastname = $_GET['lastname'];
	$city = $_GET['city'];
	$state = $_GET['state'];
	$Country = $_GET['Country'];
	$phoneNo = $_GET['phoneNo'];
	$email = $_GET['email'];
	$prjcode = $_GET['prjcode'];
	$comment = $_GET['comment'];
	$visitDate='2015-07-07T11:11:11';
	$websiteName = $_GET['websiteName'];
	$addCode = $_GET['addCode'];
	$type = $_GET['type'];
	$Src = $_GET['Src'];
	$CountryCode = $_GET['CountryCode'];
	$rating = $_GET['rating'];
	$trnsId = $_GET['trnsId'];
	$trnsStatus = $_GET['trnsStatus'];
	$BrokerAccID = '0016F00001xyJvfQAE';
	$BrokerConID = '0036F000026MHPCQA4';
	
	if($title == 'NA'){
		$title = '';
	}
	if($firstName == 'NA'){
		$firstName = '';
	}
	if($lastname == 'NA'){
		$lastname = '';
	}
	if($city == 'NA'){
		$city = '';
	}
	if($state == 'NA'){
		$state = '';
	}
	if($Country == 'NA'){
		$Country = '';
	}
	if($phoneNo == 'NA'){
		$phoneNo = '';
	}
	if($email == 'NA'){
		$email = '';
	}
	if($prjcode == 'NA'){
		$prjcode = '';
	}
	if($websiteName == 'NA'){
		$websiteName = '';
	}
	if($addCode == 'NA'){
		$addCode = '';
	}
	if($Src == 'NA'){
		$Src = '';
	}
	if($CountryCode == 'NA'){
		$CountryCode = '';
	}
	if($rating == 'NA'){
		$rating = '';
	}
	if($trnsId == 'NA'){
		$trnsId = '';
	}
	if($trnsStatus == 'NA'){
		$trnsStatus = '';
	}
	if($BrokerAccID == 'NA'){
		$BrokerAccID = '';
	}
	if($BrokerConID == 'NA'){
		$BrokerConID = '';
	}
	
	/*<xsd:element name="title" type="xsd:string" nillable="true"/>
	<xsd:element name="firstName" type="xsd:string" nillable="true"/>
	<xsd:element name="lastname" type="xsd:string" nillable="true"/>
	<xsd:element name="city" type="xsd:string" nillable="true"/>
	<xsd:element name="state" type="xsd:string" nillable="true"/>
	<xsd:element name="Country" type="xsd:string" nillable="true"/>
	<xsd:element name="phoneNo" type="xsd:string" nillable="true"/>
	<xsd:element name="email" type="xsd:string" nillable="true"/>
	<xsd:element name="prjcode" type="xsd:string" nillable="true"/>
	<xsd:element name="comment" type="xsd:string" nillable="true"/>
	<xsd:element name="visitDate" type="xsd:dateTime" nillable="true"/>
	<xsd:element name="websiteName" type="xsd:string" nillable="true"/>
	<xsd:element name="addCode" type="xsd:string" nillable="true"/>
	<xsd:element name="type" type="xsd:string" nillable="true"/>
	<xsd:element name="Src" type="xsd:string" nillable="true"/>
	<xsd:element name="CountryCode" type="xsd:string" nillable="true"/>
	<xsd:element name="rating" type="xsd:string" nillable="true"/>
	<xsd:element name="trnsId" type="xsd:string" nillable="true"/>
	<xsd:element name="trnsStatus" type="xsd:string" nillable="true"/>
	<xsd:element name="BrokerAccID" type="xsd:string" nillable="true"/>
	<xsd:element name="BrokerConID" type="xsd:string" nillable="true"/>*/
	
	 //'title=' + 'NA' + '&firstName=' + firstname + '&lastname=' + lastname + '&city=' + city + '&state=' + state + '&Country=' + country + '&phoneNo=' + mobile + '&email=' + email + '&prjcode=' + taxno + '&comment=' + description + '&visitDate=' + 'NA' + '&websiteName=' + 'NA' + '&addCode=' + 'NA' + '&type=' + apartment + '&Src=' + 'Broker' + '&CountryCode=' + 'NA' + '&rating=' + 'Hot' + '&trnsId=' + 'NA' + '&trnsStatus=' + 'NA' + '&BrokerAccID=' + brokeraccountid + '&BrokerConID=' + brokerid,
	
	$responseLeadData = array(
		"title" => $title,
		"firstName" => $firstName,
		"lastname" => $lastname,
		"city" => $city,
		"state" => $state,
		"Country" => $Country,
		"phoneNo" => $phoneNo,
		"email" => $email,
		"prjcode" => $prjcode,
		"comment" => $comment,
		"visitDate" => $visitDate,
		"websiteName" => $websiteName,
		"addCode" => $addCode,
		"type" => $type,
		"Src" => $Src,
		"CountryCode" => $CountryCode,
		"rating" => $rating,
		"trnsId" => $trnsId,
		"trnsStatus" => $trnsStatus,
		"BrokerAccID" => $BrokerAccID,
		"BrokerConID" => $BrokerConID		
	);
	//print_r($responseLeadData);
	//echo "</br>".'--';
	$parsedURL = parse_url($mySforceConnection->getLocation());
	define ("_SFDC_SERVER_", substr($parsedURL['host'],0,strpos($parsedURL['host'], '.')));
	define ("_WS_NAME_", 'WebToBrokerEnquiryCreation');
	define ("_WS_WSDL_", $_SERVER['DOCUMENT_ROOT'].'/GodrejPartnerConnect/soapclient/WebToBrokerEnquiryCreation.xml');
	define ("_WS_ENDPOINT_", 'https://' . _SFDC_SERVER_ . '.salesforce.com/services/wsdl/class/' . _WS_NAME_);
	define ("_WS_NAMESPACE_", 'http://soap.sforce.com/schemas/class/' . _WS_NAME_);
	$client = new SoapClient(_WS_WSDL_);
	$sforce_header = new SoapHeader(_WS_NAMESPACE_, "SessionHeader", array("sessionId" => $mySforceConnection->getSessionId()));
	$client->__setSoapHeaders(array($sforce_header));

	$upsertResponse = $client->EnquiryContact($responseLeadData);
	//print_r($upsertResponse);
	//exit;
	$response = array();
	
	foreach ($upsertResponse as $created){
		//echo $created->id;

		if($created->Id != ''){
			$response['status'] = 'success';
		}else{
			$response['status'] = 'error';
		}
	}

	echo json_encode($response);
	
?>