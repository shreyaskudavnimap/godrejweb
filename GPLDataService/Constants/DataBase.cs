﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLDataService.Constants
{
    public class DataBase
    {
        private readonly string ConStr = ConfigurationManager.ConnectionStrings[Constants.Connection.DBCONNECTIONSTRING].ConnectionString;

        public DataSet GetTable(string query)
        {
            SqlConnection _connection = new SqlConnection(ConStr);

            if (_connection.State == ConnectionState.Closed)
                _connection.Open();
            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandTimeout = 60;
            DataSet ds = new DataSet();
            SqlDataAdapter ad = new SqlDataAdapter(query, _connection);
            ad.Fill(ds);
            _connection.Close();
            return ds;
        }
        public int GetScalar(string query)
        {
            int RowAffected = 0;
            SqlConnection _connection = new SqlConnection(ConStr);

            if (_connection.State == ConnectionState.Closed)
                _connection.Open();
            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandTimeout = 60;
            cmd.Connection = _connection;
            RowAffected = (int)cmd.ExecuteScalar();
            _connection.Close();
            return RowAffected;
        }
        public int GetRowAffected(string query)
        {
            int RowAffected = 0;
            SqlConnection _connection = new SqlConnection(ConStr);

            if (_connection.State == ConnectionState.Closed)
                _connection.Open();
            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandTimeout = 60;
            cmd.Connection = _connection;
            RowAffected = cmd.ExecuteNonQuery();
            _connection.Close();
            return RowAffected;
        }

        public int ExecuteTableValueSP(string query, DataTable dt, string TypeType)
        {
            int RowAffected = 0;
            SqlConnection _connection = new SqlConnection(ConStr);

            if (_connection.State == ConnectionState.Closed)
                _connection.Open();



            SqlCommand cmd = new SqlCommand(query);
            SqlParameter param = new SqlParameter();
            param.ParameterName = "@TreeTypeTable";
            param.Value = dt;
            param.TypeName = TypeType;
            param.SqlDbType = SqlDbType.Structured;

            cmd.Parameters.Add(param);

            cmd.CommandTimeout = 60;
            cmd.Connection = _connection;
            RowAffected = (int)cmd.ExecuteScalar();
            _connection.Close();
            return RowAffected;
        }



    }
}
