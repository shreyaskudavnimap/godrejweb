﻿using GPLDataService.Constants;
using GPLDataService.Outputs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLDataService.Services
{
    public class QuartersService: ExceptionHandlerService
    {
        string ConStr = ConfigurationManager.ConnectionStrings[Constants.Connection.DBCONNECTIONSTRING].ConnectionString;

        public QuartersService()
        {

        }

        public IList<Quarter> GetQuarter(string panNo, string flag)
        {
            IList<Quarter> quarterlist = new List<Quarter>();
            SqlConnection con = new SqlConnection(ConStr);
            try
            {
                string StoreProcedure = ConfigurationManager.AppSettings["GetQuarterList"].ToString();
                SqlCommand cmd = new SqlCommand(StoreProcedure, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@panNo", panNo);
                cmd.Parameters.AddWithValue("@flag", flag);
                con.Open();

                SqlDataReader rdr = cmd.ExecuteReader();
                quarterlist = new GenericReader<Quarter>().CreateList(rdr);

                //if(rdr.NextResult())
                //{
                //    while (rdr.Read())
                //    {
                //        Quarter qrtr = new Quarter();
                //        qrtr.quarter = rdr["Quarter"].ToString();
                //        quarterlist.Add(qrtr);
                //    }
                //}


            }
            finally
            {
                IsConnectionOpenThenClose(con);
            }
            return quarterlist;
        }
    }
}
