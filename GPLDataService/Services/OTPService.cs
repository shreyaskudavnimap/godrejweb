﻿using GPLDataService.Constants;
using GPLDataService.Inputs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GPLDataService.Services
{
    public class OTPService: ExceptionHandlerService
    {
        string ConStr = ConfigurationManager.ConnectionStrings[Constants.Connection.DBCONNECTIONSTRING].ConnectionString;

        string MessageText1 = ConfigurationManager.AppSettings["OTPMessage1"].ToString();
        string MessageText2 = ConfigurationManager.AppSettings["OTPMessage2"].ToString();
        string Msg = "";
        EmailService emailService = new EmailService();

        public string SendOTP(string Recepient, string Message, int OTPAction)
        {
            string receiptMobNo = Recepient;
            var smsurl = "";
            Recepient = "91" + Convert.ToString(Recepient);
            string OTP = GenerateOtp();
            this.Msg = string.Concat(new string[]
            {
                this.MessageText1,
                " ",
                OTP,
                " ",
                this.MessageText2
            });

            //XML Reading
            string filePath = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory);
            string xmlFilePath = filePath + @"\MasterData.xml";
            var xmlDoc = new XmlDocument();
            try
            {
                using (var fs = new FileStream(xmlFilePath, FileMode.Open, FileAccess.Read))
                {
                    xmlDoc.Load(fs);
                    var formTag = xmlDoc.GetElementsByTagName("ReadSMS");
                    string Msg1, Msg2, Msg3;
                    foreach (XmlNode node in formTag)
                    {
                        Msg1 = node.Attributes["text1"].Value;
                        Msg2 = node.Attributes["text2"].Value;
                       // Msg3 = node.Attributes["text3"].Value;
                        smsurl = Msg1 + Recepient + Msg2 + Msg;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            WebRequest request = WebRequest.Create(smsurl);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            //read the response
            StreamReader responseReader = new StreamReader(response.GetResponseStream());
            string resultmsg = responseReader.ReadToEnd();

            //SMS Logging for perticular Option
            OTPLog _logotp = new OTPLog();
            _logotp.message = Msg;
            _logotp.recepient = receiptMobNo;
            _logotp.otpAction = OTPAction;
            _logotp.resultMsg = resultmsg;
            _logotp.OTP = OTP;
            LogOtp(_logotp);
            return OTP;
        }

        public string SendExistingOTP(string Recepient, string otp, int OTPAction)
        {
            string receiptMobNo = Recepient;
            var smsurl = "";
            Recepient = "91" + Convert.ToString(Recepient);
            string OTP = otp;
            this.Msg = string.Concat(new string[]
            {
                this.MessageText1,
                " ",
                OTP,
                " ",
                this.MessageText2
            });

            //XML Reading
            string filePath = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory);
            string xmlFilePath = filePath + @"\MasterData.xml";
            var xmlDoc = new XmlDocument();
            try
            {
                using (var fs = new FileStream(xmlFilePath, FileMode.Open, FileAccess.Read))
                {
                    xmlDoc.Load(fs);
                    var formTag = xmlDoc.GetElementsByTagName("ReadSMS");
                    string Msg1, Msg2, Msg3;
                    foreach (XmlNode node in formTag)
                    {
                        Msg1 = node.Attributes["text1"].Value;
                        Msg2 = node.Attributes["text2"].Value;
                        // Msg3 = node.Attributes["text3"].Value;
                        smsurl = Msg1 + Recepient + Msg2 + Msg;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            WebRequest request = WebRequest.Create(smsurl);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            //read the response
            StreamReader responseReader = new StreamReader(response.GetResponseStream());
            string resultmsg = responseReader.ReadToEnd();

            //SMS Logging for perticular Option
            OTPLog _logotp = new OTPLog();
            _logotp.message = Msg;
            _logotp.recepient = receiptMobNo;
            _logotp.otpAction = OTPAction;
            _logotp.resultMsg = resultmsg;
            _logotp.OTP = OTP;
            LogOtp(_logotp);
            return OTP;
        }


        public int ExistingOtp(string Recepient,string emailId)
        {
            int Otp = 0;
            string Msg = "";
            
            SqlConnection con = new SqlConnection(ConStr);
            try
            {
                string StoreProcedure = ConfigurationManager.AppSettings["GetExistingOtp"].ToString();
                SqlCommand cmd = new SqlCommand(StoreProcedure, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@mobileNo", Recepient);
               
                con.Open();

                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Otp = Convert.ToInt32(rdr["pin"]);
                }
                if (Otp != 0)
                {
                    this.Msg = string.Concat(new string[]
                    {
                         this.MessageText1, " ", Otp.ToString()," ",this.MessageText2
                    });
                    //SendExistingOTP(Recepient, Otp.ToString(), 117);
                    emailService.SendAnEmail(Otp.ToString(), emailId);
                }
            }
            finally
            {
                IsConnectionOpenThenClose(con);
            }
            return Otp;
        }

        public string GenerateOtp()
        {
            string otp = string.Empty;
            Random r = new Random();
            int otpInt = r.Next(111111, 999999);
            return otpInt + "";
        }

        public int LogOtp(OTPLog _LogOTP)
        {
            int ResponseCode = 0;

            DataBase _con = new DataBase();

            string StoredProcedure = ConfigurationManager.AppSettings["InsertOTP"].ToString();

            using (SqlConnection con = new SqlConnection(ConStr))
            {
                SqlCommand cmd = new SqlCommand(StoredProcedure, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@receiver", _LogOTP.recepient);
                cmd.Parameters.AddWithValue("@message", _LogOTP.message);
                cmd.Parameters.AddWithValue("@otpAction", _LogOTP.otpAction);
                cmd.Parameters.AddWithValue("@resultMsg", _LogOTP.resultMsg);
                cmd.Parameters.AddWithValue("@otp", _LogOTP.OTP);
                con.Open();
                ResponseCode = (int)cmd.ExecuteScalar();
                con.Close();
            }

            return ResponseCode;
        }

        public bool sendCustomSMS(EOIShare eOIShare)
        {
            string resultMsg = "";

            var smsurl = "";

            string Recepient = "91" + Convert.ToString(eOIShare.mobileNo);

            string Msg = eOIShare.customerFirstName + ' ' + eOIShare.customerLastName;
            //XML Reading
            string filePath = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory);
            string xmlFilePath = filePath + @"\MasterData.xml";
            var xmlDoc = new XmlDocument();
            try
            {
                using (var fs = new FileStream(xmlFilePath, FileMode.Open, FileAccess.Read))
                {
                    xmlDoc.Load(fs);
                    var formTag = xmlDoc.GetElementsByTagName("ReadSMS");
                    string Msg1, Msg2, Msg3;
                    foreach (XmlNode node in formTag)
                    {
                        Msg1 = node.Attributes["text1"].Value;
                        Msg2 = node.Attributes["text2"].Value;
                        Msg3 = node.Attributes["text3"].Value;
                        smsurl = Msg1 + Recepient + Msg2 + Msg + Msg3;
                    }

                }
            }
            catch (Exception ex)
            {
            }
            WebRequest request = WebRequest.Create(smsurl);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            //read the response
            StreamReader responseReader = new StreamReader(response.GetResponseStream());
            resultMsg = responseReader.ReadToEnd();
    
            return true;
        }

        public int ValidateNewUser(ValidateOTP _data)
        {
            int result = 0;
            try
            {
                new DataBase();
                string cmdText = ConfigurationManager.AppSettings["ValidateNewUser"].ToString();
                using (SqlConnection sqlConnection = new SqlConnection(this.ConStr))
                {
                    SqlCommand expr_30 = new SqlCommand(cmdText, sqlConnection);
                    expr_30.CommandType = CommandType.StoredProcedure;
                    expr_30.Parameters.AddWithValue("@userName", _data.userName);
                    expr_30.Parameters.AddWithValue("@otpAction", 117);
                    expr_30.Parameters.AddWithValue("@otp", _data.otp);
                    sqlConnection.Open();
                    result = (int)expr_30.ExecuteScalar();
                    sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                base.LogExpection(ex);
            }
            return result;
        }
    }
}
