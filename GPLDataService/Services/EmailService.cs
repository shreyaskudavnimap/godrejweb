﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace GPLDataService.Services
{
    public class EmailService
    {
        public SmtpClient GetEmailConfiguration()
        {
            //SmtpClient smtp = new SmtpClient();
            //smtp.Host = "smtp.gmail.com";
            //smtp.Port = 587;
            //smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            //smtp.UseDefaultCredentials = false;
            //smtp.Credentials = new System.Net.NetworkCredential()
            //{
            //    UserName = "pmp4397@gmail.com",
            //    Password = "7350040607"
            //};
            //smtp.EnableSsl = true;

            // For Production
            SmtpClient smtp = new SmtpClient(ConfigurationManager.AppSettings["ClientHost"], Convert.ToInt32(ConfigurationManager.AppSettings["Port"]));
            smtp.Credentials = new System.Net.NetworkCredential()
            {
                UserName = ConfigurationManager.AppSettings["Email"],
                Password = ConfigurationManager.AppSettings["Password"]
            };
            smtp.EnableSsl = true;
            return smtp;
        }

        public void SendAnEmail(string OTP,string email)
        {
                try
                {
                string textBody = "Dear Sir/Madam,<br><br>" +
                "Your verification code is " + OTP + " to access the EOI and Booking Details section on the App.<br>"
                + "In case, you are facing any difficulties in login." +  
                "Please drop us an email on <a href = " + ConfigurationManager.AppSettings["EmailLink"].ToString() + " >" + ConfigurationManager.AppSettings["EmailLink"].ToString() + "</a>" +
                " <br><br>Regards, <br>Godrej Channel Partner Team ";

                string mailSubject = ConfigurationManager.AppSettings["MailSubject"].ToString();

                MailMessage mailMessage = new MailMessage();

                mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["Email"].ToString());
                mailMessage.Subject = mailSubject;
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = textBody;
                
                mailMessage.To.Add(new MailAddress(email));
                SmtpClient smtp = new SmtpClient();
                smtp = GetEmailConfiguration();
                smtp.Send(mailMessage);
            }
            catch (Exception e)
                {
                    var data = e.Message.ToString();
                }
            
        }
    }
}
