﻿using GPLDataService.Constants;
using GPLDataService.Inputs;
using GPLDataService.Outputs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLDataService.Services
{
    public class EOIShareService : ExceptionHandlerService
    {
        string ConStr = ConfigurationManager.ConnectionStrings[Constants.Connection.DBCONNECTIONSTRING].ConnectionString;

        public EOIShareService()
        {

        }

        public int SubmitEOIShareData(EOIShare eoiShare)
        {
            int ResponseCode = 0;
            try
            {
                string StoreProcedure = ConfigurationManager.AppSettings["AddEOIShare"].ToString();
               
                using (SqlConnection con = new SqlConnection(ConStr))
                {
                    SqlCommand cmd = new SqlCommand()
                    {
                        CommandText = StoreProcedure,
                        Connection = con
                    };
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@customerFirstName", eoiShare.customerFirstName );
                    cmd.Parameters.AddWithValue("@customerLastName", eoiShare.customerLastName );
                    cmd.Parameters.AddWithValue("@mobileNo", eoiShare.mobileNo);
                    cmd.Parameters.AddWithValue("@CityId", eoiShare.cityId );
                    cmd.Parameters.AddWithValue("@projectId", eoiShare.projectId);
                    cmd.Parameters.AddWithValue("@emailId", eoiShare.emailId);
                    cmd.Parameters.AddWithValue("@brokerId", eoiShare.brokerId);

                    con.Open();
                    ResponseCode = (int)cmd.ExecuteScalar();

                    con.Close();
                }
            }
            catch (Exception ex)
            {
                LogExpection(ex);
            }
            return ResponseCode;
        }

        public List<EOIStatus> GetEOIStatusData(string brokerId, string cityId, string projectId)
        {
            List<EOIStatus> data = new List<EOIStatus>();
            try
            {
                string StoreProcedure = ConfigurationManager.AppSettings["GetEOIStatus"].ToString();
               
                using (SqlConnection con = new SqlConnection(ConStr))
                {
                    SqlCommand cmd = new SqlCommand()
                    {
                        CommandText = StoreProcedure,
                        Connection = con
                    };
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@brokerId", brokerId);
                    cmd.Parameters.AddWithValue("@cityId", cityId);
                    cmd.Parameters.AddWithValue("@projectId", projectId);
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    data = new GenericReader<EOIStatus>().CreateList(rdr);
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                LogExpection(ex);
            }
            return data;
        }


        public Emapanelment GetLoginDetails(string username, string password)
        {
            Emapanelment emapanelment = new Emapanelment();
            try
            {
                string commandText = ConfigurationManager.AppSettings["GetLogin"].ToString();
                using (SqlConnection sqlConnection = new SqlConnection(this.ConStr))
                {
                    SqlCommand expr_2C = new SqlCommand();
                    expr_2C.CommandText = commandText;
                    expr_2C.Connection = sqlConnection;
                    expr_2C.CommandType = CommandType.StoredProcedure;
                    expr_2C.Parameters.AddWithValue("@uname", username);
                    expr_2C.Parameters.AddWithValue("@pwd", password);
                    sqlConnection.Open();
                    SqlDataReader sqlDataReader = expr_2C.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        emapanelment.isSentOTP = sqlDataReader["isSentOTP"].ToString();
                    }
                    if (sqlDataReader.NextResult())
                    {
                        while (sqlDataReader.Read())
                        {
                            emapanelment.userid = sqlDataReader["EmplID"].ToString();
                            emapanelment.brokerid = sqlDataReader["BrokerID"].ToString();
                            emapanelment.name = sqlDataReader["name"].ToString();
                            emapanelment.brokeraccountid = sqlDataReader["BrokerAccountID"].ToString();
                            emapanelment.email = sqlDataReader["email"].ToString();
                            emapanelment.mobileno = sqlDataReader["Mobile"].ToString();
                            emapanelment.panno = sqlDataReader["pan_no"].ToString();
                            emapanelment.brokercontactid = sqlDataReader["BrokerContactID"].ToString();
                        }
                    }
                    sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                base.LogExpection(ex);
            }
            return emapanelment;
        }


    }
}



