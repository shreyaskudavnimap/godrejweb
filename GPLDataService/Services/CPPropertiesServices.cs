﻿using GPLDataService.Constants;
using GPLDataService.Inputs;
using GPLDataService.Outputs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLDataService.Services
{
    public class CPPropertiesServices: ExceptionHandlerService
    {
        string ConStr = ConfigurationManager.ConnectionStrings[Constants.Connection.DBCONNECTIONSTRING].ConnectionString;
        public CPPropertiesServices()
        {
        }

        public IList<CPProperties> GetCPProperties(string quarter, string panno)
        {
            IList<CPProperties> cplist = new List<CPProperties>();
            SqlConnection con = new SqlConnection(ConStr);
            try
            {
                string StoreProcedure = ConfigurationManager.AppSettings["GetCPData"].ToString();
                SqlCommand cmd = new SqlCommand(StoreProcedure, con);
                cmd.CommandType = CommandType.StoredProcedure;
               
                cmd.Parameters.AddWithValue("@quarter", quarter);
                cmd.Parameters.AddWithValue("@brokerPanNo", panno);
                con.Open();

                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    CPProperties cp = new CPProperties();
                    cp.customerName = rdr["customerName"].ToString();
                    cp.projectName = rdr["projectName"].ToString();
                    cp.registrationDate = rdr["registrationDate"].ToString();
                    cp.value = rdr["value"].ToString();
                    cp.paymentStatus = rdr["paymentStatus"].ToString();
                    cplist.Add(cp);
                }

            }
            finally
            {
                IsConnectionOpenThenClose(con);
            }
            return cplist;
        }

        public IList<WalkIn> GetWalkInData(string quarter, string panno)
        {
            IList<WalkIn> walkInlist = new List<WalkIn>();
            SqlConnection con = new SqlConnection(ConStr);
            try
            {
                string StoreProcedure = ConfigurationManager.AppSettings["GetWalkInData"].ToString();
                SqlCommand cmd = new SqlCommand(StoreProcedure, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@quarter", quarter);
                cmd.Parameters.AddWithValue("@brokerPanNo", panno);
                con.Open();

                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    WalkIn walkin = new WalkIn();
                    walkin.customerName = rdr["customerName"].ToString();
                    walkin.projectName = rdr["projectName"].ToString();
                    walkin.mobileNo = rdr["mobileNo"].ToString();
                    walkin.walkInStatus = rdr["enquiryStatus"].ToString();
                    walkin.comments = rdr["comments"].ToString();
                    walkInlist.Add(walkin);
                }

            }
            finally
            {
                IsConnectionOpenThenClose(con);
            }
            return walkInlist;
        }
    }
}
