﻿using GPLDataService.Enums;
using GPLDataService.MasterResponse;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace GPLDataService.Services
{
    public class ExceptionHandlerService : ConnectionStatus
    {
        string filePath = ConfigurationManager.AppSettings["ExceptionFolderPath"].ToString();
        public void LogExpection(Exception ex)
        {
            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace +
                   "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            }
            SendAnEmail(ex);


        }
        public BaseResponse LogExceptionAndReturnErrorData(Exception ex)
        {

            BaseResponse _response = new BaseResponse();

            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace +
                   "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            }
            _response.error = new Error()
            {
                code = ex.Message.ToString(),
            };
            _response.status = Status.Fail.ToString();
            _response.statusCode = Convert.ToInt32(Status.Fail);
           
          
            SendAnEmail(ex);

            return _response;
        }

        public ExceptionResponse LogExceptionAndReturnError(Exception ex)
        {
           
            ExceptionResponse _response = new ExceptionResponse();

            Error error = new Error();

            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace +
                   "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            }
            _response.status = Status.Fail.ToString();
            _response.statusCode = Convert.ToInt32(Status.Fail);
            error.code = Convert.ToInt32(Status.Fail).ToString();
            error.message = "Something went wrong";
            error.innerError = new InnerError
            {
                date = System.DateTime.Now.ToString(),
            };
            _response.error = error;

            SendAnEmail(ex);
            return _response;
        }


        public void PrintLog(string data)
        {
            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine("Data :" + data + DateTime.Now.ToString());
                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            }
        }

        public void SendAnEmail(Exception ex)
        {
            bool isMailTriggerOptionEnable = Convert.ToBoolean(ConfigurationManager.AppSettings["ExceptionEmailService"].ToString());

            if (isMailTriggerOptionEnable)
            {
                string email = ConfigurationManager.AppSettings["Recipient"].ToString();
                string password = ConfigurationManager.AppSettings["ExcptnPwd"].ToString();
                string recipient = ConfigurationManager.AppSettings["Recipient"].ToString();
                try
                {
                    using (MailMessage mailMessage = new MailMessage())
                    {
                        StringBuilder sb = new StringBuilder();

                        sb.AppendFormat("Message: " + ex.Message + "<br />");
                        sb.AppendFormat("Exception Type: " + ex.GetType() + "<br/>");
                        sb.AppendFormat("Stack Trace: " + ex.StackTrace);

                        mailMessage.From = new MailAddress(email);
                        mailMessage.Subject = "Crash Report for GODREJ CHANNEL PARTNER API";
                        mailMessage.Body = sb.ToString();
                        mailMessage.IsBodyHtml = true;
                        mailMessage.To.Add(new MailAddress(recipient));
                        SmtpClient smtp = new SmtpClient();
                        smtp.Host = "smtp.gmail.com";
                        smtp.EnableSsl = true;
                        System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                        NetworkCred.UserName = email;
                        NetworkCred.Password = password;
                        smtp.UseDefaultCredentials = true;
                        smtp.Credentials = NetworkCred;
                        smtp.Port = 587;
                        smtp.Send(mailMessage);
                    }
                }
                catch (Exception e)
                {
                    var data = e.Message.ToString();
                }
            }
        }
    }
  
}
