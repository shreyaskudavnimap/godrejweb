﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GPLDataService.Common
{
    [DataContract]
    public class Banners
    {
        [DataMember]
        public string cityname { get; set; }

        [DataMember]
        public string status { get; set; }
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string imagename { get; set; }
        [DataMember]
        public string createddate { get; set; }
        [DataMember]
        public string relatedLink { get; set; }
        [DataMember]
        public string title { get; set; }
        [DataMember]
        public string caption { get; set; }
    }
}
