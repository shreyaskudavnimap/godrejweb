﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLDataService.Enums
{
    public enum OTPAction
    {
        Login = 111,
        SignUp = 112,
        ForgotPassword = 113,
        ChangePassword = 114,
        ChangeMobileNumber = 115,
        MenuShow = 116
    }
}
