﻿using GPLDataService.MasterResponse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLDataService.Outputs
{
    namespace GPLDataService.Outputs
    {
        public class QuarterList : BaseResponse
        {
            public IList<Quarter> data;
        }

        public class Quarter
        {
            public string quarter
            {
                get;
                set;
            }
        }
    }


}
