﻿using GPLDataService.MasterResponse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GPLDataService.Outputs
{
    [DataContract]
    public class CPPropertiesVM: BaseResponse
    {
        [DataMember]
        public IList<CPProperties> data { get; set; }
    }

    [DataContract]
    public class CPProperties
    {
        [DataMember]
        public string customerName { get; set; }
        [DataMember]
        public string projectName { get; set; }
        [DataMember]
        public string registrationDate { get; set; }
        [DataMember]
        public string value { get; set; }
        [DataMember]
        public string paymentStatus { get; set; }

    }
}
