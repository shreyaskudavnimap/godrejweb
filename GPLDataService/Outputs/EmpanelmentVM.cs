﻿using GPLDataService.MasterResponse;
using System;
using System.Runtime.Serialization;

namespace GPLDataService.Outputs
{
    [DataContract]
    public class EmpanelmentVM : BaseResponse
    {
        [DataMember]
        public Emapanelment data
        {
            get;
            set;
        }

    }


        [DataContract]
        public class Emapanelment
        {
            [DataMember]
            public string isSentOTP
            {
                get;
                set;
            }

            [DataMember]
            public string userid
            {
                get;
                set;
            }

            [DataMember]
            public string password
            {
                get;
                set;
            }

            [DataMember]
            public string brokerid
            {
                get;
                set;
            }

            [DataMember]
            public string name
            {
                get;
                set;
            }

            [DataMember]
            public string mobileno
            {
                get;
                set;
            }

            [DataMember]
            public string email
            {
                get;
                set;
            }

            [DataMember]
            public string panno
            {
                get;
                set;
            }

            [DataMember]
            public string company
            {
                get;
                set;
            }

            [DataMember]
            public string region
            {
                get;
                set;
            }

            [DataMember]
            public string image
            {
                get;
                set;
            }

            [DataMember]
            public string imagename
            {
                get;
                set;
            }

            [DataMember]
            public string status
            {
                get;
                set;
            }

            [DataMember]
            public string brokeraccountid
            {
                get;
                set;
            }

            [DataMember]
            public string brokercontactid
            {
                get;
                set;
            }
        }
 }


