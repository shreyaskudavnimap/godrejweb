﻿using GPLDataService.MasterResponse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GPLDataService.Outputs
{
    [DataContract]
    public class WalkInVM: BaseResponse
    {
        [DataMember]
        public IList<WalkIn> data { get; set; }
    }

    [DataContract]
    public class WalkIn
    {
        [DataMember]
        public string customerName { get; set; }
        [DataMember]
        public string projectName { get; set; }
        [DataMember]
        public string mobileNo { get; set; }
        [DataMember]
        public string walkInStatus { get; set; }
        [DataMember]
        public string comments { get; set; }
    }
}
