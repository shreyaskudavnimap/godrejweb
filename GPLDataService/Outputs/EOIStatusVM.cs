﻿using GPLDataService.MasterResponse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GPLDataService.Outputs
{
    [DataContract]
    public class EOIStatusVM: BaseResponse
    {
        [DataMember]
       public List<EOIStatus> data { get; set; }
    }
    [DataContract]
    public class EOIStatus
    {
        [DataMember]
        public string mobileNo { get; set; }
        [DataMember]
        public string dateOfSharing { get; set; }
        [DataMember]
        public bool eoiVisited { get; set; }
        [DataMember]
        public bool eoiSubmited { get; set; }
    }
}
