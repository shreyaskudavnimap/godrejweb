﻿using GPLDataService.MasterResponse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLDataService.Outputs
{
    public class OTP:BaseResponse
    {
        public int Otp { get; set; }
    }
}
