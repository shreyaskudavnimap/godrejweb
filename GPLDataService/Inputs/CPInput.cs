﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLDataService.Inputs
{
    public class CPInput
    {
        public string quarter
        {
            get;
            set;
        }

        public string brokerPanno
        {
            get;
            set;
        }
    }
}
