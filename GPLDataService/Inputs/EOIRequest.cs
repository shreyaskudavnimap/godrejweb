﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLDataService.Inputs
{
    public class EOIRequest
    {
        public string cityId { get; set; }
        public string projectId { get; set; }
    }
}
