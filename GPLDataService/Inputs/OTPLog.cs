﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLDataService.Inputs
{
    public class OTPLog
    {
        public string recepient { get; set; }
        public string resultMsg { get; set; }
        public string message { get; set; }
        public int otpAction { get; set; }
        public string OTP { get; set; }
    }
}
