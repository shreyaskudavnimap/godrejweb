﻿using System;

namespace GPLDataService.Inputs
{
    public class MenuRequest
    {
        public string brokerId
        {
            get;
            set;
        }

        public string brokerPanno
        {
            get;
            set;
        }

        public string mobileNo
        {
            get;
            set;
        }

        public string emailId
        {
            get;
            set;
        }
    }
}
