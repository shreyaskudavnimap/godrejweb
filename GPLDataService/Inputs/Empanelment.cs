﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GPLDataService.Inputs
{
    [DataContract]
    public class Empanelment
    {
        [DataMember]
        public string Password
        { get; set; }

        [DataMember]
        public string Company_Name
        { get; set; }

        [DataMember]
        public string entity_type
        { get; set; }

        [DataMember]
        public string FirstName
        { get; set; }

        [DataMember]
        public string LastName
        { get; set; }

        [DataMember]
        public string Mobile
        { get; set; }

        [DataMember]
        public string Email
        { get; set; }

        [DataMember]
        public string communication_address
        { get; set; }

        [DataMember]
        public string registered_address
        { get; set; }

        [DataMember]
        public string Pan_No
        { get; set; }

        [DataMember]
        public string pancerti
        { get; set; }

        [DataMember]
        public string BrokerID
        { get; set; }

        [DataMember]
        public string BrokerContactID
        { get; set; }

        [DataMember]
        public List<ReraDocs> ReraDoc
        { get; set; }

        [DataMember]
        public List<GstDocs> GstDoc
        { get; set; }

        [DataMember]
        public string status
        { get; set; }

        [DataMember]
        public string Billing_City
        { get; set; }

        [DataMember]
        public string Billing_Zip
        { get; set; }

        [DataMember]
        public string Billing_Street
        { get; set; }

        [DataMember]
        public string Billing_State
        { get; set; }

        [DataMember]
        public string Billing_Country
        { get; set; }

        [DataMember]
        public string Registered_City
        { get; set; }

        [DataMember]
        public string Registered_Zip
        { get; set; }

        [DataMember]
        public string Registered_Street
        { get; set; }

        [DataMember]
        public string Registered_State
        { get; set; }

        [DataMember]
        public string Registered_Country
        { get; set; }

        [DataMember]
        public string Communication_City
        { get; set; }

        [DataMember]
        public string Communication_Zip
        { get; set; }

        [DataMember]
        public string Communication_Street
        { get; set; }

        [DataMember]
        public string Communication_State
        { get; set; }

        [DataMember]
        public string Communication_Country
        { get; set; }
    }

    [DataContract]
    public class ReraDocs
    {
        [DataMember]
        public string state
        { get; set; }

        [DataMember]
        public string rerano
        { get; set; }

        [DataMember]
        public string reracerti
        { get; set; }
    }


    [DataContract]
    public class GstDocs
    {
        [DataMember]
        public string state
        { get; set; }

        [DataMember]
        public string gstno
        { get; set; }

        [DataMember]
        public string gstcerti
        { get; set; }
    }
}
