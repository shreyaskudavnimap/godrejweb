﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLDataService.Inputs
{
    public class EOIShare
    {
        public string brokerId { get; set; }
        public string customerFirstName { get; set; }
        public string customerLastName { get; set; }
        public string emailId { get; set; }
        public string mobileNo { get; set; }
        public int projectId { get; set; }
        public int cityId { get; set; }
    }
}
