﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLDataService.Inputs
{
    public class ValidateOTP
    {
        public string userName
        {
            get;
            set;
        }

        public string otp
        {
            get;
            set;
        }

        public string otpAction
        {
            get;
            set;
        }
    }
}
