﻿using GPLDataService.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLDataService.MasterResponse
{
    public class InvalidRequest
    {
        public ResponseBase BadParameters()
        {
            ResponseBase _response = new ResponseBase();

            _response.status = Status.Fail.ToString();
            _response.error = "Bad Parameters Or Invalid Token.";
            _response.errorCode = 498;

            return _response;
        }
    }
}
