﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GPLDataService.MasterResponse
{
  
    public class ExceptionResponse
    {
        public string status { get; set; }
        public int statusCode { get; set; }
        public Error error { get; set; }
    }
    
    public class ResponseBase
    {
        public string status { get; set; }
        public string error { get; set; }
        public int errorCode { get; set; }
    }
    [DataContract]
    public class BaseResponse
    {
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public int statusCode { get; set; }
        [DataMember]
        public Error error { get; set; }
    }

    public class Error
    {
        public string code { get; set; }
        public string message { get; set; }
        public InnerError innerError { get; set; }
    }

    public class InnerError
    {
        public string request_id { get; set; }
        public string date { get; set; }
    }
}
