﻿using System.Configuration;
using System.IO;

namespace GodrejChannelPartnerWCFService.Helper
{
    public class AttachmentHandler
    {
        private Stream _stream { get; set; }

        private string _fileName { get; set; }

        public AttachmentHandler(string fileName, Stream stream)
        {
            this._stream = stream;
            this._fileName = fileName;
        }

        public string SaveFileStream()
        {
            var filePath = ConfigurationManager.AppSettings["AttachmentPhysicalPath"].ToString();
            filePath = filePath + this._fileName;
            var fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write);
            _stream.CopyTo(fileStream);
            fileStream.Dispose();
            return filePath;
        }

        //public string SaveProfileImageToPhysicalLocation()
        //{
        //    var fileName = "";
        //    try
        //    {
        //        HttpPostedFile httpPostedFile = null;
        //        var filePath = ConfigurationManager.AppSettings["AttachmentPhysicalPath"].ToString();
        //        var extension = _postedFile.FileName.Substring(_postedFile.FileName.LastIndexOf('.')).ToLower();

        //        if (!Directory.Exists(filePath))
        //        {
        //            Directory.CreateDirectory(filePath); // if it doesn't exist, create
        //        }

        //        fileName = Guid.NewGuid().ToString() + extension;
        //        httpPostedFile.SaveAs(Path.Combine(filePath, fileName));
        //    }
        //    catch (Exception)
        //    {
        //        fileName = null;
        //        return "fail";
        //    }
        //    return "success";
        //}
    }
}