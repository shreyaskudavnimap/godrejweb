﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using GPLDataService.Common;
using GPLDataService.Inputs;
using GPLDataService.MasterResponse;
using GPLDataService.Outputs;

namespace GodrejChannelPartnerWCFService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IEOIService
    {

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "saveEOIShareData")]
        BaseResponse saveEOIShare(EOIShare eoiShare);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, 
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare,
         UriTemplate = "getEOIStatus/{brokerId}/{cityId}/{projectId}")]
         EOIStatusVM getEOIStatusData(string brokerId, string cityId, string projectId);

      
        [OperationContract, WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, 
         ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare,
         UriTemplate = "login/{username}/{password}")]
        EmpanelmentVM getLoginDetails(string username, string password);

      

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, 
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "getOTPForMenu")]
        OTP getOTPForMenu(MenuRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, 
            BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "verifyOtp")]
        BaseResponse VerifyOTP(ValidateOTP request);

    }



}
