﻿
using GPLDataService.Enums;
using GPLDataService.Inputs;
using GPLDataService.MasterResponse;
using GPLDataService.Outputs;
using GPLDataService.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace GodrejChannelPartnerWCFService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IEOIService
    {
        EOIShareService shareService = new EOIShareService();
        public BaseResponse saveEOIShare(EOIShare eoiShare)
        {
            BaseResponse response = new BaseResponse();

           
            try
            {
                if (eoiShare != null)
                {
                    IDictionary<int, BaseResponse> dictionary = new Dictionary<int, BaseResponse>();
                    dictionary.Add(200, new BaseResponse
                    {
                        status = Status.Success.ToString(),
                        statusCode = Convert.ToInt32(Status.Success)

                    });
                    dictionary.Add(400, new BaseResponse
                    {
                        statusCode = Convert.ToInt32(Status.Fail),
                        status = Status.Fail.ToString(),
                       
                    });
                  
                    OTPService otpService = new OTPService();
                    bool submitStatus = dictionary.TryGetValue(shareService.SubmitEOIShareData(eoiShare), out response);

                }
                else
                {
                    response.status = "Invalid parameters";
                    response.statusCode = Convert.ToInt32(Status.Fail);
                    response.error = new Error()
                    {
                        message = "One of the parameter is mising",
                    };
                }

            }
            catch (Exception ex)
            {
                ExceptionHandlerService _exceptionService = new ExceptionHandlerService();
                BaseResponse _exceptionResponse = new BaseResponse();
                _exceptionResponse = _exceptionService.LogExceptionAndReturnErrorData(ex);
                response.status = _exceptionResponse.status;
                response.error = _exceptionResponse.error;
                response.statusCode = _exceptionResponse.statusCode;
            }
           
            return response;
        }

        private string contentType = "application/json";
        public object HttpGet(string url)
        {
            object response = string.Empty;
            WebRequest httpWebRequest = WebRequest.Create(url);

            httpWebRequest.ContentType = contentType;
            httpWebRequest.Method = "GET";

            using (WebResponse httpResponse = httpWebRequest.GetResponse())
            {
                using (Stream dataStream = httpResponse.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(dataStream))
                    {
                        using (JsonReader sdr = new JsonTextReader(reader))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            response = serializer.Deserialize(sdr).ToString();

                        }
                    }
                }
            }
            return response;

        }

        public EOIStatusVM getEOIStatusData(string brokerId,string cityId, string projectId)
        {
            EOIStatusVM response = new EOIStatusVM();

            try
            {
                if (!string.IsNullOrEmpty(brokerId) && !string.IsNullOrEmpty(cityId) && !string.IsNullOrEmpty(projectId))
                {
                    response.data = shareService.GetEOIStatusData(brokerId,cityId,projectId);
                    if (response.data != null)
                    {
                        response.status = Status.Success.ToString();
                        response.statusCode = Convert.ToInt32(Status.Success);
                    }
                    else
                    {
                        response.status = Status.Fail.ToString();
                        response.statusCode = Convert.ToInt32(Status.Fail);
                    }
                }
                else
                {
                    response.status = Status.Fail.ToString();
                    response.statusCode = Convert.ToInt32(Status.Fail);
                   
                }

            }
            catch (Exception ex)
            {
                ExceptionHandlerService _exceptionService = new ExceptionHandlerService();
                BaseResponse _exceptionResponse = new BaseResponse();
                _exceptionResponse = _exceptionService.LogExceptionAndReturnErrorData(ex);
                response.status = _exceptionResponse.status;
                response.error = _exceptionResponse.error;
                response.statusCode = _exceptionResponse.statusCode;
            }

            return response;
        }

        // GodrejChannelPartnerWCFService.Service1
        public EmpanelmentVM getLoginDetails(string username, string password)
        {
            EmpanelmentVM empanelmentVM = new EmpanelmentVM();
            try
            {
                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                {
                    empanelmentVM.data = this.shareService.GetLoginDetails(username, password);
                    empanelmentVM.status = Status.Success.ToString();
                    empanelmentVM.statusCode = (Convert.ToInt32(Status.Success));
                }
                else
                {
                    empanelmentVM.statusCode = (Convert.ToInt32(Status.Fail));
                    empanelmentVM.status = Status.Fail.ToString();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandlerService _exceptionService = new ExceptionHandlerService();
                BaseResponse _exceptionResponse = new BaseResponse();
                _exceptionResponse = _exceptionService.LogExceptionAndReturnErrorData(ex);
                empanelmentVM.status = _exceptionResponse.status;
                empanelmentVM.error = _exceptionResponse.error;
                empanelmentVM.statusCode = _exceptionResponse.statusCode;
            }
            return empanelmentVM;
        }

        // GodrejChannelPartnerWCFService.Service1
        public OTP getOTPForMenu(MenuRequest request)
        {
            OTP response = new OTP();
            OTPService oTPService = new OTPService();
            EmailService emailService = new EmailService();
            try
            {
                if (request != null)
                {
                    int otp = oTPService.ExistingOtp(request.mobileNo,request.emailId);
                    
                    if (otp == 0)
                    {
                        string OTPs = oTPService.SendOTP(request.mobileNo, "", 117);
                        emailService.SendAnEmail(OTPs, request.emailId);
                        response.status = Status.Success.ToString();
                        response.statusCode = Convert.ToInt32(Status.Success);
                    }
                    else
                    {
                        response.status = Status.Success.ToString();
                        response.statusCode = Convert.ToInt32(Status.Success);
                        response.Otp = otp;       
                    }
                }
                else
                {
                    response.status = Status.Fail.ToString();
                    response.statusCode = Convert.ToInt32(Status.Fail);
                }
            }
            catch (Exception ex)
            {
                ExceptionHandlerService _exceptionService = new ExceptionHandlerService();
                BaseResponse _exceptionResponse = new BaseResponse();
                _exceptionResponse = _exceptionService.LogExceptionAndReturnErrorData(ex);
                response.status = _exceptionResponse.status;
                response.error = _exceptionResponse.error;
                response.statusCode = _exceptionResponse.statusCode;
            }
            return response;
        }

        // GodrejChannelPartnerWCFService.Service1
        public BaseResponse VerifyOTP(ValidateOTP request)
        {
            BaseResponse response = new BaseResponse();
            OTPService oTPService = new OTPService();
            try
            {
                if (request != null)
                {
                    IDictionary<int, BaseResponse> dictionary = new Dictionary<int, BaseResponse>();
                    dictionary.Add(200, new BaseResponse
                    {
                        status = Status.Success.ToString(),
                        statusCode = Convert.ToInt32(Status.Success)

                    });
                    dictionary.Add(498, new BaseResponse
                    {
                        statusCode = Convert.ToInt32(Status.Fail),
                        status = "Invalid OTP/OTP has been Expired.",

                    });
                    OTPService otpService = new OTPService();
                    bool submitStatus = dictionary.TryGetValue(otpService.ValidateNewUser(request), out response);

                }
                else
                {
                    response.status = Status.Fail.ToString();
                    response.statusCode = Convert.ToInt32(Status.Fail);
                }
            }
            catch (Exception ex)
            {
                ExceptionHandlerService _exceptionService = new ExceptionHandlerService();
                BaseResponse _exceptionResponse = new BaseResponse();
                _exceptionResponse = _exceptionService.LogExceptionAndReturnErrorData(ex);
                response.status = _exceptionResponse.status;
                response.error = _exceptionResponse.error;
                response.statusCode = _exceptionResponse.statusCode;
            }
            return response;
        }


    }
}
