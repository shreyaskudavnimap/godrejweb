﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace GodrejChannelPartnerWCFService
{
    public class WLog
    {

        public static void WriteLog(string textToLog, string logfilePrefix)
        {
            string rootPhysicalPath = "";
            if (HttpRuntime.AppDomainAppId != null)
            {
                rootPhysicalPath = HttpContext.Current.Server.MapPath("~");
            }
            else
            {
                rootPhysicalPath = "";
            }
            rootPhysicalPath = rootPhysicalPath + @"\Log\";
            if (!Directory.Exists(rootPhysicalPath))
            {
                Directory.CreateDirectory(rootPhysicalPath);
            }
            textToLog = "\n" + textToLog;

            string filePath = rootPhysicalPath + logfilePrefix + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + ".log";
            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.Write(textToLog);
            }
        }
    }
}