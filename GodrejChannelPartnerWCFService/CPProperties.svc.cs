﻿using GodrejChannelPartnerWCFService.Helper;
using GPLDataService.Enums;
using GPLDataService.MasterResponse;
using GPLDataService.Outputs;
using GPLDataService.Services;
using HttpMultipartParser;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Reflection;

namespace GodrejChannelPartnerWCFService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CPProperties" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select CPProperties.svc or CPProperties.svc.cs at the Solution Explorer and start debugging.
    public class CPProperties : ICPProperties
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[GPLDataService.Constants.Connection.DBCONNECTIONSTRING].ConnectionString);
        // string ConStr = ConfigurationManager.ConnectionStrings[GPLDataService.Constants.Connection.DBCONNECTIONSTRING].ConnectionString;
        public QuarterList getQuarterList(string panNo, string flag)
        {
            QuarterList response = new QuarterList();

            try
            {
                QuartersService quartersService = new QuartersService();
                response.data = quartersService.GetQuarter(panNo, flag);
                if (response.data != null)
                {
                    response.status = Status.Success.ToString();
                    response.statusCode = Convert.ToInt32(Status.Success);
                }
                else
                {
                    response.status = Status.Fail.ToString();
                    response.statusCode = Convert.ToInt32(Status.Fail);
                }


            }
            catch (Exception ex)
            {
                ExceptionHandlerService _exceptionService = new ExceptionHandlerService();
                BaseResponse _exceptionResponse = new BaseResponse();
                _exceptionResponse = _exceptionService.LogExceptionAndReturnErrorData(ex);
                response.status = _exceptionResponse.status;
                response.error = _exceptionResponse.error;
                response.statusCode = _exceptionResponse.statusCode;
            }

            return response;
        }


        public CPPropertiesVM getCPPropertiesData(string quarter, string panno)
        {
            CPPropertiesVM response = new CPPropertiesVM();

            try
            {
                CPPropertiesServices cPPropertiesServices = new CPPropertiesServices();
                if (!string.IsNullOrEmpty(quarter) && !string.IsNullOrEmpty(panno))
                {
                    response.data = cPPropertiesServices.GetCPProperties(quarter, panno);
                    if (response.data != null)
                    {
                        response.status = Status.Success.ToString();
                        response.statusCode = Convert.ToInt32(Status.Success);
                    }
                    else
                    {
                        response.status = Status.Fail.ToString();
                        response.statusCode = Convert.ToInt32(Status.Fail);
                    }
                }
                else
                {
                    response.status = Status.Fail.ToString();
                    response.statusCode = Convert.ToInt32(Status.Fail);

                }

            }
            catch (Exception ex)
            {
                ExceptionHandlerService _exceptionService = new ExceptionHandlerService();
                BaseResponse _exceptionResponse = new BaseResponse();
                _exceptionResponse = _exceptionService.LogExceptionAndReturnErrorData(ex);
                response.status = _exceptionResponse.status;
                response.error = _exceptionResponse.error;
                response.statusCode = _exceptionResponse.statusCode;
            }

            return response;
        }

        public WalkInVM getWalkInData(string quarter, string panno)
        {
            WalkInVM response = new WalkInVM();

            try
            {
                CPPropertiesServices cPPropertiesServices = new CPPropertiesServices();
                if (!string.IsNullOrEmpty(quarter) && !string.IsNullOrEmpty(panno))
                {
                    response.data = cPPropertiesServices.GetWalkInData(quarter, panno);
                    if (response.data != null)
                    {
                        response.status = Status.Success.ToString();
                        response.statusCode = Convert.ToInt32(Status.Success);
                    }
                    else
                    {
                        response.status = Status.Fail.ToString();
                        response.statusCode = Convert.ToInt32(Status.Fail);
                    }
                }
                else
                {
                    response.status = Status.Fail.ToString();
                    response.statusCode = Convert.ToInt32(Status.Fail);

                }

            }
            catch (Exception ex)
            {
                ExceptionHandlerService _exceptionService = new ExceptionHandlerService();
                BaseResponse _exceptionResponse = new BaseResponse();
                _exceptionResponse = _exceptionService.LogExceptionAndReturnErrorData(ex);
                response.status = _exceptionResponse.status;
                response.error = _exceptionResponse.error;
                response.statusCode = _exceptionResponse.statusCode;
            }

            return response;
        }

        public Banners[] gethomeBanners()
        {
            List<Banners> Bannerdetails = new List<Banners>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetHomeBanners", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Banners b = new Banners();
                        b.status = "success";
                        b.id = Convert.ToString(ds.Tables[0].Rows[i]["id"]);
                        b.imagename = ConfigurationManager.AppSettings["BaseUrl"] + ConfigurationManager.AppSettings["BannerPath"] + Convert.ToString(ds.Tables[0].Rows[i]["imagename"]);
                        b.createddate = Convert.ToString(ds.Tables[0].Rows[i]["createddate"]);
                        b.relatedLink = Convert.ToString(ds.Tables[0].Rows[i]["relatedLink"]);
                        b.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        b.caption = Convert.ToString(ds.Tables[0].Rows[i]["caption"]);
                        b.cityname = Convert.ToString(ds.Tables[0].Rows[i]["cityname"]);
                        Bannerdetails.Add(b);
                    }
                    return Bannerdetails.ToArray();
                }
                else
                {
                    Banners b = new Banners();
                    b.status = "fail";
                    b.id = "null";
                    b.imagename = "null";
                    b.createddate = "null";
                    b.relatedLink = "null";
                    b.title = "null";
                    b.caption = "null";
                    Bannerdetails.Add(b);
                    return Bannerdetails.ToArray();
                }
            }
            catch (Exception ex)
            {
                Banners b = new Banners();
                b.status = ex.Message.ToString();
                b.id = "null";
                b.imagename = "null";
                b.createddate = "null";
                b.relatedLink = "null";
                b.title = "null";
                b.caption = "null";
                Bannerdetails.Add(b);
                return Bannerdetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        public commonOutput InsertEmpl(GPLDataService.Inputs.Empanelment empl)
        {
            //Convert Datatable of rera
            commonOutput obj = new commonOutput();
            DataTable dtrera = new DataTable();
            if (empl.ReraDoc.Count > 0)
                dtrera = ToDataTable(empl.ReraDoc);

            //Convert Datatable of gst
            DataTable dtgst = new DataTable();
            if (empl.GstDoc.Count > 0)
                dtgst = ToDataTable(empl.GstDoc);
            string EmplID = "";
            //if (conn.State != ConnectionState.Open)
            //    conn.Open();

            SqlCommand cmd = new SqlCommand("SP_InsertEmplDetails", conn);

            cmd.Parameters.AddWithValue("@company", empl.Company_Name);
            cmd.Parameters.AddWithValue("@entitytype", empl.entity_type);
            cmd.Parameters.AddWithValue("@FirstName", empl.FirstName);
            cmd.Parameters.AddWithValue("@LastName", empl.LastName);
            cmd.Parameters.AddWithValue("@mobile", empl.Mobile);
            cmd.Parameters.AddWithValue("@email", empl.Email);
            cmd.Parameters.AddWithValue("@commaddress", empl.communication_address);
            cmd.Parameters.AddWithValue("@regaddress", empl.registered_address);
            cmd.Parameters.AddWithValue("@panno", empl.Pan_No);
            cmd.Parameters.AddWithValue("@pancerti", empl.pancerti);
            cmd.Parameters.AddWithValue("@Billing_City", empl.Billing_City);
            cmd.Parameters.AddWithValue("@Billing_Zip", empl.Billing_Zip);
            cmd.Parameters.AddWithValue("@Billing_Street", empl.Billing_Street);
            cmd.Parameters.AddWithValue("@Billing_State", empl.Billing_State);
            cmd.Parameters.AddWithValue("@Billing_Country", empl.Billing_Country);
            cmd.Parameters.AddWithValue("@Registered_City", empl.Registered_City);
            cmd.Parameters.AddWithValue("@Registered_Zip", empl.Registered_Zip);
            cmd.Parameters.AddWithValue("@Registered_Street", empl.Registered_Street);
            cmd.Parameters.AddWithValue("@Registered_State", empl.Registered_State);
            cmd.Parameters.AddWithValue("@Registered_Country", empl.Registered_Country);
            cmd.Parameters.AddWithValue("@Communication_City", empl.Communication_City);
            cmd.Parameters.AddWithValue("@Communication_Zip", empl.Communication_Zip);
            cmd.Parameters.AddWithValue("@Communication_Street", empl.Communication_Street);
            cmd.Parameters.AddWithValue("@Communication_State", empl.Communication_State);
            cmd.Parameters.AddWithValue("@Communication_Country", empl.Communication_Country);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@ReraDocsList", dtrera);
            //cmd.Parameters.AddWithValue("@GstDocsList", dtgst);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    // message = Common_Function.ConvertResultToAlert(dr, queryString, model.User);
                    EmplID = dr["EmplID"].ToString();
                    dr.Close();
                    if (dtrera.Rows.Count > 0)
                    {
                        foreach (DataRow row in dtrera.Rows)
                        {
                            InsertRera(row, EmplID);
                        }
                    }


                    if (dtgst.Rows.Count > 0)
                    {
                        foreach (DataRow row in dtgst.Rows)
                        {
                            InsertGst(row, EmplID);
                        }
                    }
                    /*
                    MailMessage mail1 = new MailMessage();
                    SmtpClient SmtpServer1 = new SmtpClient(ConfigurationManager.AppSettings["ClientHost"]);

                    mail1.From = new MailAddress(ConfigurationManager.AppSettings["ClientFrom"]);
                    mail1.To.Add(empl.Email);
                    mail1.Subject = "Godrej Partner Connect APP | Empanelment request";
                    mail1.IsBodyHtml = true;
                    string path1 = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                    StreamReader sr1 = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Mailer/empl_user.html"));
                    mail1.Body = sr1.ReadToEnd().Replace("user_name", empl.FirstName + ' ' + empl.LastName);
                    SmtpServer1.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
                    SmtpServer1.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["ClientFrom"], ConfigurationManager.AppSettings["ClientPwd"]);
                    SmtpServer1.EnableSsl = true;
                    SmtpServer1.Send(mail1);
                    sr1.Close();

                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["ClientHost"]);
                    mail.From = new MailAddress(ConfigurationManager.AppSettings["ClientFrom"]);
                   
                    mail.To.Add("sujata.mirge@atritechnocrat.com");
                    //mail.To.Add("swati.saxena@godrejproperties.com");
                    mail.To.Add("snehal.lanjekar@godrejproperties.com");
                   
                    mail.Subject = "CP Documents";
                    mail.IsBodyHtml = true;
                    string Uplodefile = ConfigurationManager.AppSettings["AttachmentPhysicalPath"];
                    mail.Attachments.Add(new Attachment(Uplodefile + empl.pancerti));
                    string rera_state = "";
                    string rera_no = "";
                    string gst_state = "";
                    string gst_no = "";
                    if (dtrera.Rows.Count > 0)
                    {
                        foreach (DataRow row in dtrera.Rows)
                        {
                            rera_state += row["state"].ToString();
                            rera_no += row["rerano"].ToString();
                            mail.Attachments.Add(new Attachment(Uplodefile + row["reracerti"].ToString()));
                        }
                    }

                    if (dtgst.Rows.Count > 0)
                    {
                        foreach (DataRow row in dtgst.Rows)
                        {
                            gst_state += row["state"].ToString();
                            gst_no += row["gstno"].ToString();
                            mail.Attachments.Add(new Attachment(Uplodefile + row["gstcerti"].ToString()));
                        }
                    }
                    string commaddress = empl.Communication_Street + ' ' + empl.Communication_City + ' ' + empl.Communication_Zip + ' ' + empl.Communication_State + ' ' + empl.Communication_Country;
                    string regaddress = empl.Registered_Street + ' ' + empl.Registered_City + ' ' + empl.Registered_Zip + ' ' + empl.Registered_State + ' ' + empl.Registered_Country;

                    string path = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                    StreamReader sr = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Mailer/admin_mail.html"));
                    mail.Body = sr.ReadToEnd().Replace("user_name", empl.FirstName + ' ' + empl.LastName).Replace("user_mobile", empl.Mobile).Replace("user_email", empl.Email).Replace("user_company", empl.Company_Name).Replace("user_entitytype", empl.entity_type).Replace("user_commaddress", commaddress).Replace("user_regaddress", regaddress).Replace("user_pan", empl.Pan_No)
                    .Replace("user_rera_state", rera_state).Replace("user_rera_no", rera_no).Replace("user_gst_state", gst_state).Replace("user_gst_no", gst_no);
                    SmtpServer.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
                    // SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                    // SmtpServer.UseDefaultCredentials = false;
                    SmtpServer.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["ClientFrom"], ConfigurationManager.AppSettings["ClientPwd"]);
                    SmtpServer.EnableSsl = true;
                    SmtpServer.Send(mail);
                    sr.Close();


                    return "success";
                    */
                }
            }

            //cmd.Parameters.AddWithValue("@brokerid", empl.brokerid);
            //cmd.Parameters.Add("@outID", SqlDbType.Char, 500);
            //cmd.Parameters["@outID"].Direction = ParameterDirection.Output;
            //cmd.Connection = conn;
            // cmd.CommandType = System.Data.CommandType.StoredProcedure;

            //int result = cmd.ExecuteNonQuery();

            //EmplID = cmd.Parameters["@outID"].Value.ToString().Trim();
            //if (result == 1)
            //{
            //    status = "success";
            //}
            //else
            //{
            //    status = "fail";
            //}

            conn.Close();

            //return status;
            //return "{\"EmplID\":\"" + EmplID + "\"}";
            return obj;
        }
        public bool InsertRera(DataRow row, string EmplID)
        {
            SqlCommand cmd = new SqlCommand("SP_InsertEmplReraDocs", conn);

            cmd.Parameters.AddWithValue("@EmplID", EmplID);
            cmd.Parameters.AddWithValue("@state", row["state"]);
            cmd.Parameters.AddWithValue("@rerano", row["rerano"]);
            cmd.Parameters.AddWithValue("@reracerti", row["reracerti"]);
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            int result = cmd.ExecuteNonQuery();

            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool InsertGst(DataRow row, string EmplID)
        {
            SqlCommand cmd = new SqlCommand("SP_InsertEmplGstDocs", conn);

            cmd.Parameters.AddWithValue("@EmplID", EmplID);
            cmd.Parameters.AddWithValue("@state", row["state"]);
            cmd.Parameters.AddWithValue("@gstno", row["gstno"]);
            cmd.Parameters.AddWithValue("@gstcerti", row["gstcerti"]);
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            int result = cmd.ExecuteNonQuery();

            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public string SaveAttachment(Stream stream)
        {
            /*
            MultipartFormDataParser parser = new MultipartFormDataParser(stream);
            string imagePath = "";
            try
            {
                var postedFile = parser.Files[0]; // "attachment" this is a key
                

                if (postedFile != null)
                {
                    AttachmentHandler _handler = new AttachmentHandler(postedFile.FileName, postedFile.Data);

                    imagePath = _handler.SaveFileStream();
                    imagePath = "success";
                }
                else
                {
                    imagePath = "fail";
                }


            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                imagePath = "Unable to save file";
            }

            return imagePath;
            */
            string imagePath = "";
            try
            {
                MultipartFormDataParser parser = new MultipartFormDataParser(stream);
                var postedFile = parser.Files[0];
                var bytes = ReadFully(postedFile.Data);
                //StreamReader reader = new StreamReader(stream);
                string path = ConfigurationManager.AppSettings["AttachmentPhysicalPath"];
                File.WriteAllBytes(path + postedFile.FileName, bytes);
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                imagePath = "Unable to save file";
                WLog.WriteLog("ex" + ex.Message + "\n ex " + ex.InnerException, "SaveAttachment");
            }
            return imagePath;
        }

        static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}

public class commonOutput
{
    public string status { get; set; }

}
