﻿using GPLDataService.Outputs;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace GodrejChannelPartnerWCFService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICPProperties" in both code and config file together.
    [ServiceContract]
    public interface ICPProperties
    {
        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json,
              ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare,
           UriTemplate = "getQuarters/{panNo}/{flag}")]
        QuarterList getQuarterList(string panNo, string flag);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json,
             ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare,
          UriTemplate = "getCPProperties/{quarter}/{panNo}")]
        CPPropertiesVM getCPPropertiesData(string quarter, string panNo);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare,
         UriTemplate = "getWalkInData/{quarter}/{panNo}")]
        WalkInVM getWalkInData(string quarter, string panNo);

        [OperationContract]
        [WebInvoke(Method = "POST",
                   RequestFormat = WebMessageFormat.Json,
                   ResponseFormat = WebMessageFormat.Json,
                   UriTemplate = "InsertEmpl")]
        commonOutput InsertEmpl(GPLDataService.Inputs.Empanelment empl);

        [OperationContract]
        [WebInvoke(Method = "GET",
          ResponseFormat = WebMessageFormat.Json,
          BodyStyle = WebMessageBodyStyle.Wrapped,
          UriTemplate = "getbannerjson")]
        Banners[] gethomeBanners();


        [OperationContract]
        [WebInvoke(Method = "POST",
          ResponseFormat = WebMessageFormat.Json,
          BodyStyle = WebMessageBodyStyle.Wrapped,
          UriTemplate = "attachment/upload")]
        string SaveAttachment(Stream stream);


    }
}
