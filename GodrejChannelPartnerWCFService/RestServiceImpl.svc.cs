﻿using GPLDataService.Inputs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Mail;

namespace GodrejChannelPartnerWCFService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RestServiceImpl" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select RestServiceImpl.svc or RestServiceImpl.svc.cs at the Solution Explorer and start debugging.
    public class RestServiceImpl : IRestServiceImpl
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[GPLDataService.Constants.Connection.DBCONNECTIONSTRING].ConnectionString);
        string constring = ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString;
        string BaseURL = ConfigurationManager.AppSettings["BaseUrl"];
        string Base = "http://netbizlabs.com/godrejchap_admin/";

        #region IRestServiceImpl Members

        #region Method to device id for android users

        public UserInfo[] FetchAndroidUser()
        {
            List<UserInfo> Bannerdetails = new List<UserInfo>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetAndroidUser", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        UserInfo b = new UserInfo();
                        b.status = "success";
                        b.username = Convert.ToString(ds.Tables[0].Rows[i]["Name"]);
                        b.usermobile = Convert.ToString(ds.Tables[0].Rows[i]["Mobile"]);
                        b.useremail = Convert.ToString(ds.Tables[0].Rows[i]["email"]);
                        b.deviceid = Convert.ToString(ds.Tables[0].Rows[i]["Deviceid"]);
                        Bannerdetails.Add(b);
                    }
                    return Bannerdetails.ToArray();
                }
                else
                {
                    UserInfo b = new UserInfo();
                    b.status = "fail";
                    b.username = "null";
                    b.usermobile = "null";
                    b.useremail = "null";
                    b.deviceid = "null";
                    Bannerdetails.Add(b);
                    return Bannerdetails.ToArray();
                }
            }
            catch (Exception ex)
            {
                UserInfo b = new UserInfo();
                b.status = ex.Message.ToString();
                b.username = "null";
                b.usermobile = "null";
                b.useremail = "null";
                b.deviceid = "null";
                Bannerdetails.Add(b);
                return Bannerdetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to device id for ios users

        public UserInfo[] FetchIosUser()
        {
            List<UserInfo> Bannerdetails = new List<UserInfo>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetIosUser", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        UserInfo b = new UserInfo();
                        b.status = "success";
                        b.username = Convert.ToString(ds.Tables[0].Rows[i]["Name"]);
                        b.usermobile = Convert.ToString(ds.Tables[0].Rows[i]["Mobile"]);
                        b.useremail = Convert.ToString(ds.Tables[0].Rows[i]["email"]);
                        b.deviceid = Convert.ToString(ds.Tables[0].Rows[i]["Deviceid"]);
                        Bannerdetails.Add(b);
                    }
                    return Bannerdetails.ToArray();
                }
                else
                {
                    UserInfo b = new UserInfo();
                    b.status = "fail";
                    b.username = "null";
                    b.usermobile = "null";
                    b.useremail = "null";
                    b.deviceid = "null";
                    Bannerdetails.Add(b);
                    return Bannerdetails.ToArray();
                }
            }
            catch (Exception ex)
            {
                UserInfo b = new UserInfo();
                b.status = ex.Message.ToString();
                b.username = "null";
                b.usermobile = "null";
                b.useremail = "null";
                b.deviceid = "null";
                Bannerdetails.Add(b);
                return Bannerdetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to update Device ID

        public EmpanelmentContact[] GetDevice(string userid, string deviceid, string platform)
        {
            List<EmpanelmentContact> lsproject = new List<EmpanelmentContact>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_UpdateDevice", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@userid", userid);
                comm.Parameters.AddWithValue("@deviceid", deviceid);
                comm.Parameters.AddWithValue("@platform", platform);
                SqlParameter pmout = new SqlParameter();
                pmout.ParameterName  = "@outStatus";
                pmout.DbType = DbType.String;
                pmout.Size = 1024;
                pmout.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout);
                Int32 sqlRows = comm.ExecuteNonQuery();
                EmpanelmentContact objBroker = new EmpanelmentContact();
                if (sqlRows != 0)
                {
                    objBroker.status = comm.Parameters["@outStatus"].Value.ToString();
                    lsproject.Add(objBroker);
                }
                else
                {
                    objBroker.status = "fail";
                    lsproject.Add(objBroker);
                }
                return lsproject.ToArray();
            }
            catch (Exception)
            {
                EmpanelmentContact objBroker = new EmpanelmentContact();
                objBroker.status = "fail";
                lsproject.Add(objBroker);
                return lsproject.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to update password as per brokerid

        public EmpanelmentContact[] RemovePic(string brokerid)
        {
            List<EmpanelmentContact> lsEmpanelment = new List<EmpanelmentContact>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_UpdateEmpl", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@brokerid", brokerid);
                comm.Parameters.AddWithValue("@profilepic", "NULL");
                SqlParameter pmout = new SqlParameter();
                pmout.ParameterName = "@outStatus";
                pmout.DbType = DbType.String;
                pmout.Size = 1024;
                pmout.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout);

                Int32 sqlRows = comm.ExecuteNonQuery();
                EmpanelmentContact objEmpanelment = new EmpanelmentContact();
                if (sqlRows != 0)
                {
                    objEmpanelment.status = comm.Parameters["@outStatus"].Value.ToString();
                    lsEmpanelment.Add(objEmpanelment);
                }
                else
                {
                    objEmpanelment.status = "fail";
                    lsEmpanelment.Add(objEmpanelment);
                }
                return lsEmpanelment.ToArray();
            }
            catch (Exception)
            {
                EmpanelmentContact objEmpanelment = new EmpanelmentContact();
                objEmpanelment.status = "fail";
                lsEmpanelment.Add(objEmpanelment);
                return lsEmpanelment.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to fetch password as per brokerid

        public EmpanelmentContact[] FetchEmail(string email)
        {
            List<EmpanelmentContact> lsEmpanelment = new List<EmpanelmentContact>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetEmail", conn);
                comm.Parameters.AddWithValue("@email", email);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    EmpanelmentContact[] b = new EmpanelmentContact[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        EmpanelmentContact objEmpanelment = new EmpanelmentContact();
                        objEmpanelment.status = "success";
                        lsEmpanelment.Add(objEmpanelment);
                    }
                }
                else
                {
                    EmpanelmentContact objEmpanelment = new EmpanelmentContact();
                    objEmpanelment.status = "fail";
                    lsEmpanelment.Add(objEmpanelment);
                }
                return lsEmpanelment.ToArray();
            }
            catch (Exception)
            {
                EmpanelmentContact objEmpanelment = new EmpanelmentContact();
                objEmpanelment.status = "fail";
                lsEmpanelment.Add(objEmpanelment);
                return lsEmpanelment.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to request for password as per brokerid

        public EmpanelmentContact[] Password(string email)
        {
            List<EmpanelmentContact> lsEmpanelment = new List<EmpanelmentContact>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_RequestPassword", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@email", email);
                SqlParameter pmout = new SqlParameter();
                pmout.ParameterName = "@outStatus";
                pmout.DbType = DbType.String;
                pmout.Size = 1024;
                pmout.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout);

                Int32 sqlRows = comm.ExecuteNonQuery();
                EmpanelmentContact objEmpanelment = new EmpanelmentContact();
                if (sqlRows != 0)
                {
                    objEmpanelment.status = comm.Parameters["@outStatus"].Value.ToString();
                    lsEmpanelment.Add(objEmpanelment);
                }
                else
                {
                    objEmpanelment.status = "fail";
                    lsEmpanelment.Add(objEmpanelment);
                }
                return lsEmpanelment.ToArray();
            }
            catch (Exception)
            {
                EmpanelmentContact objEmpanelment = new EmpanelmentContact();
                objEmpanelment.status = "fail";
                lsEmpanelment.Add(objEmpanelment);
                return lsEmpanelment.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to fetch password as per brokerid

        public EmpanelmentContact[] FetchPassword(string brokerid)
        {
            List<EmpanelmentContact> lsEmpanelment = new List<EmpanelmentContact>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetPassword", conn);
                comm.Parameters.AddWithValue("@brokerid", brokerid);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    EmpanelmentContact[] b = new EmpanelmentContact[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        EmpanelmentContact objEmpanelment = new EmpanelmentContact();
                        objEmpanelment.status = "success";
                        objEmpanelment.password = Convert.ToString(ds.Tables[0].Rows[i]["password"]);
                        objEmpanelment.email = Convert.ToString(ds.Tables[0].Rows[i]["Email"]);
                        lsEmpanelment.Add(objEmpanelment);
                    }
                }
                else
                {
                    EmpanelmentContact objEmpanelment = new EmpanelmentContact();
                    objEmpanelment.status = "fail";
                    objEmpanelment.password = "null";
                    objEmpanelment.email = "null";
                    lsEmpanelment.Add(objEmpanelment);
                }
                return lsEmpanelment.ToArray();
            }
            catch (Exception)
            {
                EmpanelmentContact objEmpanelment = new EmpanelmentContact();
                objEmpanelment.status = "fail";
                objEmpanelment.password = "null";
                objEmpanelment.email = "null";
                lsEmpanelment.Add(objEmpanelment);
                return lsEmpanelment.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to update password as per brokerid

        public EmpanelmentContact[] UpdatePassword(string brokerid, string password)
        {
            List<EmpanelmentContact> lsEmpanelment = new List<EmpanelmentContact>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_UpdatePassword", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@brokerid", brokerid);
                comm.Parameters.AddWithValue("@password", password);
                SqlParameter pmout = new SqlParameter();
                pmout.ParameterName = "@outStatus";
                pmout.DbType = DbType.String;
                pmout.Size = 1024;
                pmout.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout);

                Int32 sqlRows = comm.ExecuteNonQuery();
                EmpanelmentContact objEmpanelment = new EmpanelmentContact();
                if (sqlRows != 0)
                {
                    objEmpanelment.status = comm.Parameters["@outStatus"].Value.ToString();
                    lsEmpanelment.Add(objEmpanelment);
                }
                else
                {
                    objEmpanelment.status = "fail";
                    lsEmpanelment.Add(objEmpanelment);
                }
                return lsEmpanelment.ToArray();
            }
            catch (Exception)
            {
                EmpanelmentContact objEmpanelment = new EmpanelmentContact();
                objEmpanelment.status = "fail";
                lsEmpanelment.Add(objEmpanelment);
                return lsEmpanelment.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to insert Events Remider as per brokerid

        public Event[] InsertCalReminder(string brokerid, string eventid)
        {
            List<Event> lsEvent = new List<Event>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_InsertReminder", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@brokerid", brokerid);
                comm.Parameters.AddWithValue("@eventid", eventid);
                SqlParameter pmout = new SqlParameter();
                pmout.ParameterName = "@outStatus";
                pmout.DbType = DbType.String;
                pmout.Size = 1024;
                pmout.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout);

                Int32 sqlRows = comm.ExecuteNonQuery();
                Event objEvent = new Event();
                if (sqlRows != 0)
                {
                    objEvent.status = comm.Parameters["@outStatus"].Value.ToString();
                    lsEvent.Add(objEvent);
                }
                else
                {
                    objEvent.status = "fail";
                    lsEvent.Add(objEvent);
                }
                return lsEvent.ToArray();
            }
            catch (Exception)
            {
                Event objEvent = new Event();
                objEvent.status = "fail";
                lsEvent.Add(objEvent);
                return lsEvent.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get Top Performers

        public Performer[] GetPerformerDetails()
        {
            List<Performer> lsPerformer = new List<Performer>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetPerformerDetails", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Performer[] b = new Performer[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Performer objPerformer = new Performer();
                        objPerformer.status = "success";
                        objPerformer.Vendor_code = Convert.ToString(ds.Tables[0].Rows[i]["VendorAcctNo"]);
                        objPerformer.name = Convert.ToString(ds.Tables[0].Rows[i]["Name"]);
                        objPerformer.project = Convert.ToString(ds.Tables[0].Rows[i]["Project"]);
                        objPerformer.org_Name = Convert.ToString(ds.Tables[0].Rows[i]["OrgName"]);
                        objPerformer.sales_no = Convert.ToString(ds.Tables[0].Rows[i]["Sales_No"]);
                        lsPerformer.Add(objPerformer);
                    }
                }
                else
                {
                    Performer objPerformer = new Performer();
                    objPerformer.status = "fail";
                    objPerformer.Vendor_code = "null";
                    objPerformer.name = "null";
                    objPerformer.project = "null";
                    objPerformer.org_Name = "null";
                    objPerformer.sales_no = "null";
                    lsPerformer.Add(objPerformer);
                }
                return lsPerformer.ToArray();
            }
            catch (Exception)
            {
                Performer objPerformer = new Performer();
                objPerformer.status = "fail";
                objPerformer.Vendor_code = "null";
                objPerformer.name = "null";
                objPerformer.project = "null";
                objPerformer.org_Name = "null";
                objPerformer.sales_no = "null";
                lsPerformer.Add(objPerformer);
                return lsPerformer.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get lead details

        public Lead[] GetLeadDetails(string brokerid)
        {
            List<Lead> lsLead = new List<Lead>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetLeadDetails", conn);
                comm.Parameters.AddWithValue("@brokerid", brokerid);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Lead[] b = new Lead[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Lead objLead = new Lead();
                        objLead.status = "success";
                        objLead.po_no = Convert.ToString(ds.Tables[0].Rows[i]["PO_no"]);
                        objLead.project = Convert.ToString(ds.Tables[0].Rows[i]["Project"]);
                        objLead.leadstatus = Convert.ToString(ds.Tables[0].Rows[i]["leadstatus"]);
                        lsLead.Add(objLead);
                    }
                }
                else
                {
                    Lead objLead = new Lead();
                    objLead.status = "fail";
                    objLead.po_no = "null";
                    objLead.project = "null";
                    objLead.leadstatus = "null";
                    lsLead.Add(objLead);
                }
                return lsLead.ToArray();
            }
            catch (Exception)
            {
                Lead objLead = new Lead();
                objLead.status = "fail";
                objLead.po_no = "null";
                objLead.project = "null";
                objLead.leadstatus = "null";
                lsLead.Add(objLead);
                return lsLead.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get Commision

        public Sales[] GetCommisionDetails(string brokerid)
        {
            List<Sales> lsSales = new List<Sales>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetCommisionDetails", conn);
                comm.Parameters.AddWithValue("@brokerid", brokerid);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Sales[] b = new Sales[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Sales objSales = new Sales();
                        objSales.status = "success";
                        objSales.brokerage = Convert.ToString(ds.Tables[0].Rows[i]["Brokerage"]);
                        objSales.project = Convert.ToString(ds.Tables[0].Rows[i]["Project"]);
                        objSales.date = Convert.ToString(ds.Tables[0].Rows[i]["date"]);
                        lsSales.Add(objSales);
                    }
                }
                else
                {
                    Sales objSales = new Sales();
                    objSales.status = "fail";
                    objSales.brokerage = "null";
                    objSales.project = "null";
                    objSales.date = "null";
                    lsSales.Add(objSales);
                }
                return lsSales.ToArray();
            }
            catch (Exception)
            {
                Sales objSales = new Sales();
                objSales.status = "fail";
                objSales.brokerage = "null";
                objSales.project = "null";
                objSales.date = "null";
                lsSales.Add(objSales);
                return lsSales.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get total Commision

        public Sales[] GetTotalCommisionDetails(string brokerid)
        {
            List<Sales> lsSales = new List<Sales>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetTotalCommisionDetails", conn);
                comm.Parameters.AddWithValue("@brokerid", brokerid);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Sales[] b = new Sales[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Sales objSales = new Sales();
                        objSales.status = "success";
                        objSales.sum = Convert.ToString(ds.Tables[0].Rows[i]["sum"]);
                        lsSales.Add(objSales);
                    }
                }
                else
                {
                    Sales objSales = new Sales();
                    objSales.status = "fail";
                    objSales.sum = "null";
                    lsSales.Add(objSales);
                }
                return lsSales.ToArray();
            }
            catch (Exception)
            {
                Sales objSales = new Sales();
                objSales.status = "fail";
                objSales.sum = "null";
                lsSales.Add(objSales);
                return lsSales.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get payment details as per PO number

        public Purchase[] GetPurchaseDetails(string brokerid)
        {
            List<Purchase> lsPurchase = new List<Purchase>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetPurchaseDetails", conn);
                comm.Parameters.AddWithValue("@brokerid", brokerid);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Purchase[] b = new Purchase[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Purchase objPurchase = new Purchase();
                        objPurchase.status = "success";
                        objPurchase.po_no = Convert.ToString(ds.Tables[0].Rows[i]["Purchasing_Document_No"]);
                        objPurchase.date = Convert.ToString(ds.Tables[0].Rows[i]["date"]);
                        objPurchase.amount = Convert.ToString(ds.Tables[0].Rows[i]["Brokerage"]);
                        lsPurchase.Add(objPurchase);
                    }
                }
                else
                {
                    Purchase objPurchase = new Purchase();
                    objPurchase.status = "fail";
                    objPurchase.po_no = "null";
                    objPurchase.date = "null";
                    objPurchase.amount = "null";
                    lsPurchase.Add(objPurchase);
                }
                return lsPurchase.ToArray();
            }
            catch (Exception)
            {
                Purchase objPurchase = new Purchase();
                objPurchase.status = "fail";
                objPurchase.po_no = "null";
                objPurchase.date = "null";
                objPurchase.amount = "null";
                lsPurchase.Add(objPurchase);
                return lsPurchase.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get payment details as per PO number

        public Purchase[] GetPurchaseTotal(string brokerid)
        {
            List<Purchase> lsPurchase = new List<Purchase>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetPurchaseTotal", conn);
                comm.Parameters.AddWithValue("@brokerid", brokerid);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Purchase[] b = new Purchase[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Purchase objPurchase = new Purchase();
                        objPurchase.status = "success";
                        objPurchase.amount = Convert.ToString(ds.Tables[0].Rows[i]["sum"]);
                        lsPurchase.Add(objPurchase);
                    }
                }
                else
                {
                    Purchase objPurchase = new Purchase();
                    objPurchase.status = "fail";
                    objPurchase.amount = "null";
                    lsPurchase.Add(objPurchase);
                }
                return lsPurchase.ToArray();
            }
            catch (Exception)
            {
                Purchase objPurchase = new Purchase();
                objPurchase.status = "fail";
                objPurchase.amount = "null";
                lsPurchase.Add(objPurchase);
                return lsPurchase.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to update empanelment Details

        public EmpanelmentContact[] UpdateEmplInfo(string panno, string brokerid)
        {
            List<EmpanelmentContact> lsproject = new List<EmpanelmentContact>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_UpdateEmplDetails", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@panno", panno);
                comm.Parameters.AddWithValue("@brokerid", brokerid);
                SqlParameter pmout = new SqlParameter();
                pmout.ParameterName = "@outStatus";
                pmout.DbType = DbType.String;
                pmout.Size = 1024;
                pmout.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout);
                Int32 sqlRows = comm.ExecuteNonQuery();
                EmpanelmentContact objBroker = new EmpanelmentContact();
                if (sqlRows != 0)
                {
                    objBroker.status = comm.Parameters["@outStatus"].Value.ToString();
                    lsproject.Add(objBroker);
                }
                else
                {
                    objBroker.status = "fail";
                    lsproject.Add(objBroker);
                }
                return lsproject.ToArray();
            }
            catch (Exception)
            {
                EmpanelmentContact objBroker = new EmpanelmentContact();
                objBroker.status = "fail";
                lsproject.Add(objBroker);
                return lsproject.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get Broker Details

        public Broker[] GetBrokerDetails(string panno)
        {
            List<Broker> lsproject = new List<Broker>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetBrokerDetails", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@panno", panno);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Broker[] b = new Broker[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Broker objBroker = new Broker();
                        objBroker.status = "success";
                        objBroker.brokerid = Convert.ToString(ds.Tables[0].Rows[i]["brokerid"]);
                        objBroker.name = Convert.ToString(ds.Tables[0].Rows[i]["name"]);
                        objBroker.mobile = Convert.ToString(ds.Tables[0].Rows[i]["mobile_no"]);
                        objBroker.email = Convert.ToString(ds.Tables[0].Rows[i]["email"]);
                        objBroker.panno = Convert.ToString(ds.Tables[0].Rows[i]["pan_no"]);
                        objBroker.service_tax_no = Convert.ToString(ds.Tables[0].Rows[i]["service_tax_no"]);
                        objBroker.tinno = Convert.ToString(ds.Tables[0].Rows[i]["tin_no"]);
                        objBroker.company = Convert.ToString(ds.Tables[0].Rows[i]["company"]);
                        objBroker.enterprise_membership_id = Convert.ToString(ds.Tables[0].Rows[i]["enterprise_membership_id"]);
                        objBroker.empanelmentid = Convert.ToString(ds.Tables[0].Rows[i]["empanelmentid"]);
                        lsproject.Add(objBroker);
                    }
                }
                else
                {
                    Broker objBroker = new Broker();
                    objBroker.status = "fail";
                    objBroker.brokerid = "null";
                    objBroker.name = "null";
                    objBroker.mobile = "null";
                    objBroker.email = "null";
                    objBroker.panno = "null";
                    objBroker.service_tax_no = "null";
                    objBroker.tinno = "null";
                    objBroker.company = "null";
                    objBroker.enterprise_membership_id = "null";
                    objBroker.empanelmentid = "null";
                    lsproject.Add(objBroker);
                }
                return lsproject.ToArray();
            }
            catch (Exception)
            {
                Broker objBroker = new Broker();
                objBroker.status = "fail";
                objBroker.brokerid = "null";
                objBroker.name = "null";
                objBroker.mobile = "null";
                objBroker.email = "null";
                objBroker.panno = "null";
                objBroker.service_tax_no = "null";
                objBroker.tinno = "null";
                objBroker.company = "null";
                objBroker.enterprise_membership_id = "null";
                objBroker.empanelmentid = "null";
                lsproject.Add(objBroker);
                return lsproject.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to check broker exist

        public EmpanelmentContact[] GetEmplInfo(string panno)
        {
            List<EmpanelmentContact> lsproject = new List<EmpanelmentContact>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetEmplInfo", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@panno", panno);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    EmpanelmentContact[] b = new EmpanelmentContact[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        EmpanelmentContact objBroker = new EmpanelmentContact();
                        objBroker.status = "success";
                        objBroker.brokerid = Convert.ToString(ds.Tables[0].Rows[i]["brokerid"]);
                        lsproject.Add(objBroker);
                    }
                }
                else
                {
                    EmpanelmentContact objBroker = new EmpanelmentContact();
                    objBroker.status = "fail";
                    objBroker.brokerid = "null";
                    lsproject.Add(objBroker);
                }
                return lsproject.ToArray();
            }
            catch (Exception)
            {
                EmpanelmentContact objBroker = new EmpanelmentContact();
                objBroker.status = "fail";
                objBroker.brokerid = "null";
                lsproject.Add(objBroker);
                return lsproject.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get User Info
        public EmpanelmentContact[] GetUserDetails(string userid)
        {
            List<EmpanelmentContact> listEmpanelment = new List<EmpanelmentContact>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetUserDetails", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@userid", userid);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    //EmpanelmentContact[] b = new EmpanelmentContact[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        EmpanelmentContact objEmpanelment = new EmpanelmentContact();
                        objEmpanelment.status = "success";
                        objEmpanelment.name = Convert.ToString(ds.Tables[0].Rows[i]["Name"]);
                        objEmpanelment.email = Convert.ToString(ds.Tables[0].Rows[i]["Email"]);
                        objEmpanelment.mobileno = Convert.ToString(ds.Tables[0].Rows[i]["Mobile"]);
                        objEmpanelment.company = Convert.ToString(ds.Tables[0].Rows[i]["Company_Name"]);
                        objEmpanelment.image = Base + "images/ProfilePic/" + Convert.ToString(ds.Tables[0].Rows[i]["ProfilePic"]);
                        objEmpanelment.imagename = Convert.ToString(ds.Tables[0].Rows[i]["ProfilePic"]);
                        listEmpanelment.Add(objEmpanelment);
                    }
                }
                else
                {
                    EmpanelmentContact objEmpanelment = new EmpanelmentContact();
                    objEmpanelment.status = "fail";
                    objEmpanelment.name = "null";
                    objEmpanelment.email = "null";
                    objEmpanelment.mobileno = "null";
                    objEmpanelment.company = "null";
                    listEmpanelment.Add(objEmpanelment);
                }
                return listEmpanelment.ToArray();
            }
            catch (Exception)
            {
                EmpanelmentContact objEmpanelment = new EmpanelmentContact();
                objEmpanelment.status = "fail";
                objEmpanelment.name = "null";
                objEmpanelment.email = "null";
                objEmpanelment.mobileno = "null";
                objEmpanelment.company = "null";
                listEmpanelment.Add(objEmpanelment);
                return listEmpanelment.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region To Insert Empanelment details

        public EmpanelmentContact[] InsertEmpl(string name, string mobile, string email, string panno, string taxno, string tinno, string company, string region, string enterpriseid, string brokerid)
        {
            List<EmpanelmentContact> listEmpl = new List<EmpanelmentContact>();
            try
            {
                string tax, tin, eid, broker = "";
                if (taxno == "")
                {
                    tax = "null";
                }
                else
                {
                    tax = taxno;
                }
                if (taxno == "")
                {
                    tin = "null";
                }
                else
                {
                    tin = tinno;
                }
                if (enterpriseid == "")
                {
                    eid = "null";
                }
                else
                {
                    eid = enterpriseid;
                }
                if (brokerid == "")
                {
                    broker = "null";
                }
                else
                {
                    broker = brokerid;
                }
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_InsertEmpl", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@name", name);
                comm.Parameters.AddWithValue("@mobile", mobile);
                comm.Parameters.AddWithValue("@email", email);
                comm.Parameters.AddWithValue("@panno", panno);
                comm.Parameters.AddWithValue("@taxno", tax);
                comm.Parameters.AddWithValue("@tinno", tin);
                comm.Parameters.AddWithValue("@company", company);
                comm.Parameters.AddWithValue("@region", region);
                comm.Parameters.AddWithValue("@enterpriseid", eid);
                comm.Parameters.AddWithValue("@brokerid", broker);
                SqlParameter pmout = new SqlParameter();
                pmout.ParameterName = "@outStatus";
                pmout.DbType = DbType.String;
                pmout.Size = 1024;
                pmout.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout);
                SqlParameter pmout1 = new SqlParameter();
                pmout1.ParameterName = "@outID";
                pmout1.DbType = DbType.String;
                pmout1.Size = 1024;
                pmout1.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout1);
                Int32 sqlRows = comm.ExecuteNonQuery();
                EmpanelmentContact objEmpl = new EmpanelmentContact();
                if (sqlRows != 0)
                {
                    objEmpl.status = comm.Parameters["@outStatus"].Value.ToString();
                    objEmpl.userid = comm.Parameters["@outID"].Value.ToString();
                    listEmpl.Add(objEmpl);
                }
                else
                {
                    objEmpl.status = "fail";
                    objEmpl.userid = "null";
                    listEmpl.Add(objEmpl);
                }
                return listEmpl.ToArray();
            }
            catch (Exception Ex)
            {
                EmpanelmentContact objEmpl = new EmpanelmentContact();
                objEmpl.status = "fail";
                objEmpl.userid = "null";
                listEmpl.Add(objEmpl);
                return listEmpl.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region To Insert Appointment details

        public Appointment[] InsertAppt(string name, string mobile, string email, string company, string city, string project, string comments, string brokerid, string date)
        {
            List<Appointment> listEmpl = new List<Appointment>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("usp_BookAppointment", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@name ", name);
                comm.Parameters.AddWithValue("@email", email);
                comm.Parameters.AddWithValue("@mobile", mobile);
                comm.Parameters.AddWithValue("@company", company);
                comm.Parameters.AddWithValue("@city", city);
                comm.Parameters.AddWithValue("@location", project);
                comm.Parameters.AddWithValue("@appointmentDate", date);
                comm.Parameters.AddWithValue("@comments", comments);
                comm.Parameters.AddWithValue("@brokerid", brokerid);
                SqlParameter pmout = new SqlParameter();
                pmout.ParameterName = "@outStatus";
                pmout.DbType = DbType.String;
                pmout.Size = 1024;
                pmout.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout);

                SqlParameter pmoutcode = new SqlParameter();
                pmoutcode.ParameterName = "@sapcode";
                pmoutcode.DbType = DbType.String;
                pmoutcode.Size = 1024;
                pmoutcode.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmoutcode);

                SqlParameter pmoutemail = new SqlParameter();
                pmoutemail.ParameterName = "@rmemail";
                pmoutemail.DbType = DbType.String;
                pmoutemail.Size = 1024;
                pmoutemail.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmoutemail);
                Int32 sqlRows = comm.ExecuteNonQuery();
                Appointment objEmpl = new Appointment();
                if (sqlRows != 0)
                {
                    try
                    {
                        MailMessage mail = new MailMessage();
                        mail.To = comm.Parameters["@rmemail"].Value.ToString();
                        mail.From = email;
                        mail.BodyFormat = MailFormat.Html;
                        mail.Subject = "Book A Site Visit";
                        string path = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                        StreamReader sr = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Mailer/book_a_site_visit.html"));
                        mail.Body = sr.ReadToEnd().Replace("cust_name", name).Replace("user_code", comm.Parameters["@sapcode"].Value.ToString()).Replace("user_date", date);

                        //string BodyMessage = "<html> <body><h4>Login Details for your channel partner account:</h4> <br/> username : " + email + ".<br/> password :" + pwd + ".";
                        //mail.Body = "test";
                        mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
                        mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "info@netbizlabs.com"); //set your username here
                        mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "info321");	//set your password here
                        //StreamReader sr = new StreamReader(Server.MapPath(""));
                        //mail.Body = sr.ReadToEnd().Replace("Email", email);
                        SmtpMail.SmtpServer = "mail.netbiz.in";
                        SmtpMail.Send(mail);

                        objEmpl.status = comm.Parameters["@outStatus"].Value.ToString();
                    }
                    catch (Exception ex)
                    {
                        objEmpl.status = ex.ToString();
                    }
                    listEmpl.Add(objEmpl);
                }
                else
                {
                    objEmpl.status = "fail";
                    listEmpl.Add(objEmpl);
                }
                return listEmpl.ToArray();
            }
            catch (Exception Ex)
            {
                Appointment objEmpl = new Appointment();
                objEmpl.status = "fail";
                listEmpl.Add(objEmpl);
                return listEmpl.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region To Insert Broker details

        public Broker[] InsertBrokerInfo(string name, string mobile, string email, string panno, string taxno, string tinno, string company, string enterpriseid, string emplid)
        {
            List<Broker> listBroker = new List<Broker>();
            try
            {
                string tax, tin, eid = "";
                if (taxno == "")
                {
                    tax = "null";
                }
                else
                {
                    tax = taxno;
                }
                if (taxno == "")
                {
                    tin = "null";
                }
                else
                {
                    tin = tinno;
                }
                if (enterpriseid == "")
                {
                    eid = "null";
                }
                else
                {
                    eid = enterpriseid;
                }
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_InsertBroker", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@name", name);
                comm.Parameters.AddWithValue("@mobile", mobile);
                comm.Parameters.AddWithValue("@email", email);
                comm.Parameters.AddWithValue("@panno", panno);
                comm.Parameters.AddWithValue("@taxno", tax);
                comm.Parameters.AddWithValue("@tinno", tin);
                comm.Parameters.AddWithValue("@company", company);
                comm.Parameters.AddWithValue("@enterpriseid", eid);
                comm.Parameters.AddWithValue("@brokerid", emplid);
                SqlParameter pmout = new SqlParameter();
                pmout.ParameterName = "@outStatus";
                pmout.DbType = DbType.String;
                pmout.Size = 1024;
                pmout.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout);
                Int32 sqlRows = comm.ExecuteNonQuery();
                Broker objBroker = new Broker();
                if (sqlRows != 0)
                {
                    objBroker.status = comm.Parameters["@outStatus"].Value.ToString();
                    listBroker.Add(objBroker);
                }
                else
                {
                    objBroker.status = "fail";
                    listBroker.Add(objBroker);
                }
                return listBroker.ToArray();
            }
            catch (Exception Ex)
            {
                Broker objBroker = new Broker();
                objBroker.status = "fail";
                listBroker.Add(objBroker);
                return listBroker.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region To update username/pwd

        public EmpanelmentContact[] InsertLogin(string emplid, string pwd, string email, string name)
        {
            //string ProgramPath = HttpContext.Current.Server.MapPath("~/empanelment_form.html");
            List<EmpanelmentContact> listEmpl = new List<EmpanelmentContact>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_InsertEmplLogin", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@emplid", emplid);
                comm.Parameters.AddWithValue("@uname", email);
                comm.Parameters.AddWithValue("@pwd", pwd);

                Int32 sqlRows = comm.ExecuteNonQuery();
                EmpanelmentContact objEmpl = new EmpanelmentContact();
                if (sqlRows != 0)
                {
                    try
                    {
                        MailMessage mail = new MailMessage();
                        mail.To = email;
                        mail.From = "info@godrejproperties.com";
                        mail.BodyFormat = MailFormat.Html;
                        mail.Subject = "Channel Partner app - Welcome details";
                        string path = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                        StreamReader sr = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Mailer/empanelment_form.html"));
                        mail.Body = sr.ReadToEnd().Replace("user_name", name).Replace("user_email", email).Replace("user_pwd", pwd);

                        //string BodyMessage = "<html> <body><h4>Login Details for your channel partner account:</h4> <br/> username : " + email + ".<br/> password :" + pwd + ".";
                        //mail.Body = "test";
                        mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
                        mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "info@netbizlabs.com"); //set your username here
                        mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "info321");	//set your password here
                        //StreamReader sr = new StreamReader(Server.MapPath(""));
                        //mail.Body = sr.ReadToEnd().Replace("Email", email);
                        SmtpMail.SmtpServer = "mail.netbiz.in";
                        SmtpMail.Send(mail);

                        objEmpl.status = "success";
                        //listEmpl.Add(objEmpl);
                    }
                    catch (Exception ex)
                    {
                        objEmpl.status = ex.ToString();
                    }
                    listEmpl.Add(objEmpl);
                }
                else
                {
                    objEmpl.status = "fail";
                    listEmpl.Add(objEmpl);
                }
                return listEmpl.ToArray();
            }
            catch (Exception Ex)
            {
                EmpanelmentContact objEmpl = new EmpanelmentContact();
                objEmpl.status = "fail";
                listEmpl.Add(objEmpl);
                return listEmpl.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get All active current Projects List according to city

        public Property[] GetPropertyByCity(string id)
        {
            List<Property> lsproject = new List<Property>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetPropertyBYCity", conn);
                comm.Parameters.AddWithValue("@cityid", Convert.ToInt32(id));
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Property[] b = new Property[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Property objproject = new Property();
                        objproject.status = "success";
                        objproject.propertyid = Convert.ToString(ds.Tables[0].Rows[i]["id"]);
                        objproject.description = Convert.ToString(ds.Tables[0].Rows[i]["description"]);
                        objproject.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        objproject.image = BaseURL + "images/Projects/" + Convert.ToString(ds.Tables[0].Rows[i]["image"]);
                        lsproject.Add(objproject);
                    }
                }
                else
                {
                    Property objproject = new Property();
                    objproject.status = "fail";
                    objproject.propertyid = "null";
                    objproject.description = "null";
                    objproject.title = "null";
                    objproject.image = "null";
                    lsproject.Add(objproject);
                }
                return lsproject.ToArray();
            }
            catch (Exception)
            {
                Property objproject = new Property();
                objproject.status = "fail";
                objproject.propertyid = "null";
                objproject.description = "null";
                objproject.title = "null";
                objproject.image = "null";
                lsproject.Add(objproject);
                return lsproject.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get all active offers and schemes

        public Offer[] GetOffers()
        {
            List<Offer> lsOffer = new List<Offer>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_Getoffers", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Offer[] b = new Offer[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Offer objOffer = new Offer();
                        objOffer.status = "success";
                        objOffer.offerid = Convert.ToString(ds.Tables[0].Rows[i]["id"]);
                        objOffer.description = Convert.ToString(ds.Tables[0].Rows[i]["description"]);
                        objOffer.image = BaseURL + "images/Projects/" + Convert.ToString(ds.Tables[0].Rows[i]["image"]);
                        objOffer.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        lsOffer.Add(objOffer);
                    }
                }
                else
                {
                    Offer objOffer = new Offer();
                    objOffer.status = "fail";
                    objOffer.offerid = "null";
                    objOffer.description = "null";
                    objOffer.image = "null";
                    objOffer.title = "null";
                    lsOffer.Add(objOffer);
                }
                return lsOffer.ToArray();
            }
            catch (Exception)
            {
                Offer objOffer = new Offer();
                objOffer.status = "fail";
                objOffer.offerid = "null";
                objOffer.description = "null";
                objOffer.image = "null";
                objOffer.title = "null";
                lsOffer.Add(objOffer);
                return lsOffer.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get all active faqs

        public Faq[] FetchFaq(string type)
        {
            List<Faq> lsFaq = new List<Faq>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_Getfaq", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@type", type);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Faq[] b = new Faq[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Faq objFaq = new Faq();
                        objFaq.status = "success";
                        objFaq.question = Convert.ToString(ds.Tables[0].Rows[i]["question"]);
                        objFaq.answer = Convert.ToString(ds.Tables[0].Rows[i]["answer"]);
                        lsFaq.Add(objFaq);
                    }
                }
                else
                {
                    Faq objFaq = new Faq();
                    objFaq.status = "fail";
                    objFaq.question = "null";
                    objFaq.answer = "null";
                    lsFaq.Add(objFaq);
                }
                return lsFaq.ToArray();
            }
            catch (Exception)
            {
                Faq objFaq = new Faq();
                objFaq.status = "fail";
                objFaq.question = "null";
                objFaq.answer = "null";
                lsFaq.Add(objFaq);
                return lsFaq.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to insert faqs

        public Faq[] EnterFaq(string question, string brokerid, string type)
        {
            List<Faq> lsFaq = new List<Faq>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_Insertfaq", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@question", question);
                comm.Parameters.AddWithValue("@brokerid", brokerid);
                comm.Parameters.AddWithValue("@type", type);

                SqlParameter pmout = new SqlParameter();
                pmout.ParameterName = "@outStatus";
                pmout.DbType = DbType.String;
                pmout.Size = 1024;
                pmout.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout);


                Int32 sqlRows = comm.ExecuteNonQuery();
                Faq objFaq = new Faq();
                if (sqlRows != 0)
                {
                    objFaq.status = comm.Parameters["@outStatus"].Value.ToString();
                    lsFaq.Add(objFaq);
                }
                else
                {
                    objFaq.status = "fail";
                    lsFaq.Add(objFaq);
                }
                return lsFaq.ToArray();
            }
            catch (Exception Ex)
            {
                Faq objFaq = new Faq();
                objFaq.status = "fail";
                lsFaq.Add(objFaq);
                return lsFaq.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get all active Events

        public Event[] FetchEvents()
        {
            List<Event> lsEvent = new List<Event>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetEvents", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Event[] b = new Event[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Event objEvent = new Event();
                        objEvent.status = "success";
                        objEvent.id = Convert.ToString(ds.Tables[0].Rows[i]["id"]);
                        objEvent.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        objEvent.description = Convert.ToString(ds.Tables[0].Rows[i]["description"]);
                        objEvent.image = BaseURL + "images/Events/" + Convert.ToString(ds.Tables[0].Rows[i]["image"]);
                        objEvent.date = Convert.ToString(ds.Tables[0].Rows[i]["date"]);
                        objEvent.place = Convert.ToString(ds.Tables[0].Rows[i]["place"]);
                        lsEvent.Add(objEvent);
                    }
                }
                else
                {
                    Event objEvent = new Event();
                    objEvent.status = "fail";
                    objEvent.title = "null";
                    objEvent.description = "null";
                    objEvent.image = "null";
                    objEvent.date = "null";
                    objEvent.place = "null";
                    lsEvent.Add(objEvent);
                }
                return lsEvent.ToArray();
            }
            catch (Exception)
            {
                Event objEvent = new Event();
                objEvent.status = "fail";
                objEvent.title = "null";
                objEvent.description = "null";
                objEvent.image = "null";
                objEvent.date = "null";
                objEvent.place = "null";
                lsEvent.Add(objEvent);
                return lsEvent.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get all active News Feed

        public News[] FetchNews()
        {
            List<News> lsNews = new List<News>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetNews", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    News[] b = new News[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        News objNews = new News();
                        objNews.status = "success";
                        objNews.id = Convert.ToString(ds.Tables[0].Rows[i]["id"]);
                        objNews.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        objNews.description = Convert.ToString(ds.Tables[0].Rows[i]["description"]);
                        objNews.image = BaseURL + "images/News/" + Convert.ToString(ds.Tables[0].Rows[i]["image"]);
                        objNews.date = Convert.ToString(ds.Tables[0].Rows[i]["date"]);
                        objNews.video = Convert.ToString(ds.Tables[0].Rows[i]["video"]);
                        lsNews.Add(objNews);
                    }
                }
                else
                {
                    News objNews = new News();
                    objNews.status = "fail";
                    objNews.title = "null";
                    objNews.description = "null";
                    objNews.image = "null";
                    objNews.video = "null";
                    lsNews.Add(objNews);
                }
                return lsNews.ToArray();
            }
            catch (Exception)
            {
                News objNews = new News();
                objNews.status = "fail";
                objNews.title = "null";
                objNews.description = "null";
                objNews.image = "null";
                objNews.video = "null";
                lsNews.Add(objNews);
                return lsNews.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get all active News Feed as per news

        public News[] FetchNewsDetails(string id)
        {
            List<News> lsNews = new List<News>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetNewsByID", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@id", id);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    News[] b = new News[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        News objNews = new News();
                        objNews.status = "success";
                        objNews.id = Convert.ToString(ds.Tables[0].Rows[i]["id"]);
                        objNews.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        objNews.date = Convert.ToString(ds.Tables[0].Rows[i]["date"]);
                        objNews.description = Convert.ToString(ds.Tables[0].Rows[i]["description"]);
                        objNews.image = BaseURL + "images/News/" + Convert.ToString(ds.Tables[0].Rows[i]["image"]);
                        lsNews.Add(objNews);
                    }
                }
                else
                {
                    News objNews = new News();
                    objNews.status = "fail";
                    objNews.title = "null";
                    objNews.date = "null";
                    objNews.description = "null";
                    objNews.image = "null";
                    lsNews.Add(objNews);
                }
                return lsNews.ToArray();
            }
            catch (Exception)
            {
                News objNews = new News();
                objNews.status = "fail";
                objNews.title = "null";
                objNews.date = "null";
                objNews.description = "null";
                objNews.image = "null";
                lsNews.Add(objNews);
                return lsNews.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get all active News Feed videos as per news

        public News[] FetchVideoByNews(string id)
        {
            List<News> lsNews = new List<News>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetVideoByID", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@id", id);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    News[] b = new News[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        News objNews = new News();
                        objNews.status = "success";
                        objNews.video = Convert.ToString(ds.Tables[0].Rows[i]["videourl"]);
                        lsNews.Add(objNews);
                    }
                }
                else
                {
                    News objNews = new News();
                    objNews.status = "fail";
                    objNews.video = "null";
                    lsNews.Add(objNews);
                }
                return lsNews.ToArray();
            }
            catch (Exception)
            {
                News objNews = new News();
                objNews.status = "fail";
                objNews.video = "null";
                lsNews.Add(objNews);
                return lsNews.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to insert Events Remider as per brokerid

        public Event[] InsertReminder(string brokerid, string eventid)
        {
            List<Event> lsEvent = new List<Event>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_InsertReminder", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@brokerid", brokerid);
                comm.Parameters.AddWithValue("@eventid", eventid);
                SqlParameter pmout = new SqlParameter();
                pmout.ParameterName = "@outStatus";
                pmout.DbType = DbType.String;
                pmout.Size = 1024;
                pmout.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout);

                Int32 sqlRows = comm.ExecuteNonQuery();
                Event objEvent = new Event();
                if (sqlRows != 0)
                {
                    objEvent.status = comm.Parameters["@outStatus"].Value.ToString();
                    lsEvent.Add(objEvent);
                }
                else
                {
                    objEvent.status = "fail";
                    lsEvent.Add(objEvent);
                }
                return lsEvent.ToArray();
            }
            catch (Exception)
            {
                Event objEvent = new Event();
                objEvent.status = "fail";
                lsEvent.Add(objEvent);
                return lsEvent.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get all active Events Remider as per brokerid

        public Event[] FetchEventsReminder(string brokerid)
        {
            List<Event> lsEvent = new List<Event>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetEventsReminder", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@brokerid", brokerid);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Event[] b = new Event[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Event objEvent = new Event();
                        objEvent.status = "success";
                        objEvent.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        objEvent.date = Convert.ToString(ds.Tables[0].Rows[i]["date"]);
                        objEvent.type = Convert.ToString(ds.Tables[0].Rows[i]["type"]);
                        lsEvent.Add(objEvent);
                    }
                }
                else
                {
                    Event objEvent = new Event();
                    objEvent.status = "fail";
                    objEvent.title = "null";
                    objEvent.date = "null";
                    objEvent.type = "null";
                    lsEvent.Add(objEvent);
                }
                return lsEvent.ToArray();
            }
            catch (Exception)
            {
                Event objEvent = new Event();
                objEvent.status = "fail";
                objEvent.title = "null";
                objEvent.date = "null";
                objEvent.type = "null";
                lsEvent.Add(objEvent);
                return lsEvent.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get all active Events Remider as per brokerid

        public Event[] FetchEventsReminderDate(string brokerid, string date)
        {
            List<Event> lsEvent = new List<Event>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetEventsReminderDate", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@brokerid", brokerid);
                comm.Parameters.AddWithValue("@date", date);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Event[] b = new Event[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Event objEvent = new Event();
                        objEvent.status = "success";
                        objEvent.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        objEvent.type = Convert.ToString(ds.Tables[0].Rows[i]["type"]);
                        lsEvent.Add(objEvent);
                    }
                }
                else
                {
                    Event objEvent = new Event();
                    objEvent.status = "fail";
                    objEvent.title = "null";
                    objEvent.type = "null";
                    lsEvent.Add(objEvent);
                }
                return lsEvent.ToArray();
            }
            catch (Exception)
            {
                Event objEvent = new Event();
                objEvent.status = "fail";
                objEvent.title = "null";
                objEvent.type = "null";
                lsEvent.Add(objEvent);
                return lsEvent.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get all active Events as per id

        public Event[] Attend(string broker, string id)
        {
            List<Event> lsEvent = new List<Event>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetAttendByID", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@brokerid", broker);
                comm.Parameters.AddWithValue("@id", id);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Event[] b = new Event[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Event objEvent = new Event();
                        objEvent.status = "success";
                        objEvent.attend = Convert.ToString(ds.Tables[0].Rows[i]["Reminder"]);
                        lsEvent.Add(objEvent);
                    }
                }
                else
                {
                    Event objEvent = new Event();
                    objEvent.status = "fail";
                    objEvent.attend = "null";
                    lsEvent.Add(objEvent);
                }
                return lsEvent.ToArray();
            }
            catch (Exception)
            {
                Event objEvent = new Event();
                objEvent.status = "fail";
                objEvent.attend = "null";
                lsEvent.Add(objEvent);
                return lsEvent.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get all active Events as per id

        public Event[] FetchEventsDetails(string id)
        {
            List<Event> lsEvent = new List<Event>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetEventsByID", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@id", id);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Event[] b = new Event[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Event objEvent = new Event();
                        objEvent.status = "success";
                        objEvent.id = Convert.ToString(ds.Tables[0].Rows[i]["id"]);
                        objEvent.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        objEvent.description = Convert.ToString(ds.Tables[0].Rows[i]["description"]);
                        objEvent.image = BaseURL + "images/Events/" + Convert.ToString(ds.Tables[0].Rows[i]["image"]);
                        objEvent.date = Convert.ToString(ds.Tables[0].Rows[i]["date"]);
                        objEvent.place = Convert.ToString(ds.Tables[0].Rows[i]["place"]);
                        lsEvent.Add(objEvent);
                    }
                }
                else
                {
                    Event objEvent = new Event();
                    objEvent.status = "fail";
                    objEvent.title = "null";
                    objEvent.description = "null";
                    objEvent.image = "null";
                    objEvent.date = "null";
                    objEvent.place = "null";
                    lsEvent.Add(objEvent);
                }
                return lsEvent.ToArray();
            }
            catch (Exception)
            {
                Event objEvent = new Event();
                objEvent.status = "fail";
                objEvent.title = "null";
                objEvent.description = "null";
                objEvent.image = "null";
                objEvent.date = "null";
                objEvent.place = "null";
                lsEvent.Add(objEvent);
                return lsEvent.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to update Events as per id

        public Event[] UpdateEventsDetails(string id, string attend)
        {
            List<Event> lsEvent = new List<Event>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_UpdateEventsByID", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@id", id);
                comm.Parameters.AddWithValue("@attend", attend);

                SqlParameter pmout = new SqlParameter();
                pmout.ParameterName = "@outStatus";
                pmout.DbType = DbType.String;
                pmout.Size = 1024;
                pmout.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout);

                Int32 sqlRows = comm.ExecuteNonQuery();
                Event objEvent = new Event();
                if (sqlRows != 0)
                {
                    objEvent.status = comm.Parameters["@outStatus"].Value.ToString();
                    lsEvent.Add(objEvent);
                }
                else
                {
                    objEvent.status = "fail";
                    lsEvent.Add(objEvent);
                }
                return lsEvent.ToArray();
            }
            catch (Exception Ex)
            {
                Event objEvent = new Event();
                objEvent.status = "fail";
                lsEvent.Add(objEvent);
                return lsEvent.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get offer as per offerid

        public Offer[] GetOffersdetails(string offerid)
        {
            List<Offer> lsOffer = new List<Offer>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_Getofferdetails", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@offerid", offerid);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Offer[] b = new Offer[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Offer objOffer = new Offer();
                        objOffer.status = "success";
                        objOffer.offerid = Convert.ToString(ds.Tables[0].Rows[i]["id"]);
                        objOffer.description = Convert.ToString(ds.Tables[0].Rows[i]["description"]);
                        objOffer.image = BaseURL + "images/Projects/" + Convert.ToString(ds.Tables[0].Rows[i]["image"]);
                        lsOffer.Add(objOffer);
                    }
                }
                else
                {
                    Offer objOffer = new Offer();
                    objOffer.status = "fail";
                    objOffer.offerid = "null";
                    objOffer.description = "null";
                    objOffer.image = "null";
                    lsOffer.Add(objOffer);
                }
                return lsOffer.ToArray();
            }
            catch (Exception)
            {
                Offer objOffer = new Offer();
                objOffer.status = "fail";
                objOffer.offerid = "null";
                objOffer.description = "null";
                objOffer.image = "null";
                lsOffer.Add(objOffer);
                return lsOffer.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get All active current Projects List

        public Property[] GetProperty()
        {
            List<Property> lsproject = new List<Property>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetProperty", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Property[] b = new Property[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Property objproject = new Property();
                        objproject.status = "success";
                        objproject.propertyid = Convert.ToString(ds.Tables[0].Rows[i]["id"]);
                        objproject.description = Convert.ToString(ds.Tables[0].Rows[i]["description"]);
                        objproject.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        objproject.image = BaseURL + "images/Projects/" + Convert.ToString(ds.Tables[0].Rows[i]["image"]);
                        lsproject.Add(objproject);
                    }
                }
                else
                {
                    Property objproject = new Property();
                    objproject.status = "fail";
                    objproject.propertyid = "null";
                    objproject.description = "null";
                    objproject.title = "null";
                    objproject.image = "null";
                    lsproject.Add(objproject);
                }
                return lsproject.ToArray();
            }
            catch (Exception)
            {
                Property objproject = new Property();
                objproject.status = "fail";
                objproject.propertyid = "null";
                objproject.description = "null";
                objproject.title = "null";
                objproject.image = "null";
                lsproject.Add(objproject);
                return lsproject.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get All active current Projects details as per particular property

        public Property[] GetPropertyDetails(string propertyid)
        {
            List<Property> lsproject = new List<Property>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetPropertyDetails", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@propertyid", propertyid);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Property[] b = new Property[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Property objproject = new Property();
                        objproject.status = "success";
                        objproject.propertyid = Convert.ToString(ds.Tables[0].Rows[i]["id"]);
                        objproject.description = Convert.ToString(ds.Tables[0].Rows[i]["description"]);
                        objproject.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        objproject.image = BaseURL + "images/Projects/" + Convert.ToString(ds.Tables[0].Rows[i]["image"]);
                        lsproject.Add(objproject);
                    }
                }
                else
                {
                    Property objproject = new Property();
                    objproject.status = "fail";
                    objproject.propertyid = "null";
                    objproject.description = "null";
                    objproject.title = "null";
                    objproject.image = "null";
                    lsproject.Add(objproject);
                }
                return lsproject.ToArray();
            }
            catch (Exception)
            {
                Property objproject = new Property();
                objproject.status = "fail";
                objproject.propertyid = "null";
                objproject.description = "null";
                objproject.title = "null";
                objproject.image = "null";
                lsproject.Add(objproject);
                return lsproject.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get All active Projects List with Project Type & Project Status parameters

        public Projects[] GetProjects(string projecttype, string projectstatus)
        {
            List<Projects> lsproject = new List<Projects>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetProject", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@projecttype", projecttype);
                comm.Parameters.AddWithValue("@projectstatus", projectstatus);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Projects[] b = new Projects[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Projects objproject = new Projects();
                        objproject.status = "success";
                        objproject.projectid = Convert.ToString(ds.Tables[0].Rows[i]["projectid"]);
                        objproject.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        objproject.cityname = Convert.ToString(ds.Tables[0].Rows[i]["cityname"]);
                        lsproject.Add(objproject);
                    }
                }
                else
                {
                    Projects objproject = new Projects();
                    objproject.status = "fail";
                    objproject.projectid = "null";
                    objproject.title = "null";
                    lsproject.Add(objproject);
                }
                return lsproject.ToArray();
            }
            catch (Exception)
            {
                Projects objproject = new Projects();
                objproject.status = "fail";
                objproject.projectid = "null";
                objproject.title = "null";
                objproject.cityname = "null";
                lsproject.Add(objproject);
                return lsproject.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get All active cities
        public City[] FetchCities()
        {
            List<City> objcitydetails = new List<City>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                comm = new SqlCommand("SP_GetCities", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    City[] b = new City[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        City bCity = new City();
                        bCity.status = "success";
                        bCity.cityid = Convert.ToString(ds.Tables[0].Rows[i]["cityid"]);
                        bCity.cityname = Convert.ToString(ds.Tables[0].Rows[i]["cityname"]);
                        objcitydetails.Add(bCity);
                    }
                }
                else
                {
                    City bCity = new City();
                    bCity.status = "fail";
                    bCity.cityid = "null";
                    bCity.cityname = "null";
                    objcitydetails.Add(bCity);
                }
                return objcitydetails.ToArray();
            }
            catch (Exception)
            {
                City bCity = new City();
                bCity.status = "fail";
                bCity.cityid = "null";
                bCity.cityname = "null";
                objcitydetails.Add(bCity);
                return objcitydetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region Method to Get Appointment Info
        public Appointment[] GetApptDetails(string userid, string month)
        {
            List<Appointment> listEmpanelment = new List<Appointment>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetApptDetails", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@userid", userid);
                comm.Parameters.AddWithValue("@month", month);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Appointment[] b = new Appointment[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Appointment objEmpanelment = new Appointment();
                        objEmpanelment.status = "success";
                        objEmpanelment.project = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        objEmpanelment.approve = Convert.ToString(ds.Tables[0].Rows[i]["approve"]);
                        objEmpanelment.city = Convert.ToString(ds.Tables[0].Rows[i]["city"]);
                        objEmpanelment.appointmentDate = Convert.ToString(ds.Tables[0].Rows[i]["appointmentDate"]);
                        objEmpanelment.comments = Convert.ToString(ds.Tables[0].Rows[i]["comments"]);
                        objEmpanelment.length = Convert.ToString(ds.Tables[0].Rows[i]["collength"]);
                        listEmpanelment.Add(objEmpanelment);
                    }
                }
                else
                {
                    Appointment objEmpanelment = new Appointment();
                    objEmpanelment.status = "fail";
                    objEmpanelment.project = "null";
                    objEmpanelment.approve = "null";
                    objEmpanelment.city = "null";
                    objEmpanelment.appointmentDate = "null";
                    objEmpanelment.comments = "null";
                    objEmpanelment.length = "null";
                    listEmpanelment.Add(objEmpanelment);
                }
                return listEmpanelment.ToArray();
            }
            catch (Exception)
            {
                Appointment objEmpanelment = new Appointment();
                objEmpanelment.status = "fail";
                objEmpanelment.project = "null";
                objEmpanelment.approve = "null";
                objEmpanelment.city = "null";
                objEmpanelment.appointmentDate = "null";
                objEmpanelment.comments = "null";
                objEmpanelment.length = "null";
                listEmpanelment.Add(objEmpanelment);
                return listEmpanelment.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region Method to Get Project image
        public ProjectsMenu[] FetchProjImage(string projectid)
        {
            List<ProjectsMenu> Menu = new List<ProjectsMenu>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetProjectimage", conn);
                comm.Parameters.AddWithValue("@projectid", Convert.ToInt32(projectid));
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ProjectsMenu b = new ProjectsMenu();
                        b.status = "success";
                        b.image = BaseURL + "images/Projects/" + Convert.ToString(ds.Tables[0].Rows[i]["image"]);

                        Menu.Add(b);
                    }
                    return Menu.ToArray();
                }
                else
                {
                    ProjectsMenu b = new ProjectsMenu();
                    b.status = "fail";
                    b.image = "null";
                    Menu.Add(b);
                    return Menu.ToArray();
                }
            }
            catch (Exception ex)
            {
                ProjectsMenu b = new ProjectsMenu();
                b.status = "fail";
                b.image = "null";
                Menu.Add(b);
                return Menu.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get Project Active Menu
        public ProjectsMenu[] GetProjectMenu(string projectid)
        {
            List<ProjectsMenu> Menu = new List<ProjectsMenu>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetProjectmenu", conn);
                comm.Parameters.AddWithValue("@projectid", Convert.ToInt32(projectid));
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ProjectsMenu b = new ProjectsMenu();
                        b.status = "success";
                        b.projecttitle = Convert.ToString(ds.Tables[0].Rows[i]["projecttitle"]);
                        b.cityname = Convert.ToString(ds.Tables[0].Rows[i]["cityname"]);
                        b.projectid = Convert.ToString(ds.Tables[0].Rows[i]["projectid"]);
                        b.menuid = Convert.ToString(ds.Tables[0].Rows[i]["menuid"]);
                        b.menutitle = Convert.ToString(ds.Tables[0].Rows[i]["menutitle"]);
                        //b.image = BaseURL + "images/Projects/" + Convert.ToString(ds.Tables[0].Rows[i]["image"]);

                        Menu.Add(b);
                    }
                    return Menu.ToArray();
                }
                else
                {
                    ProjectsMenu b = new ProjectsMenu();
                    b.status = "fail";
                    b.projecttitle = "null";
                    b.projectid = "null";
                    b.menuid = "null";
                    b.image = "null";
                    Menu.Add(b);
                    return Menu.ToArray();
                }
            }
            catch (Exception ex)
            {
                ProjectsMenu b = new ProjectsMenu();
                b.status = "fail";
                b.projectid = "null";
                b.projecttitle = "null";
                b.menuid = "null";
                b.image = "null";
                Menu.Add(b);
                return Menu.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get Project Name

        public ProjectsMenu[] FetchProjectName(string projectid)
        {
            List<ProjectsMenu> Menu = new List<ProjectsMenu>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetProjectName", conn);
                comm.Parameters.AddWithValue("@projectid", Convert.ToInt32(projectid));
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ProjectsMenu b = new ProjectsMenu();
                        b.status = "success";
                        b.projecttitle = Convert.ToString(ds.Tables[0].Rows[i]["projecttitle"]);
                        b.cityname = Convert.ToString(ds.Tables[0].Rows[i]["cityname"]);
                        Menu.Add(b);
                    }
                    return Menu.ToArray();
                }
                else
                {
                    ProjectsMenu b = new ProjectsMenu();
                    b.status = "fail";
                    b.projecttitle = "null";
                    b.cityname = "null";
                    Menu.Add(b);
                    return Menu.ToArray();
                }
            }
            catch (Exception ex)
            {
                ProjectsMenu b = new ProjectsMenu();
                b.status = "fail";
                b.projecttitle = "null";
                b.cityname = "null";
                Menu.Add(b);
                return Menu.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get All active banners

        public Banners[] gethomeBanners()
        {
            List<Banners> Bannerdetails = new List<Banners>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetHomeBanners", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Banners b = new Banners();
                        b.status = "success";
                        b.id = Convert.ToString(ds.Tables[0].Rows[i]["id"]);
                        b.imagename = BaseURL + "images/HomeImages/resize/" + Convert.ToString(ds.Tables[0].Rows[i]["imagename"]);
                        b.createddate = Convert.ToString(ds.Tables[0].Rows[i]["createddate"]);
                        b.relatedLink = Convert.ToString(ds.Tables[0].Rows[i]["relatedLink"]);
                        b.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        b.caption = Convert.ToString(ds.Tables[0].Rows[i]["caption"]);
                        b.cityname = Convert.ToString(ds.Tables[0].Rows[i]["cityname"]);
                        Bannerdetails.Add(b);
                    }
                    return Bannerdetails.ToArray();
                }
                else
                {
                    Banners b = new Banners();
                    b.status = "fail";
                    b.id = "null";
                    b.imagename = "null";
                    b.createddate = "null";
                    b.relatedLink = "null";
                    b.title = "null";
                    b.caption = "null";
                    Bannerdetails.Add(b);
                    return Bannerdetails.ToArray();
                }
            }
            catch (Exception ex)
            {
                Banners b = new Banners();
                b.status = ex.Message.ToString();
                b.id = "null";
                b.imagename = "null";
                b.createddate = "null";
                b.relatedLink = "null";
                b.title = "null";
                b.caption = "null";
                Bannerdetails.Add(b);
                return Bannerdetails.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Register a new user
        //public UserInfo[] InsertUserInfo(string username, string useremail, string usermobile, string usercity, string loginname, string password, string name, string socialid, string logintype, string userpic)

        public UserInfo[] InsertUserInfo(string username, string useremail, string usermobile, string google_fbid, string logintype)
        {
            //   password = Encryptdata(password.Trim().Replace(" ", "+"));
            //   DataSet ds = new DataSet();
            //   ds.ReadXml("http://maps.googleapis.com/maps/api/distancematrix/xml?origins=19.01761,72.85616&destinations=18.52043,73.85674");

            List<UserInfo> listuser = new List<UserInfo>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_InsertUserInfo", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                //comm.Parameters.AddWithValue("@name", name);
                comm.Parameters.AddWithValue("@username", username);
                comm.Parameters.AddWithValue("@useremail", useremail);
                comm.Parameters.AddWithValue("@usermobile", usermobile);
                comm.Parameters.AddWithValue("@google_fbid", google_fbid);
                comm.Parameters.AddWithValue("@logintype", logintype);
                //comm.Parameters.AddWithValue("@loginname", loginname);
                //comm.Parameters.AddWithValue("@password", password);
                //comm.Parameters.AddWithValue("@socialid", socialid);
                //comm.Parameters.AddWithValue("@userpic", userpic);

                SqlParameter pmout = new SqlParameter();
                pmout.ParameterName = "@outStatus";
                pmout.DbType = DbType.String;
                pmout.Size = 1024;
                pmout.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout);

                SqlParameter outusername = new SqlParameter();
                outusername.ParameterName = "@outusername";
                outusername.DbType = DbType.String;
                outusername.Size = 1024;
                outusername.Direction = ParameterDirection.Output;
                comm.Parameters.Add(outusername);


                SqlParameter outuserid = new SqlParameter();
                outuserid.ParameterName = "@outuserid";
                outuserid.DbType = DbType.String;
                outuserid.Size = 1024;
                outuserid.Direction = ParameterDirection.Output;
                comm.Parameters.Add(outuserid);

                Int32 sqlRows = comm.ExecuteNonQuery();
                UserInfo objUserInfo = new UserInfo();
                if (sqlRows != 0)
                {
                    objUserInfo.status = comm.Parameters["@outStatus"].Value.ToString();
                    objUserInfo.username = comm.Parameters["@outusername"].Value.ToString();
                    objUserInfo.userid = comm.Parameters["@outuserid"].Value.ToString();
                    listuser.Add(objUserInfo);
                    if (objUserInfo.status == "success")
                    {
                        SendMailAdmin(username, useremail, "test@test.com", "Account has been successful created");
                    }
                }
                else
                {
                    objUserInfo.status = "fail";
                    listuser.Add(objUserInfo);
                }
                return listuser.ToArray();
            }
            catch (Exception Ex)
            {
                UserInfo objUserInfo = new UserInfo();
                objUserInfo.status = "fail";
                listuser.Add(objUserInfo);
                return listuser.ToArray();
            }
            finally
            {
                conn.Close();
            }

        }

        #endregion

        #region Method to Get login details

        public EmpanelmentContact[] GetLoginDetails(string username, string password)
        {
            WLog.WriteLog("Username =" + username + ", password=" + password, "GetLoginDetails");
            List <EmpanelmentContact> lslogin = new List<EmpanelmentContact>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetLoginDetails", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@uname", username);
                comm.Parameters.AddWithValue("@pwd", password);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    EmpanelmentContact[] b = new EmpanelmentContact[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        EmpanelmentContact objlogin = new EmpanelmentContact();
                        objlogin.status = "success";
                        objlogin.userid = Convert.ToString(ds.Tables[0].Rows[i]["EmplContID"]);
                        objlogin.brokerid = Convert.ToString(ds.Tables[0].Rows[i]["BrokerID"]);

                        lslogin.Add(objlogin);
                    }
                }
                else
                {
                    EmpanelmentContact objlogin = new EmpanelmentContact();
                    objlogin.status = "fail";
                    objlogin.userid = "null";
                    objlogin.brokerid = "null";
                    lslogin.Add(objlogin);
                }
                return lslogin.ToArray();
            }
            catch (Exception ex)
            {
                EmpanelmentContact objlogin = new EmpanelmentContact();
                objlogin.status = "fail";
                objlogin.userid = "null";
                objlogin.brokerid = "null";
                lslogin.Add(objlogin);
                return lslogin.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get Registered User Info
        public EmpanelmentContact[] GetUserInfo(string brokerid)
        {
            List<EmpanelmentContact> listuser = new List<EmpanelmentContact>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetUserInfo", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@brokerid", brokerid);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    EmpanelmentContact[] b = new EmpanelmentContact[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        EmpanelmentContact objEmpanelment = new EmpanelmentContact();
                        objEmpanelment.status = "success";
                        objEmpanelment.name = Convert.ToString(ds.Tables[0].Rows[i]["Name"]);
                        objEmpanelment.email = Convert.ToString(ds.Tables[0].Rows[i]["Email"]);
                        objEmpanelment.mobileno = Convert.ToString(ds.Tables[0].Rows[i]["Mobile"]);
                        objEmpanelment.company = Convert.ToString(ds.Tables[0].Rows[i]["Company_Name"]);
                        listuser.Add(objEmpanelment);
                    }
                }
                else
                {
                    EmpanelmentContact objEmpanelment = new EmpanelmentContact();
                    objEmpanelment.status = "fail";
                    objEmpanelment.name = "null";
                    objEmpanelment.email = "null";
                    listuser.Add(objEmpanelment);
                }
                return listuser.ToArray();
            }
            catch (Exception)
            {
                EmpanelmentContact objEmpanelment = new EmpanelmentContact();
                objEmpanelment.status = "fail";
                objEmpanelment.name = "null";
                objEmpanelment.email = "null";
                listuser.Add(objEmpanelment);
                return listuser.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region Method to Get Project Count Status wise
        public ProjectsCount[] GetProjectCount(string projecttype)
        {
            List<ProjectsCount> lsprojectcount = new List<ProjectsCount>();
            try
            {
                SqlCommand comm;
                SqlDataAdapter da;
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetProjectCount", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@projecttype", projecttype);
                SqlParameter Status_CurrGET = new SqlParameter();
                Status_CurrGET.ParameterName = "@Status_Curr";
                Status_CurrGET.DbType = DbType.String;
                Status_CurrGET.Size = 1024;
                Status_CurrGET.Direction = ParameterDirection.Output;
                comm.Parameters.Add(Status_CurrGET);

                SqlParameter Status_CompGET = new SqlParameter();
                Status_CompGET.ParameterName = "@Status_Comp";
                Status_CompGET.DbType = DbType.String;
                Status_CompGET.Size = 1024;
                Status_CompGET.Direction = ParameterDirection.Output;
                comm.Parameters.Add(Status_CompGET);

                SqlParameter Status_UPcomGET = new SqlParameter();
                Status_UPcomGET.ParameterName = "@Status_UPcom";
                Status_UPcomGET.DbType = DbType.String;
                Status_UPcomGET.Size = 1024;
                Status_UPcomGET.Direction = ParameterDirection.Output;
                comm.Parameters.Add(Status_UPcomGET);


                Int32 currentcount = comm.ExecuteNonQuery();
                ProjectsCount objCount = new ProjectsCount();

                if (currentcount != 0)
                {
                    objCount.current_count = comm.Parameters["@Status_Curr"].Value.ToString();
                    objCount.complete_count = comm.Parameters["@Status_Comp"].Value.ToString();
                    objCount.upcoming_count = comm.Parameters["@Status_UPcom"].Value.ToString();
                    objCount.status = "success";
                    lsprojectcount.Add(objCount);
                    return lsprojectcount.ToArray();
                }
                else
                {
                    objCount.current_count = "0";
                    objCount.complete_count = "0";
                    objCount.upcoming_count = "0";
                    objCount.status = "fail";
                    lsprojectcount.Add(objCount);
                    return lsprojectcount.ToArray();
                }
            }
            catch (Exception ex)
            {
                ProjectsCount objCount = new ProjectsCount();
                objCount.current_count = "0";
                objCount.complete_count = "0";
                objCount.upcoming_count = "0";
                objCount.status = "fail";
                lsprojectcount.Add(objCount);
                return lsprojectcount.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to Get Project Inner Menu Content
        public ProjectsInnerDetails[] GetProjectsInnerDetails(string menuid, string projectid)
        {
            List<ProjectsInnerDetails> objinner = new List<ProjectsInnerDetails>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetProjectsInnerDetails", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@menuid", Convert.ToInt16(menuid));
                comm.Parameters.AddWithValue("@projectid", Convert.ToInt16(projectid));

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ProjectsInnerDetails[] b = new ProjectsInnerDetails[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ProjectsInnerDetails objinnerdet = new ProjectsInnerDetails();
                        objinnerdet.status = "success";
                        objinnerdet.cityname = Convert.ToString(ds.Tables[0].Rows[i]["cityname"]);
                        objinnerdet.id = Convert.ToString(ds.Tables[0].Rows[i]["id"]);
                        objinnerdet.menutitle = Convert.ToString(ds.Tables[0].Rows[i]["menutitle"]);
                        objinnerdet.description = Convert.ToString(ds.Tables[0].Rows[i]["description"]);
                        objinnerdet.image = BaseURL + "images/Projects/" + Convert.ToString(ds.Tables[0].Rows[i]["image"]);
                        objinnerdet.projecttitle = Convert.ToString(ds.Tables[0].Rows[i]["projecttitle"]);
                        objinner.Add(objinnerdet);
                    }
                }
                else
                {
                    ProjectsInnerDetails objinnerdet = new ProjectsInnerDetails();
                    objinnerdet.status = "fail";
                    objinnerdet.cityname = "null";
                    objinnerdet.id = "null";
                    objinnerdet.menutitle = "null";
                    objinnerdet.description = "null";
                    objinnerdet.image = "null";
                    objinnerdet.projecttitle = "null";
                    objinner.Add(objinnerdet);
                }
                return objinner.ToArray();
            }
            catch (Exception)
            {
                ProjectsInnerDetails objinnerdet = new ProjectsInnerDetails();
                objinnerdet.status = "fail";
                objinnerdet.cityname = "null";
                objinnerdet.id = "null";
                objinnerdet.menutitle = "null";
                objinnerdet.description = "null";
                objinnerdet.image = "null";
                objinnerdet.projecttitle = "null";
                objinner.Add(objinnerdet);
                return objinner.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region Method For Project Search by Title
        public Projects[] GetSearchProperty_title(string projecttitle)
        {
            List<Projects> lsproject = new List<Projects>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetSearchProperty_title", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@projecttitle", projecttitle);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Projects[] b = new Projects[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Projects objproject = new Projects();
                        objproject.status = "success";
                        objproject.projectid = Convert.ToString(ds.Tables[0].Rows[i]["projectid"]);
                        objproject.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        objproject.cityname = Convert.ToString(ds.Tables[0].Rows[i]["cityname"]);
                        lsproject.Add(objproject);
                    }
                }
                else
                {
                    Projects objproject = new Projects();
                    objproject.status = "fail";
                    objproject.projectid = "null";
                    objproject.title = "null";
                    objproject.cityname = "null";
                    lsproject.Add(objproject);
                }
                return lsproject.ToArray();
            }
            catch (Exception)
            {
                Projects objproject = new Projects();
                objproject.status = "fail";
                objproject.projectid = "null";
                objproject.title = "null";
                objproject.cityname = "null";
                objproject.status = "null";
                lsproject.Add(objproject);
                return lsproject.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region Decryptdata the string value

        public string Encryptdata(string password)
        {
            string strmsg = string.Empty;
            byte[] encode = new byte[password.Length];
            encode = Encoding.UTF8.GetBytes(password);
            strmsg = Convert.ToBase64String(encode);
            return strmsg;
        }
        public string Decryptdata(string encryptpwd)
        {
            encryptpwd = encryptpwd.Trim().Replace(" ", "+");
            string decryptpwd = string.Empty;
            UTF8Encoding encodepwd = new UTF8Encoding();
            Decoder Decode = encodepwd.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
            int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            decryptpwd = new String(decoded_char);
            return decryptpwd;
        }
        #endregion

        #region To Get All Property search accoding to City

        public Projects[] GetSearchProperty_City(string projectCity)
        {
            List<Projects> lsproject = new List<Projects>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetSearchProperty_City", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@projectCity", projectCity);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Projects[] b = new Projects[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Projects objproject = new Projects();
                        objproject.status = "success";
                        objproject.projectid = Convert.ToString(ds.Tables[0].Rows[i]["projectid"]);
                        objproject.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        objproject.cityname = Convert.ToString(ds.Tables[0].Rows[i]["cityname"]);
                        objproject.status = Convert.ToString(ds.Tables[0].Rows[i]["status"]);
                        lsproject.Add(objproject);
                    }
                }
                else
                {
                    Projects objproject = new Projects();
                    objproject.status = "fail";
                    objproject.projectid = "null";
                    objproject.title = "null";
                    objproject.cityname = "null";
                    objproject.status = "null";
                    lsproject.Add(objproject);
                }
                return lsproject.ToArray();
            }
            catch (Exception)
            {
                Projects objproject = new Projects();
                objproject.status = "fail";
                objproject.projectid = "null";
                objproject.title = "null";
                objproject.cityname = "null";
                objproject.status = "null";
                lsproject.Add(objproject);
                return lsproject.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region To Get Response Message
        public Messages[] InsertMessages(string brokerid, string msgtype, string message, string country)
        {
            string mobile = "", Messagetype = "";
            List<Messages> lsmessage = new List<Messages>();
            try
            {
                if (msgtype.ToUpper() == "MOBILE")
                {
                    mobile = message;
                }
                else if (msgtype.ToUpper() == "TEXT")
                {
                    Messagetype = message;
                }

                SqlConnection conn;
                SqlCommand comm;

                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                comm = new SqlCommand("SP_InsertMessages", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@brokerid", brokerid);
                comm.Parameters.AddWithValue("@msgtype", msgtype);
                comm.Parameters.AddWithValue("@message", Messagetype);
                comm.Parameters.AddWithValue("@mobile", mobile);
                SqlParameter pmout = new SqlParameter();
                pmout.ParameterName = "@outStatus";
                pmout.DbType = DbType.String;
                pmout.Size = 1024;
                pmout.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout);
                Int32 sqlRows = comm.ExecuteNonQuery();
                if (sqlRows != 0)
                {
                    Messages ms = new Messages();
                    ms.status = comm.Parameters["@outStatus"].Value.ToString();
                    lsmessage.Add(ms);
                }
                else
                {
                    Messages ms = new Messages();
                    ms.status = "fail";
                    lsmessage.Add(ms);
                }
                return lsmessage.ToArray();
            }
            catch (Exception Ex)
            {
                Messages ms = new Messages();
                ms.status = "fail";
                lsmessage.Add(ms);
                return lsmessage.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region To get All Rooms

        public Rooms[] GetRooms()
        {
            List<Rooms> lsRooms = new List<Rooms>();
            try
            {
                //string constring = ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString;
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetRooms", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Projects[] b = new Projects[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Rooms objRooms = new Rooms();
                        objRooms.status = "success";
                        objRooms.Roomid = Convert.ToString(ds.Tables[0].Rows[i]["Roomid"]);
                        objRooms.Roomtype = Convert.ToString(ds.Tables[0].Rows[i]["Roomtype"]);
                        objRooms.Createddate = Convert.ToString(ds.Tables[0].Rows[i]["Createddate"]);
                        lsRooms.Add(objRooms);
                    }
                }
                else
                {
                    Rooms objRooms = new Rooms();
                    objRooms.status = "fail";
                    objRooms.Roomid = "null";
                    objRooms.Roomtype = "null";
                    objRooms.Createddate = "null";
                    lsRooms.Add(objRooms);
                }
                return lsRooms.ToArray();
            }
            catch (Exception)
            {
                Rooms objRooms = new Rooms();
                objRooms.status = "fail";
                objRooms.Roomid = "null";
                objRooms.Roomtype = "null";
                objRooms.Createddate = "null";
                lsRooms.Add(objRooms);
                return lsRooms.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region To get All ProjectVSRoom

        public ProjectVSRoom[] GetProjectVSRoom()
        {
            List<ProjectVSRoom> lsProjectVSRoom = new List<ProjectVSRoom>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_ProjectVSRoom", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ProjectVSRoom[] b = new ProjectVSRoom[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ProjectVSRoom objProjectVSRoom = new ProjectVSRoom();
                        objProjectVSRoom.Status = "success";
                        objProjectVSRoom.Roomtype = Convert.ToString(ds.Tables[0].Rows[i]["Roomtype"]);
                        objProjectVSRoom.Title = Convert.ToString(ds.Tables[0].Rows[i]["Title"]);
                        objProjectVSRoom.Createddate = Convert.ToString(ds.Tables[0].Rows[i]["Createddate"]);
                        lsProjectVSRoom.Add(objProjectVSRoom);
                    }
                }
                else
                {
                    ProjectVSRoom objProjectVSRoom = new ProjectVSRoom();
                    objProjectVSRoom.Status = "fail";
                    objProjectVSRoom.Roomtype = "null";
                    objProjectVSRoom.Title = "null";
                    objProjectVSRoom.Createddate = "null";
                    lsProjectVSRoom.Add(objProjectVSRoom);
                }
                return lsProjectVSRoom.ToArray();
            }
            catch (Exception)
            {
                ProjectVSRoom objProjectVSRoom = new ProjectVSRoom();
                objProjectVSRoom.Status = "fail";
                objProjectVSRoom.Roomtype = "null";
                objProjectVSRoom.Title = "null";
                objProjectVSRoom.Createddate = "null";
                lsProjectVSRoom.Add(objProjectVSRoom);
                return lsProjectVSRoom.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region To Insert Inquery details

        public Enquiry[] InsertEnquiry(string firstname, string lastname, string email, string country, string mobile, string projectid, string description, string state, string city, string userid, string apartment)
        {
            List<Enquiry> listEnquiry = new List<Enquiry>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_InsertEnquiryInfo", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@firstname", firstname);
                comm.Parameters.AddWithValue("@lastname", lastname);
                comm.Parameters.AddWithValue("@email", email);
                comm.Parameters.AddWithValue("@country", country);
                comm.Parameters.AddWithValue("@mobile", mobile);
                comm.Parameters.AddWithValue("@projectid", projectid);
                comm.Parameters.AddWithValue("@description", description);
                comm.Parameters.AddWithValue("@state", state);
                comm.Parameters.AddWithValue("@city", city);
                comm.Parameters.AddWithValue("@userid", userid);
                comm.Parameters.AddWithValue("@apartment", apartment);
                SqlParameter pmout = new SqlParameter();
                pmout.ParameterName = "@outStatus";
                pmout.DbType = DbType.String;
                pmout.Size = 1024;
                pmout.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout);
                Int32 sqlRows = comm.ExecuteNonQuery();
                Enquiry objEnquiry = new Enquiry();
                if (sqlRows != 0)
                {
                    objEnquiry.status = comm.Parameters["@outStatus"].Value.ToString();
                    listEnquiry.Add(objEnquiry);

                    if (objEnquiry.status == "success")
                    {
                        //string URL = "https://www.salesforce.com/servlet/servlet.WebToLead?first_name=" + txtFirstName.Text + "&last_name=" + txtLastName.Text + "&00N90000000k2nZ=" + ddlCountry.SelectedValue + "&00N90000000kb3h=" + txtcity.Text + "&00N90000000kb3i=" + txtMobNo2.Text + "&email=" + txtEmailID.Text + "&00N90000000I4Zw=''&00N90000000I4Zx=''&URL=WAP&00N90000000HYVj=" + System.DateTime.Now.ToString("MM/dd/yyyy") + "&00N900000012Up9=Godrej United, Whitefield,Bengaluru&rating=''&00N900000012Up8=''&00N90000000HYVr=''&01290000000HHj5=ResidentialLead";
                        string URL = "https://www.salesforce.com/servlet/servlet.WebToLead?first_name=" + firstname + "&last_name=" + lastname + "&00N90000000kb3i=" + mobile + "&email=" + email + "&00N90000000I4Zw=''&00N90000000I4Zx=''&URL=FBTAB&00N90000000HYVj=" + System.DateTime.Now.ToString("MM/dd/yyyy") + "&00N900000012Up9=" + projectid + "&rating=''&00N900000012Up8=''&00N90000000HYVr=''&01290000000HHj4=";
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                        request.KeepAlive = false;
                        request.ProtocolVersion = HttpVersion.Version10;
                        request.Method = "POST";
                        request.Timeout = 30000;
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                        //// send email
                        //SendMailAdmin(firstname + " " + lastname, email, "test@test.com", "Enquire has been submited");
                    }
                    //  objEnquiry.status = "success";
                }
                else
                {
                    objEnquiry.status = "fail";
                    listEnquiry.Add(objEnquiry);
                }
                return listEnquiry.ToArray();
            }
            catch (Exception Ex)
            {
                Enquiry objEnquiry = new Enquiry();
                objEnquiry.status = "fail";
                listEnquiry.Add(objEnquiry);
                return listEnquiry.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region To Insert lead details

        public Enquiry[] InsertLead(string firstname, string lastname, string email, string country, string mobile, string projectid, string project, string description, string state, string city, string apartment, string userid, string brokerid)
        {
            List<Enquiry> listEnquiry = new List<Enquiry>();
            string URL = "";
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_InsertLeadInfo", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@firstname", firstname);
                comm.Parameters.AddWithValue("@lastname", lastname);
                comm.Parameters.AddWithValue("@email", email);
                comm.Parameters.AddWithValue("@country", country);
                comm.Parameters.AddWithValue("@mobile", mobile);
                comm.Parameters.AddWithValue("@projectid", projectid);
                comm.Parameters.AddWithValue("@description", description);
                comm.Parameters.AddWithValue("@state", state);
                comm.Parameters.AddWithValue("@city", city);
                comm.Parameters.AddWithValue("@apartment", apartment);
                comm.Parameters.AddWithValue("@userid", userid); ;
                comm.Parameters.AddWithValue("@brokerid", brokerid);
                SqlParameter pmout = new SqlParameter();
                pmout.ParameterName = "@outStatus";
                pmout.DbType = DbType.String;
                pmout.Size = 1024;
                pmout.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout);
                Int32 sqlRows = comm.ExecuteNonQuery();
                Enquiry objEnquiry = new Enquiry();
                if (sqlRows != 0)
                {
                    objEnquiry.status = comm.Parameters["@outStatus"].Value.ToString();
                    listEnquiry.Add(objEnquiry);

                    //if (objEnquiry.status == "success")
                    //{
                    //    if (projectid == "51")
                    //        URL = "https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8&oid=00D900000000kOQ&URL=&lead_source=Channel Partner&00N90000000I4Zx=Channel Partner&lead_origin=Channel Partner&00N90000000HYW3=Open&00N90000000HYVj=" + System.DateTime.Now.ToString("MM/dd/yyyy") + "&first_name=" + firstname.Replace(' ', '+') + "&last_name=" + lastname.Replace(' ', '+') + "&00N90000000k2nZ=" + country.Replace(' ', '+') + "&00N90000000kb3h=&city=" + city + "&00N90000000kb3i=" + mobile + "&00N900000012Up9=Godrej+United,+Whitefield,Bengaluru&email=" + email + "&recordType=&00N90000000HYVr=&rating=Warm&Campaign_ID=70190000000o4ui&00N90000000kWuc=";
                    //    else if (projectid == "53")
                    //        URL = "https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8&oid=00D900000000kOQ&URL=&lead_source=Channel Partner&00N90000000I4Zx=Channel Partner&lead_origin=Channel Partner&00N90000000HYW3=Open&00N90000000HYVj=" + System.DateTime.Now.ToString("MM/dd/yyyy") + "&first_name=" + firstname.Replace(' ', '+') + "&last_name=" + lastname.Replace(' ', '+') + "&00N90000000k2nZ=" + country.Replace(' ', '+') + "&00N90000000kb3h=&city=" + city + "&00N90000000kb3i=" + mobile + "&00N900000012Up9=Godrej+City,+Panvel,+Mumbai&email=" + email + "&recordType=&00N90000000HYVr=&rating=Warm&Campaign_ID=70190000000o4ui&00N90000000kWuc=";
                    //    else if (projectid == "56")
                    //        URL = "https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8&oid=00D900000000kOQ&URL=&lead_source=Channel Partner&00N90000000I4Zx=Channel Partner&lead_origin=Channel Partner&00N90000000HYW3=Open&00N90000000HYVj=" + System.DateTime.Now.ToString("MM/dd/yyyy") + "&first_name=" + firstname.Replace(' ', '+') + "&last_name=" + lastname.Replace(' ', '+') + "&00N90000000k2nZ=" + country.Replace(' ', '+') + "&00N90000000kb3h=&city=" + city + "&00N90000000kb3i=" + mobile + "&00N900000012Up9=Godrej+Icon,+Gurgaon&email=" + email + "&recordType=&00N90000000HYVr=&rating=Warm&Campaign_ID=70190000000o4ui&00N90000000kWuc=";
                    //    else if (projectid == "14")
                    //        URL = "https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8&oid=00D900000000kOQ&URL=&lead_source=Channel Partner&00N90000000I4Zx=Channel Partner&lead_origin=Channel Partner&00N90000000HYW3=Open&00N90000000HYVj=" + System.DateTime.Now.ToString("MM/dd/yyyy") + "&first_name=" + firstname.Replace(' ', '+') + "&last_name=" + lastname.Replace(' ', '+') + "&00N90000000k2nZ=" + country.Replace(' ', '+') + "&00N90000000kb3h=&city=" + city + "&00N90000000kb3i=" + mobile + "&00N900000012Up9=Godrej+17,+Bengaluru&email=" + email + "&recordType=&00N90000000HYVr=&rating=Warm&Campaign_ID=70190000000o4ui&00N90000000kWuc=";
                    //    else if (projectid == "57")
                    //        URL = "https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8&oid=00D900000000kOQ&URL=&lead_source=Channel Partner&00N90000000I4Zx=Channel Partner&lead_origin=Channel Partner&00N90000000HYW3=Open&00N90000000HYVj=" + System.DateTime.Now.ToString("MM/dd/yyyy") + "&first_name=" + firstname.Replace(' ', '+') + "&last_name=" + lastname.Replace(' ', '+') + "&00N90000000k2nZ=" + country.Replace(' ', '+') + "&00N90000000kb3h=&city=" + city + "&00N90000000kb3i=" + mobile + "&00N900000012Up9=Townhomes+at+Godrej+Gold+County,+Bengaluru&email=" + email + "&recordType=&00N90000000HYVr=&rating=Warm&Campaign_ID=70190000000o4ui&00N90000000kWuc=";
                    //    else if (projectid == "58")
                    //        URL = "https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8&oid=00D900000000kOQ&URL=&lead_source=Channel Partner&00N90000000I4Zx=Channel Partner&lead_origin=Channel Partner&00N90000000HYW3=Open&00N90000000HYVj=" + System.DateTime.Now.ToString("MM/dd/yyyy") + "&first_name=" + firstname.Replace(' ', '+') + "&last_name=" + lastname.Replace(' ', '+') + "&00N90000000k2nZ=" + country.Replace(' ', '+') + "&00N90000000kb3h=&city=" + city + "&00N90000000kb3i=" + mobile + "&00N900000012Up9=Godrej+Aria,+Gurgaon&email=" + email + "&recordType=&00N90000000HYVr=&rating=Warm&Campaign_ID=70190000000o4ui&00N90000000kWuc=";
                    //    else
                    //        URL = "https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8&oid=00D900000000kOQ&URL=&lead_source=Channel Partner&00N90000000I4Zx=Channel Partner&lead_origin=Channel Partner&00N90000000HYW3=Open&00N90000000HYVj=" + System.DateTime.Now.ToString("MM/dd/yyyy") + "&first_name=" + firstname.Replace(' ', '+') + "&last_name=" + lastname.Replace(' ', '+') + "&00N90000000k2nZ=" + country.Replace(' ', '+') + "&00N90000000kb3h=&city=" + city + "&00N90000000kb3i=" + mobile + "&00N900000012Up9=" + project.Replace(' ', '+') + "&email=" + email + "&recordType=&00N90000000HYVr=&rating=Warm&Campaign_ID=&00N90000000kWuc=";

                    //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                    //    request.KeepAlive = false;
                    //    request.ProtocolVersion = HttpVersion.Version10;
                    //    request.Method = "POST";
                    //    request.Timeout = 30000;
                    //    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    //    //// send email
                    //    //SendMailAdmin(firstname + " " + lastname, email, "test@test.com", "Enquire has been submited");
                    //}
                }
                else
                {
                    objEnquiry.status = "fail";
                    listEnquiry.Add(objEnquiry);
                }
                return listEnquiry.ToArray();
            }
            catch (Exception Ex)
            {
                Enquiry objEnquiry = new Enquiry();
                objEnquiry.status = "fail";
                objEnquiry.url = "fail";
                listEnquiry.Add(objEnquiry);
                return listEnquiry.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region To get user Credentials

        public UserInfo[] GetCredentials(string Username, string Password)
        {
            Password = Encryptdata(Password.Trim().Replace(" ", "+"));
            List<UserInfo> lsUserInfo = new List<UserInfo>();
            try
            {
                //string constring = ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString;
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                comm = new SqlCommand("SP_GetCredentials", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@username", Username);
                comm.Parameters.AddWithValue("@Password", Password);

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {

                    UserInfo[] b = new UserInfo[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {

                        UserInfo objUserInfo = new UserInfo();
                        objUserInfo.status = "success";
                        objUserInfo.name = Convert.ToString(ds.Tables[0].Rows[i]["name"]);
                        lsUserInfo.Add(objUserInfo);
                    }
                }
                else
                {
                    UserInfo objUserInfo = new UserInfo();
                    objUserInfo.status = "fail";
                    objUserInfo.name = "null";
                    lsUserInfo.Add(objUserInfo);
                }
                return lsUserInfo.ToArray();
            }
            catch (Exception)
            {
                UserInfo objUserInfo = new UserInfo();
                objUserInfo.status = "fail";
                objUserInfo.name = "null";
                lsUserInfo.Add(objUserInfo);
                return lsUserInfo.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region To get all Project Accoding to ID's
        public Projects[] GetProjectsIds(string ids)
        {
            List<Projects> lsproject = new List<Projects>();
            try
            {
                //string constring = ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString;
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                comm = new SqlCommand("SP_GetProject_ID", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@projectId", ids);

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Projects[] b = new Projects[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Projects objproject = new Projects();
                        objproject.status = "success";
                        objproject.projectid = Convert.ToString(ds.Tables[0].Rows[i]["projectid"]);
                        objproject.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        objproject.cityname = Convert.ToString(ds.Tables[0].Rows[i]["cityname"]);
                        objproject.status = Convert.ToString(ds.Tables[0].Rows[i]["status"]);
                        lsproject.Add(objproject);
                    }
                }
                else
                {
                    Projects objproject = new Projects();
                    objproject.status = "fail";
                    objproject.projectid = "null";
                    objproject.title = "null";
                    objproject.cityname = "null";
                    lsproject.Add(objproject);
                }
                return lsproject.ToArray();
            }
            catch (Exception)
            {
                Projects objproject = new Projects();
                objproject.status = "fail";
                objproject.projectid = "null";
                objproject.title = "null";
                objproject.cityname = "null";
                objproject.status = "null";
                lsproject.Add(objproject);
                return lsproject.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region To get all the Gallery Images
        public Gallery[] FetchGallery(string projectid)
        {
            List<Gallery> lsGallery = new List<Gallery>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetGallery", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@projectId", projectid);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Gallery[] b = new Gallery[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Gallery objGallery = new Gallery();
                        objGallery.status = "success";
                        objGallery.GalleryID = Convert.ToString(ds.Tables[0].Rows[i]["GalleryID"]);
                        objGallery.Image = BaseURL + "images/Gallery/" + Convert.ToString(ds.Tables[0].Rows[i]["Image"]);
                        lsGallery.Add(objGallery);
                    }
                }
                else
                {
                    Gallery objGallery = new Gallery();
                    objGallery.status = "fail";
                    objGallery.GalleryID = "null";
                    objGallery.Image = "null";
                    lsGallery.Add(objGallery);

                }
                return lsGallery.ToArray();
            }
            catch (Exception)
            {
                Gallery objGallery = new Gallery();
                objGallery.status = "fail";
                objGallery.GalleryID = "null";
                objGallery.Image = "null";

                lsGallery.Add(objGallery);
                return lsGallery.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region To get Videos
        public Videos[] FetchVideos(string projectid, string userid)
        {
            List<Videos> lsVideos = new List<Videos>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetVideos", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@projectId", projectid);
                comm.Parameters.AddWithValue("@userid", userid);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Videos[] b = new Videos[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Videos objVideos = new Videos();
                        objVideos.Status = "success";
                        objVideos.VideoId = Convert.ToString(ds.Tables[0].Rows[i]["VideoId"]);
                        objVideos.videoUrl = Convert.ToString(ds.Tables[0].Rows[i]["videoUrl"]);
                        objVideos.VideoName = Convert.ToString(ds.Tables[0].Rows[i]["VideoName"]);
                        objVideos.projectId = Convert.ToString(ds.Tables[0].Rows[i]["projectId"]);
                        objVideos.Createddate = Convert.ToString(ds.Tables[0].Rows[i]["Createddate"]);
                        objVideos.favourite = Convert.ToString(ds.Tables[0].Rows[i]["favourite"]);
                        lsVideos.Add(objVideos);
                    }
                }
                else
                {
                    Videos objVideos = new Videos();
                    objVideos.Status = "fail";
                    objVideos.VideoId = "null";
                    objVideos.videoUrl = "null";
                    objVideos.VideoName = "null";
                    objVideos.projectId = "null";
                    objVideos.Createddate = "null";
                    objVideos.favourite = "null";
                    lsVideos.Add(objVideos);
                }
                return lsVideos.ToArray();
            }
            catch (Exception Ex)
            {
                Videos objVideos = new Videos();
                objVideos.Status = "fail";
                objVideos.VideoId = "null";
                objVideos.videoUrl = "null";
                objVideos.VideoName = "null";
                objVideos.projectId = "null";
                objVideos.Createddate = "null";
                objVideos.favourite = "null";
                lsVideos.Add(objVideos);
                return lsVideos.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region To Get All property accoding to Addvance Search

        public Projects[] GetProperty_AddvanceSearch(string projectid, string projectRoom)
        {
            List<Projects> lsproject = new List<Projects>();
            try
            {
                //string constring = ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString;
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetSearchProperty_Rooms", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@projectid", projectid);
                comm.Parameters.AddWithValue("@projectRooms", projectRoom);

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Projects[] b = new Projects[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {

                        Projects objproject = new Projects();
                        objproject.status = "success";
                        objproject.projectid = Convert.ToString(ds.Tables[0].Rows[i]["projectid"]);
                        objproject.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        objproject.cityname = Convert.ToString(ds.Tables[0].Rows[i]["cityname"]);
                        objproject.status = Convert.ToString(ds.Tables[0].Rows[i]["status"]);
                        lsproject.Add(objproject);
                    }
                }
                else
                {
                    Projects objproject = new Projects();
                    objproject.status = "fail";
                    objproject.projectid = "null";
                    objproject.title = "null";
                    objproject.cityname = "null";
                    lsproject.Add(objproject);
                }
                return lsproject.ToArray();
            }
            catch (Exception)
            {
                Projects objproject = new Projects();
                objproject.status = "fail";
                objproject.projectid = "null";
                objproject.title = "null";
                objproject.cityname = "null";
                lsproject.Add(objproject);
                return lsproject.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region To Autocomplete functionality

        public Projects[] GetAutocomplete(string projecttitle)
        {
            List<Projects> lsproject = new List<Projects>();
            try
            {
                //string constring = ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString;
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_Gettitle", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@projecttitle", projecttitle);

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Projects[] b = new Projects[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Projects objproject = new Projects();
                        objproject.status = "success";
                        objproject.projectid = Convert.ToString(ds.Tables[0].Rows[i]["projectid"]);
                        objproject.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        lsproject.Add(objproject);
                    }
                }
                else
                {
                    Projects objproject = new Projects();
                    objproject.status = "fail";
                    objproject.projectid = "null";
                    objproject.title = "null";
                    lsproject.Add(objproject);
                }
                return lsproject.ToArray();
            }
            catch (Exception)
            {
                Projects objproject = new Projects();
                objproject.status = "fail";
                objproject.projectid = "Null";
                objproject.title = "null";
                lsproject.Add(objproject);
                return lsproject.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region To Get latitude longitude value Accoding to project ID's
        public Project_lati_longi[] Getlatlongitude(string projectId)
        {
            List<Project_lati_longi> lsproject = new List<Project_lati_longi>();
            try
            {
                //string constring = ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString;
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                comm = new SqlCommand("SP_GetProject_latlongitude", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@projectId", projectId);

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Project_lati_longi[] b = new Project_lati_longi[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Project_lati_longi objproject = new Project_lati_longi();
                        objproject.status = "success";
                        objproject.latitude = Convert.ToString(ds.Tables[0].Rows[i]["latitude"]);
                        objproject.longitude = Convert.ToString(ds.Tables[0].Rows[i]["longitude"]);
                        objproject.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        lsproject.Add(objproject);
                    }
                }
                else
                {
                    Project_lati_longi objproject = new Project_lati_longi();
                    objproject.status = "fail";
                    objproject.latitude = "null";
                    objproject.longitude = "null";
                    lsproject.Add(objproject);
                }
                return lsproject.ToArray();
            }
            catch (Exception)
            {
                Project_lati_longi objproject = new Project_lati_longi();
                objproject.status = "fail";
                objproject.latitude = "null";
                objproject.longitude = "null";
                lsproject.Add(objproject);
                return lsproject.ToArray();
            }
            finally
            {
                conn.Close();
            }

        }

        #endregion

        #region Add the User Favourit list
        public UserFavourites[] InsertUserFavourite(string projectid, string userid, string flag)
        {
            List<UserFavourites> listUserFavourites = new List<UserFavourites>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                comm = new SqlCommand("SP_InsertUserFavourite", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@projectid", projectid);
                comm.Parameters.AddWithValue("@userid", userid);
                comm.Parameters.AddWithValue("@flag", flag);
                SqlParameter pmout = new SqlParameter();
                pmout.ParameterName = "@outStatus";
                pmout.DbType = DbType.String;
                pmout.Size = 1024;
                pmout.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout);
                Int32 sqlRows = comm.ExecuteNonQuery();
                UserFavourites objUserInfo = new UserFavourites();
                if (sqlRows != 0)
                {
                    objUserInfo.status = comm.Parameters["@outStatus"].Value.ToString();
                    listUserFavourites.Add(objUserInfo);
                }
                else
                {
                    objUserInfo.status = "fail";
                    listUserFavourites.Add(objUserInfo);
                }
                return listUserFavourites.ToArray();
            }
            catch (Exception Ex)
            {
                UserFavourites objUserInfo = new UserFavourites();
                objUserInfo.status = "fail";
                listUserFavourites.Add(objUserInfo);
                return listUserFavourites.ToArray();
            }
            finally
            {
                conn.Close();
            }

        }

        #endregion

        #region Get the User Favourit list
        public UserFavourites[] FetchUserFavourite(string projectid, string userid)
        {
            List<UserFavourites> listUserFavourites = new List<UserFavourites>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetUserFavourite", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@projectid", projectid);
                comm.Parameters.AddWithValue("@userid", userid);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    UserFavourites[] b = new UserFavourites[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        UserFavourites objlsFavourites = new UserFavourites();
                        objlsFavourites.status = "success";
                        objlsFavourites.projectid = Convert.ToString(ds.Tables[0].Rows[i]["projectid"]);
                        objlsFavourites.userid = Convert.ToString(ds.Tables[0].Rows[i]["userid"]);
                        objlsFavourites.createddate = Convert.ToString(ds.Tables[0].Rows[i]["createddate"]);
                        listUserFavourites.Add(objlsFavourites);
                    }
                }
                else
                {
                    UserFavourites objlsFavourites = new UserFavourites();
                    objlsFavourites.status = "fail";
                    objlsFavourites.projectid = "null";
                    objlsFavourites.userid = "null";
                    objlsFavourites.createddate = "null";
                    listUserFavourites.Add(objlsFavourites);

                }
                return listUserFavourites.ToArray();
            }
            catch (Exception Ex)
            {
                UserFavourites objlsFavourites = new UserFavourites();
                objlsFavourites.status = "fail";
                objlsFavourites.projectid = "null";
                objlsFavourites.userid = "null";
                objlsFavourites.createddate = "null";
                listUserFavourites.Add(objlsFavourites);
                return listUserFavourites.ToArray();
            }
        }

        #endregion

        #region Add the Recent Search information
        public RecentSearch[] InsertRecentSearchinfo(string projectid, string userid)
        {
            List<RecentSearch> listRecentSearch = new List<RecentSearch>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_InsertRecentSearch", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@projectid", projectid);
                comm.Parameters.AddWithValue("@userid", userid);
                SqlParameter pmout = new SqlParameter();
                pmout.ParameterName = "@outStatus";
                pmout.DbType = DbType.String;
                pmout.Size = 1024;
                pmout.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout);
                Int32 sqlRows = comm.ExecuteNonQuery();
                RecentSearch objRecentSearch = new RecentSearch();
                if (sqlRows != 0)
                {
                    objRecentSearch.status = comm.Parameters["@outStatus"].Value.ToString();
                    listRecentSearch.Add(objRecentSearch);
                }
                else
                {
                    objRecentSearch.status = "fail";
                    listRecentSearch.Add(objRecentSearch);
                }
                return listRecentSearch.ToArray();
            }
            catch (Exception Ex)
            {
                RecentSearch objRecentSearch = new RecentSearch();
                objRecentSearch.status = "fail";
                listRecentSearch.Add(objRecentSearch);
                return listRecentSearch.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Get the User Recent search
        public RecentSearch[] FetchRecentSearchinfo(string userid)
        {
            List<RecentSearch> listRecentSearch = new List<RecentSearch>();
            try
            {

                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                comm = new SqlCommand("SP_getRecentSearch", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@userid", userid);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    RecentSearch[] b = new RecentSearch[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        RecentSearch objRecentSearch = new RecentSearch();
                        objRecentSearch.status = "success";
                        objRecentSearch.projectid = Convert.ToString(ds.Tables[0].Rows[i]["projectid"]);
                        // objRecentSearch.userid = Convert.ToString(ds.Tables[0].Rows[i]["userid"]);
                        objRecentSearch.createddate = Convert.ToString(ds.Tables[0].Rows[i]["createdate"]);

                        objRecentSearch.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        objRecentSearch.cityname = Convert.ToString(ds.Tables[0].Rows[i]["cityname"]);
                        objRecentSearch.image = BaseURL + "images/projects/resize/" + Convert.ToString(ds.Tables[0].Rows[i]["image"]);
                        listRecentSearch.Add(objRecentSearch);
                    }
                }
                else
                {
                    RecentSearch objRecentSearch = new RecentSearch();
                    objRecentSearch.status = "fail";
                    objRecentSearch.projectid = "null";
                    objRecentSearch.userid = "null";
                    objRecentSearch.createddate = "null";
                    listRecentSearch.Add(objRecentSearch);

                }
                return listRecentSearch.ToArray();
            }
            catch (Exception Ex)
            {
                RecentSearch objRecentSearch = new RecentSearch();
                objRecentSearch.status = "fail";
                objRecentSearch.projectid = "null";
                objRecentSearch.userid = "null";
                objRecentSearch.createddate = "null";
                listRecentSearch.Add(objRecentSearch);
                return listRecentSearch.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region To get Projects features
        public ProjectHighlights[] FetchProjectFetures(string projectid)
        {
            List<ProjectHighlights> lsfeatures = new List<ProjectHighlights>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                comm = new SqlCommand("SP_GetProjectHighlights", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@projectId", projectid);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ProjectHighlights[] b = new ProjectHighlights[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ProjectHighlights objlsfeatures = new ProjectHighlights();
                        objlsfeatures.status = "success";
                        objlsfeatures.projectid = Convert.ToString(ds.Tables[0].Rows[i]["projectid"]);
                        objlsfeatures.menuid = Convert.ToString(ds.Tables[0].Rows[i]["menuid"]);
                        objlsfeatures.icon = BaseURL + "images/Highlights/" + Convert.ToString(ds.Tables[0].Rows[i]["icon"]);
                        objlsfeatures.description = Convert.ToString(ds.Tables[0].Rows[i]["description"]);
                        objlsfeatures.createddate = Convert.ToString(ds.Tables[0].Rows[i]["createddate"]);
                        lsfeatures.Add(objlsfeatures);
                    }
                }
                else
                {
                    ProjectHighlights objlsfeatures = new ProjectHighlights();
                    objlsfeatures.status = "fail";
                    objlsfeatures.projectid = "null";
                    objlsfeatures.menuid = "null";
                    objlsfeatures.icon = "null";
                    objlsfeatures.description = "null";
                    objlsfeatures.createddate = "null";
                    lsfeatures.Add(objlsfeatures);

                }
                return lsfeatures.ToArray();
            }
            catch (Exception Ex)
            {
                ProjectHighlights objlsfeatures = new ProjectHighlights();
                objlsfeatures.status = "fail";
                objlsfeatures.projectid = "null";
                objlsfeatures.menuid = "null";
                objlsfeatures.icon = "null";
                objlsfeatures.description = "null";
                objlsfeatures.createddate = "null";
                lsfeatures.Add(objlsfeatures);
                return lsfeatures.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region To get Projects Floor Plan
        public FloorPlans[] FetchProjectFloorPlans(string projectid)
        {
            List<FloorPlans> lsFloorPlans = new List<FloorPlans>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                comm = new SqlCommand("SP_GetFloorPlans", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@projectId", projectid);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    FloorPlans[] b = new FloorPlans[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        FloorPlans objlsFloorPlans = new FloorPlans();
                        objlsFloorPlans.status = "success";
                        objlsFloorPlans.projectid = Convert.ToString(ds.Tables[0].Rows[i]["projectid"]);
                        objlsFloorPlans.menuid = Convert.ToString(ds.Tables[0].Rows[i]["menuid"]);
                        objlsFloorPlans.image = BaseURL + "images/FloorPlans/" + Convert.ToString(ds.Tables[0].Rows[i]["image"]);
                        objlsFloorPlans.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        // objlsFloorPlans.createddate = Convert.ToString(ds.Tables[0].Rows[i]["createddate"]);
                        objlsFloorPlans.id = Convert.ToString(ds.Tables[0].Rows[i]["id"]);
                        objlsFloorPlans.cityname = Convert.ToString(ds.Tables[0].Rows[i]["cityname"]);
                        objlsFloorPlans.projecttitle = Convert.ToString(ds.Tables[0].Rows[i]["projecttitle"]);
                        lsFloorPlans.Add(objlsFloorPlans);
                    }
                }
                else
                {
                    FloorPlans objlsFloorPlans = new FloorPlans();
                    objlsFloorPlans.status = "fail";
                    objlsFloorPlans.projectid = "null";
                    objlsFloorPlans.menuid = "null";
                    objlsFloorPlans.image = "null";
                    objlsFloorPlans.title = "null";
                    objlsFloorPlans.createddate = "null";
                    objlsFloorPlans.id = "null";
                    objlsFloorPlans.cityname = "null";
                    objlsFloorPlans.projecttitle = "null";
                    lsFloorPlans.Add(objlsFloorPlans);

                }
                return lsFloorPlans.ToArray();
            }
            catch (Exception Ex)
            {
                FloorPlans objlsFloorPlans = new FloorPlans();
                objlsFloorPlans.status = "fail";
                objlsFloorPlans.projectid = "null";
                objlsFloorPlans.menuid = "null";
                objlsFloorPlans.image = "null";
                objlsFloorPlans.title = "null";
                objlsFloorPlans.createddate = "null";
                objlsFloorPlans.id = "null";
                objlsFloorPlans.cityname = "null";
                objlsFloorPlans.projecttitle = "null";
                lsFloorPlans.Add(objlsFloorPlans);
                return lsFloorPlans.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region To Insert Emi details

        public EmiEnquire[] InsertEmiInfo(string userid, string projectid, string EmiAmount, string EmiIntrest, string NoofYears, string EmiPM)
        {
            List<EmiEnquire> lsEmiEnquire = new List<EmiEnquire>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_InsertEMIEnquiryInfo", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@userid", userid);
                comm.Parameters.AddWithValue("@projectid", projectid);
                comm.Parameters.AddWithValue("@EmiAmount", EmiAmount);
                comm.Parameters.AddWithValue("@EmiIntrest", EmiIntrest);
                comm.Parameters.AddWithValue("@NoofYears", NoofYears);
                comm.Parameters.AddWithValue("@EmiPM", EmiPM);
                SqlParameter pmout = new SqlParameter();
                pmout.ParameterName = "@outStatus";
                pmout.DbType = DbType.String;
                pmout.Size = 1024;
                pmout.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout);

                SqlParameter pusername = new SqlParameter();
                pusername.ParameterName = "@username";
                pusername.DbType = DbType.String;
                pusername.Size = 1024;
                pusername.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pusername);

                SqlParameter pemail = new SqlParameter();
                pemail.ParameterName = "@email";
                pemail.DbType = DbType.String;
                pemail.Size = 1024;
                pemail.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pemail);

                Int32 sqlRows = comm.ExecuteNonQuery();
                EmiEnquire objEmiEnquire = new EmiEnquire();
                if (sqlRows != 0)
                {
                    objEmiEnquire.status = comm.Parameters["@outStatus"].Value.ToString();
                    objEmiEnquire.username = comm.Parameters["@username"].Value.ToString();
                    objEmiEnquire.email = comm.Parameters["@email"].Value.ToString();
                    lsEmiEnquire.Add(objEmiEnquire);
                }
                else
                {
                    objEmiEnquire.status = "fail";
                    lsEmiEnquire.Add(objEmiEnquire);
                }
                return lsEmiEnquire.ToArray();
            }
            catch (Exception Ex)
            {
                EmiEnquire objEmiEnquire = new EmiEnquire();
                objEmiEnquire.status = "fail";
                lsEmiEnquire.Add(objEmiEnquire);
                return lsEmiEnquire.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region To add Email subcription details
        public getEmailSubscription[] Insertsubscription(string Subscription, string userid)
        {
            List<getEmailSubscription> lsSubscription = new List<getEmailSubscription>();
            try
            {

                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                comm = new SqlCommand("SP_InsertEmailSubscription", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@userid", userid);
                comm.Parameters.AddWithValue("@Subscription", Subscription);

                SqlParameter pmout = new SqlParameter();
                pmout.ParameterName = "@outStatus";
                pmout.DbType = DbType.String;
                pmout.Size = 1024;
                pmout.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout);


                Int32 sqlRows = comm.ExecuteNonQuery();
                getEmailSubscription objSubscription = new getEmailSubscription();
                if (sqlRows != 0)
                {
                    objSubscription.status = comm.Parameters["@outStatus"].Value.ToString();
                    lsSubscription.Add(objSubscription);
                }
                else
                {
                    objSubscription.status = "fail";
                    lsSubscription.Add(objSubscription);
                }
                return lsSubscription.ToArray();
            }
            catch (Exception Ex)
            {
                getEmailSubscription objSubscription = new getEmailSubscription();
                objSubscription.status = "fail";
                lsSubscription.Add(objSubscription);
                return lsSubscription.ToArray();
            }
            finally
            {
                conn.Close();
            }

        }

        #endregion

        #region To get Email Subcription
        public getEmailSubscription[] FetchEmailSubcription(string userid)
        {
            List<getEmailSubscription> lsemailsub = new List<getEmailSubscription>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                comm = new SqlCommand("SP_getEmailSubscription", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@userid", userid);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    getEmailSubscription[] b = new getEmailSubscription[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        getEmailSubscription objemail = new getEmailSubscription();
                        objemail.status = "success";
                        objemail.userid = Convert.ToString(ds.Tables[0].Rows[i]["userid"]);
                        objemail.Subscription = Convert.ToString(ds.Tables[0].Rows[i]["subscription"]);
                        objemail.createddate = Convert.ToString(ds.Tables[0].Rows[i]["createdate"]);
                        lsemailsub.Add(objemail);
                    }
                }
                else
                {
                    getEmailSubscription objemail = new getEmailSubscription();
                    objemail.status = "fail";
                    objemail.userid = "null";
                    objemail.createddate = "null";
                    lsemailsub.Add(objemail);
                }
                return lsemailsub.ToArray();
            }
            catch (Exception Ex)
            {
                getEmailSubscription objemail = new getEmailSubscription();
                objemail.status = "fail";
                objemail.userid = "null";
                objemail.createddate = "null";
                lsemailsub.Add(objemail);

                return lsemailsub.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Send Email

        public void SendMailAdmin(string Fname, string Emailto, string EmailFrom, string strMessage)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = EmailFrom.Trim();
                //mail.To = "anagha@netbiz.in";
                mail.To = Emailto;
                mail.BodyFormat = MailFormat.Html;
                mail.Subject = "New Query from " + Fname + " ";
                string BodyMessage = "<html> <body><h4>New Query Details </h4> <br/>Query : " + strMessage + " <br/> Name : " + Fname + "";
                mail.Body = BodyMessage;
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "info@netbizlabs.com"); //set your username here
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "info321");	//set your password here
                //StreamReader sr = new StreamReader(Server.MapPath(""));
                //mail.Body = sr.ReadToEnd().Replace("Email", email);
                SmtpMail.SmtpServer = "mail.netbiz.in";
                SmtpMail.Send(mail);
            }
            catch (Exception ex)
            {
                //Response.Write(ex.Message);
            }
        }
        #endregion

        #region To get pushnotifications
        public Getpushnotifications[] Fetchpushnotifications(string userid)
        {
            List<Getpushnotifications> lspushnotifications = new List<Getpushnotifications>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                comm = new SqlCommand("SP_getpushnotifications", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@userid", userid);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Getpushnotifications[] b = new Getpushnotifications[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Getpushnotifications objpush = new Getpushnotifications();
                        objpush.status = "success";
                        objpush.userid = Convert.ToString(ds.Tables[0].Rows[i]["userid"]);
                        objpush.message = Convert.ToString(ds.Tables[0].Rows[i]["message"]);
                        objpush.status = Convert.ToString(ds.Tables[0].Rows[i]["status"]);
                        objpush.createddate = Convert.ToString(ds.Tables[0].Rows[i]["createddate"]);
                        lspushnotifications.Add(objpush);
                    }
                }
                else
                {
                    Getpushnotifications objpush = new Getpushnotifications();
                    objpush.status = "fail";
                    objpush.userid = "null";
                    objpush.message = "null";
                    objpush.status = "null";
                    objpush.createddate = "null";
                    lspushnotifications.Add(objpush);
                }
                return lspushnotifications.ToArray();
            }
            catch (Exception Ex)
            {
                Getpushnotifications objpush = new Getpushnotifications();
                objpush.status = "fail";
                objpush.userid = "null";
                objpush.message = "null";
                objpush.status = "null";
                objpush.createddate = "null";
                lspushnotifications.Add(objpush);
                return lspushnotifications.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region To add pushnotifications details
        public Getpushnotifications[] Insertpushnotifications(string userid, string status, string message)
        {
            List<Getpushnotifications> lspush = new List<Getpushnotifications>();
            try
            {

                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                comm = new SqlCommand("SP_Insertpushnotifications", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@userid", userid);
                comm.Parameters.AddWithValue("@status", status);
                comm.Parameters.AddWithValue("@message", message);
                SqlParameter pmout = new SqlParameter();
                pmout.ParameterName = "@outStatus";
                pmout.DbType = DbType.String;
                pmout.Size = 1024;
                pmout.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout);


                Int32 sqlRows = comm.ExecuteNonQuery();
                Getpushnotifications objpush = new Getpushnotifications();
                if (sqlRows != 0)
                {
                    objpush.status = comm.Parameters["@outStatus"].Value.ToString();
                    lspush.Add(objpush);
                }
                else
                {
                    objpush.status = "fail";
                    lspush.Add(objpush);
                }
                return lspush.ToArray();
            }
            catch (Exception Ex)
            {
                Getpushnotifications objpush = new Getpushnotifications();
                objpush.status = "fail";
                lspush.Add(objpush);
                return lspush.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region To get Projects inner plansfeatures
        public FloorPlans[] FetchProjectinnerFloorPlans(string projectid, string planid)
        {
            List<FloorPlans> lsFloorPlans = new List<FloorPlans>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                comm = new SqlCommand("SP_GetInnerFloorPlans", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@projectId", projectid);
                comm.Parameters.AddWithValue("@planid", planid);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    FloorPlans[] b = new FloorPlans[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        FloorPlans objlsFloorPlans = new FloorPlans();
                        objlsFloorPlans.status = "success";
                        //objlsFloorPlans.projectid = Convert.ToString(ds.Tables[0].Rows[i]["projectid"]);
                        //objlsFloorPlans.menuid = Convert.ToString(ds.Tables[0].Rows[i]["menuid"]);
                        objlsFloorPlans.image = BaseURL + "images/FloorPlans/" + Convert.ToString(ds.Tables[0].Rows[i]["image"]);
                        objlsFloorPlans.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        //objlsFloorPlans.createddate = Convert.ToString(ds.Tables[0].Rows[i]["createddate"]);
                        lsFloorPlans.Add(objlsFloorPlans);
                    }
                }
                else
                {
                    FloorPlans objlsFloorPlans = new FloorPlans();
                    objlsFloorPlans.status = "fail";
                    objlsFloorPlans.projectid = "null";
                    objlsFloorPlans.menuid = "null";
                    objlsFloorPlans.image = "null";
                    objlsFloorPlans.title = "null";
                    objlsFloorPlans.createddate = "null";
                    lsFloorPlans.Add(objlsFloorPlans);

                }
                return lsFloorPlans.ToArray();
            }
            catch (Exception Ex)
            {
                FloorPlans objlsFloorPlans = new FloorPlans();
                objlsFloorPlans.status = "fail";
                objlsFloorPlans.projectid = "null";
                objlsFloorPlans.menuid = "null";
                objlsFloorPlans.image = "null";
                objlsFloorPlans.title = "null";
                objlsFloorPlans.createddate = "null";
                lsFloorPlans.Add(objlsFloorPlans);
                return lsFloorPlans.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method For Get project Apartment
        public Projects[] GetProjectApartment(string projectid)
        {
            List<Projects> lsproject = new List<Projects>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetProject_apartment", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@projectid", projectid);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Projects[] b = new Projects[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Projects objproject = new Projects();
                        objproject.status = "success";
                        objproject.projectid = Convert.ToString(ds.Tables[0].Rows[i]["projectid"]);
                        objproject.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        objproject.apartment = Convert.ToString(ds.Tables[0].Rows[i]["RoomType"]);
                        lsproject.Add(objproject);
                    }
                }
                else
                {
                    Projects objproject = new Projects();
                    objproject.status = "fail";
                    objproject.projectid = "null";
                    objproject.title = "null";
                    objproject.apartment = "null";
                    lsproject.Add(objproject);
                }
                return lsproject.ToArray();
            }
            catch (Exception)
            {
                Projects objproject = new Projects();
                objproject.status = "fail";
                objproject.projectid = "null";
                objproject.title = "null";
                objproject.apartment = "null";
                lsproject.Add(objproject);
                return lsproject.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region Method to Get All active Projects

        public Projects[] GetAllProjects()
        {
            List<Projects> lsproject = new List<Projects>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetAllProject", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Projects[] b = new Projects[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Projects objproject = new Projects();
                        objproject.status = "success";
                        objproject.projectid = Convert.ToString(ds.Tables[0].Rows[i]["projectid"]);
                        objproject.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        objproject.cityname = Convert.ToString(ds.Tables[0].Rows[i]["cityname"]);
                        lsproject.Add(objproject);
                    }
                }
                else
                {
                    Projects objproject = new Projects();
                    objproject.status = "fail";
                    objproject.projectid = "null";
                    objproject.title = "null";
                    objproject.cityname = "null";
                    lsproject.Add(objproject);
                }
                return lsproject.ToArray();
            }
            catch (Exception)
            {
                Projects objproject = new Projects();
                objproject.status = "fail";
                objproject.projectid = "null";
                objproject.title = "null";
                objproject.cityname = "null";
                lsproject.Add(objproject);
                return lsproject.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method to updatet User Info
        public UserInfo[] updatetUserInfo(string userid, string usermobile, string name)
        {
            List<UserInfo> listuser = new List<UserInfo>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_UpdateUserInfo", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@userid", userid);
                comm.Parameters.AddWithValue("@usermobile", usermobile);
                comm.Parameters.AddWithValue("@name", name);

                SqlParameter pmout = new SqlParameter();
                pmout.ParameterName = "@outStatus";
                pmout.DbType = DbType.String;
                pmout.Size = 1024;
                pmout.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout);

                Int32 sqlRows = comm.ExecuteNonQuery();
                UserInfo objUserInfo = new UserInfo();
                if (sqlRows != 0)
                {
                    objUserInfo.status = comm.Parameters["@outStatus"].Value.ToString();
                    listuser.Add(objUserInfo);
                }
                else
                {
                    objUserInfo.status = "fail";
                    listuser.Add(objUserInfo);
                }
                return listuser.ToArray();
            }
            catch (Exception Ex)
            {
                UserInfo objUserInfo = new UserInfo();
                objUserInfo.status = "fail";
                listuser.Add(objUserInfo);
                return listuser.ToArray();
            }
            finally
            {
                conn.Close();
            }

        }

        #endregion

        #region To Get All Property search accoding to CityID

        public Projects[] GetProperty_ByCityId(string cityid, string projecttype, string projectstatus)
        {
            List<Projects> lsproject = new List<Projects>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetProperty_ByCityId", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@cityid", cityid);
                comm.Parameters.AddWithValue("@projecttype", projecttype);
                comm.Parameters.AddWithValue("@projectstatus", projectstatus);

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Projects[] b = new Projects[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Projects objproject = new Projects();
                        objproject.status = "success";
                        objproject.projectid = Convert.ToString(ds.Tables[0].Rows[i]["projectid"]);
                        objproject.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        objproject.cityname = Convert.ToString(ds.Tables[0].Rows[i]["cityname"]);
                        lsproject.Add(objproject);
                    }
                }
                else
                {
                    Projects objproject = new Projects();
                    objproject.status = "fail";
                    objproject.projectid = "null";
                    objproject.title = "null";
                    objproject.cityname = "null";
                    lsproject.Add(objproject);
                }
                return lsproject.ToArray();
            }
            catch (Exception)
            {
                Projects objproject = new Projects();
                objproject.status = "fail";
                objproject.projectid = "null";
                objproject.title = "null";
                objproject.cityname = "null";
                lsproject.Add(objproject);
                return lsproject.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region To Get All property accoding to RoomID's

        public Projects[] GetProperty_ByRoomID(string roomid, string projecttype, string projectstatus, string userid)
        {
            List<Projects> lsproject = new List<Projects>();
            try
            {
                //string constring = ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString;
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetSearchProperty_RoomID", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@roomid", roomid);
                comm.Parameters.AddWithValue("@projecttype", projecttype);
                comm.Parameters.AddWithValue("@projectstatus", projectstatus);
                comm.Parameters.AddWithValue("@userid", userid);

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Projects[] b = new Projects[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {

                        Projects objproject = new Projects();
                        objproject.status = "success";
                        objproject.projectid = Convert.ToString(ds.Tables[0].Rows[i]["projectid"]);
                        objproject.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        objproject.cityname = Convert.ToString(ds.Tables[0].Rows[i]["cityname"]);
                        lsproject.Add(objproject);
                    }
                }
                else
                {
                    Projects objproject = new Projects();
                    objproject.status = "fail";
                    objproject.projectid = "null";
                    objproject.title = "null";
                    objproject.cityname = "null";
                    objproject.status = "null";
                    lsproject.Add(objproject);
                }
                return lsproject.ToArray();
            }
            catch (Exception)
            {
                Projects objproject = new Projects();
                objproject.status = "fail";
                objproject.projectid = "null";
                objproject.title = "null";
                objproject.cityname = "null";
                lsproject.Add(objproject);
                return lsproject.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Get the  perticular User Favourit
        public UserFavourites[] FetchUserFavourite_userid(string userid)
        {
            List<UserFavourites> listUserFavourites = new List<UserFavourites>();
            try
            { // nitesh
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_GetUserFavourite_userid", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@userid", userid);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    UserFavourites[] b = new UserFavourites[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {

                        UserFavourites objlsFavourites = new UserFavourites();
                        objlsFavourites.status = "success";
                        objlsFavourites.projectid = Convert.ToString(ds.Tables[0].Rows[i]["projectid"]);
                        objlsFavourites.userid = Convert.ToString(ds.Tables[0].Rows[i]["userid"]);
                        objlsFavourites.createddate = Convert.ToString(ds.Tables[0].Rows[i]["createddate"]);
                        objlsFavourites.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        objlsFavourites.favorite = Convert.ToString(ds.Tables[0].Rows[i]["favorite"]);
                        objlsFavourites.cityname = Convert.ToString(ds.Tables[0].Rows[i]["cityname"]);

                        listUserFavourites.Add(objlsFavourites);
                    }
                }
                else
                {
                    UserFavourites objlsFavourites = new UserFavourites();
                    objlsFavourites.status = "fail";
                    objlsFavourites.projectid = "null";
                    objlsFavourites.userid = "null";
                    objlsFavourites.createddate = "null";
                    listUserFavourites.Add(objlsFavourites);

                }
                return listUserFavourites.ToArray();
            }
            catch (Exception Ex)
            {
                UserFavourites objlsFavourites = new UserFavourites();
                objlsFavourites.status = "fail";
                objlsFavourites.projectid = "null";
                objlsFavourites.userid = "null";
                objlsFavourites.createddate = "null";
                listUserFavourites.Add(objlsFavourites);
                return listUserFavourites.ToArray();
            }
        }

        #endregion

        #region Method to updatet User Info
        public UserInfo[] updatetUserInfo_myaccount(string userid, string type, string typevalue)
        {
            List<UserInfo> listuser = new List<UserInfo>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_UpdateUserInfo_Myaccount", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@userid", userid);
                comm.Parameters.AddWithValue("@type", type);
                comm.Parameters.AddWithValue("@typevalue", typevalue);

                SqlParameter pmout = new SqlParameter();
                pmout.ParameterName = "@outStatus";
                pmout.DbType = DbType.String;
                pmout.Size = 1024;
                pmout.Direction = ParameterDirection.Output;
                comm.Parameters.Add(pmout);

                Int32 sqlRows = comm.ExecuteNonQuery();
                UserInfo objUserInfo = new UserInfo();
                if (sqlRows != 0)
                {
                    objUserInfo.status = comm.Parameters["@outStatus"].Value.ToString();
                    listuser.Add(objUserInfo);
                }
                else
                {
                    objUserInfo.status = "fail";
                    listuser.Add(objUserInfo);
                }
                return listuser.ToArray();
            }
            catch (Exception Ex)
            {
                UserInfo objUserInfo = new UserInfo();
                objUserInfo.status = "fail";
                listuser.Add(objUserInfo);
                return listuser.ToArray();
            }
            finally
            {
                conn.Close();
            }

        }

        #endregion

        #region Method to Get All active Projects for lati_longi

        public Project_lati_longi[] GetAllProject_lati_longi()
        {
            List<Project_lati_longi> lsproject = new List<Project_lati_longi>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_Project_lati_longi", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Project_lati_longi[] b = new Project_lati_longi[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Project_lati_longi objproject = new Project_lati_longi();
                        objproject.status = "success";
                        objproject.projectid = Convert.ToString(ds.Tables[0].Rows[i]["projectid"]);
                        objproject.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        objproject.latitude = Convert.ToString(ds.Tables[0].Rows[i]["latitude"]);
                        objproject.longitude = Convert.ToString(ds.Tables[0].Rows[i]["longitude"]);

                        lsproject.Add(objproject);
                    }
                }
                else
                {
                    Project_lati_longi objproject = new Project_lati_longi();
                    objproject.status = "fail";
                    objproject.projectid = "null";
                    objproject.title = "null";
                    objproject.latitude = "null";
                    objproject.longitude = "null";
                    lsproject.Add(objproject);
                }
                return lsproject.ToArray();
            }
            catch (Exception)
            {
                Project_lati_longi objproject = new Project_lati_longi();
                objproject.status = "fail";
                objproject.projectid = "null";
                objproject.title = "null";
                objproject.latitude = "null";
                objproject.longitude = "null";
                lsproject.Add(objproject);
                return lsproject.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Method For Get All Apartment
        public Projects[] Get_apartment(string cityid)
        {
            List<Projects> lsproject = new List<Projects>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_Get_apartment", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@cityid", cityid);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Projects[] b = new Projects[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Projects objproject = new Projects();
                        objproject.status = "success";
                        lsproject.Add(objproject);
                    }
                }
                else
                {
                    Projects objproject = new Projects();
                    objproject.status = "fail";
                    objproject.projectid = "null";
                    objproject.title = "null";
                    lsproject.Add(objproject);
                }
                return lsproject.ToArray();
            }
            catch (Exception)
            {
                Projects objproject = new Projects();
                objproject.status = "fail";
                objproject.projectid = "null";
                objproject.title = "null";
                lsproject.Add(objproject);
                return lsproject.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region Method For Get All Project Apartment
        public Projects[] Get_Project_apartment(string roomid, string cityid)
        {
            List<Projects> lsproject = new List<Projects>();
            try
            {
                SqlConnection conn;
                SqlCommand comm;
                SqlDataAdapter da;
                conn = new SqlConnection(constring);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                comm = new SqlCommand("SP_Get_Project_apartment", conn);
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@roomid", roomid);
                comm.Parameters.AddWithValue("@cityid", cityid);
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Projects[] b = new Projects[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Projects objproject = new Projects();
                        objproject.status = "success";
                        objproject.projectid = Convert.ToString(ds.Tables[0].Rows[i]["projectid"]);
                        objproject.title = Convert.ToString(ds.Tables[0].Rows[i]["title"]);
                        lsproject.Add(objproject);
                    }
                }
                else
                {
                    Projects objproject = new Projects();
                    objproject.status = "fail";
                    objproject.projectid = "null";
                    objproject.title = "null";

                    lsproject.Add(objproject);
                }
                return lsproject.ToArray();
            }
            catch (Exception)
            {
                Projects objproject = new Projects();
                objproject.status = "fail";
                objproject.projectid = "null";
                objproject.title = "null";
                lsproject.Add(objproject);
                return lsproject.ToArray();
            }
            finally
            {
                conn.Close();
            }
        }

        public EmpanelmentContact[] UpdateEmplInfoNew(string panno, string brokerid, string brokeraccountid)
        {
            throw new NotImplementedException();
        }

        public wsdlcodes[] GetleadCodes(string id)
        {
            throw new NotImplementedException();
        }

        public EmpanelmentContact[] InsertEmpl(string name, string mobile, string email, string panno, string taxno, string company, string region, string brokerid)
        {
            throw new NotImplementedException();
        }

        public Enquiry[] InsertLead1(string firstname, string lastname, string email, string country, string mobile, string projectid, string project, string description, string state, string city, string apartment, string userid, string brokerid, string remark, string sfdcname)
        {
            throw new NotImplementedException();
        }

        public Enquiry[] InsertLead2(string firstname, string lastname, string email, string country, string mobile, string projectid, string project, string description, string state, string city, string apartment, string userid, string brokerid, string remark, string sfdcname)
        {
            throw new NotImplementedException();
        }

        public Document[] Get_TextDoc()
        {
            throw new NotImplementedException();
        }

        public Document[] GetDocById(string id)
        {
            throw new NotImplementedException();
        }

        public UserInfo[] GetCampaignDetails(string brokerid)
        {
            throw new NotImplementedException();
        }

        public void Redirect()
        {
            WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Redirect;
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Location", "http://www.microsoft.com");
        }
        #endregion

        #endregion
    }
}
