﻿using Hangfire;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Hangfire.SqlServer;
using GPLPartnerConnect.Model.Empanel;
using GPLPartnerConnect.CommonUtility;
using System.Data.Common;
using GPLPartnerConnect.DAL;
using System.Configuration;
using System.Globalization;

namespace SchedulerService
{
    public partial class Service1 : ServiceBase
    {
        private BackgroundJobServer _server;
        DbCommand _sqlcommand = null;
        DataAccess dataAccess = new DataAccess();

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            var sqlStorage = new SqlServerStorage(ConfigurationManager.ConnectionStrings["DbConnection"].ToString());
            var options = new BackgroundJobServerOptions
            {
                ServerName = "DatabaseConnection"
            };
            JobStorage.Current = sqlStorage;

            _server = new BackgroundJobServer();
            RecurringJob.AddOrUpdate(() => RunBackground(), "0 0/3 0 ? * * *");
        }

        public void RunBackground()
        {
            //using (StreamWriter sw = File.AppendText(@"C:\Users\shrey\OneDrive\Desktop\delete\WriteFile.txt"))
            //{
            //    sw.WriteLine("Hi");
            //}

            var foo = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
            var end = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
            var start = Convert.ToDateTime(end).Add(new TimeSpan(0, -15, 0)).ToString("yyyy-MM-dd HH:mm:ss.fff");       //last 15 min 

            var ds = GetNotificationData(start, end);
            try
            {
                SendNotification(ds.Tables[0], "0");
            }
            catch(Exception ex)
            {

            }
        }

        public void SendNotification(DataTable dt, string userid)
        {
            int cnt = 0, totaldevicecnt = 0;
            mNotificationLog obj = new mNotificationLog();
            foreach (DataRow row in dt.Rows)
            {
                obj.NotificationID = row["NotificationID"].ToString();
                obj.NotificationTitle = row["NotificationTitle"].ToString();
                obj.NotificationBody = row["NotificationBody"].ToString();
                obj.NotificationSourceID = row["NotificationSourceID"].ToString();
                obj.NotificationType = row["NotificationType"].ToString();
                obj.NotificationSubTitle = row["SubTitle"].ToString();
                obj.ChannelType = row["ChannelType"].ToString();
                obj.MobileUrl = row["MobileUrl"].ToString();
                obj.ChannelID = row["ChannelID"].ToString();
                obj.IsRedirect = row["IsRedirect"].ToString();
                obj.InsertedBy = userid;
                obj.Active = "Y";
                obj.SubTitle = row["SubTitle"].ToString();
                obj.NotificationImage = row["NotificationImage"].ToString();

                DataSet dsEmp = GetActiveDevice();
                totaldevicecnt += dsEmp.Tables[0].Rows.Count;
                foreach (DataRow rowEmp in dsEmp.Tables[0].Rows)
                {
                    obj.SendStatus = "";
                    obj.ErrorMsg = "";
                    obj.EmplContDeviceLogID = rowEmp["EmplContDeviceLogID"].ToString();
                    obj.EmplContID = rowEmp["EmplContID"].ToString();
                    obj.DeviceToken = rowEmp["DeviceToken"].ToString();
                    obj.MobilePlatform = rowEmp["MobilePlatform"].ToString();

                    obj.NotificationLogID = InsertNotificationLog(obj, "A").ToString();

                    GPLPartnerConnect.Model.FCM.mMsg fcmmsg = new GPLPartnerConnect.Model.FCM.mMsg();
                    fcmmsg.to = obj.DeviceToken;
                    fcmmsg.data.title = obj.NotificationTitle;
                    fcmmsg.data.body = obj.NotificationSubTitle;
                    fcmmsg.data.ChannelType = obj.ChannelType;
                    fcmmsg.data.id = obj.ChannelID;
                    fcmmsg.data.NotificationLogID = obj.NotificationLogID;

                    GPLPartnerConnect.Model.FCM.mRespon fcmResp = GPLNotification.Instance.SendNotification(fcmmsg);



                    cnt += Convert.ToInt16(fcmResp.success);
                    if (fcmResp.success == "0")
                    {
                        obj.SendStatus = "N";
                        obj.ErrorMsg = fcmResp.results[0].error;
                    }
                    else
                    {
                        obj.SendStatus = "Y";
                    }

                    //UpdateNotificationLog(obj);
                }
                UpdateNotificationSendStatus(obj.NotificationID);
            }
            //return cnt + " of " + totaldevicecnt;
        }

        private string InsertNotificationLog(mNotificationLog obj, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_NotificationLog", ref _sqlcommand,
                "@NotificationLogID", SqlDbType.VarChar, obj.NotificationLogID, ParameterDirection.Input,
                "@NotificationID", SqlDbType.VarChar, obj.NotificationID, ParameterDirection.Input,
                "@EmplContDeviceLogID", SqlDbType.VarChar, obj.EmplContDeviceLogID, ParameterDirection.Input,
                "@EmplContID", SqlDbType.VarChar, obj.EmplContID, ParameterDirection.Input,
                "@DeviceToken", SqlDbType.VarChar, obj.DeviceToken, ParameterDirection.Input,
                "@NotificationTitle", SqlDbType.VarChar, obj.NotificationTitle, ParameterDirection.Input,
                "@NotificationBody", SqlDbType.VarChar, obj.NotificationBody, ParameterDirection.Input,
                "@NotificationSourceID", SqlDbType.VarChar, obj.NotificationSourceID, ParameterDirection.Input,
                "@NotificationType", SqlDbType.VarChar, obj.NotificationType, ParameterDirection.Input,
                "@ChannelType", SqlDbType.VarChar, obj.ChannelType, ParameterDirection.Input,
                "@IsRedirect", SqlDbType.VarChar, obj.IsRedirect, ParameterDirection.Input,
                "@MobileUrl", SqlDbType.VarChar, obj.MobileUrl, ParameterDirection.Input,
                "@ChannelID", SqlDbType.VarChar, obj.ChannelID, ParameterDirection.Input,
                "@SendStatus", SqlDbType.VarChar, obj.SendStatus, ParameterDirection.Input,
                "@ErrorMsg", SqlDbType.VarChar, obj.ErrorMsg, ParameterDirection.Input,
                "@Active", SqlDbType.VarChar, obj.Active, ParameterDirection.Input,
                "@InsertedBy", SqlDbType.VarChar, obj.InsertedBy, ParameterDirection.Input,
                "@SubTitle", SqlDbType.VarChar, obj.SubTitle, ParameterDirection.Input,
                "@MobilePlatform", SqlDbType.VarChar, obj.MobilePlatform, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input,
                "@NotificationImage", SqlDbType.VarChar, obj.NotificationImage, ParameterDirection.Input);

            return ds.Tables[0].Rows[0]["NotificationLogID"].ToString();
        }

        public DataSet UpdateNotificationSendStatus(string NotificationID)
        {
            mNotification obj = new mNotification();
            obj.NotificationID = NotificationID;
            return NotificationData(obj, "SENT");
        }

        public DataSet NotificationData(mNotification obj, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_sc_ScheduleNotification", ref _sqlcommand,
                "@NotificationID", SqlDbType.VarChar, obj.NotificationID, ParameterDirection.Input,
                "@NotificationTitle", SqlDbType.VarChar, obj.NotificationTitle, ParameterDirection.Input,
                "@NotificationBody", SqlDbType.VarChar, obj.NotificationBody, ParameterDirection.Input,
                "@SubTitle", SqlDbType.VarChar, obj.SubTitle, ParameterDirection.Input,
                "@ChannelType", SqlDbType.VarChar, obj.ChannelType, ParameterDirection.Input,
                //"@NotificationSourceID", SqlDbType.VarChar, obj.NotificationSourceID, ParameterDirection.Input,
                "@NotificationType", SqlDbType.VarChar, obj.NotificationType, ParameterDirection.Input,
                "@MobileUrl", SqlDbType.VarChar, obj.MobileUrl, ParameterDirection.Input,
                "@ChannelID", SqlDbType.VarChar, obj.ChannelID, ParameterDirection.Input,
                "@IsRedirect", SqlDbType.VarChar, obj.IsRedirect, ParameterDirection.Input,
                "@HasSend", SqlDbType.VarChar, obj.HasSend, ParameterDirection.Input,
                "@SentOn", SqlDbType.VarChar, obj.SendOn, ParameterDirection.Input,
                //"@ScheduledFor", SqlDbType.VarChar, obj.ScheduledFor, ParameterDirection.Input,
                "@Active", SqlDbType.VarChar, obj.Active, ParameterDirection.Input,
                "@InsertedBy", SqlDbType.VarChar, obj.InsertedBy, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input,
                "@NotificationImage", SqlDbType.VarChar, obj.NotificationImage, ParameterDirection.Input);

            return ds;
        }

        public DataSet GetActiveDevice()
        {
            mEmplContDeviceLog mobj = new mEmplContDeviceLog();
            return EmplContDeviceLogData(mobj, "D");
        }

        public DataSet GetNotificationData(string start, string end)
        {
            DataSet ds = dataAccess.GetDataSet("usp_sc_GetScheduleNotification", ref _sqlcommand,
                "@start", SqlDbType.VarChar, start, ParameterDirection.Input,
                "@end", SqlDbType.VarChar, end, ParameterDirection.Input);

            return ds;
            }
        public DataSet ScheduleNotificationData(mScheduleNotification obj, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_sc_ScheduleNotification", ref _sqlcommand,
                "@NotificationID", SqlDbType.VarChar, obj.NotificationID, ParameterDirection.Input,
                "@NotificationTitle", SqlDbType.VarChar, obj.NotificationTitle, ParameterDirection.Input,
                "@NotificationBody", SqlDbType.VarChar, obj.NotificationBody, ParameterDirection.Input,
                "@NotificationType", SqlDbType.VarChar, obj.NotificationType, ParameterDirection.Input,
                "@ChannelID", SqlDbType.VarChar, obj.ChannelID, ParameterDirection.Input,
                "@ChannelType", SqlDbType.VarChar, obj.ChannelType, ParameterDirection.Input,
                "@SubTitle", SqlDbType.VarChar, obj.SubTitle, ParameterDirection.Input,
                "@MobileUrl", SqlDbType.VarChar, obj.MobileUrl, ParameterDirection.Input,
                "@HasSend", SqlDbType.VarChar, obj.HasSend, ParameterDirection.Input,
                "@IsRedirect", SqlDbType.VarChar, obj.IsRedirect, ParameterDirection.Input,
                "@SentOn", SqlDbType.VarChar, obj.SentOn, ParameterDirection.Input,
                "@NotificationDate", SqlDbType.VarChar, obj.NotificationDate, ParameterDirection.Input,
                "@ScheduleDate", SqlDbType.VarChar, obj.ScheduleDate, ParameterDirection.Input,
                "@Active", SqlDbType.VarChar, obj.Active, ParameterDirection.Input,
                "@InsertedBy", SqlDbType.VarChar, obj.InsertedBy, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input,
                "@NotificationImage", SqlDbType.VarChar, obj.NotificationImage, ParameterDirection.Input);

            return ds;
        }

        public DataSet EmplContDeviceLogData(mEmplContDeviceLog empDev, String Flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_EmplContDeviceLog", ref _sqlcommand,
               "@EmplContDeviceLogID", SqlDbType.VarChar, empDev.EmplContDeviceLogID, ParameterDirection.Input,
               "@EmplContID", SqlDbType.VarChar, empDev.EmplContID, ParameterDirection.Input,
               "@DeviceToken", SqlDbType.VarChar, empDev.DeviceToken, ParameterDirection.Input,
               "@MobilePlatform", SqlDbType.VarChar, empDev.MobilePlatform, ParameterDirection.Input,
               "@Active", SqlDbType.VarChar, empDev.Active, ParameterDirection.Input,
               "@Flag", SqlDbType.VarChar, Flag, ParameterDirection.Input);
            return ds;

        }
        protected override void OnStop()
        {
            _server.Dispose();
        }
    }
}
